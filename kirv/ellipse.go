package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// Ellipse specifies the properties of an ellipse.
type Ellipse struct {
	// A is the semi-major axis length.
	A float64
	// B is the semi-minor axis length.
	B float64
}

// NewEllipse constructs a new parametric ellipse curve with the given
// properties.
//
// The curve parameter is measured in units of radians.
func NewEllipse(spec Ellipse) Parametric {
	A, B := spec.A, spec.B
	return NewParametric(func(t float64) maf.Vec {
		s, c := math.Sincos(t)
		return maf.Vec{X: A * c, Y: B * s}
	}, ParSpec{
		Tangent: func(t float64) maf.Vec {
			s, c := math.Sincos(t)
			return maf.Vec{X: A * -s, Y: B * c}.Unit()
		},
		Normal: func(t float64) maf.Vec {
			// Let c == cos(t) and s == sin(t) then
			//
			// r(t) =   Ac i   +  Bs j
			//
			// v(t) =  -As i   +  Bc j
			//
			// a(t) =  -Ac i   -  Bs j
			s, c := math.Sincos(t)
			v := maf.Vec{X: A * -s, Y: B * c}
			a := maf.Vec{X: A * -c, Y: B * -s}
			return UnitNormal(v, a)
		},
		Radius: func(t float64) float64 {
			s, c := math.Sincos(t)
			v := maf.Vec{X: A * -s, Y: B * c}
			a := maf.Vec{X: A * -c, Y: B * -s}
			return Radius(v, a)
		},
		PosTan: func(t float64) (maf.Vec, maf.Vec) {
			s, c := math.Sincos(t)
			return maf.Vec{X: A * c, Y: B * s},
				maf.Vec{X: A * -s, Y: B * c}.Unit()
		},
		NormRad: func(t float64) (maf.Vec, float64) {
			s, c := math.Sincos(t)
			v := maf.Vec{X: A * -s, Y: B * c}
			a := maf.Vec{X: A * -c, Y: B * -s}
			return UnitNormal(v, a), Radius(v, a)
		},
		PosTanNormRad: func(t float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			s, c := math.Sincos(t)
			v := maf.Vec{X: A * -s, Y: B * c}
			a := maf.Vec{X: A * -c, Y: B * -s}
			return maf.Vec{X: A * c, Y: B * s},
				maf.Vec{X: A * -s, Y: B * c}.Unit(),
				UnitNormal(v, a), Radius(v, a)
		},
	})
}

// NewEllipsePosTan function finds the position and unit tangent vector for the
// ellipse curve with the given properties.
//
// The curve parameter is measured in units of radians.
func NewEllipsePosTan(spec Ellipse) func(t float64) (maf.Vec2, maf.Vec2) {
	A, B := spec.A, spec.B
	return func(t float64) (maf.Vec2, maf.Vec2) {
		s, c := math.Sincos(t)
		return maf.Vec2{X: A * c, Y: B * s},
			maf.Vec2{X: A * -s, Y: B * c}.Unit()
	}
}
