package kirv_test

import (
	"math"
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/tezd"
)

func TestNumericalNorm(t *testing.T) {
	circ := kirv.NewCircle(2.0)

	for _, T := range []float64{
		0,
		maf.QuartPi,
		maf.HalfPi,
		maf.HalfPi + maf.QuartPi,
		math.Pi,
	} {
		want := kirv.NumericalNormal(circ, T)
		got := kirv.NumericalNorm(circ, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	ellipse := kirv.NewEllipse(kirv.Ellipse{A: 2, B: 1})

	for _, T := range []float64{
		0,
		maf.QuartPi,
		maf.HalfPi,
		maf.HalfPi + maf.QuartPi,
		math.Pi,
	} {
		want := kirv.NumericalNormal(ellipse, T)
		got := kirv.NumericalNorm(ellipse, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	sin := kirv.Sin

	for _, T := range []float64{
		math.Pi / 6,
		-math.Pi / 6,
		maf.HalfPi,
		-maf.HalfPi,
		math.Pi - math.Pi/6,
		-(math.Pi - math.Pi/6),
	} {
		want := kirv.NumericalNormal(sin, T)
		got := kirv.NumericalNorm(sin, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	cyc := kirv.NewCycloid(2.0)

	for _, T := range []float64{
		0 + 0.0001,
		maf.HalfPi,
		maf.TwoPi - 0.0001,
	} {
		want := kirv.NumericalNormal(cyc, T)
		got := kirv.NumericalNorm(cyc, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	spiral := kirv.NewArchimedean(kirv.Archimedean{
		A: 1.0,
		B: 2.0,
	})

	for _, T := range []float64{
		0 + 0.0001,
		maf.QuartPi,
		maf.HalfPi,
		math.Pi,
		maf.TwoPi,
		maf.TwoPi + maf.HalfPi,
	} {
		want := kirv.NumericalNormal(spiral, T)
		got := kirv.NumericalNorm(spiral, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	helix := kirv.NewHelix(kirv.Helix{R: 2.0, P: 1.0})

	for _, T := range []float64{
		0,
		math.Pi,
		maf.TwoPi,
		-maf.HalfPi,
	} {
		want := kirv.NumericalNormal(helix, T)
		got := kirv.NumericalNorm(helix, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	proj := kirv.NewTimeDepProjectile(kinem.Projectile{
		A: -9.80665,
		U: maf.Vec{X: 10, Y: 10, Z: 0},
	})

	for _, T := range []float64{
		-10, -5, 0, 1, 5, 10,
	} {

		want := kirv.NumericalNormal(proj, T)
		got := kirv.NumericalNorm(proj, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	sinusoid := kirv.NewSinusoid(kirv.Sinusoid{
		A: 2, L: 1, P: 0, V: 2,
	}, kirv.ExpDecay{})(0) // Sinusoid for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := kirv.NumericalNormal(sinusoid, x)
		got := kirv.NumericalNorm(sinusoid, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	sDecaySinusoid := kirv.NewSinusoid(kirv.Sinusoid{
		A: 2, L: 1, P: 0, V: 2,
	}, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})(0) // Sinusoid for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := kirv.NumericalNormal(sDecaySinusoid, x)
		got := kirv.NumericalNorm(sDecaySinusoid, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	standing := kirv.NewStanding(kirv.Standing{
		A: 2, L: 1, W: 1,
	}, kirv.ExpDecay{})(0) // Standing wave for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := kirv.NumericalNormal(standing, x)
		got := kirv.NumericalNormal(standing, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	sDecayStanding := kirv.NewStanding(kirv.Standing{
		A: 2, L: 1, W: 1,
	}, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})(0) // Standing wave for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := kirv.NumericalNormal(sDecayStanding, x)
		got := kirv.NumericalNormal(sDecayStanding, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}
}

func TestNumericalNormal(t *testing.T) {
	circ := kirv.NewCircle(2.0)

	for _, T := range []float64{
		0, maf.QuartPi, maf.HalfPi, maf.HalfPi + maf.QuartPi, math.Pi,
	} {
		want := circ.Normal(T)
		got := kirv.NumericalNormal(circ, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	ellipse := kirv.NewEllipse(kirv.Ellipse{A: 2, B: 1})

	for _, T := range []float64{
		0, maf.QuartPi, maf.HalfPi, maf.HalfPi + maf.QuartPi, math.Pi,
	} {
		want := ellipse.Normal(T)
		got := kirv.NumericalNormal(ellipse, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	sin := kirv.Sin

	for _, T := range []float64{
		math.Pi / 6, -math.Pi / 6,
		maf.HalfPi, -maf.HalfPi,
		math.Pi - math.Pi/6, -(math.Pi - math.Pi/6),
	} {
		want := sin.Normal(T)
		got := kirv.NumericalNormal(sin, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	cyc := kirv.NewCycloid(2.0)

	for _, T := range []float64{
		0 + 0.0001,
		maf.HalfPi,
		maf.TwoPi - 0.0001,
	} {
		want := cyc.Normal(T)
		got := kirv.NumericalNormal(cyc, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	spiral := kirv.NewArchimedean(kirv.Archimedean{
		A: 1.0,
		B: 2.0,
	})

	for _, T := range []float64{
		0 + 0.0001,
		maf.QuartPi,
		maf.HalfPi,
		math.Pi,
		maf.TwoPi,
		maf.TwoPi + maf.HalfPi,
	} {
		want := spiral.Normal(T)
		got := kirv.NumericalNormal(spiral, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	helix := kirv.NewHelix(kirv.Helix{R: 2.0, P: 1.0})

	for _, T := range []float64{
		0,
		math.Pi,
		maf.TwoPi,
		-maf.HalfPi,
	} {
		want := helix.Normal(T)
		got := kirv.NumericalNormal(helix, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	proj := kirv.NewTimeDepProjectile(kinem.Projectile{
		A: -9.80665,
		U: maf.Vec{X: 10, Y: 10, Z: 0},
	})

	for _, T := range []float64{
		-10, -5, 0, 1, 5, 10,
	} {

		want := proj.Normal(T)
		got := kirv.NumericalNormal(proj, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	sinusoid := kirv.NewSinusoid(kirv.Sinusoid{
		A: 2, L: 1, P: 0, V: 2,
	}, kirv.ExpDecay{})(0) // Sinusoid for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := sinusoid.Normal(x)
		got := kirv.NumericalNormal(sinusoid, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	sDecaySinusoid := kirv.NewSinusoid(kirv.Sinusoid{
		A: 2, L: 1, P: 0, V: 2,
	}, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})(0) // Sinusoid for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := sDecaySinusoid.Normal(x)
		got := kirv.NumericalNormal(sDecaySinusoid, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	standing := kirv.NewStanding(kirv.Standing{
		A: 2, L: 1, W: 1,
	}, kirv.ExpDecay{})(0) // Standing wave for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := standing.Normal(x)
		got := kirv.NumericalNormal(standing, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	sDecayStanding := kirv.NewStanding(kirv.Standing{
		A: 2, L: 1, W: 1,
	}, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})(0) // Standing wave for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := sDecayStanding.Normal(x)
		got := kirv.NumericalNormal(sDecayStanding, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}
}

func TestNumericalPosTanNormRad(t *testing.T) {
	c := kirv.NewCircle(2.0)

	for _, T := range []float64{
		0,
		maf.HalfPi / 2,
		maf.HalfPi,
		math.Pi,
		-maf.HalfPi,
	} {
		want1, want2, want3, want4 := c.Position(T), c.Tangent(T),
			c.Normal(T), c.Radius(T)
		got1, got2, got3, got4 := kirv.NumericalPosTanNormRad(c, T)
		if !maf.CompareVec(want1, got1, 2) || !maf.CompareVec(want2, got2, 2) ||
			!maf.CompareVec(want3, got3, 2) || !tezd.CompareFloat64(want4, got4, 2) {
			t.Errorf(
				"Want1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n"+
					"Want3:\n%#v\nGot3:\n%#v\nWant4:\n%#v\nGot4:\n%#v\n",
				want1, got1, want2, got2, want3, got3, want4, got4)
		}

		got3, got4 = kirv.NumericalNormalRadius(c, T)
		if !maf.CompareVec(want3, got3, 2) || !tezd.CompareFloat64(want4, got4, 2) {
			t.Errorf("Want3:\n%#v\nGot3:\n%#v\nWant4:\n%#v\nGot4:\n%#v\n",
				want3, got3, want4, got4)
		}
	}
}

func TestNumericalRadius(t *testing.T) {
	circ := kirv.NewCircle(2.0)

	for _, T := range []float64{
		0,
		maf.HalfPi / 2,
		maf.HalfPi,
		math.Pi,
		-maf.HalfPi,
	} {
		want := circ.Radius(T)
		got := kirv.NumericalRadius(circ, T)
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	ellipse := kirv.NewEllipse(kirv.Ellipse{A: 2, B: 1})

	for _, T := range []float64{
		0,
		maf.HalfPi / 2,
		maf.HalfPi,
		math.Pi,
		-maf.HalfPi,
	} {
		want := ellipse.Radius(T)
		got := kirv.NumericalRadius(ellipse, T)
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	sin := kirv.Sin

	for _, T := range []float64{
		-maf.HalfPi,
		-maf.HalfPi / 2,
		maf.HalfPi / 2,
		maf.HalfPi,
	} {
		want := sin.Radius(T)
		got := kirv.NumericalRadius(sin, T)
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	cyc := kirv.NewCycloid(2.0)

	for _, T := range []float64{
		0 + 0.0001,
		maf.HalfPi,
		maf.TwoPi - 0.0001,
	} {
		want := cyc.Radius(T)
		got := kirv.NumericalRadius(cyc, T)
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	spiral := kirv.NewArchimedean(kirv.Archimedean{
		A: 1.0,
		B: 2.0,
	})

	for _, T := range []float64{
		0 + 0.0001,
		maf.QuartPi,
		maf.HalfPi,
		math.Pi,
		maf.TwoPi,
		maf.TwoPi + maf.HalfPi,
	} {
		want := spiral.Radius(T)
		got := kirv.NumericalRadius(spiral, T)
		if !tezd.CompareFloat64(want, got, 1) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	helix := kirv.NewHelix(kirv.Helix{R: 2.0, P: 1.0})

	for _, T := range []float64{
		0,
		math.Pi,
		maf.TwoPi,
		-maf.HalfPi,
	} {
		want := helix.Radius(T)
		got := kirv.NumericalRadius(helix, T)
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	proj := kirv.NewTimeDepProjectile(kinem.Projectile{
		A: -9.80665,
		U: maf.Vec{X: 10, Y: 10, Z: 0},
	})

	for _, T := range []float64{
		-10, -5, 0, 5, 10,
	} {

		want := proj.Radius(T)
		got := kirv.NumericalRadius(proj, T)
		if !tezd.CompareFloat64(want, got, 0) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	sinusoid := kirv.NewSinusoid(kirv.Sinusoid{
		A: 2, L: 1, P: 0, V: 2,
	}, kirv.ExpDecay{})(0) // Sinusoid for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := sinusoid.Radius(x)
		got := kirv.NumericalRadius(sinusoid, x)
		if !tezd.CompareFloat64(want, got, 1) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	sDecaySinusoid := kirv.NewSinusoid(kirv.Sinusoid{
		A: 2, L: 1, P: 0, V: 2,
	}, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})(0) // Sinusoid for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := sDecaySinusoid.Radius(x)
		got := kirv.NumericalRadius(sDecaySinusoid, x)
		if !tezd.CompareFloat64(want, got, 0) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	standing := kirv.NewStanding(kirv.Standing{
		A: 2, L: 1, W: 1,
	}, kirv.ExpDecay{})(0) // Standing wave for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := standing.Radius(x)
		got := kirv.NumericalRadius(standing, x)
		if !tezd.CompareFloat64(want, got, 0) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	sDecayStanding := kirv.NewStanding(kirv.Standing{
		A: 2, L: 1, W: 1,
	}, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})(0) // Standing wave for t = 0.
	// Undefined inflection points at x = 0, 0.5, 1, 1.5, ...

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1,
	} {
		want := sDecayStanding.Radius(x)
		got := kirv.NumericalRadius(sDecayStanding, x)
		if !tezd.CompareFloat64(want, got, 0) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}
}

func TestNumericalTangent(t *testing.T) {
	circ := kirv.NewCircle(2.0)

	for _, T := range []float64{
		0,
		maf.QuartPi,
		maf.HalfPi,
		maf.HalfPi + maf.QuartPi,
		math.Pi,
	} {

		want := circ.Tangent(T)
		got := kirv.NumericalTangent(circ, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	ellipse := kirv.NewEllipse(kirv.Ellipse{A: 2, B: 1})

	for _, T := range []float64{
		0,
		maf.QuartPi,
		maf.HalfPi,
		maf.HalfPi + maf.QuartPi,
		math.Pi,
	} {

		want := ellipse.Tangent(T)
		got := kirv.NumericalTangent(ellipse, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	sin := kirv.Sin

	for _, T := range []float64{
		math.Pi / 6,
		-math.Pi / 6,
		maf.HalfPi,
		-maf.HalfPi,
		math.Pi - math.Pi/6,
		-(math.Pi - math.Pi/6),
	} {

		want := sin.Tangent(T)
		got := kirv.NumericalTangent(sin, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	cyc := kirv.NewCycloid(2.0)

	for _, T := range []float64{
		0 + 0.0001,
		maf.HalfPi,
		maf.TwoPi - 0.0001,
	} {
		want := cyc.Tangent(T)
		got := kirv.NumericalTangent(cyc, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	spiral := kirv.NewArchimedean(kirv.Archimedean{
		A: 1.0,
		B: 2.0,
	})

	for _, T := range []float64{
		0 + 0.0001,
		maf.QuartPi,
		maf.HalfPi,
		math.Pi,
		maf.TwoPi,
		maf.TwoPi + maf.HalfPi,
	} {
		want := spiral.Tangent(T)
		got := kirv.NumericalTangent(spiral, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	helix := kirv.NewHelix(kirv.Helix{R: 2.0, P: 1.0})

	for _, T := range []float64{
		0,
		math.Pi,
		maf.TwoPi,
		-maf.HalfPi,
	} {
		want := helix.Tangent(T)
		got := kirv.NumericalTangent(helix, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	proj := kirv.NewTimeDepProjectile(kinem.Projectile{
		A: -9.80665,
		U: maf.Vec{X: 10, Y: 10, Z: 0},
	})

	for _, T := range []float64{
		-10, -5, 0, 5, 10,
	} {

		want := proj.Tangent(T)
		got := kirv.NumericalTangent(proj, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}

	sinusoid := kirv.NewSinusoid(kirv.Sinusoid{
		A: 2, L: 1, P: 0, V: 2,
	}, kirv.ExpDecay{})(0) // Sinusoid for t = 0.

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1, 0,
	} {
		want := sinusoid.Tangent(x)
		got := kirv.NumericalTangent(sinusoid, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	sDecaySinusoid := kirv.NewSinusoid(kirv.Sinusoid{
		A: 2, L: 1, P: 0, V: 2,
	}, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})(0) // Sinusoid for t = 0.

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1, 0,
	} {
		want := sDecaySinusoid.Tangent(x)
		got := kirv.NumericalTangent(sDecaySinusoid, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	standing := kirv.NewStanding(kirv.Standing{
		A: 2, L: 1, W: 1,
	}, kirv.ExpDecay{})(0) // Standing wave for t = 0.

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1, 0,
	} {
		want := standing.Tangent(x)
		got := kirv.NumericalTangent(standing, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}

	sDecayStanding := kirv.NewStanding(kirv.Standing{
		A: 2, L: 1, W: 1,
	}, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})(0) // Standing wave for t = 0.

	for _, x := range []float64{
		-0.4, 0.4, -1.1, 1.1, 0,
	} {
		want := sDecayStanding.Tangent(x)
		got := kirv.NumericalTangent(sDecayStanding, x)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nx:\n%#v\nWant:\n%#v\nGot:\n%#v\n", x, want, got)
		}
	}
}

func TestParametric_PosTanNormRad(t *testing.T) {
	type posTanNormRader interface {
		Position(t float64) maf.Vec

		Tangent(t float64) maf.Vec

		Normal(t float64) maf.Vec

		Radius(t float64) float64

		PosTan(t float64) (maf.Vec, maf.Vec)

		NormRad(t float64) (maf.Vec, float64)

		PosTanNormRad(t float64) (maf.Vec, maf.Vec, maf.Vec, float64)
	}

	type curve struct {
		name string
		posTanNormRader
		params []float64
	}

	vc := []curve{
		{
			name:            "circ",
			posTanNormRader: kirv.NewCircle(2.0),
			params: []float64{
				0,
				maf.QuartPi,
				maf.HalfPi,
				maf.HalfPi + maf.QuartPi,
				math.Pi,
			},
		},
		{
			name:            "ellipse",
			posTanNormRader: kirv.NewEllipse(kirv.Ellipse{A: 2, B: 1}),
			params: []float64{
				0,
				maf.QuartPi,
				maf.HalfPi,
				maf.HalfPi + maf.QuartPi,
				math.Pi,
			},
		},
		{
			name:            "sin",
			posTanNormRader: kirv.Sin,
			params: []float64{
				math.Pi / 6,
				-math.Pi / 6,
				maf.HalfPi,
				-maf.HalfPi,
				math.Pi - math.Pi/6,
				-(math.Pi - math.Pi/6),
			},
		},
		{
			name:            "cyc",
			posTanNormRader: kirv.NewCycloid(2.0),
			params: []float64{
				0,
				maf.QuartPi,
				maf.HalfPi,
				maf.TwoPi,
			},
		},
		{
			name:            "helix",
			posTanNormRader: kirv.NewHelix(kirv.Helix{R: 2.0, P: 1.0}),
			params: []float64{
				0,
				math.Pi,
				maf.TwoPi,
				-maf.HalfPi,
			},
		},
		{
			name: "timeDepProj",
			posTanNormRader: kirv.NewTimeDepProjectile(kinem.Projectile{
				A: -9.80665,
				U: maf.Vec{X: 10, Y: 10, Z: 0},
			}),
			params: []float64{
				-10, -5, 0, 1, 5, 10,
			},
		},
		{
			name: "sinusoid",
			posTanNormRader: kirv.NewSinusoid(kirv.Sinusoid{
				A: 2, L: 1, P: 0, V: 2,
			}, kirv.ExpDecay{})(0),
			params: []float64{
				-0.4, 0.4, -1.1, 1.1,
			},
		},
		{
			name: "sDecaySinusoid",
			posTanNormRader: kirv.NewSinusoid(kirv.Sinusoid{
				A: 2, L: 1, P: 0, V: 2,
			}, kirv.ExpDecay{
				Space: kirv.HalfLife(0.7).Decay(),
			})(0),
			params: []float64{
				-0.4, 0.4, -1.1, 1.1,
			},
		},
		{
			name: "standing",
			posTanNormRader: kirv.NewStanding(kirv.Standing{
				A: 2, L: 1, W: 1,
			}, kirv.ExpDecay{})(0),
			params: []float64{
				-0.4, 0.4, -1.1, 1.1,
			},
		},
		{
			name: "sDecayStanding",
			posTanNormRader: kirv.NewStanding(kirv.Standing{
				A: 2, L: 1, W: 1,
			}, kirv.ExpDecay{
				Space: kirv.HalfLife(0.7).Decay(),
			})(0),
			params: []float64{
				-0.4, 0.4, -1.1, 1.1,
			},
		},
	}

	for _, c := range vc {
		for _, T := range c.params {
			want1, want2, want3, want4 := c.Position(T), c.Tangent(T),
				c.Normal(T), c.Radius(T)
			got1, got2, got3, got4 := c.PosTanNormRad(T)
			if !maf.CompareVec(want1, got1, 2) || !maf.CompareVec(want2, got2, 2) ||
				!maf.CompareVec(want3, got3, 2) || !tezd.CompareFloat64(want4, got4, 2) {
				t.Errorf("Name:\n%#v\n"+
					"Want1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n"+
					"Want3:\n%#v\nGot3:\n%#v\nWant4:\n%#v\nGot4:\n%#v\n",
					c.name, want1, got1, want2, got2, want3, got3, want4, got4)
			}

			got1, got2 = c.PosTan(T)
			got3, got4 = c.NormRad(T)
			if !maf.CompareVec(want1, got1, 2) || !maf.CompareVec(want2, got2, 2) ||
				!maf.CompareVec(want3, got3, 2) || !tezd.CompareFloat64(want4, got4, 2) {
				t.Errorf("Name:\n%#v\n"+
					"Want1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n"+
					"Want3:\n%#v\nGot3:\n%#v\nWant4:\n%#v\nGot4:\n%#v\n",
					c.name, want1, got1, want2, got2, want3, got3, want4, got4)
			}
		}
	}
}
