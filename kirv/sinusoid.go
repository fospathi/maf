package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// Sinusoid specifies the properties of a sine wave travelling to the right.
type Sinusoid struct {
	// Amplitude. Maximum displacement from Y = 0.
	A float64
	// Wavelength.
	L float64
	// Phase. The angle of the oscillation at t = 0, measured in radians.
	P float64
	// Linear speed.
	V float64
}

// ExpDecay contains positive decay constants for an exponential decay.
type ExpDecay struct {
	// An exponential decay constant for a space dimension.
	Space float64
	// An exponential decay constant in time.
	Time float64
}

// Decays returns true if the receiver decay has any non zero decay constants.
func (d ExpDecay) Decays() bool {
	return d.Space > 0 || d.Time > 0
}

// HalfLife is an exponential decay half-life value.
type HalfLife float64

// Decay is the decay constant which corresponds to the receiver half-life
// value.
func (hl HalfLife) Decay() float64 {
	return math.Ln2 / float64(hl)
}

// NewSinusoid returns a function which constructs new parametric sinusoid
// curves.
//
// The returned function accepts a time value and the curve parameter of the
// resulting parametric curve is measured in units of distance.
func NewSinusoid(spec Sinusoid, decay ExpDecay) func(t float64) Parametric {
	if decay.Decays() {
		return func(t float64) Parametric {
			return newDecayingSinusoid(t, spec, decay)
		}
	}
	return func(t float64) Parametric {
		return newSinusoid(t, spec)
	}
}

func newSinusoid(t float64, spec Sinusoid) Parametric {
	return NewParametric(func(x float64) maf.Vec {
		k := maf.TwoPi / spec.L // Wavenumber.
		w := k * spec.V         // Angular frequency.
		return maf.Vec{X: x, Y: spec.A * math.Sin(k*x-w*t+spec.P)}
	}, ParSpec{
		Tangent: func(x float64) maf.Vec {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			return maf.Vec{
				X: 1,
				Y: k * spec.A * math.Cos(k*x-w*t+spec.P),
			}.Unit()
		},
		Normal: func(x float64) maf.Vec {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.

			// r(x) =  x i   +       A sin(k x - w t + P) j
			//
			// v(x) =  1 i   +     k A cos(k x - w t + P) j
			//
			// a(x) =           -k k A sin(k x - w t + P) j
			s, c := math.Sincos(k*x - w*t + spec.P)
			v := maf.Vec{X: 1, Y: k * spec.A * c}
			a := maf.Vec{Y: -k * k * spec.A * s}
			return UnitNormal(v, a)
		},
		Radius: func(x float64) float64 {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.

			s, c := math.Sincos(k*x - w*t + spec.P)
			v := maf.Vec{X: 1, Y: k * spec.A * c}
			a := maf.Vec{Y: -k * k * spec.A * s}
			return Radius(v, a)
		},
		PosTan: func(x float64) (maf.Vec, maf.Vec) {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			return maf.Vec{X: x, Y: spec.A * math.Sin(k*x-w*t+spec.P)},
				maf.Vec{
					X: 1,
					Y: k * spec.A * math.Cos(k*x-w*t+spec.P),
				}.Unit()
		},
		NormRad: func(x float64) (maf.Vec, float64) {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			s, c := math.Sincos(k*x - w*t + spec.P)
			v := maf.Vec{X: 1, Y: k * spec.A * c}
			a := maf.Vec{Y: -k * k * spec.A * s}
			return UnitNormal(v, a), Radius(v, a)
		},
		PosTanNormRad: func(x float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			s, c := math.Sincos(k*x - w*t + spec.P)
			v := maf.Vec{X: 1, Y: k * spec.A * c}
			a := maf.Vec{Y: -k * k * spec.A * s}
			return maf.Vec{X: x, Y: spec.A * math.Sin(k*x-w*t+spec.P)},
				maf.Vec{
					X: 1,
					Y: k * spec.A * math.Cos(k*x-w*t+spec.P),
				}.Unit(),
				UnitNormal(v, a), Radius(v, a)
		},
	})
}

func newDecayingSinusoid(
	t float64,
	spec Sinusoid,
	decay ExpDecay,
) Parametric {

	switch {
	case decay.Space > 0 && decay.Time > 0:
		spec.A = spec.A * math.Exp(-decay.Time*t)
		fallthrough
	case decay.Space > 0:
		return newSpaceDecayingSinusoid(t, spec, decay.Space)
	default: // The time decay constant is non zero.
		spec.A = spec.A * math.Exp(-decay.Time*t)
		return newSinusoid(t, spec)
	}
}

func newSpaceDecayingSinusoid(
	t float64,
	spec Sinusoid,
	D float64, // space decay
) Parametric {

	return NewParametric(func(x float64) maf.Vec {
		k := maf.TwoPi / spec.L // Wavenumber.
		w := k * spec.V         // Angular frequency.
		return maf.Vec{
			X: x,
			Y: spec.A * math.Exp(-D*x) * math.Sin(k*x-w*t+spec.P),
		}
	}, ParSpec{
		Tangent: func(x float64) maf.Vec {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			s, c := math.Sincos(k*x - w*t + spec.P)
			e, A := math.Exp(-D*x), spec.A
			//  Let D = spaceDecay value
			//
			//    r(x) = x i + [A e^(-D x) sin(k x - w t + P)] j
			//             -                                   -
			//
			//   r'(x) = 1 i + A[ -D e^(-D x) sin(k x - w t + P) + k e^(-D x) cos(k x - w t + P)] j
			//             -                                                                      -
			//
			//  r''(x) = A [ D D e^(-D x) sin(k x - w t + P) + -D k e^(-D x) cos(k x - w t + P) +
			//               -D k e^(-D x) cos(k x - w t + P)] - k k e^(-D x) sin(k x - w t + P) ] j
			//                                                                                     -
			return maf.Vec{X: 1, Y: A * (-D*e*s + k*e*c)}.Unit()
		},
		Normal: func(x float64) maf.Vec {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			s, c := math.Sincos(k*x - w*t + spec.P)
			e, A := math.Exp(-D*x), spec.A

			v := maf.Vec{X: 1, Y: A * (-D*e*s + k*e*c)}
			a := maf.Vec{Y: A * (D*D*e*s - D*k*e*c - D*k*e*c - k*k*e*s)}
			return UnitNormal(v, a)
		},
		Radius: func(x float64) float64 {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			s, c := math.Sincos(k*x - w*t + spec.P)
			e, A := math.Exp(-D*x), spec.A

			v := maf.Vec{X: 1, Y: A * (-D*e*s + k*e*c)}
			a := maf.Vec{Y: A * (D*D*e*s - D*k*e*c - D*k*e*c - k*k*e*s)}
			return Radius(v, a)
		},
		PosTan: func(x float64) (maf.Vec, maf.Vec) {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			s, c := math.Sincos(k*x - w*t + spec.P)
			e, A := math.Exp(-D*x), spec.A
			return maf.Vec{
					X: x,
					Y: spec.A * math.Exp(-D*x) * math.Sin(k*x-w*t+spec.P),
				},
				maf.Vec{X: 1, Y: A * (-D*e*s + k*e*c)}.Unit()
		},
		NormRad: func(x float64) (maf.Vec, float64) {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			s, c := math.Sincos(k*x - w*t + spec.P)
			e, A := math.Exp(-D*x), spec.A
			v := maf.Vec{X: 1, Y: A * (-D*e*s + k*e*c)}
			a := maf.Vec{Y: A * (D*D*e*s - D*k*e*c - D*k*e*c - k*k*e*s)}
			return UnitNormal(v, a), Radius(v, a)
		},
		PosTanNormRad: func(x float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			k := maf.TwoPi / spec.L // Wavenumber.
			w := k * spec.V         // Angular frequency.
			s, c := math.Sincos(k*x - w*t + spec.P)
			e, A := math.Exp(-D*x), spec.A
			v := maf.Vec{X: 1, Y: A * (-D*e*s + k*e*c)}
			a := maf.Vec{Y: A * (D*D*e*s - D*k*e*c - D*k*e*c - k*k*e*s)}
			return maf.Vec{
					X: x,
					Y: spec.A * math.Exp(-D*x) * math.Sin(k*x-w*t+spec.P),
				},
				maf.Vec{X: 1, Y: A * (-D*e*s + k*e*c)}.Unit(),
				UnitNormal(v, a), Radius(v, a)
		},
	})
}

// NewSinusoidPosTan is a function that takes a given time value and produces,
// for that time value, a function that accepts distance values and produces the
// position and unit tangent vector for the sinusoid curve.
func NewSinusoidPosTan(
	spec Sinusoid, decay ExpDecay,
) func(t float64) func(x float64) (maf.Vec2, maf.Vec2) {

	if decay.Decays() {
		return func(t float64) func(x float64) (maf.Vec2, maf.Vec2) {
			return newDecayingSinusoidPosTan(t, spec, decay)
		}
	}
	return func(t float64) func(x float64) (maf.Vec2, maf.Vec2) {
		return newSinusoidPosTan(t, spec)
	}
}

func newSinusoidPosTan(
	t float64, spec Sinusoid,
) func(x float64) (maf.Vec2, maf.Vec2) {

	return func(x float64) (maf.Vec2, maf.Vec2) {
		k := maf.TwoPi / spec.L // Wavenumber.
		w := k * spec.V         // Angular frequency.
		return maf.Vec2{X: x, Y: spec.A * math.Sin(k*x-w*t+spec.P)},
			maf.Vec2{
				X: 1,
				Y: k * spec.A * math.Cos(k*x-w*t+spec.P),
			}.Unit()
	}
}

func newDecayingSinusoidPosTan(
	t float64, spec Sinusoid, decay ExpDecay,
) func(x float64) (maf.Vec2, maf.Vec2) {

	switch {
	case decay.Space > 0 && decay.Time > 0:
		spec.A = spec.A * math.Exp(-decay.Time*t)
		fallthrough
	case decay.Space > 0:
		return newSpaceDecayingSinusoidPosTan(t, spec, decay.Space)
	default: // The time decay constant is non zero.
		spec.A = spec.A * math.Exp(-decay.Time*t)
		return newSinusoidPosTan(t, spec)
	}
}

func newSpaceDecayingSinusoidPosTan(
	t float64, spec Sinusoid, D float64, // space decay
) func(x float64) (maf.Vec2, maf.Vec2) {

	return func(x float64) (maf.Vec2, maf.Vec2) {
		k := maf.TwoPi / spec.L // Wavenumber.
		w := k * spec.V         // Angular frequency.
		s, c := math.Sincos(k*x - w*t + spec.P)
		e, A := math.Exp(-D*x), spec.A
		return maf.Vec2{
				X: x,
				Y: spec.A * math.Exp(-D*x) * math.Sin(k*x-w*t+spec.P),
			},
			maf.Vec2{X: 1, Y: A * (-D*e*s + k*e*c)}.Unit()
	}
}
