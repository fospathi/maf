package kirv_test

import (
	"math"
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/tezd"
)

func TestSin_Radius(t *testing.T) {

	sinRadOfCurv := func(x float64) float64 {
		// https://en.wikipedia.org/wiki/Radius_of_curvature#Formula
		//
		// Special case formula for a planar curve's radius of curvature:
		//
		//      | (1 + y'y')^(3/2) |
		//  R = | ---------------- |
		//      |        y''       |
		//
		// thus, for a sin curve
		//
		//       | (1 + cc)^(3/2) |
		//  R =  | -------------- |
		//       |       -s       |
		//
		s, c := math.Sincos(x)
		return math.Abs(math.Pow(1+c*c, 3.0/2.0) / (-s))
	}

	sin := kirv.Sin

	for _, T := range []float64{
		-maf.HalfPi,
		-maf.HalfPi / 2,
		maf.HalfPi / 2,
		maf.HalfPi,
	} {
		want := sinRadOfCurv(T)
		got := sin.Radius(T)
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}
}
