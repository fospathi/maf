package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
)

// Helix specifies the properties of a helix.
type Helix struct {
	// The pitch of the helix, that is, the distance travelled along the Z-axis
	// in one complete helix turn.
	P float64
	// The radius of the enclosing cylinder of the helix.
	R float64
}

// NewHelix constructs a new parametric helix curve.
//
// The curve parameter is measured in units of radians.
//
// The helix rotates around the Z-axis and starts on the positive X-axis.
func NewHelix(spec Helix) Parametric {
	return NewParametric(func(t float64) maf.Vec {
		s, c := math.Sincos(t)
		return maf.Vec{X: spec.R * c, Y: spec.R * s, Z: spec.P * t / maf.TwoPi}
	}, ParSpec{
		Tangent: func(t float64) maf.Vec {
			s, c := math.Sincos(t)
			//
			//   r(t) =   r c i   +   r s j   +   (p t / 2π) k
			//                -           -                  -
			//
			//  r'(t) = - r s i   +   r c j   +     (p / 2π) k
			//                -           -                  -
			//
			// r''(t) = - r c i   -   r s j
			//                -           -
			return maf.Vec{
				X: -spec.R * s,
				Y: spec.R * c,
				Z: spec.P / maf.TwoPi,
			}.Unit()
		},
		Normal: func(t float64) maf.Vec {
			s, c := math.Sincos(t)
			v := maf.Vec{X: -spec.R * s, Y: spec.R * c, Z: spec.P / maf.TwoPi}
			a := maf.Vec{X: -spec.R * c, Y: -spec.R * s}
			return UnitNormal(v, a)
		},
		Radius: func(t float64) float64 {
			s, c := math.Sincos(t)
			v := maf.Vec{X: -spec.R * s, Y: spec.R * c, Z: spec.P / maf.TwoPi}
			a := maf.Vec{X: -spec.R * c, Y: -spec.R * s}
			return Radius(v, a)
		},
		PosTan: func(t float64) (maf.Vec, maf.Vec) {
			s, c := math.Sincos(t)
			return maf.Vec{
					X: spec.R * c,
					Y: spec.R * s,
					Z: spec.P * t / maf.TwoPi,
				}, maf.Vec{
					X: -spec.R * s,
					Y: spec.R * c,
					Z: spec.P / maf.TwoPi,
				}.Unit()
		},
		NormRad: func(t float64) (maf.Vec, float64) {
			s, c := math.Sincos(t)
			v := maf.Vec{X: -spec.R * s, Y: spec.R * c, Z: spec.P / maf.TwoPi}
			a := maf.Vec{X: -spec.R * c, Y: -spec.R * s}
			return UnitNormal(v, a), Radius(v, a)
		},
		PosTanNormRad: func(t float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			s, c := math.Sincos(t)
			v := maf.Vec{X: -spec.R * s, Y: spec.R * c, Z: spec.P / maf.TwoPi}
			a := maf.Vec{X: -spec.R * c, Y: -spec.R * s}
			return maf.Vec{
					X: spec.R * c,
					Y: spec.R * s,
					Z: spec.P * t / maf.TwoPi,
				}, maf.Vec{
					X: -spec.R * s,
					Y: spec.R * c,
					Z: spec.P / maf.TwoPi,
				}.Unit(),
				UnitNormal(v, a), Radius(v, a)
		},
	})
}

// NewHelixPosTan function finds the position and unit tangent vector for the
// given helix curve.
//
// The curve parameter is measured in units of radians.
func NewHelixPosTan(spec Helix) func(t float64) (maf.Vec, maf.Vec) {

	return func(t float64) (maf.Vec, maf.Vec) {
		s, c := math.Sincos(t)
		return maf.Vec{X: spec.R * c, Y: spec.R * s, Z: spec.P * t / maf.TwoPi},
			maf.Vec{
				X: -spec.R * s,
				Y: spec.R * c,
				Z: spec.P / maf.TwoPi,
			}.Unit()
	}
}

// NewTimeDepHelix constructs a new parametric helix curve.
//
// The curve parameter is measured in units of time.
//
// The helix rotates around the Z-axis and starts on the positive X-axis.
func NewTimeDepHelix(spec kinem.Helical) Parametric {
	return NewParametric(func(t float64) maf.Vec {
		s, c := math.Sincos(spec.W * t)
		vz := spec.P / (maf.TwoPi / spec.W)
		return maf.Vec{X: spec.R * c, Y: spec.R * s, Z: vz * t}
	}, ParSpec{
		Tangent: func(t float64) maf.Vec {
			s, c := math.Sincos(spec.W * t)
			vz := spec.P / (maf.TwoPi / spec.W)
			return maf.Vec{
				X: -spec.W * spec.R * s,
				Y: spec.W * spec.R * c,
				Z: vz,
			}.Unit()
		},
		Normal: func(t float64) maf.Vec {
			s, c := math.Sincos(spec.W * t)
			vz := spec.P / (maf.TwoPi / spec.W)
			v := maf.Vec{
				X: -spec.W * spec.R * s,
				Y: spec.W * spec.R * c,
				Z: vz,
			}
			a := maf.Vec{
				X: -spec.W * spec.W * spec.R * c,
				Y: -spec.W * spec.W * spec.R * s,
			}
			return UnitNormal(v, a)
		},
		Radius: func(t float64) float64 {
			s, c := math.Sincos(spec.W * t)
			vz := spec.P / (maf.TwoPi / spec.W)
			v := maf.Vec{
				X: -spec.W * spec.R * s,
				Y: spec.W * spec.R * c,
				Z: vz,
			}
			a := maf.Vec{
				X: -spec.W * spec.W * spec.R * c,
				Y: -spec.W * spec.W * spec.R * s,
			}
			return Radius(v, a)
		},
		PosTan: func(t float64) (maf.Vec, maf.Vec) {
			s, c := math.Sincos(spec.W * t)
			vz := spec.P / (maf.TwoPi / spec.W)
			return maf.Vec{X: spec.R * c, Y: spec.R * s, Z: vz * t},
				maf.Vec{
					X: -spec.W * spec.R * s,
					Y: spec.W * spec.R * c,
					Z: vz,
				}.Unit()
		},
		NormRad: func(t float64) (maf.Vec, float64) {
			s, c := math.Sincos(spec.W * t)
			vz := spec.P / (maf.TwoPi / spec.W)
			v := maf.Vec{
				X: -spec.W * spec.R * s,
				Y: spec.W * spec.R * c,
				Z: vz,
			}
			a := maf.Vec{
				X: -spec.W * spec.W * spec.R * c,
				Y: -spec.W * spec.W * spec.R * s,
			}
			return UnitNormal(v, a), Radius(v, a)
		},
		PosTanNormRad: func(t float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			s, c := math.Sincos(spec.W * t)
			vz := spec.P / (maf.TwoPi / spec.W)
			v := maf.Vec{
				X: -spec.W * spec.R * s,
				Y: spec.W * spec.R * c,
				Z: vz,
			}
			a := maf.Vec{
				X: -spec.W * spec.W * spec.R * c,
				Y: -spec.W * spec.W * spec.R * s,
			}
			return maf.Vec{X: spec.R * c, Y: spec.R * s, Z: vz * t},
				maf.Vec{
					X: -spec.W * spec.R * s,
					Y: spec.W * spec.R * c,
					Z: vz,
				}.Unit(),
				UnitNormal(v, a), Radius(v, a)
		},
	})
}

// NewTimeDepHelixPosTan function finds the position and unit tangent vector for
// the given helix curve.
//
// The curve parameter is measured in units of time.
func NewTimeDepHelixPosTan(
	spec kinem.Helical,
) func(t float64) (maf.Vec, maf.Vec) {

	return func(t float64) (maf.Vec, maf.Vec) {
		s, c := math.Sincos(spec.W * t)
		vz := spec.P / (maf.TwoPi / spec.W)
		return maf.Vec{X: spec.R * c, Y: spec.R * s, Z: vz * t},
			maf.Vec{
				X: -spec.W * spec.R * s,
				Y: spec.W * spec.R * c,
				Z: vz,
			}.Unit()
	}
}
