package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// Sin is a parametric sin curve.
//
// The curve parameter is measured in units of radians.
var Sin = NewParametric(
	func(t float64) maf.Vec {
		return maf.Vec{X: t, Y: math.Sin(t)}
	},
	ParSpec{
		Tangent: func(t float64) maf.Vec {
			return maf.Vec{X: 1, Y: math.Cos(t)}.Unit()
		},
		Normal: func(t float64) maf.Vec {
			// https://gist.github.com/fospathi/15451aaf3ea3a8bd90b16d00576c4d7a
			s, c := math.Sincos(t)
			kn := maf.Vec{X: c, Y: -1}.Scale(s / ((1 + c*c) * (1 + c*c)))
			return kn.Unit()
		},
		Radius: func(t float64) float64 {
			s, c := math.Sincos(t)
			kn := maf.Vec{X: c, Y: -1}.Scale(s / ((1 + c*c) * (1 + c*c)))
			return 1.0 / kn.Mag()
		},
		PosTan: func(t float64) (maf.Vec, maf.Vec) {
			s, c := math.Sincos(t)
			return maf.Vec{X: t, Y: s},
				maf.Vec{X: 1, Y: c}.Unit()
		},
		NormRad: func(t float64) (maf.Vec, float64) {
			s, c := math.Sincos(t)
			kn := maf.Vec{X: c, Y: -1}.Scale(s / ((1 + c*c) * (1 + c*c)))
			return kn.Unit(), 1.0 / kn.Mag()
		},
		PosTanNormRad: func(t float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			s, c := math.Sincos(t)
			kn := maf.Vec{X: c, Y: -1}.Scale(s / ((1 + c*c) * (1 + c*c)))
			return maf.Vec{X: t, Y: s}, maf.Vec{X: 1, Y: c}.Unit(),
				kn.Unit(), 1.0 / kn.Mag()
		},
	},
)
