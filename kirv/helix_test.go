package kirv_test

import (
	"math"
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/tezd"
)

func TestHelix_Radius(t *testing.T) {

	r, p := 2.0, 1.0
	helixRadOfCurv := func(t float64) float64 {
		// Is constant for a helix!

		// https://en.wikipedia.org/wiki/Helix#Arc_length,_curvature_and_torsion
		//
		// For a parametric curve (a*cos(t), a*sin(t), bt)
		//
		//        1     aa + bb
		//  R  =  -  =  -------
		//        k      | a |
		return (r*r + (p/maf.TwoPi)*(p/maf.TwoPi)) / math.Abs(r)
	}
	roc := helixRadOfCurv(0)
	helix := kirv.NewHelix(kirv.Helix{P: p, R: r})
	for _, T := range []float64{
		0,
		math.Pi,
		maf.TwoPi,
		-maf.HalfPi,
	} {
		want := roc
		got := helix.Radius(T)
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}
}
