package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
)

// Archimedean specifies the variables of a polar coordinates equation
// describing an Archimedean spiral.
//
// r = a + bθ
//
// To infer b from an equivalent spiral trace described with physical velocities
// v and w:
//
// b = v / w
type Archimedean struct {
	// Initial displacement from the origin along the X-axis.
	A float64
	// The ratio of the radial speed to the angular speed. The distance between
	// the loops is 2πB assuming the polar angle is measured in radians.
	B float64
}

// NewArchimedean constructs a new parametric Archimedean spiral curve.
//
// The curve parameter is measured in units of radians.
//
// The curve is constrained to the XY-plane and starts at the origin or on the
// X-axis.
func NewArchimedean(spec Archimedean) Parametric {

	// Polar method
	// ============
	//
	// The general polar formula for velocity and acceleration are
	//
	//                .  ^              .  ^
	//   v =         (r) r +         (r θ) θ
	//   -               -                 -
	//
	//        ‥     . .  ^      ‥     . .  ^
	//   a = (r - r θ θ) r + (r θ + 2 r θ) θ
	//   -               -                 -
	//
	// where
	//
	//   ^
	//   r =  cos(0) i + sin(0) j
	//   -           -          -
	//
	//   ^
	//   θ = -sin(0) i + cos(0) j
	//   -           -          -
	//
	// Let r = d + vt and θ = wt
	//
	// Then, for an Archimedean spiral
	//   .               ‥
	//   r = v           r = 0
	//
	//   .               ‥
	//   θ = w           θ = 0
	//
	// Thus, for an Archimedean spiral
	//
	//   v = v [cos(0) i + sin(0) j] + rw [-sin(0) i + cos(0) j]
	//   -             -          -                -          -
	//
	//     = (v cos(0) - rw sin(0)) i + (v sin(0) + rw cos(0)) j
	//                              -                          -
	//
	//   a = (-rww) [cos(0) i + sin(0) j] + 2vw [-sin(0) i + cos(0) j]
	//   -                  -          -                 -          -
	//
	// Let v = 1, then from b = v / w, w = 1 / b
	//
	//   v = (cos(0) - (r/b) sin(0)) i + (sin(0) + (r/b) cos(0)) j
	//   -                           -                           -
	//
	//   a = (-r/(bb)) [cos(0) i + sin(0) j] + (2/b) [-sin(0) i + cos(0) j]
	//   -                     -          -                   -          -
	//

	return NewParametric(func(t float64) maf.Vec {
		s, c := math.Sincos(t)
		r := spec.A + spec.B*t
		return maf.Vec{X: r * c, Y: r * s}
	}, ParSpec{
		Tangent: func(t float64) maf.Vec {
			s, c := math.Sincos(t)
			r := spec.A + spec.B*t
			return maf.Vec{X: c - (r/spec.B)*s, Y: s + (r/spec.B)*c}.Unit()
		},
		Normal: func(t float64) maf.Vec {
			s, c := math.Sincos(t)
			r := spec.A + spec.B*t
			v := maf.Vec{X: c - (r/spec.B)*s, Y: s + (r/spec.B)*c}
			a := maf.Vec{
				X: (-2/spec.B)*s - (r/(spec.B*spec.B))*c,
				Y: (2/spec.B)*c - (r/(spec.B*spec.B))*s,
			}
			return UnitNormal(v, a)
		},
		Radius: func(t float64) float64 {
			s, c := math.Sincos(t)
			r := spec.A + spec.B*t
			v := maf.Vec{X: c - (r/spec.B)*s, Y: s + (r/spec.B)*c}
			a := maf.Vec{
				X: (-2/spec.B)*s - (r/(spec.B*spec.B))*c,
				Y: (2/spec.B)*c - (r/(spec.B*spec.B))*s,
			}
			return Radius(v, a)
		},
		PosTan: func(t float64) (maf.Vec, maf.Vec) {
			s, c := math.Sincos(t)
			r := spec.A + spec.B*t
			return maf.Vec{X: r * c, Y: r * s},
				maf.Vec{X: c - (r/spec.B)*s, Y: s + (r/spec.B)*c}.Unit()
		},
		NormRad: func(t float64) (maf.Vec, float64) {
			s, c := math.Sincos(t)
			r := spec.A + spec.B*t
			v := maf.Vec{X: c - (r/spec.B)*s, Y: s + (r/spec.B)*c}
			a := maf.Vec{
				X: (-2/spec.B)*s - (r/(spec.B*spec.B))*c,
				Y: (2/spec.B)*c - (r/(spec.B*spec.B))*s,
			}
			return UnitNormal(v, a), Radius(v, a)
		},
		PosTanNormRad: func(t float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			s, c := math.Sincos(t)
			r := spec.A + spec.B*t
			v := maf.Vec{X: c - (r/spec.B)*s, Y: s + (r/spec.B)*c}
			a := maf.Vec{
				X: (-2/spec.B)*s - (r/(spec.B*spec.B))*c,
				Y: (2/spec.B)*c - (r/(spec.B*spec.B))*s,
			}
			return maf.Vec{X: r * c, Y: r * s},
				maf.Vec{X: c - (r/spec.B)*s, Y: s + (r/spec.B)*c}.Unit(),
				UnitNormal(v, a),
				Radius(v, a)
		},
	})
}

// NewArchimedeanPosTan function finds the position and unit tangent vector for
// the given spiral curve.
//
// The curve parameter is measured in units of radians.
func NewArchimedeanPosTan(
	spec Archimedean,
) func(t float64) (maf.Vec2, maf.Vec2) {

	return func(t float64) (maf.Vec2, maf.Vec2) {
		s, c := math.Sincos(t)
		r := spec.A + spec.B*t
		return maf.Vec2{X: r * c, Y: r * s},
			maf.Vec2{X: c - (r/spec.B)*s, Y: s + (r/spec.B)*c}.Unit()
	}
}

// NewTimeDepArchimedean constructs a new parametric Archimedean spiral curve.
//
// The curve parameter is measured in units of time.
//
// The curve is constrained to the XY-plane and starts at the origin or on the
// X-axis.
func NewTimeDepArchimedean(spec kinem.Archimedean) Parametric {
	return NewParametric(func(t float64) maf.Vec {
		s, c := math.Sincos(spec.W * t)
		r := spec.D + spec.V*t
		return maf.Vec{X: r * c, Y: r * s}
	}, ParSpec{
		Tangent: func(t float64) maf.Vec {
			s, c := math.Sincos(spec.W * t)
			r := spec.D + spec.V*t
			return maf.Vec{
				X: spec.V*c - spec.W*r*s,
				Y: spec.V*s + spec.W*r*c,
			}.Unit()
		},
		Normal: func(t float64) maf.Vec {
			s, c := math.Sincos(spec.W * t)
			r := spec.D + spec.V*t
			v := maf.Vec{
				X: spec.V*c - spec.W*r*s,
				Y: spec.V*s + spec.W*r*c,
			}
			a := maf.Vec{
				X: -2*spec.V*spec.W*s - spec.W*spec.W*r*c,
				Y: 2*spec.V*spec.W*c - spec.W*spec.W*r*s,
			}
			return UnitNormal(v, a)
		},
		Radius: func(t float64) float64 {
			s, c := math.Sincos(spec.W * t)
			r := spec.D + spec.V*t
			v := maf.Vec{
				X: spec.V*c - spec.W*r*s,
				Y: spec.V*s + spec.W*r*c,
			}
			a := maf.Vec{
				X: -2*spec.V*spec.W*s - spec.W*spec.W*r*c,
				Y: 2*spec.V*spec.W*c - spec.W*spec.W*r*s,
			}
			return Radius(v, a)
		},
		PosTan: func(t float64) (maf.Vec, maf.Vec) {
			s, c := math.Sincos(spec.W * t)
			r := spec.D + spec.V*t
			return maf.Vec{X: r * c, Y: r * s},
				maf.Vec{
					X: spec.V*c - spec.W*r*s,
					Y: spec.V*s + spec.W*r*c,
				}.Unit()
		},
		NormRad: func(t float64) (maf.Vec, float64) {
			s, c := math.Sincos(spec.W * t)
			r := spec.D + spec.V*t
			v := maf.Vec{
				X: spec.V*c - spec.W*r*s,
				Y: spec.V*s + spec.W*r*c,
			}
			a := maf.Vec{
				X: -2*spec.V*spec.W*s - spec.W*spec.W*r*c,
				Y: 2*spec.V*spec.W*c - spec.W*spec.W*r*s,
			}
			return UnitNormal(v, a), Radius(v, a)
		},
		PosTanNormRad: func(t float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			s, c := math.Sincos(spec.W * t)
			r := spec.D + spec.V*t
			v := maf.Vec{
				X: spec.V*c - spec.W*r*s,
				Y: spec.V*s + spec.W*r*c,
			}
			a := maf.Vec{
				X: -2*spec.V*spec.W*s - spec.W*spec.W*r*c,
				Y: 2*spec.V*spec.W*c - spec.W*spec.W*r*s,
			}
			return maf.Vec{X: r * c, Y: r * s},
				maf.Vec{
					X: spec.V*c - spec.W*r*s,
					Y: spec.V*s + spec.W*r*c,
				}.Unit(),
				UnitNormal(v, a), Radius(v, a)
		},
	})
}

// NewTimeDepArchimedeanPosTan function finds the position and unit tangent
// vector for the given spiral curve.
//
// The curve parameter is measured in units of time.
func NewTimeDepArchimedeanPosTan(
	spec kinem.Archimedean,
) func(t float64) (maf.Vec2, maf.Vec2) {

	return func(t float64) (maf.Vec2, maf.Vec2) {
		s, c := math.Sincos(spec.W * t)
		r := spec.D + spec.V*t
		return maf.Vec2{X: r * c, Y: r * s},
			maf.Vec2{
				X: spec.V*c - spec.W*r*s,
				Y: spec.V*s + spec.W*r*c,
			}.Unit()
	}
}
