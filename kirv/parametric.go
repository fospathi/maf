package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// NewParametric constructs a new Parametric.
//
// The first, and always necessary, argument is the position function which
// defines the curve.
func NewParametric(pos func(t float64) maf.Vec, spec ParSpec) Parametric {
	par := Parametric{
		pos:           pos,
		tan:           spec.Tangent,
		norm:          spec.Normal,
		rad:           spec.Radius,
		posTan:        spec.PosTan,
		normRad:       spec.NormRad,
		posTanNormRad: spec.PosTanNormRad,
	}

	if par.tan == nil {
		par.tan = func(t float64) maf.Vec {
			return NumericalTangent(par, t)
		}
	}

	if par.norm == nil {
		if spec.Tangent != nil {
			par.norm = func(t float64) maf.Vec {
				return NumericalNorm(par, t)
			}
		} else {
			par.norm = func(t float64) maf.Vec {
				return NumericalNormal(par, t)
			}
		}
	}

	if par.rad == nil {
		par.rad = func(t float64) float64 {
			return NumericalRadius(par, t)
		}
	}

	if par.posTan == nil {
		if spec.Tangent != nil {
			par.posTan = func(t float64) (maf.Vec, maf.Vec) {
				return pos(t), spec.Tangent(t)
			}
		} else {
			par.posTan = func(t float64) (maf.Vec, maf.Vec) {
				return pos(t), NumericalTangent(par, t)
			}
		}
	}

	if par.normRad == nil {
		if spec.Normal != nil || spec.Radius != nil {
			par.normRad = func(t float64) (maf.Vec, float64) {
				return par.norm(t), par.rad(t)
			}
		} else {
			par.normRad = func(t float64) (maf.Vec, float64) {
				return NumericalNormalRadius(par, t)
			}
		}
	}

	if par.posTanNormRad == nil {
		if spec.PosTan != nil || spec.NormRad != nil {
			par.posTanNormRad = func(t float64) (
				maf.Vec, maf.Vec, maf.Vec, float64) {

				pos, tan := par.PosTan(t)
				norm, rad := par.NormRad(t)
				return pos, tan, norm, rad
			}
		} else {
			par.posTanNormRad = func(t float64) (
				maf.Vec, maf.Vec, maf.Vec, float64) {

				return NumericalPosTanNormRad(par, t)
			}
		}
	}

	return par
}

// ParSpec specifies some extra function(s) that define a parametric curve.
//
// Optional functions should be specified if possible and they take precedence
// over their equivalent approximate numerical methods which are substituted in
// their absence.
//
// Note that combination functions and dedicated functions can use different
// ways of calculating the same property and hence may return somewhat different
// values.
type ParSpec struct {
	// Optional. Defines the unit tangent direction vector of the curve.
	Tangent func(t float64) maf.Vec

	// Optional. Defines the unit principal normal vector of the curve.
	Normal func(t float64) maf.Vec

	// Optional. Defines the radius of curvature of the curve.
	Radius func(t float64) float64

	// Optional. Defines the position of the curve and the unit tangent
	// direction vector of the curve. May be more efficient than calling
	// position and tangent separately.
	PosTan func(t float64) (maf.Vec, maf.Vec)

	// Optional. Defines the unit principal normal vector and the radius of
	// curvature of the curve. May be more efficient than calling normal and
	// radius separately.
	NormRad func(t float64) (maf.Vec, float64)

	// Optional. Defines the position, unit tangent direction vector, unit
	// principal normal vector and radius of curvature of the curve. May be more
	// efficient than calling position, tangent, normal, and radius separately.
	PosTanNormRad func(t float64) (maf.Vec, maf.Vec, maf.Vec, float64)
}

// A Parametric curve in 3D space.
//
// Note that the combination methods which compute multiple curve properties at
// once may use different methods than the dedicated methods and hence may
// return somewhat different values.
type Parametric struct {
	pos           func(float64) maf.Vec
	tan           func(float64) maf.Vec
	norm          func(float64) maf.Vec
	rad           func(float64) float64
	posTan        func(float64) (maf.Vec, maf.Vec)
	normRad       func(float64) (maf.Vec, float64)
	posTanNormRad func(float64) (maf.Vec, maf.Vec, maf.Vec, float64)
}

// Position of the curve at the given curve parameter.
func (par Parametric) Position(t float64) maf.Vec {
	return par.pos(t)
}

// Tangent unit direction vector of the curve at the given curve parameter.
func (par Parametric) Tangent(t float64) maf.Vec {
	return par.tan(t)
}

// Normal is the unit principal normal vector of the curve at the given curve
// parameter.
func (par Parametric) Normal(t float64) maf.Vec {
	return par.norm(t)
}

// Radius of curvature of the curve at the given curve parameter.
func (par Parametric) Radius(t float64) float64 {
	return par.rad(t)
}

// PosTan is the position and unit tangent direction vector of the curve at the
// given curve parameter.
//
// May be more efficient than calling position and tangent separately.
func (par Parametric) PosTan(t float64) (maf.Vec, maf.Vec) {
	return par.posTan(t)
}

// PosTanFunc is the function used by PosTan.
func (par Parametric) PosTanFunc() func(t float64) (maf.Vec, maf.Vec) {
	return par.posTan
}

// NormRad is the unit principal normal vector and radius of curvature of the
// curve at the given curve parameter.
//
// May be more efficient than calling normal and radius separately.
func (par Parametric) NormRad(t float64) (maf.Vec, float64) {
	return par.normRad(t)
}

// PosTanNormRad is the position, unit tangent direction vector, unit principal
// normal vector, and radius of curvature of the curve at the given curve
// parameter.
//
// May be more efficient than calling position, tangent, normal, and radius
// separately.
func (par Parametric) PosTanNormRad(t float64) (
	maf.Vec, maf.Vec, maf.Vec, float64) {

	return par.posTanNormRad(t)
}

// CentreOfCurvature of the curve, if it exists, at the given curve parameter.
func (par Parametric) CentreOfCurvature(t float64) maf.Vec {
	pos, _, norm, rad := par.posTanNormRad(t)
	return pos.Add(norm.Scale(rad))
}

// Osculating circle of the curve, if it exists, at the given curve parameter.
func (par Parametric) Osculating(t float64) maf.Circle {
	pos, tan, norm, rad := par.posTanNormRad(t)
	return maf.Circle{
		C:      pos.Add(norm.Scale(rad)),
		Normal: tan.Cross(norm),
		R:      rad,
	}
}

// NumericalPosTan is the position and a rough approximation of the given
// curve's unit tangent vector at the given parameter value.
//
// The given function shall define the position of a curve.
func NumericalPosTan(
	pos func(t float64) maf.Vec, t float64,
) (maf.Vec, maf.Vec) {

	return pos(t), pos(t + H).Subtract(pos(t - H)).Unit()
}

// NumericalPosTanFunc returns a new function that returns both the position and
// a rough approximation of the given curve's unit tangent vector for a given
// parameter value.
//
// The argument function defines the position of a curve.
func NumericalPosTanFunc(
	pos func(t float64) maf.Vec,
) func(t float64) (maf.Vec, maf.Vec) {

	return func(t float64) (maf.Vec, maf.Vec) {
		return pos(t), pos(t + H).Subtract(pos(t - H)).Unit()
	}
}

// NumericalPosInwardsTanFunc returns a new function that returns both the
// position and a rough approximation of the given curve's unit tangent vector
// for a given parameter value.
//
// The argument function defines the position of a curve. The argument float
// parameter value is the centre of the given function's valid domain interval.
//
// The tangent is calculated only with positions whose parameter values are
// between the given parameter and the centre of the domain. This makes it safe
// to use at domain limits outside which the position is not defined.
func NumericalPosInwardsTanFunc(
	pos func(t float64) maf.Vec, cen float64,
) func(t float64) (maf.Vec, maf.Vec) {

	return func(t float64) (maf.Vec, maf.Vec) {
		switch {
		case t == cen:
			return pos(t), pos(t + H).Subtract(pos(t - H)).Unit()
		case t < cen:
			return pos(t), pos(t + H).Subtract(pos(t)).Unit()
		default:
			return pos(t), pos(t).Subtract(pos(t - H)).Unit()
		}
	}
}

// A Positioner is a parametric curve with a known position at a given curve
// parameter value.
type Positioner interface {
	Position(t float64) maf.Vec
}

// NumericalTangent is a rough approximation of the given curve's unit tangent
// vector at the given parameter value.
func NumericalTangent(par Positioner, t float64) maf.Vec {
	f := par.Position

	// An approximation for the first derivative is
	//
	//   f'(x) ~= f(x+h) - f(x-h)
	//            ---------------
	//                   2h
	//
	// Since only the unit tangent vector is required the .Scale(1 / (2 * h)) is
	// redundant.
	return f(t + H).Subtract(f(t - H)).Unit()
}

// NumericalNormal is a rough approximation of the given curve's unit principal
// normal vector at the given parameter value.
//
// At the parameter value the curve shall not be an inflection point. Nor shall
// the curve be linear.
func NumericalNormal(par Positioner, t float64) maf.Vec {
	f, h := par.Position, H
	a, b, c := f(t), f(t-h), f(t+h)
	const (
		maxIters  = 5
		tolerance = 1e-6
		base      = 1e2
	)
	// The angle/curve between ab and ac needs to be sufficiently
	// small/curvaceous that a circle can be made.
	for i := 0; i < maxIters; i++ {
		if ang := a.To(b).Angle(a.To(c)); ang < (math.Pi - tolerance) {
			break
		}
		h = h * base
		b, c = f(t-h), f(t+h)
	}
	return a.To(maf.Triangle{A: a, B: b, C: c}.Circumcircle().C).Unit()
}

// A Tangenter is a parametric curve with a known unit tangent at a given curve
// parameter value.
type Tangenter interface {
	Tangent(t float64) maf.Vec
}

// NumericalNorm is a rough approximation of the given curve's unit principal
// normal vector at the given parameter value.
//
// At the parameter value the curve shall not be an inflection point. Nor shall
// the curve be linear.
func NumericalNorm(par Tangenter, t float64) maf.Vec {
	f := par.Tangent
	a, b, c := f(t), f(t-H), f(t+H)
	return b.Cross(c).Cross(a).Unit()
}

// NumericalNormalRadius is a rough approximation of the given curve's unit
// principal normal vector and radius of curvature at the given parameter value.
//
// At the parameter value the curve shall not be an inflection point. Nor shall
// the curve be linear.
func NumericalNormalRadius(par Positioner, t float64) (maf.Vec, float64) {
	f := par.Position
	a, b, c := f(t), f(t-H), f(t+H)
	circ := maf.Triangle{A: a, B: b, C: c}.Circumcircle()
	return a.To(circ.C).Unit(), circ.R
}

// NumericalRadius is a numerical approximation of the given curve's radius of
// curvature at the given parameter value.
//
// At the parameter value the curve shall not be an inflection point. Nor shall
// the curve be linear.
func NumericalRadius(par Positioner, t float64) float64 {

	// From the cosine rule for triangles: r² = r² + d² - 2rd cos(θ)
	//                                      r = d/(2*cos(θ))
	// 	         B
	// 	        /-                    AB is a taken as a circle chord. The
	// 	       /    -  r              direction of AC is taken as the
	// 	    d /        -              approximate direction of an equal length
	// 	     /  θ         -           chord on the same circle.
	//    A /_ _ _ _ _ _ _ _ _        The angle of the chords is 2θ.
	//      \	 	  r
	//       \  θ
	//        \
	//         \
	//          \
	//           C
	//
	f, h := par.Position, H
	a, b, c := f(t), f(t-h), f(t+h)
	const (
		maxIters  = 5
		tolerance = 1e-7
		base      = 1e2
	)
	var (
		ang float64
	)
	// The angle/curve between ab and ac needs to be sufficiently
	// small/curvaceous that a circle can be made.
	for i := 0; i < maxIters; i++ {
		if ang = a.To(b).Angle(a.To(c)); ang < (math.Pi - tolerance) {
			break
		}
		h = h * base
		b, c = f(t-h), f(t+h)
	}
	chord := a.To(b)
	θ, d := ang/2, chord.Mag()
	return d / (2 * math.Cos(θ))
}

// NumericalPosTanNormRad is a numerical approximation of the given curve's
// position, unit tangent, unit principal normal, and radius of curvature at the
// given parameter value.
//
// At the parameter value the curve shall not be an inflection point. Nor shall
// the curve be linear.
func NumericalPosTanNormRad(par Positioner, t float64) (
	maf.Vec, maf.Vec, maf.Vec, float64) {

	f := par.Position
	a, b, c := f(t), f(t-H), f(t+H)
	tan := f(t + H).Subtract(f(t - H)).Scale(1 / (2 * H)).Unit()
	circ := maf.Triangle{A: a, B: b, C: c}.Circumcircle()
	return a, tan, a.To(circ.C).Unit(), circ.R
}

// UnitNormal vector at a non-inflecting point of a curve with the given
// non-parallel velocity and acceleration.
func UnitNormal(v, a maf.Vec) maf.Vec {
	// One possible formula for the principal unit normal vector of a curve is
	//
	//   ^     a⊥          a⊥ = the component of the acceleration which
	//   n = ------              changes the direction of the velocity.
	//   -   | a⊥ |
	//
	aPar := a.Resolution(v)
	aPerp := a.Subtract(aPar)
	return aPerp.Unit()
}

// Radius of curvature at a non-inflecting point of a curve with the given
// non-parallel velocity and acceleration.
func Radius(v, a maf.Vec) float64 {
	// One formula for the curvature of a curve is
	//
	//        | v(t) x a(t) |
	//    κ = ---------------
	//           | v(t) |³
	//
	// where r = 1 / κ
	vMag := v.Mag()
	return (vMag * vMag * vMag) / v.Cross(a).Mag()
}
