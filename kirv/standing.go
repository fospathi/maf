package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// Standing specifies the properties of a sinusoidal standing wave.
type Standing struct {
	// Amplitude. Maximum displacement from Y = 0.
	//
	// This is twice the maximum amplitude of the two constituent waves that
	// form this resulting standing wave.
	A float64

	// Wavelength.
	L float64

	// Angular velocity, related to the frequency (f) by: w = 2πf.
	W float64
}

// NewStanding returns a function which constructs new parametric sinusoidal
// standing wave curves.
//
// The returned function accepts a time value and the curve parameter of the
// resulting parametric curve is measured in units of distance.
func NewStanding(spec Standing, decay ExpDecay) func(t float64) Parametric {
	if decay.Decays() {
		return func(t float64) Parametric {
			return newDecayingStanding(t, spec, decay)
		}
	}
	return func(t float64) Parametric {
		return newStanding(t, spec)
	}
}

func newStanding(t float64, spec Standing) Parametric {
	return NewParametric(func(x float64) maf.Vec {
		// https://en.wikipedia.org/wiki/Standing_wave#Standing_wave_on_an_infinite_length_string

		A, w, k := spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
		return maf.Vec{
			X: x,
			Y: 2 * A * math.Sin(k*x) * math.Cos(w*t),
		}
	}, ParSpec{
		Tangent: func(x float64) maf.Vec {
			A, w, k := spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.

			// r(x) = x i +     A [ sin(k x - w t) + sin(k x + w t) ] j
			//          -                                             -
			//
			// v(x) = 1 i +   k A [ cos(k x - w t) + cos(k x + w t) ] j
			//          -                                             -
			//
			// a(x) =     - k k A [ sin(k x - w t) + sin(k x + w t) ] j
			//                                                        -
			return maf.Vec{
				X: 1,
				Y: k*A*math.Cos(k*x-w*t) + k*A*math.Cos(k*x+w*t),
			}.Unit()
		},
		Normal: func(x float64) maf.Vec {
			A, w, k := spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			v := maf.Vec{X: 1, Y: k*A*c1 + k*A*c2}
			a := maf.Vec{Y: -k*k*A*s1 - k*k*A*s2}
			return UnitNormal(v, a)
		},
		Radius: func(x float64) float64 {
			A, w, k := spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			v := maf.Vec{X: 1, Y: k*A*c1 + k*A*c2}
			a := maf.Vec{Y: -k*k*A*s1 - k*k*A*s2}
			return Radius(v, a)
		},
		PosTan: func(x float64) (maf.Vec, maf.Vec) {
			A, w, k := spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			r := maf.Vec{X: x, Y: A*s1 + A*s2}
			v := maf.Vec{X: 1, Y: k*A*c1 + k*A*c2}
			return r, v.Unit()
		},
		NormRad: func(x float64) (maf.Vec, float64) {
			A, w, k := spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			v := maf.Vec{X: 1, Y: k*A*c1 + k*A*c2}
			a := maf.Vec{Y: -k*k*A*s1 - k*k*A*s2}
			return UnitNormal(v, a), Radius(v, a)
		},
		PosTanNormRad: func(x float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			A, w, k := spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			r := maf.Vec{X: x, Y: A*s1 + A*s2}
			v := maf.Vec{X: 1, Y: k*A*c1 + k*A*c2}
			a := maf.Vec{Y: -k*k*A*s1 - k*k*A*s2}
			return r, v.Unit(), UnitNormal(v, a), Radius(v, a)
		},
	})
}

func newDecayingStanding(
	t float64,
	spec Standing,
	decay ExpDecay,
) Parametric {

	switch {
	case decay.Space > 0 && decay.Time > 0:
		spec.A = spec.A * math.Exp(-decay.Time*t)
		fallthrough
	case decay.Space > 0:
		return newSpaceDecayingStanding(t, spec, decay.Space)
	default: // The time decay constant is non zero.
		spec.A = spec.A * math.Exp(-decay.Time*t)
		return newStanding(t, spec)
	}
}

func newSpaceDecayingStanding(
	t float64,
	spec Standing,
	D float64, // space decay
) Parametric {

	return NewParametric(func(x float64) maf.Vec {
		e, A, w, k := math.Exp(-D*x), spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
		return maf.Vec{
			X: x,
			Y: e * 2 * A * math.Sin(k*x) * math.Cos(w*t),
		}
	}, ParSpec{
		Tangent: func(x float64) maf.Vec {
			e, A, w, k := math.Exp(-D*x), spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)

			// r(x) = x i + A e^(-D x) [ sin(k x - w t) + sin(k x + w t) ] j
			//          -                                                  -
			//
			// v(x) = 1 i + -A D e^(-D x) [ sin(k x - w t) + sin(k x + w t) ] j +
			//          -                                                     -
			//
			//        A e^(-D x) [ k cos(k x - w t) + k cos(k x + w t) ] j
			//                                                           -
			//
			//      = 1 i + A e^(-D x) [ -D sin(k x - w t) - D sin(k x + w t) +
			//          -
			//
			//                           k cos(k x - w t) + k cos(k x + w t) ] j
			//                                                                 -
			//
			// a(x) = -A D e^(-D x) [ -D sin(k x - w t) - D sin(k x + w t) +
			//
			//                        k cos(k x - w t) + k cos(k x + w t) ] j +
			//                                                              -
			//
			//          A e^(-D x) [ -k D cos(k x - w t) - k D cos(k x + w t) -
			//
			//                       k k sin(k x - w t) - k k sin(k x + w t) ] j
			//                                                                 -
			//
			//      = A e^(-D x) [ D D sin(k x - w t) + D D sin(k x + w t) -
			//
			//                     k D cos(k x - w t) - k D cos(k x + w t) -
			//
			//                     k D cos(k x - w t) - k D cos(k x + w t) -
			//
			//                     k k sin(k x - w t) - k k sin(k x + w t) ] j
			//                                                               -
			//
			//      = A e^(-D x) [   D D sin(k x - w t) + D D sin(k x + w t) -
			//
			//                     2 k D cos(k x - w t) - 2 k D cos(k x + w t) -
			//
			//                       k k sin(k x - w t) - k k sin(k x + w t) ] j
			//                                                                 -

			return maf.Vec{
				X: 1,
				Y: A * e * (-D*s1 - D*s2 + k*c1 + k*c2),
			}.Unit()
		},
		Normal: func(x float64) maf.Vec {
			e, A, w, k := math.Exp(-D*x), spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			v := maf.Vec{X: 1, Y: A * e * (-D*s1 - D*s2 + k*c1 + k*c2)}
			a := maf.Vec{Y: A * e * (D*D*(s1+s2) - 2*k*D*(c1+c2) - k*k*(s1+s2))}
			return UnitNormal(v, a)
		},
		Radius: func(x float64) float64 {
			e, A, w, k := math.Exp(-D*x), spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			v := maf.Vec{X: 1, Y: A * e * (-D*s1 - D*s2 + k*c1 + k*c2)}
			a := maf.Vec{Y: A * e * (D*D*(s1+s2) - 2*k*D*(c1+c2) - k*k*(s1+s2))}
			return Radius(v, a)
		},
		PosTan: func(x float64) (maf.Vec, maf.Vec) {
			e, A, w, k := math.Exp(-D*x), spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			r := maf.Vec{X: x, Y: A * e * (s1 + s2)}
			v := maf.Vec{X: 1, Y: A * e * (-D*s1 - D*s2 + k*c1 + k*c2)}
			return r, v.Unit()
		},
		NormRad: func(x float64) (maf.Vec, float64) {
			e, A, w, k := math.Exp(-D*x), spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			v := maf.Vec{X: 1, Y: A * e * (-D*s1 - D*s2 + k*c1 + k*c2)}
			a := maf.Vec{Y: A * e * (D*D*(s1+s2) - 2*k*D*(c1+c2) - k*k*(s1+s2))}
			return UnitNormal(v, a), Radius(v, a)
		},
		PosTanNormRad: func(x float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			e, A, w, k := math.Exp(-D*x), spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
			s1, c1 := math.Sincos(k*x - w*t)
			s2, c2 := math.Sincos(k*x + w*t)
			r := maf.Vec{X: x, Y: A * e * (s1 + s2)}
			v := maf.Vec{X: 1, Y: A * e * (-D*s1 - D*s2 + k*c1 + k*c2)}
			a := maf.Vec{Y: A * e * (D*D*(s1+s2) - 2*k*D*(c1+c2) - k*k*(s1+s2))}
			return r, v.Unit(), UnitNormal(v, a), Radius(v, a)
		},
	})
}

// NewStandingPosTan is a function that takes a given time value and produces,
// for that time value, a function that accepts distance values and produces the
// position and unit tangent vector for the standing wave curve.
func NewStandingPosTan(
	spec Standing, decay ExpDecay,
) func(t float64) func(x float64) (maf.Vec2, maf.Vec2) {

	if decay.Decays() {
		return func(t float64) func(x float64) (maf.Vec2, maf.Vec2) {
			return newDecayingStandingPosTan(t, spec, decay)
		}
	}
	return func(t float64) func(x float64) (maf.Vec2, maf.Vec2) {
		return newStandingPosTan(t, spec)
	}
}

func newStandingPosTan(
	t float64, spec Standing,
) func(x float64) (maf.Vec2, maf.Vec2) {

	return func(x float64) (maf.Vec2, maf.Vec2) {
		A, w, k := spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
		s1, c1 := math.Sincos(k*x - w*t)
		s2, c2 := math.Sincos(k*x + w*t)
		r := maf.Vec2{X: x, Y: A*s1 + A*s2}
		v := maf.Vec2{X: 1, Y: k*A*c1 + k*A*c2}
		return r, v.Unit()
	}
}

func newDecayingStandingPosTan(
	t float64, spec Standing, decay ExpDecay,
) func(x float64) (maf.Vec2, maf.Vec2) {

	switch {
	case decay.Space > 0 && decay.Time > 0:
		spec.A = spec.A * math.Exp(-decay.Time*t)
		fallthrough
	case decay.Space > 0:
		return newSpaceDecayingStandingPosTan(t, spec, decay.Space)
	default: // The time decay constant is non zero.
		spec.A = spec.A * math.Exp(-decay.Time*t)
		return newStandingPosTan(t, spec)
	}
}

func newSpaceDecayingStandingPosTan(
	t float64, spec Standing, D float64, // space decay
) func(x float64) (maf.Vec2, maf.Vec2) {

	return func(x float64) (maf.Vec2, maf.Vec2) {
		e, A, w, k := math.Exp(-D*x), spec.A/2, spec.W, maf.TwoPi/spec.L // k = wavenumber.
		s1, c1 := math.Sincos(k*x - w*t)
		s2, c2 := math.Sincos(k*x + w*t)
		r := maf.Vec2{X: x, Y: A * e * (s1 + s2)}
		v := maf.Vec2{X: 1, Y: A * e * (-D*s1 - D*s2 + k*c1 + k*c2)}
		return r, v.Unit()
	}
}
