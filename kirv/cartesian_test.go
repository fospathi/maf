package kirv_test

import (
	"math"
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/tezd"
)

func TestXYCartesian_NumericalRadius(t *testing.T) {
	circR := 2.0
	upperSemiCirc := kirv.NewXYCartesian(func(x float64) float64 {
		return math.Sqrt(circR*circR - x*x)
	}, kirv.XYCarSpec{})

	want := circR
	got := kirv.XYCartesianNumericalRadius(upperSemiCirc, 0)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = circR
	got = kirv.XYCartesianNumericalRadius(upperSemiCirc, -circR/2)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = circR
	got = kirv.XYCartesianNumericalRadius(upperSemiCirc, -circR)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = circR
	got = kirv.XYCartesianNumericalRadius(upperSemiCirc, circR/2)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = circR
	got = kirv.XYCartesianNumericalRadius(upperSemiCirc, circR)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	circR = 0.01
	lowerSemiCirc := kirv.NewXYCartesian(func(x float64) float64 {
		return -math.Sqrt(circR*circR - x*x)
	}, kirv.XYCarSpec{})

	want = circR
	got = kirv.XYCartesianNumericalRadius(lowerSemiCirc, 0)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = circR
	got = kirv.XYCartesianNumericalRadius(lowerSemiCirc, -circR/2)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = circR
	got = kirv.XYCartesianNumericalRadius(lowerSemiCirc, -circR)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = circR
	got = kirv.XYCartesianNumericalRadius(lowerSemiCirc, circR/2)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = circR
	got = kirv.XYCartesianNumericalRadius(lowerSemiCirc, circR)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	sin := kirv.NewXYCartesian(func(x float64) float64 {
		return math.Sin(x)
	}, kirv.XYCarSpec{})

	sinRadOfCurv := func(x float64) float64 {
		// https://en.wikipedia.org/wiki/Radius_of_curvature#Formula
		//
		// Special case formula for a planar curve:
		//
		//      | (1 + y'y')^(3/2) |
		//  R = | ---------------- |
		//      |        y''       |
		//
		// thus, for a sin curve
		//
		//       | (1 + cc)^(3/2) |
		//  R =  | -------------- |
		//       |       -s       |
		//
		s, c := math.Sincos(x)
		return math.Abs(math.Pow(1+c*c, 3.0/2.0) / (-s))
	}

	want = sinRadOfCurv(-maf.HalfPi)
	got = kirv.XYCartesianNumericalRadius(sin, -maf.HalfPi)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = sinRadOfCurv(-maf.HalfPi / 2)
	got = kirv.XYCartesianNumericalRadius(sin, -maf.HalfPi/2)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = sinRadOfCurv(maf.HalfPi / 2)
	got = kirv.XYCartesianNumericalRadius(sin, maf.HalfPi/2)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = sinRadOfCurv(maf.HalfPi)
	got = kirv.XYCartesianNumericalRadius(sin, maf.HalfPi)
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
