/*
Package kirv is a library of elementary curves.
*/
package kirv

import "math"

const (
	// H is the small variable increment for numerical approximations.
	//
	// The value of H is a balance between increment size (smaller is better)
	// and significant digits (more is better).
	//
	// Determined through trial and error on the sin function.
	H = 1e-6
)

// SqrtH is the square root of H.
var SqrtH = math.Sqrt(H)
