package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
)

// NewTimeDepProjectile constructs a new parametric projectile curve.
//
// The curve parameter is measured in units of time.
//
// The motion is constrained to the XY-plane, starts at the origin, and is
// subject to a constant acceleration parallel to the Y-axis.
//
// Beware of the degenerate case of a projectile with no horizontal velocity
// component. Such a straight-line-ish motion is not a very good subject for a
// parametric curve.
func NewTimeDepProjectile(spec kinem.Projectile) Parametric {
	a, x, y := spec.A, spec.U.X, spec.U.Y
	s := x*x + y*y

	// Let x and y be the initial velocity components
	//
	//
	// Let   r(t) = x t i + (y t + a t t / 2) j
	//                  -                     -
	//
	//
	//         dr
	//         -- = x i + (y + a t) j
	//         dt     -             -
	//
	//
	//          ^   dr    | dr |
	//          t = --  / | -- |
	//          -   dt    | dt |
	//
	//
	//            = [ x i + (y + a t) j ] / sqrt[ x x + (y + at)(y + at) ]
	//                  -             -
	//
	//
	//              | dr |
	// Let  | v | = | -- | = sqrt [ x x + (y + at)(y + at) ]
	//              | dt |
	//                     = sqrt [ a a t t + 2 a y t + y y + x x ]
	//
	//                     = sqrt [ a a t t + 2 a y t + s ]
	//
	//
	// Since
	//
	//
	//          ^    d  ^     | d  ^ |
	//          n =  -- t  /  | -- t |
	//          -    dt -     | dt - |
	//
	//
	//                                             d  ^
	// The unit normal then is the unit vector of  -- t
	//                                             dt -
	//
	//
	// From the quotient rule
	//
	//
	//                                                           a a t + a y
	//               ( a j ) ( | v | ) - ( x i + (y + a t) j ) ( ----------- )
	//       d  ^        -                   -             -        | v |
	//       -- t =  ---------------------------------------------------------
	//       dt -                      | v | | v |
	//
	//
	// One formula for the curvature of a curve is
	//
	//
	//              | d  ^ |
	//          κ = | -- t | /  | v |
	//              | dt - |
	//
	//
	// So the reciprocal of this, the radius of curvature, is
	//
	//
	//          1           | d  ^ |
	//          - = | v | / | -- t |
	//          κ           | dt - |

	return NewParametric(func(t float64) maf.Vec {
		return maf.Vec{X: x * t, Y: y*t + a*t*t/2.0}
	}, ParSpec{
		Tangent: func(t float64) maf.Vec {
			v := math.Sqrt(a*a*t*t + 2*a*y*t + s)
			return maf.Vec{X: x / v, Y: (y + a*t) / v}
		},
		Normal: func(t float64) maf.Vec {
			v := math.Sqrt(a*a*t*t + 2*a*y*t + s)
			return maf.Vec{
				X: -x * (a*a*t + a*y) / (v * v * v),
				Y: a/v - (y+a*t)*(a*a*t+a*y)/(v*v*v),
			}.Unit()
		},
		Radius: func(t float64) float64 {
			v := math.Sqrt(a*a*t*t + 2*a*y*t + s)
			return v / maf.Vec{
				X: -x * (a*a*t + a*y) / (v * v * v),
				Y: a/v - (y+a*t)*(a*a*t+a*y)/(v*v*v),
			}.Mag()
		},
		PosTan: func(t float64) (maf.Vec, maf.Vec) {
			v := math.Sqrt(a*a*t*t + 2*a*y*t + s)
			return maf.Vec{X: x * t, Y: y*t + a*t*t/2.0},
				maf.Vec{X: x / v, Y: (y + a*t) / v}
		},
		NormRad: func(t float64) (maf.Vec, float64) {
			v := math.Sqrt(a*a*t*t + 2*a*y*t + s)
			return maf.Vec{
					X: -x * (a*a*t + a*y) / (v * v * v),
					Y: a/v - (y+a*t)*(a*a*t+a*y)/(v*v*v),
				}.Unit(),
				v / maf.Vec{
					X: -x * (a*a*t + a*y) / (v * v * v),
					Y: a/v - (y+a*t)*(a*a*t+a*y)/(v*v*v),
				}.Mag()
		},
		PosTanNormRad: func(t float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			v := math.Sqrt(a*a*t*t + 2*a*y*t + s)
			return maf.Vec{X: x * t, Y: y*t + a*t*t/2.0},
				maf.Vec{X: x / v, Y: (y + a*t) / v},
				maf.Vec{
					X: -x * (a*a*t + a*y) / (v * v * v),
					Y: a/v - (y+a*t)*(a*a*t+a*y)/(v*v*v),
				}.Unit(),
				v / maf.Vec{
					X: -x * (a*a*t + a*y) / (v * v * v),
					Y: a/v - (y+a*t)*(a*a*t+a*y)/(v*v*v),
				}.Mag()
		},
	})
}

// NewTimeDepProjectilePosTan function finds the position and unit tangent
// vector for the given projectile curve.
//
// The curve parameter is measured in units of time.
func NewTimeDepProjectilePosTan(
	spec kinem.Projectile,
) func(t float64) (maf.Vec2, maf.Vec2) {

	a, x, y := spec.A, spec.U.X, spec.U.Y
	s := x*x + y*y
	return func(t float64) (maf.Vec2, maf.Vec2) {
		v := math.Sqrt(a*a*t*t + 2*a*y*t + s)
		return maf.Vec2{X: x * t, Y: y*t + a*t*t/2.0},
			maf.Vec2{X: x / v, Y: (y + a*t) / v}
	}
}
