package kirv_test

import (
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kirv"
)

func TestNewStandingPosTan(t *testing.T) {
	spec := kirv.Standing{
		A: 2, L: 1, W: 1,
	}
	posTan := kirv.NewStandingPosTan(spec, kirv.ExpDecay{})
	param := kirv.NewStanding(spec, kirv.ExpDecay{})(0)

	pos, tan := posTan(0)(1.1)
	got1, got2 := maf.Vec{X: pos.X, Y: pos.Y}, maf.Vec{X: tan.X, Y: tan.Y}
	want1, want2 := param.PosTan(1.1)
	if !maf.CompareVec(got1, want1, 2) || !maf.CompareVec(got2, want2, 2) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
			want1, got1, want2, got2)
	}

	posTan = kirv.NewStandingPosTan(spec, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})
	param = kirv.NewStanding(spec, kirv.ExpDecay{
		Space: kirv.HalfLife(0.7).Decay(),
	})(0)

	pos, tan = posTan(0)(1.1)
	got1, got2 = maf.Vec{X: pos.X, Y: pos.Y}, maf.Vec{X: tan.X, Y: tan.Y}
	want1, want2 = param.PosTan(1.1)
	if !maf.CompareVec(got1, want1, 2) || !maf.CompareVec(got2, want2, 2) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
			want1, got1, want2, got2)
	}
}
