package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// NewCircle constructs a new parametric circle curve with the given radius.
//
// The curve parameter is measured in units of radians.
func NewCircle(r float64) Parametric {
	return NewParametric(func(t float64) maf.Vec {
		s, c := math.Sincos(t)
		return maf.Vec{X: r * c, Y: r * s}
	}, ParSpec{
		Tangent: func(t float64) maf.Vec {
			s, c := math.Sincos(t)
			return maf.Vec{X: -s, Y: c}
		},
		Normal: func(t float64) maf.Vec {
			s, c := math.Sincos(t)
			return maf.Vec{X: -c, Y: -s}
		},
		Radius: func(t float64) float64 {
			return r
		},
		PosTan: func(t float64) (maf.Vec, maf.Vec) {
			s, c := math.Sincos(t)
			return maf.Vec{X: r * c, Y: r * s}, maf.Vec{X: -s, Y: c}
		},
		NormRad: func(t float64) (maf.Vec, float64) {
			s, c := math.Sincos(t)
			return maf.Vec{X: -c, Y: -s}, r
		},
		PosTanNormRad: func(t float64) (maf.Vec, maf.Vec, maf.Vec, float64) {
			s, c := math.Sincos(t)
			return maf.Vec{X: r * c, Y: r * s}, maf.Vec{X: -s, Y: c},
				maf.Vec{X: -c, Y: -s}, r
		},
	})
}

// NewCirclePosTan function finds the position and unit tangent vector for the
// circle curve with the given radius.
//
// The curve parameter is measured in units of radians.
func NewCirclePosTan(r float64) func(t float64) (maf.Vec2, maf.Vec2) {

	return func(t float64) (maf.Vec2, maf.Vec2) {
		s, c := math.Sincos(t)
		return maf.Vec2{X: r * c, Y: r * s}, maf.Vec2{X: -s, Y: c}
	}
}
