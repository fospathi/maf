package kirv_test

import (
	"math"
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/tezd"
)

func TestArchimedean(t *testing.T) {
	spec1 := kirv.Archimedean{A: 1.0, B: 2.0}
	spec2 := kinem.Archimedean{
		D: spec1.A,
		V: 1.0,
		W: 1.0 / spec1.B,
	}
	// Two equivalent spirals.
	arch1 := kirv.NewArchimedean(spec1)
	arch2 := kirv.NewTimeDepArchimedean(spec2)

	for _, T1 := range []float64{
		0 + 0.0001,
		maf.QuartPi,
		maf.HalfPi,
		math.Pi,
		maf.TwoPi,
		maf.TwoPi + maf.HalfPi,
	} {
		T2 := T1 / spec2.W // w = angle / time ==> time = angle / w

		want1 := arch1.Position(T1)
		want2 := arch1.Tangent(T1)
		want3 := arch1.Normal(T1)
		got1 := arch2.Position(T2)
		got2 := arch2.Tangent(T2)
		got3 := arch2.Normal(T2)
		if !maf.CompareVec(want1, got1, 2) || !maf.CompareVec(want2, got2, 2) ||
			!maf.CompareVec(want3, got3, 2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
				want1, got1, want2, got2, want3, got3)
		}
	}

	for _, T1 := range []float64{
		0 + 0.0001,
		maf.QuartPi,
		maf.HalfPi,
		math.Pi,
		maf.TwoPi,
		maf.TwoPi + maf.HalfPi,
	} {
		want1, want2, want3, want4 := arch1.Position(T1), arch1.Tangent(T1),
			arch1.Normal(T1), arch1.Radius(T1)
		got1, got2, got3, got4 := arch1.PosTanNormRad(T1)
		if !maf.CompareVec(want1, got1, 2) || !maf.CompareVec(want2, got2, 2) ||
			!maf.CompareVec(want3, got3, 2) || !tezd.CompareFloat64(want4, got4, 2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\nWant4:\n%#v\nGot4:\n%#v\n",
				want1, got1, want2, got2, want3, got3, want4, got4)
		}
		got1, got2 = arch1.PosTan(T1)
		got3, got4 = arch1.NormRad(T1)
		if !maf.CompareVec(want1, got1, 2) || !maf.CompareVec(want2, got2, 2) ||
			!maf.CompareVec(want3, got3, 2) || !tezd.CompareFloat64(want4, got4, 2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\nWant4:\n%#v\nGot4:\n%#v\n",
				want1, got1, want2, got2, want3, got3, want4, got4)
		}

		f := kirv.NewArchimedeanPosTan(spec1)
		{
			want1, want2 := maf.Vec2{X: want1.X, Y: want1.Y}, maf.Vec2{X: want2.X, Y: want2.Y}
			got1, got2 := f(T1)
			if !maf.CompareVec2(want1, got1, 2) || !maf.CompareVec2(want2, got2, 2) {
				t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
					want1, got1, want2, got2)
			}
		}
	}

	for _, T1 := range []float64{
		0 + 0.0001,
		maf.QuartPi,
		maf.HalfPi,
		math.Pi,
		maf.TwoPi,
		maf.TwoPi + maf.HalfPi,
	} {
		T1 /= spec2.W
		want1, want2, want3, want4 := arch2.Position(T1), arch2.Tangent(T1),
			arch2.Normal(T1), arch2.Radius(T1)
		got1, got2, got3, got4 := arch2.PosTanNormRad(T1)
		if !maf.CompareVec(want1, got1, 2) || !maf.CompareVec(want2, got2, 2) ||
			!maf.CompareVec(want3, got3, 2) || !tezd.CompareFloat64(want4, got4, 2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\nWant4:\n%#v\nGot4:\n%#v\n",
				want1, got1, want2, got2, want3, got3, want4, got4)
		}
		got1, got2 = arch2.PosTan(T1)
		got3, got4 = arch2.NormRad(T1)
		if !maf.CompareVec(want1, got1, 2) || !maf.CompareVec(want2, got2, 2) ||
			!maf.CompareVec(want3, got3, 2) || !tezd.CompareFloat64(want4, got4, 2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n"+
				"Want3:\n%#v\nGot3:\n%#v\nWant4:\n%#v\nGot4:\n%#v\n",
				want1, got1, want2, got2, want3, got3, want4, got4)
		}

		f := kirv.NewTimeDepArchimedeanPosTan(spec2)
		{
			want1, want2 := maf.Vec2{X: want1.X, Y: want1.Y}, maf.Vec2{X: want2.X, Y: want2.Y}
			got1, got2 := f(T1)
			if !maf.CompareVec2(want1, got1, 2) || !maf.CompareVec2(want2, got2, 2) {
				t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
			}
		}
	}
}
