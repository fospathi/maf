package kirv_test

import (
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
	"gitlab.com/fospathi/maf/kirv"
	"gitlab.com/fospathi/universal/tezd"
)

func TestTimeDepProjectile_Normal(t *testing.T) {
	spec := kinem.Projectile{
		A: -9.80665,
		U: maf.Vec{X: 10, Y: 10, Z: 0},
	}
	proj := kirv.NewTimeDepProjectile(spec)

	projectileNormal := func(spec kinem.Projectile, t float64) maf.Vec {
		// One formula for the principal unit normal vector of a curve is
		//
		//
		//    ^     a⊥       where a⊥ = the component of the acceleration which
		//    n = ------                 changes the direction of the velocity.
		//    -   | a⊥ |
		//
		//
		// r(t) = xt i  +  (yt + att/2) j
		//
		// v(t) =  x i  +      (y + at) j
		//
		// a(t) =                     a j
		x, y := spec.U.X, spec.U.Y
		v := maf.Vec{X: x, Y: y + spec.A*t}
		a := maf.Vec{Y: spec.A}
		aPar := a.Resolution(v)
		aPerp := a.Subtract(aPar)
		return aPerp.Unit()
	}

	for _, T := range []float64{
		-10, -5, 0, 1, 5, 10,
	} {

		want := proj.Normal(T)
		got := projectileNormal(spec, T)
		if !maf.CompareVec(want, got, 2) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}
}

func TestProjectile_Radius(t *testing.T) {
	spec := kinem.Projectile{
		A: -9.80665,
		U: maf.Vec{X: 10, Y: 10, Z: 0},
	}
	proj := kirv.NewTimeDepProjectile(spec)

	projectileCurvature := func(spec kinem.Projectile, t float64) float64 {
		// One formula for the curvature of a curve is
		//
		//
		//        | v(t) x a(t) |
		//    κ = ---------------
		//           | v(t) |³
		//
		//
		// r(t) = xt i  +  (yt + att/2) j
		//
		// v(t) =  x i  +      (y + at) j
		//
		// a(t) =                     a j
		x, y := spec.U.X, spec.U.Y
		v := maf.Vec{X: x, Y: y + spec.A*t}
		vMag := v.Mag()
		a := maf.Vec{Y: spec.A}
		return v.Cross(a).Mag() / (vMag * vMag * vMag)
	}

	for _, T := range []float64{
		-10, -5, 0, 5, 10,
	} {

		want := proj.Radius(T)
		got := 1 / projectileCurvature(spec, T)
		if !tezd.CompareFloat64(want, got, 5) {
			t.Errorf("\nT:\n%#v\nWant:\n%#v\nGot:\n%#v\n", T, want, got)
		}
	}
}
