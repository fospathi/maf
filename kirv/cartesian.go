package kirv

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// NewXYCartesian constructs a new XYCartesian.
//
// The first, and always necessary, argument is the ordinate function which
// defines the curve and maps the abscissa (X-coordinate) to the corresponding
// ordinate (Y-coordinate).
func NewXYCartesian(ord func(float64) float64, spec XYCarSpec) XYCartesian {
	car := XYCartesian{
		ord:           ord,
		grad:          spec.Gradient,
		conc:          spec.Concavity,
		ordGradConc:   spec.OrdGradConc,
		rad:           spec.Radius,
		posTanNormRad: spec.PosTanNormRad,
	}

	if car.grad == nil {
		car.grad = func(x float64) float64 {
			return NumericalGradient(car, x)
		}
	}

	if car.conc == nil {
		if spec.Gradient != nil {
			car.conc = func(x float64) float64 {
				return NumericalConc(car, x)
			}
		} else {
			car.conc = func(x float64) float64 {
				return NumericalConcavity(car, x)
			}
		}
	}

	if car.ordGradConc == nil {
		car.ordGradConc = func(x float64) (float64, float64, float64) {
			return NumericalOrdGradConc(car, x)
		}
	}

	if car.rad == nil {
		car.rad = func(x float64) float64 {
			return XYCartesianNumericalRadius(car, x)
		}
	}

	if car.posTanNormRad == nil {
		car.posTanNormRad = func(x float64) (
			maf.Vec2, maf.Vec2, maf.Vec2, float64) {

			return XYCartesianNumericalPosTanNormRad(car, x)
		}
	}

	return car
}

// XYCarSpec specifies some extra function(s) that define a Cartesian curve in
// the XY-plane.
//
// Optional functions should be specified if possible and they take precedence
// over approximate numerical methods which are used as a last resort.
//
// Note that the combination function may use different methods than the
// dedicated functions and hence may return somewhat different values.
type XYCarSpec struct {
	// Optional. Defines the gradient (first derivative) of the curve.
	Gradient func(x float64) float64
	// Optional. Defines the concavity (second derivative) of the curve.
	Concavity func(x float64) float64
	// Optional. Defines the ordinate, gradient, and concavity of the curve. May
	// be more efficient than calling ordinate, gradient, and concavity
	// separately.
	OrdGradConc func(x float64) (float64, float64, float64)
	// Optional. Defines the radius of curvature of the curve.
	Radius func(x float64) float64
	// PosTanNormRad is the position, unit tangent direction vector, unit
	// principal normal vector, and radius of curvature of the curve at the
	// given curve parameter.
	//
	// May be more efficient than calling position, tangent, normal, and radius
	// separately.
	PosTanNormRad func(x float64) (maf.Vec2, maf.Vec2, maf.Vec2, float64)
}

// An XYCartesian is a Cartesian curve in the XY plane.
//
// Note that the combination methods which compute multiple curve properties at
// once may use different methods than the dedicated methods and hence may
// return somewhat different values.
type XYCartesian struct {
	ord           func(float64) float64
	grad          func(float64) float64
	conc          func(float64) float64
	ordGradConc   func(float64) (float64, float64, float64)
	rad           func(float64) float64
	posTanNormRad func(float64) (maf.Vec2, maf.Vec2, maf.Vec2, float64)
}

// Ordinate is the Y-coordinate of the curve at the given abscissa.
func (car XYCartesian) Ordinate(x float64) float64 {
	return car.ord(x)
}

// Gradient is the value of the first derivative of the curve at the given
// abscissa.
func (car XYCartesian) Gradient(x float64) float64 {
	return car.grad(x)
}

// Concavity is the value of the second derivative of the curve at the given
// abscissa.
func (car XYCartesian) Concavity(x float64) float64 {
	return car.conc(x)
}

// OrdGradConc is the Y-coordinate, gradient, and concavity of the curve at the
// given abscissa.
func (car XYCartesian) OrdGradConc(x float64) (float64, float64, float64) {
	return car.ordGradConc(x)
}

// Position of the curve at the given abscissa.
func (car XYCartesian) Position(x float64) maf.Vec2 {
	return maf.Vec2{X: x, Y: car.ord(x)}
}

// Tangent unit direction vector of the curve at the given abscissa.
func (car XYCartesian) Tangent(x float64) maf.Vec2 {
	g := car.grad(x)
	if math.IsInf(g, 0) {
		if math.IsInf(g, 1) {
			return maf.Vec2{X: 0, Y: 1}
		}
		return maf.Vec2{X: 0, Y: -1}
	}
	return maf.Vec2{X: 1, Y: g}.Unit()
}

// Normal is the unit principal normal vector of the curve at the given
// abscissa.
func (car XYCartesian) Normal(x float64) maf.Vec2 {
	t, c := car.Tangent(x), car.conc(x)
	var (
		n3 maf.Vec
		t3 = maf.Vec{X: t.X, Y: t.Y}
	)
	if c > 0 {
		n3 = maf.PosZ.Cross(t3)
	} else {
		n3 = maf.NegZ.Cross(t3)
	}
	return maf.Vec2{X: n3.X, Y: n3.Y}
}

// Radius of curvature of the curve at the given abscissa.
func (car XYCartesian) Radius(x float64) float64 {
	// Note that conceptually the curvature (and so to the radius of curvature)
	// != concavity.
	return car.rad(x)
}

// An Ordinater is a Cartesian curve with a known ordinate at a given abscissa.
type Ordinater interface {
	Ordinate(x float64) float64
}

// NumericalGradient is an approximation of the given curve's gradient at the
// given abscissa.
func NumericalGradient(ord Ordinater, x float64) float64 {
	f := ord.Ordinate

	// An approximation for the first derivative is
	//
	//   f'(x) ~= f(x+h) - f(x-h)
	//            ---------------
	//                   2h
	return (f(x+H) - f(x-H)) / (2 * H)
}

// NumericalConcavity is an approximation of the given curve's concavity at the
// given abscissa.
//
// At the parameter value the curve shall not be an inflection point. Nor shall
// the curve be linear.
func NumericalConcavity(ord Ordinater, x float64) float64 {
	f := ord.Ordinate
	h := SqrtH // Use a larger h to prevent a too small h*h denominator.

	// https://en.wikipedia.org/wiki/Finite_difference#Higher-order_differences
	//
	// An approximation for the second derivative is
	//
	//   f''(x) ~= f(x+h) + f(x-h) - 2f(x)
	//             -----------------------
	//                       h*h
	return (f(x+h) + f(x-h) - 2*f(x)) / (h * h)
}

// A Gradienter is a Cartesian curve with a known gradient at a given abscissa.
type Gradienter interface {
	Gradient(x float64) float64
}

// NumericalConc is a rough approximation of the given curve's concavity at the
// given abscissa.
//
// At the parameter value the curve shall not be an inflection point. Nor shall
// the curve be linear.
func NumericalConc(car Gradienter, x float64) float64 {
	f := car.Gradient

	return (f(x+H) - f(x-H)) / (2 * H)
}

// NumericalOrdGradConc is the given curve's ordinate and an approximation of
// its gradient and concavity at the given abscissa.
//
// At the parameter value the curve shall not be an inflection point. Nor shall
// the curve be linear.
func NumericalOrdGradConc(ord Ordinater, x float64) (
	float64, float64, float64) {

	f := ord.Ordinate
	y := f(x)
	return y,
		(f(x+H) - f(x-H)) / (2 * H),
		(f(x+SqrtH) + f(x-SqrtH) - 2*y) / H
}

// XYCartesianNumericalRadius is
func XYCartesianNumericalRadius(ord Ordinater, x float64) float64 {

	// AB is a taken as a circle chord. The direction of AC is taken as the
	// approximate direction of an equal length chord on the same circle. The
	// angle atwixt the two chords is 2θ.
	//
	// From the cosine rule for triangles:
	//  r² = r² + d² - 2rd cos(θ)
	//   r = d/(2*cos(θ))
	//
	//
	//                         O
	//                         |
	//                         | r
	//        B '              |              ' C
	//             '           |           '
	//                '     θ  |  θ     '
	//              d    '     |     '
	//                      '  |  '
	//                         '
	//                         A
	//
	f := ord.Ordinate
	h := H
	lower := func(x, h float64) maf.Vec2 {
		return maf.Vec2{X: x - h, Y: f(x - h)}
	}
	upper := func(x, h float64) maf.Vec2 {
		return maf.Vec2{X: x + h, Y: f(x + h)}
	}

	switch { // Avoid undefined regions.
	case math.IsNaN(f(x + h)):
		x -= h
	case math.IsNaN(f(x - h)):
		x += h
	}

	a := maf.Vec2{X: x, Y: f(x)}
	b := lower(x, h)
	const maxIters = 10
	// If necessary bring down the Y-span for better accuracy.
	for i := 0; math.Abs(a.Y-b.Y) > h && i < maxIters; i++ {
		h /= 2
		b = lower(x, h)
	}
	c := upper(x, h)

	chord := a.To(b)
	θ, d := chord.Angle(a.To(c))/2, chord.Mag()
	return d / (2 * math.Cos(θ))
}

// XYCartesianNumericalPosTanNormRad is the given curve's position and a rough
// numerical approximation of its unit tangent, unit principal normal, and
// radius of curvature at the given abscissa.
//
// At the abscissa the curve shall not be an inflection point. Nor shall the
// curve be linear.
func XYCartesianNumericalPosTanNormRad(ord Ordinater, x float64) (
	maf.Vec2, maf.Vec2, maf.Vec2, float64) {

	f := ord.Ordinate
	var (
		a = maf.Vec{X: x, Y: f(x)}
		b = maf.Vec{X: x - H, Y: f(x - H)}
		c = maf.Vec{X: x + H, Y: f(x + H)}
	)
	osculating := maf.Triangle{A: a, B: b, C: c}.Circumcircle()
	radius := a.To(osculating.C)
	r, R := radius.Unit(), radius.Mag()
	t := radius.Cross(maf.PosZ).Unit()
	if t.X < 0 {
		t = t.Scale(-1)
	}
	return maf.Vec2{X: a.X, Y: a.Y},
		maf.Vec2{X: t.X, Y: t.Y},
		maf.Vec2{X: r.X, Y: r.Y},
		R
}
