package maf

import (
	"math"
	"testing"

	"gitlab.com/fospathi/universal/tezd"
)

func TestExpSin_At(t *testing.T) {
	es := ExpSin{2, 3}

	want := 0.0
	got := es.At(math.Pi)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 1.04274
	got = es.At(1)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestExpSin_D(t *testing.T) {
	want := -770.20762
	got := ExpSin{2, 3}.D(1)(3)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = -0.00560
	got = ExpSin{-2, 3}.D(1)(math.Pi)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = -2
	got = ExpSin{-3, -2}.D(1)(0)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 0.17725
	got = ExpSin{3, -2}.D(1)(-1)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = -5242.21650
	got = ExpSin{2, 3}.D(2)(3)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = -0.02199
	got = ExpSin{-2, -3}.D(2)(3)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = -0.02666
	got = ExpSin{-2, -3}.D(3)(3)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 0.39258
	got = ExpSin{-2, -3}.D(4)(3)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 677.86755
	got = ExpSin{-2, -3}.D(10)(3)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
