package maf

import (
	"math"
)

// PlaneIntersections finds the intersection(s) of the receiver Bezier curve
// with the argument plane.
//
// Returns the values of the parameter t at the intersection points in the range
// 0..1 inclusive. The int return value is the number of intersections in the
// parameter range 0..1 inclusive. In the case of no intersections in the
// parameter range 0..1 both float return values are NaN. In the case of a
// single intersection in the parameter range 0..1 the second float return value
// is NaN.
func (b1 Bez) PlaneIntersections(p Plane) (int, float64, float64) {
	// https://gist.github.com/fospathi/971891cb0b8005710d160de0229df0d4
	a := p.Normal.Dot(b1.A.Add(b1.B.Scale(-2)).Add(b1.C))
	b := 2 * p.Normal.Dot(b1.B.Subtract(b1.A))
	c := p.Normal.Dot(b1.A.Subtract(p.P))
	const epsA float64 = 1e-8 // An arbitrary small value
	if a < epsA && a > -epsA {
		// The quadratic equation collapses to bx = -c when a == 0
		if root := -c / b; root >= 0 && root <= 1 {
			return 1, root, nan
		}
		return 0, nan, nan
	}
	discriminant := b*b - 4*a*c
	if discriminant < 0 {
		return 0, nan, nan
	}
	discriminantSqrt := math.Sqrt(discriminant)
	divisor := 1 / (a + a)
	root1 := (-b - discriminantSqrt) * divisor
	root2 := (-b + discriminantSqrt) * divisor
	if root1 >= 0 && root1 <= 1 {
		if root2 >= 0 && root2 <= 1 {
			if root1 < root2 {
				return 2, root1, root2
			}
			return 2, root2, root1
		}
		return 1, root1, nan
	}
	if root2 >= 0 && root2 <= 1 {
		return 1, root2, nan
	}
	return 0, nan, nan
}

// PlaneIntersections finds the intersection(s) of the receiver circle with the
// argument plane. The int return value is the number of intersections, which
// may be 0, 1, or 2.
func (c1 Circle) PlaneIntersections(p Plane) (int, Vec, Vec) {
	h := p.DistTo(c1.C)
	if h < c1.R {
		if a := c1.Normal.AcuteAngle(p.Normal); a > SmallAngle {
			if hR := c1.R * math.Sin(a); hR > h {
				y := p.Normal.Cross(c1.Normal).Unit()
				x := y.Cross(c1.Normal).Unit()
				if x.Dot(c1.C.To(p.P)) < 0 {
					x = x.Scale(-1)
				}
				l := (h * c1.R) / hR // From similar triangle ratios.
				midpoint := c1.C.Add(x.Scale(l))
				yLen := math.Sqrt(c1.R*c1.R - l*l)
				return 2, midpoint.Add(y.Scale(yLen)), midpoint.Add(y.Scale(-yLen))
			}
		}
	} else if h == c1.R {
		if p.SignedDistTo(c1.C) < 0 {
			return 1, c1.C.Add(p.Normal.Scale(h)), Vec{}
		}
		return 1, c1.C.Add(p.Normal.Scale(-h)), Vec{}
	}
	return 0, Vec{}, Vec{}
}

// PlaneIntersection returns the intersection point of the receiver line and
// argument plane.
//
// The argument plane shall not be parallel to the receiver line.
func (l1 Line) PlaneIntersection(p Plane) Vec {
	p1, p2 := l1.P, l1.P.Add(l1.Dir)
	d1, d2 := p.SignedDistTo(p1), p.SignedDistTo(p2)
	//
	//   ____________I (Intersection)
	//   |     |    /
	//   |   d2|  /                                             I
	// d1|     |/                                              /
	//   |    /P2                                            /
	//   |  /                                              /
	//   |/                  ======>            _________/
	//   P1                                     |      / P2
	//                                  (d1-d2) |    /
	//                                          |  /  1  (unit vector) == (P1->P2)
	//                                          |/
	//                                          P1
	//
	// Let
	//     Δd = k * Δl  (l is length)
	// then
	//  d1-d2 = k * 1
	//      k = d1-d2
	// thus
	//     Δd = (d1-d2) * Δl
	// so for the line segment (P2, I) with length Δl
	//     d2 = (d1-d2) * Δl
	//     Δl = d2 / (d1-d2)
	// and
	//      I = P2 + (d2 / (d1-d2)) * (P1->P2)
	switch {
	case d1 == 0:
		return p1
	case d2 == 0:
		return p2
	case d1 < 0 && d2 < 0:
		d1 *= -1
		d2 *= -1
		fallthrough
	case d1 > 0 && d2 > 0:
		if d1 > d2 {
			return p2.Add(l1.Dir.Scale(d2 / (d1 - d2)))
		}
		return p1.Add(l1.Dir.Scale(-d1 / (d2 - d1)))
	default:
		if d1 < 0 {
			d1 *= -1
		}
		if d2 < 0 {
			d2 *= -1
		}
		return p1.DivTo(p2, d1, d2)
	}
}

// PlaneIntersection returns the intersection line of the receiver plane and
// argument plane.
//
// The argument plane shall not be parallel to the receiver plane.
func (p1 Plane) PlaneIntersection(p2 Plane) Line {
	dir := p1.Normal.Cross(p2.Normal).Unit()
	perp := Line{Dir: p1.Normal.Cross(dir).Unit(), P: p1.P}
	return Line{Dir: dir, P: perp.PlaneIntersection(p2)}
}

// PlaneIntersection returns, if it exists, the single intersection point of the
// receiver ray and argument plane.
//
// If there is no single intersection point the returned bool value is false.
func (r1 Ray) PlaneIntersection(p Plane) (Vec, bool) {
	if r1.Dir.IsPerpendicular(p.Normal, SmallAngle) {
		return Vec{}, false
	}
	i := Line{P: r1.O, Dir: r1.Dir}.PlaneIntersection(p)
	if r1.O.To(i).Dot(r1.Dir) < 0 {
		return Vec{}, false
	}
	return i, true
}

// InteriorsIntersect returns whether the argument sphere's interior contains
// any points from the receiver sphere's interior or vice versa.
//
// Here, spheres that only touch at a single point on their surfaces are not
// considered as intersecting.
func (s1 Sphere) InteriorsIntersect(s2 Sphere) bool {
	return s1.C.DistTo(s2.C) < (s1.R + s2.R)
}

// InteriorsIntersect returns whether the argument and receiver line segments
// are non parallel and intersect at a single point, not including their end
// points.
func (ls1 LineSeg2) InteriorsIntersect(ls2 LineSeg2) bool {
	// Solve for t1 and t2 in
	// LS1 = LS1P1 + t1(LS1P2 - LS1P1)
	// LS2 = LS2P1 + t2(LS2P2 - LS2P1)
	// where LS1 = LS2, thus
	// 0 = t1(LS1P2 - LS1P1) - t2(LS2P2 - LS2P1) + (LS1P1 - LS2P1)
	//   = t1(LS1P2 - LS1P1) + t2(LS2P1 - LS2P2) + (LS1P1 - LS2P1)
	a1 := ls1.P2.X - ls1.P1.X
	b1 := ls2.P1.X - ls2.P2.X
	c1 := ls1.P1.X - ls2.P1.X
	a2 := ls1.P2.Y - ls1.P1.Y
	b2 := ls2.P1.Y - ls2.P2.Y
	c2 := ls1.P1.Y - ls2.P1.Y
	t1, t2, ok := SolveSLE2(a1, b1, c1, a2, b2, c2)
	return ok && t1 > 0 && t1 < 1 && t2 > 0 && t2 < 1
}

// InteriorsIntersectAny returns whether the receiver line segment is non
// parallel to and intersects at a single point any of the argument line
// segments, not including their end points.
func (ls1 LineSeg2) InteriorsIntersectAny(lineSegs ...LineSeg2) bool {
	for _, ls2 := range lineSegs {
		a1 := ls1.P2.X - ls1.P1.X
		b1 := ls2.P1.X - ls2.P2.X
		c1 := ls1.P1.X - ls2.P1.X
		a2 := ls1.P2.Y - ls1.P1.Y
		b2 := ls2.P1.Y - ls2.P2.Y
		c2 := ls1.P1.Y - ls2.P1.Y
		t1, t2, ok := SolveSLE2(a1, b1, c1, a2, b2, c2)
		if ok && t1 > 0 && t1 < 1 && t2 > 0 && t2 < 1 {
			return true
		}
	}
	return false
}

// ClosureIncludes returns whether the argument point is included in the set of
// points that make up the receiver triangle's interior or its edges and
// vertices.
func (tr1 Triangle2) ClosureIncludes(P Vec2) bool {
	rA := tr1.AltitudeRatioA(P)
	if rA < 0 || rA > 1 {
		return false
	}
	rB := tr1.AltitudeRatioB(P)
	if rB < 0 || rB > 1 {
		return false
	}
	rC := tr1.AltitudeRatioC(P)
	if rC < 0 || rC > 1 {
		return false
	}
	return true
}

// InteriorIncludes returns whether the argument point is included in the set of
// points that make up the receiver triangle's interior.
//
// Here, points that coincide with the edges or vertices are not considered as
// included.
func (tr1 Triangle2) InteriorIncludes(P Vec2) bool {
	rA := tr1.AltitudeRatioA(P)
	if rA <= 0 || rA >= 1 {
		return false
	}
	rB := tr1.AltitudeRatioB(P)
	if rB <= 0 || rB >= 1 {
		return false
	}
	rC := tr1.AltitudeRatioC(P)
	if rC <= 0 || rC >= 1 {
		return false
	}
	return true
}

// ClosureIncludesAny returns whether any of the argument points are included in
// the set of points that make up the receiver triangle's interior or its edges
// and vertices.
func (tr1 Triangle2) ClosureIncludesAny(points ...Vec2) bool {
	for _, P := range points {
		rA := tr1.AltitudeRatioA(P)
		if rA < 0 || rA > 1 {
			continue
		}
		rB := tr1.AltitudeRatioB(P)
		if rB < 0 || rB > 1 {
			continue
		}
		rC := tr1.AltitudeRatioC(P)
		if rC < 0 || rC > 1 {
			continue
		}
		return true
	}
	return false
}

// InteriorIncludesAny returns whether any of the argument points are included
// in the set of points that make up the receiver triangle's interior.
//
// Here, points that coincide with the edges or vertices are not considered as
// included.
func (tr1 Triangle2) InteriorIncludesAny(points ...Vec2) bool {
	for _, P := range points {
		rA := tr1.AltitudeRatioA(P)
		if rA <= 0 || rA >= 1 {
			continue
		}
		rB := tr1.AltitudeRatioB(P)
		if rB <= 0 || rB >= 1 {
			continue
		}
		rC := tr1.AltitudeRatioC(P)
		if rC <= 0 || rC >= 1 {
			continue
		}
		return true
	}
	return false
}

// ClosureIncludesAll returns whether any of the argument points are included in
// the set of points that make up the receiver triangle's interior or its edges
// and vertices.
func (tr1 Triangle2) ClosureIncludesAll(points ...Vec2) bool {
	for _, P := range points {
		rA := tr1.AltitudeRatioA(P)
		if rA < 0 || rA > 1 {
			return false
		}
		rB := tr1.AltitudeRatioB(P)
		if rB < 0 || rB > 1 {
			return false
		}
		rC := tr1.AltitudeRatioC(P)
		if rC < 0 || rC > 1 {
			return false
		}
	}
	return true
}

// InteriorsIntersect returns whether the argument triangle's interior contains
// any points from the receiver triangle's interior or vice versa.
//
// Here, triangles that only touch on their edges are not considered as
// overlapping.
func (tr1 Triangle2) InteriorsIntersect(tr2 Triangle2) bool {
	AB1 := LineSeg2{P1: tr1.A, P2: tr1.B}
	BC1 := LineSeg2{P1: tr1.B, P2: tr1.C}
	CA1 := LineSeg2{P1: tr1.C, P2: tr1.A}
	AB2 := LineSeg2{P1: tr2.A, P2: tr2.B}
	BC2 := LineSeg2{P1: tr2.B, P2: tr2.C}
	CA2 := LineSeg2{P1: tr2.C, P2: tr2.A}
	if AB2.InteriorsIntersectAny(AB1, BC1, CA1) ||
		BC2.InteriorsIntersectAny(AB1, BC1, CA1) ||
		CA2.InteriorsIntersectAny(AB1, BC1, CA1) {
		return true
	}
	if tr1.InteriorIncludesAny(tr2.A, tr2.B, tr2.C) ||
		tr2.InteriorIncludesAny(tr1.A, tr1.B, tr1.C) ||
		// Check for special cases such as an identical overlapper and wholly
		// contained triangles whose vertices all touch the containing
		// triangle's edges.
		tr1.ClosureIncludesAll(tr2.A, tr2.B, tr2.C) ||
		tr2.ClosureIncludesAll(tr1.A, tr1.B, tr1.C) {
		return true
	}
	return false
}

// InteriorsIntersectLineSeg returns whether the argument line segment shares
// any of its interior points with the receiver triangles interior.
//
// Here, exterior line segments that only touch the edges or vertices of a
// triangle are not considered as overlapping.
func (tr1 Triangle2) InteriorsIntersectLineSeg(ls LineSeg2) bool {
	return ls.InteriorsIntersectAny(
		LineSeg2{P1: tr1.A, P2: tr1.B},
		LineSeg2{P1: tr1.B, P2: tr1.C},
		LineSeg2{P1: tr1.C, P2: tr1.A}) ||
		tr1.InteriorIncludesAny(ls.P1, ls.P2) ||
		(tr1.ClosureIncludesAll(ls.P1, ls.P2) && tr1.InteriorIncludes((ls.P1.MidTo(ls.P2))))
}
