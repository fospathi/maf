package maf

// Basis is a three-dimensional Cartesian coordinate system whose axis
// directions are defined by three orthogonal unit vectors. The origin of the
// basis coincides with the origin of the implied reference basis.
type Basis struct {
	X, Y, Z Vec
}

// Frame is a floating three-dimensional Cartesian coordinate system defined by
// a basis and an origin.
type Frame struct {
	// The origin of the frame.
	O Vec
	// The axis directions of the frame.
	Axes Basis
}

// In returns a transformation matrix that transforms a vector's coordinates in
// the implied reference basis to the vector's coordinates in the receiver
// basis.
func (b1 Basis) In() Mat3 {
	// https://gist.github.com/fospathi/fd512bebff118df5d2c02055e43d936d
	return Mat3{b1.X.X, b1.Y.X, b1.Z.X, b1.X.Y, b1.Y.Y, b1.Z.Y, b1.X.Z, b1.Y.Z, b1.Z.Z}
}

// In returns a transformation matrix that transforms a vector's coordinates in
// the implied reference frame to the vector's coordinates in the receiver
// frame.
func (f1 Frame) In() Aff {
	// A translation is applied to the point and frame such that the frame
	// origin coincides with the implied reference frame origin. Note that the
	// point is not moved relative to the frame. Then the usual method of
	// converting a point's coordinates in the implied reference basis to the
	// frame's basis is applied.
	//
	//     | b.X.X  b.X.Y  b.X.Z  0 |   | 1  0  0  -f.O.X |
	//     | b.Y.X  b.Y.Y  b.Y.Z  0 | x | 0  1  0  -f.O.Y | = ...
	//     | b.Y.X  b.Z.Y  b.Z.Z  0 |   | 0  0  1  -f.O.Z |
	//     | 0      0      0      1 |   | 0  0  0  1      |
	return Aff{
		f1.Axes.X.X, f1.Axes.Y.X, f1.Axes.Z.X,
		f1.Axes.X.Y, f1.Axes.Y.Y, f1.Axes.Z.Y,
		f1.Axes.X.Z, f1.Axes.Y.Z, f1.Axes.Z.Z,
		-f1.O.X*f1.Axes.X.X - f1.O.Y*f1.Axes.X.Y - f1.O.Z*f1.Axes.X.Z,
		-f1.O.X*f1.Axes.Y.X - f1.O.Y*f1.Axes.Y.Y - f1.O.Z*f1.Axes.Y.Z,
		-f1.O.X*f1.Axes.Z.X - f1.O.Y*f1.Axes.Z.Y - f1.O.Z*f1.Axes.Z.Z}
}

// Out returns a transformation matrix that transforms a vector's coordinates in
// the receiver basis to the vector's coordinates in the implied reference
// basis.
func (b1 Basis) Out() Mat3 {
	// https://gist.github.com/fospathi/fd512bebff118df5d2c02055e43d936d
	return Mat3{b1.X.X, b1.X.Y, b1.X.Z, b1.Y.X, b1.Y.Y, b1.Y.Z, b1.Z.X, b1.Z.Y, b1.Z.Z}
}

// Out returns a transformation matrix that transforms a vector's coordinates in
// the receiver frame to the vector's coordinates in the implied reference
// frame.
func (f1 Frame) Out() (affine Aff) {
	return Aff{
		f1.Axes.X.X, f1.Axes.X.Y, f1.Axes.X.Z,
		f1.Axes.Y.X, f1.Axes.Y.Y, f1.Axes.Y.Z,
		f1.Axes.Z.X, f1.Axes.Z.Y, f1.Axes.Z.Z,
		f1.O.X, f1.O.Y, f1.O.Z}
}

// InFrom returns a transformation matrix that transforms a vector's coordinates
// in the argument basis to the vector's coordinates in the receiver basis.
func (b1 Basis) InFrom(b Basis) Mat3 {
	in := Mat3{b1.X.X, b1.Y.X, b1.Z.X, b1.X.Y, b1.Y.Y, b1.Z.Y, b1.X.Z, b1.Y.Z, b1.Z.Z}
	out := Mat3{b.X.X, b.X.Y, b.X.Z, b.Y.X, b.Y.Y, b.Y.Z, b.Z.X, b.Z.Y, b.Z.Z}
	return in.Multiply(out)
}

// InFrom returns a transformation matrix that transforms a vector's coordinates
// in the argument frame to the vector's coordinates in the receiver frame.
func (f1 Frame) InFrom(f Frame) Aff {
	in := Aff{
		f1.Axes.X.X, f1.Axes.Y.X, f1.Axes.Z.X,
		f1.Axes.X.Y, f1.Axes.Y.Y, f1.Axes.Z.Y,
		f1.Axes.X.Z, f1.Axes.Y.Z, f1.Axes.Z.Z,
		-f1.O.X*f1.Axes.X.X - f1.O.Y*f1.Axes.X.Y - f1.O.Z*f1.Axes.X.Z,
		-f1.O.X*f1.Axes.Y.X - f1.O.Y*f1.Axes.Y.Y - f1.O.Z*f1.Axes.Y.Z,
		-f1.O.X*f1.Axes.Z.X - f1.O.Y*f1.Axes.Z.Y - f1.O.Z*f1.Axes.Z.Z}
	out := Aff{
		f.Axes.X.X, f.Axes.X.Y, f.Axes.X.Z,
		f.Axes.Y.X, f.Axes.Y.Y, f.Axes.Y.Z,
		f.Axes.Z.X, f.Axes.Z.Y, f.Axes.Z.Z,
		f.O.X, f.O.Y, f.O.Z}
	return in.Multiply(out)
}

// OutTo returns a transformation matrix that transforms a vector's coordinates
// in the receiver basis to the vector's coordinates in the argument basis.
func (b1 Basis) OutTo(b Basis) Mat3 {
	in := Mat3{b.X.X, b.Y.X, b.Z.X, b.X.Y, b.Y.Y, b.Z.Y, b.X.Z, b.Y.Z, b.Z.Z}
	out := Mat3{b1.X.X, b1.X.Y, b1.X.Z, b1.Y.X, b1.Y.Y, b1.Y.Z, b1.Z.X, b1.Z.Y, b1.Z.Z}
	return in.Multiply(out)
}

// OutTo returns a transformation matrix that transforms a vector's coordinates
// in the receiver frame to the vector's coordinates in the argument frame.
func (f1 Frame) OutTo(f Frame) Aff {
	in := Aff{
		f.Axes.X.X, f.Axes.Y.X, f.Axes.Z.X,
		f.Axes.X.Y, f.Axes.Y.Y, f.Axes.Z.Y,
		f.Axes.X.Z, f.Axes.Y.Z, f.Axes.Z.Z,
		-f.O.X*f.Axes.X.X - f.O.Y*f.Axes.X.Y - f.O.Z*f.Axes.X.Z,
		-f.O.X*f.Axes.Y.X - f.O.Y*f.Axes.Y.Y - f.O.Z*f.Axes.Y.Z,
		-f.O.X*f.Axes.Z.X - f.O.Y*f.Axes.Z.Y - f.O.Z*f.Axes.Z.Z}
	out := Aff{
		f1.Axes.X.X, f1.Axes.X.Y, f1.Axes.X.Z,
		f1.Axes.Y.X, f1.Axes.Y.Y, f1.Axes.Y.Z,
		f1.Axes.Z.X, f1.Axes.Z.Y, f1.Axes.Z.Z,
		f1.O.X, f1.O.Y, f1.O.Z}
	return in.Multiply(out)
}

// PlotXY interprets the given position vector as local coordinates on the
// receiver frame's XY plane and returns the position's equivalent in
// coordinates of the frame's implied reference frame.
func (f1 Frame) PlotXY(p Vec2) Vec {
	return Vec{
		X: f1.O.X + (f1.Axes.X.X * p.X) + f1.Axes.Y.X*p.Y,
		Y: f1.O.Y + (f1.Axes.X.Y * p.X) + f1.Axes.Y.Y*p.Y,
		Z: f1.O.Z + (f1.Axes.X.Z * p.X) + f1.Axes.Y.Z*p.Y,
	}
}

// View returns a frame with the same origin as the receiver frame but
// reorientated so that its negative Z-axis points at the argument position
// vector and its Y-axis points, where possible, upwards.
//
// The argument position vector shall be given in implied reference frame
// coordinates and it shan't coincide with the receiver frame's origin.
//
// Special cases:
//  p is vertical wrt O: returned frame's Y-axis = standard frame's Z-axis.
func (f1 Frame) View(p Vec) Frame {
	z := f1.O.To(p).Unit().Opposite()
	var y Vec
	if z.AcuteAngle(PosY) > SmallAngle {
		x := PosY.Cross(z).Unit()
		y = z.Cross(x)
	} else {
		y = PosZ
	}
	f2 := Frame{O: f1.O}
	f2.Axes.X, f2.Axes.Y, f2.Axes.Z = y.Cross(z), y, z
	return f2
}

// NewFrame returns a new frame.
//
// If no arguments are provided the returned frame is a standard frame. If
// arguments are provided up to four arguments are used and interpreted as
// follows:
//  1st: origin
//  2nd: X-axis unit vector
//  3rd: Y-axis unit vector
//  4th: Z-axis unit vector
//
// If only three arguments are provided, the fourth argument is calculated as
// the cross product of the second and third arguments in that order.
func NewFrame(parts ...Vec) Frame {
	f := StdFrame
	switch len(parts) {
	case 1:
		f.O = parts[0]
	case 2:
		f.O = parts[0]
		f.Axes.X = parts[1]
	case 3:
		f.O = parts[0]
		f.Axes.X = parts[1]
		f.Axes.Y = parts[2]
		f.Axes.Z = parts[1].Cross(parts[2])
	case 4:
		f.O = parts[0]
		f.Axes.X = parts[1]
		f.Axes.Y = parts[2]
		f.Axes.Z = parts[3]
	}
	return f
}

// Local returns a new Basis which is the result of applying, to the receiver
// basis, the argument transformation expressed in the coordinates of the
// receiver basis.
func (b1 Basis) Local(a Aff) Basis {
	x := a.Transform(PosX)
	y := a.Transform(PosY)
	z := a.Transform(PosZ)
	out := b1.Out()
	return Basis{out.Transform(x), out.Transform(y), out.Transform(z)}
}

// Global returns a new Frame which is the result of applying, to the receiver
// frame, the argument transformation expressed in the coordinates of the
// implied reference frame.
func (f1 Frame) Global(a Aff) Frame {
	o := a.Transform(f1.O)
	x := a.Transform(f1.O.Add(f1.Axes.X))
	y := a.Transform(f1.O.Add(f1.Axes.Y))
	z := a.Transform(f1.O.Add(f1.Axes.Z))
	return Frame{O: o, Axes: Basis{o.To(x), o.To(y), o.To(z)}}
}

// GlobalDis returns a new Frame which results from applying, to the receiver
// frame, the argument global displacement expressed in the coordinates of the
// implied reference frame.
func (f1 Frame) GlobalDis(v Vec) Frame {
	f1.O = f1.O.Add(v)
	return f1
}

// Local returns a new Frame which is the result of applying, to the receiver
// frame, the argument transformation expressed in the coordinates of the
// receiver frame.
func (f1 Frame) Local(a Aff) Frame {
	out := f1.Out()
	o := out.Transform(a.Transform(Zero))
	x := out.Transform(a.Transform(PosX))
	y := out.Transform(a.Transform(PosY))
	z := out.Transform(a.Transform(PosZ))
	return Frame{O: o, Axes: Basis{o.To(x), o.To(y), o.To(z)}}
}

// LocalRot returns a new Frame which results from applying, to the receiver
// frame, a rotation about the argument line expressed in the coordinates of the
// receiver frame.
//
// The argument angle shall be measured in radians. Suppose the line's direction
// vector is pointing out of the screen towards you, then a positive rotation
// angle indicates an anticlockwise rotation.
func (f1 Frame) LocalRot(l Line, angle float64) Frame {
	return f1.Local(NewRot(l, angle))
}

// LocalRotBasis returns a new Frame which results from applying a local
// rotation around the argument direction vector by the argument angle.
//
// Like the argument direction vector shall be, the rotation which is applied to
// the axes/basis of the receiver frame is expressed in the coordinates of the
// receiver frame.
//
// The argument angle shall be measured in radians. Suppose the direction vector
// is pointing out of the screen towards you, then a positive rotation angle
// indicates an anticlockwise rotation.
func (f1 Frame) LocalRotBasis(v Vec, angle float64) Frame {
	f1.Axes = f1.Axes.Local(NewORot(v, angle))
	return f1
}

// LocalRotNegX returns a new Frame which results from applying a local rotation
// of the receiver frame's negative X-axis by the argument angle.
//
// The argument angle shall be measured in radians. Suppose the negative X-axis
// is pointing out of the screen towards you, then a positive rotation angle
// indicates an anticlockwise rotation.
func (f1 Frame) LocalRotNegX(angle float64) Frame {
	return f1.Local(NewXRot(-angle))
}

// LocalRotNegY returns a new Frame which results from applying a local rotation
// of the receiver frame's negative Y-axis by the argument angle.
//
// The argument angle shall be measured in radians. Suppose the negative Y-axis
// is pointing out of the screen towards you, then a positive rotation angle
// indicates an anticlockwise rotation.
func (f1 Frame) LocalRotNegY(angle float64) Frame {
	return f1.Local(NewYRot(-angle))
}

// LocalRotNegZ returns a new Frame which results from applying a local rotation
// of the receiver frame's negative Z-axis by the argument angle.
//
// The argument angle shall be measured in radians. Suppose the negative Z-axis
// is pointing out of the screen towards you, then a positive rotation angle
// indicates an anticlockwise rotation.
func (f1 Frame) LocalRotNegZ(angle float64) Frame {
	return f1.Local(NewZRot(-angle))
}

// LocalRotPosX returns a new Frame which results from applying a local rotation
// of the receiver frame's positive X-axis by the argument angle.
//
// The argument angle shall be measured in radians. Suppose the positive X-axis
// is pointing out of the screen towards you, then a positive rotation angle
// indicates an anticlockwise rotation.
func (f1 Frame) LocalRotPosX(angle float64) Frame {
	return f1.Local(NewXRot(angle))
}

// LocalRotPosY returns a new Frame which results from applying a local rotation
// of the receiver frame's positive Y-axis by the argument angle.
//
// The argument angle shall be measured in radians. Suppose the positive Y-axis
// is pointing out of the screen towards you, then a positive rotation angle
// indicates an anticlockwise rotation.
func (f1 Frame) LocalRotPosY(angle float64) Frame {
	return f1.Local(NewYRot(angle))
}

// LocalRotPosZ returns a new Frame which results from applying a local rotation
// of the receiver frame's positive Z-axis by the argument angle.
//
// The argument angle shall be measured in radians. Suppose the positive Z-axis
// is pointing out of the screen towards you, then a positive rotation angle
// indicates an anticlockwise rotation.
func (f1 Frame) LocalRotPosZ(angle float64) Frame {
	return f1.Local(NewZRot(angle))
}

// LocalDis returns a new Frame which results from applying, to the receiver
// frame, the argument local displacement expressed in the coordinates of the
// receiver frame.
func (f1 Frame) LocalDis(v Vec) Frame {
	f1.O = f1.Out().Transform(v)
	return f1
}

// LocalDisNegX returns a new Frame which results from applying, to the receiver
// frame, a local displacement along its negative X-axis by the argument
// distance.
func (f1 Frame) LocalDisNegX(d float64) Frame {
	f1.O = f1.Out().Transform(NegX.Scale(d))
	return f1
}

// LocalDisNegY returns a new Frame which results from applying, to the receiver
// frame, a local displacement along its negative Y-axis by the argument
// distance.
func (f1 Frame) LocalDisNegY(d float64) Frame {
	f1.O = f1.Out().Transform(NegY.Scale(d))
	return f1
}

// LocalDisNegZ returns a new Frame which results from applying, to the receiver
// frame, a local displacement along its negative Z-axis by the argument
// distance.
func (f1 Frame) LocalDisNegZ(d float64) Frame {
	f1.O = f1.Out().Transform(NegZ.Scale(d))
	return f1
}

// LocalDisPosX returns a new Frame which results from applying, to the receiver
// frame, a local displacement along its positive X-axis by the argument
// distance.
func (f1 Frame) LocalDisPosX(d float64) Frame {
	f1.O = f1.Out().Transform(PosX.Scale(d))
	return f1
}

// LocalDisPosY returns a new Frame which results from applying, to the receiver
// frame, a local displacement along its positive Y-axis by the argument
// distance.
func (f1 Frame) LocalDisPosY(d float64) Frame {
	f1.O = f1.Out().Transform(PosY.Scale(d))
	return f1
}

// LocalDisPosZ returns a new Frame which results from applying, to the receiver
// frame, a local displacement along its positive Z-axis by the argument
// distance.
func (f1 Frame) LocalDisPosZ(d float64) Frame {
	f1.O = f1.Out().Transform(PosZ.Scale(d))
	return f1
}

// LocalDisNegXNegZ returns a new Frame which results from applying, to the
// receiver frame, a local displacement, by the argument distance, along the
// direction which is the sum of its negative X-axis and negative Z-axis
// directions.
func (f1 Frame) LocalDisNegXNegZ(d float64) Frame {
	f1.O = f1.Out().Transform(NegXNegZ.Scale(d))
	return f1
}

// LocalDisNegXPosZ returns a new Frame which results from applying, to the
// receiver frame, a local displacement, by the argument distance, along the
// direction which is the sum of its negative X-axis and positive Z-axis
// directions.
func (f1 Frame) LocalDisNegXPosZ(d float64) Frame {
	f1.O = f1.Out().Transform(NegXPosZ.Scale(d))
	return f1
}

// LocalDisPosXNegZ returns a new Frame which results from applying, to the
// receiver frame, a local displacement, by the argument distance, along the
// direction which is the sum of its positive X-axis and negative Z-axis
// directions.
func (f1 Frame) LocalDisPosXNegZ(d float64) Frame {
	f1.O = f1.Out().Transform(PosXNegZ.Scale(d))
	return f1
}

// LocalDisPosXPosZ returns a new Frame which results from applying, to the
// receiver frame, a local displacement, by the argument distance, along the
// direction which is the sum of its positive X-axis and positive Z-axis
// directions.
func (f1 Frame) LocalDisPosXPosZ(d float64) Frame {
	f1.O = f1.Out().Transform(PosXPosZ.Scale(d))
	return f1
}
