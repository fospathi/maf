package maf

import (
	"cmp"
	"math"
	"slices"

	"gitlab.com/fospathi/universal/interval"
)

// Circle is a circle in three-dimensional space defined by its centre, radius
// and orientation.
type Circle struct {
	// The position vector of the circle centre.
	C Vec
	// The radius of the circle.
	R float64
	// The unit vector in the direction of the normal to the circle's intrinsic
	// plane.
	Normal Vec
}

// Line is a line in three-dimensional space defined by any point on the line
// and the direction of the line.
type Line struct {
	// Any position vector on the line.
	P Vec
	// The unit vector in the direction of the line.
	Dir Vec
}

// LineSeg is a line segment in three-dimensional space.
type LineSeg struct {
	// The end points of the line segment.
	P1, P2 Vec
}

// Plane is a two-dimensional plane in three-dimensional space defined by any
// position vector on the plane and a normal vector to the plane.
type Plane struct {
	// Any position vector on the plane.
	P Vec
	// The unit vector in the direction of the plane's normal.
	Normal Vec
}

// A Ray is an infinitely long half line that begins at a point called its
// origin.
type Ray struct {
	// The origin of the ray.
	O Vec
	// A unit vector in the same direction as a translation vector from the
	// origin to any other point on the ray.
	Dir Vec
}

// Sphere is a sphere in three-dimensional space defined by its centre and
// radius.
type Sphere struct {
	// The position vector of the sphere centre.
	C Vec
	// The radius of the sphere.
	R float64
}

// Triangle is a triangle in three-dimensional space defined by three vertices.
type Triangle struct {
	// The triangle's vertices.
	A, B, C Vec
}

// RandPoints anywhere on the surface of the circle.
func (c1 Circle) RandPoints(n int) []Vec {
	vp := make([]Vec, n)
	X := c1.Normal.Perpendicular()
	Y := NewORot(c1.Normal, HalfPi).Transform(X)
	for i := 0; i < n; i++ {
		s, c := math.Sincos(rdm.Float64() * TwoPi)
		r := rdm.Float64() * c1.R
		vp[i] = c1.C.Add(X.Scale(r * c).Add(Y.Scale(r * s)))
	}
	return vp
}

// TangentPositions returns the position vectors where the two possible tangent
// lines from the argument point to the receiver circle touch the circle.
//
// The argument point shall be in the same plane as the receiver circle and be
// outside of the circle.
func (c1 Circle) TangentPositions(p Vec) (Vec, Vec) {
	//            tan1            Triangle (p,tan1, C) has a right angle at tan1
	//             |\             and lengths R and PC are known
	//             | \ R          this gives us angle (p, C, tan1)
	//           h |  \
	//             |   \
	//    P-------mid---C
	//             |   /
	//             |  /
	//             | /
	//             |/
	//			  tan2
	pc := p.To(c1.C)
	pcUnit := pc.Unit()
	PC := pc.Mag()
	theta := math.Acos(c1.R / PC)
	midC := c1.R * math.Cos(theta)
	mid := p.Add(pcUnit.Scale(PC - midC))
	h := c1.Normal.Cross(pcUnit).Scale(math.Sqrt(c1.R*c1.R - midC*midC))
	return mid.Add(h), mid.Add(h.Scale(-1))
}

// Closest point on the receiver line to the argument point.
func (l1 Line) Closest(p Vec) Vec {
	P1P := l1.P.To(p)
	p1p := P1P.Dot(l1.Dir)
	return l1.P.Add(l1.Dir.Scale(p1p))
}

// ClosestPoints returns the closest points of approach between the receiver
// line and the argument line.
//
// The first return value is a point on the receiver line. The second return
// value is a point on the argument line. The returned bool value is false if
// there is no unique solution.
func (l1 Line) ClosestPoints(l2 Line) (Vec, Vec, bool) {
	// https://gist.github.com/fospathi/27facb61439d3f5f144dc992066f1bdc
	p := l1.P.To(l2.P)
	a1 := l1.Dir.Dot(l1.Dir)
	b1 := -l1.Dir.Dot(l2.Dir)
	c1 := -p.Dot(l1.Dir)
	a2 := b1
	b2 := l2.Dir.Dot(l2.Dir)
	c2 := p.Dot(l2.Dir)
	a, b, ok := SolveSLE2(a1, b1, c1, a2, b2, c2)
	if !ok {
		return Vec{}, Vec{}, false
	}
	return l1.P.Add(l1.Dir.Scale(a)), l2.P.Add(l2.Dir.Scale(b)), true
}

// ClosestPoint returns the closest point on the receiver line to the argument
// line. This function assumes that a unique solution definitely exists.
func (l1 Line) ClosestPoint(l2 Line) Vec {
	// https://gist.github.com/fospathi/27facb61439d3f5f144dc992066f1bdc
	p := l1.P.To(l2.P)
	a1 := l1.Dir.Dot(l1.Dir)
	b1 := -l1.Dir.Dot(l2.Dir)
	c1 := -p.Dot(l1.Dir)
	a2 := b1
	b2 := l2.Dir.Dot(l2.Dir)
	c2 := p.Dot(l2.Dir)
	a, _, _ := SolveSLE2(a1, b1, c1, a2, b2, c2)
	return l1.P.Add(l1.Dir.Scale(a))
}

// DistTo returns the magnitude of the orthogonal distance of the argument
// position vector to the receiver line.
func (l1 Line) DistTo(p Vec) float64 {
	return p.DistTo(l1.P.Add(l1.Dir.Scale(l1.P.To(p).Dot(l1.Dir))))
}

// IsParallel returns whether the positive angle between the receiver and
// argument lines directions is either zero or Pi to within the given positive
// tolerance argument, measured in radians.
func (l1 Line) IsParallel(l2 Line, tolerance float64) bool {
	a := l1.Dir.Angle(l2.Dir)
	return a <= tolerance || a >= math.Pi-tolerance
}

// AtT returns the position vector on the receiver line segment at the argument
// proportion of the distance from P1 to P2.
func (l1 LineSeg) AtT(t float64) Vec {
	switch t {
	case 0:
		return l1.P1
	case 1:
		return l1.P2
	default:
		return l1.P1.Add(l1.P1.To(l1.P2).Scale(t))
	}
}

// Closest returns the point on the receiver line segment that is closest to the
// argument point.
func (l1 LineSeg) Closest(p Vec) Vec {
	l := l1.P1.DistTo(l1.P2)
	dir := l1.P1.To(l1.P2).Unit()
	P1P := l1.P1.To(p)
	p1p := P1P.Dot(dir)
	switch {
	case p1p > l:
		return l1.P2
	case p1p < 0:
		return l1.P1
	default:
		return l1.P1.Add(dir.Scale(p1p))
	}
}

// Midpoint of the line segment.
func (l1 LineSeg) Midpoint() Vec {
	return l1.P1.MidTo(l1.P2)
}

// Closest returns the closest point on the receiver plane to the argument
// point.
func (p1 Plane) Closest(p Vec) Vec {
	d := p1.SignedDistTo(p)
	if d == 0 {
		return p
	}
	return p.Add(p1.Normal.Scale(-d))
}

// DistTo returns the magnitude of the orthogonal distance of the argument
// position vector to the receiver plane.
func (p1 Plane) DistTo(p Vec) float64 {
	d := p1.P.To(p).Dot(p1.Normal)
	if d < 0 {
		return -d
	}
	return d
}

// IsParallel is true if the argument direction vector is perpendicular to the
// normal of the argument plane, to within the argument positive tolerance angle
// measured in radians.
func (p1 Plane) IsParallel(v Vec, tolerance float64) bool {
	return p1.Normal.IsPerpendicular(v, tolerance)
}

// SignedDistTo returns a signed value whose magnitude is the orthogonal
// distance of the argument position vector to the receiver plane.
//
// The returned value is > 0 if the position vector is on that side of the plane
// that the normal points into.
func (p1 Plane) SignedDistTo(p Vec) float64 {
	return p1.P.To(p).Dot(p1.Normal)
}

// Circle returns the circle containing the set of points on the receiver
// sphere's surface that coincide with the argument plane.
//
// If the argument plane does not generate a circle then the returned bool value
// is false and the returned circle empty.
func (s1 Sphere) Circle(plane Plane) Circle {
	d := plane.DistTo(s1.C)
	return Circle{
		C:      plane.Closest(s1.C),
		Normal: plane.Normal,
		R:      math.Sqrt(s1.R*s1.R - d*d),
	}
}

// BisectorAC returns the position vector of the point on the receiver
// triangle's segment AC such that a line through vertex B and it bisects angle
// B.
func (t1 Triangle) BisectorAC() Vec {
	return t1.DivideAC(t1.B.To(t1.A).Mag(), t1.B.To(t1.C).Mag())
}

// Centroid returns the arithmetic mean of the receiver triangle's vertices.
func (t1 Triangle) Centroid() Vec {
	return Vec{X: (t1.A.X + t1.B.X + t1.C.X) / 3,
		Y: (t1.A.Y + t1.B.Y + t1.C.Y) / 3,
		Z: (t1.A.Z + t1.B.Z + t1.C.Z) / 3}
}

// Circumcircle returns the unique circle that circumscribes the receiver
// triangle's vertices.
func (t1 Triangle) Circumcircle() Circle {
	ab := t1.A.To(t1.B)
	ac := t1.A.To(t1.C)
	norm := ab.Cross(ac).Unit()
	c := Line{
		P:   t1.A.MidTo(t1.B),
		Dir: ab.Cross(norm).Unit(),
	}.ClosestPoint(
		Line{
			P:   t1.A.MidTo(t1.C),
			Dir: ac.Cross(norm).Unit(),
		})
	return Circle{C: c, Normal: norm, R: t1.A.To(c).Mag()}
}

// Coincide reports whether the triangles share the same vertices.
func (t1 Triangle) Coincide(t2 Triangle) bool {
	tI := [3]Vec{t1.A, t1.B, t1.C}
	tJ := [3]Vec{t2.A, t2.B, t2.C}
loop:
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if tI[i] == tJ[j] {
				continue loop
			}
		}
		return false
	}
	return true
}

// DivideAC returns the position vector P on the receiver triangle's segment AC
// such that the ratio of magnitude AP to magnitude PC equals the ratio of
// argument1 to argument2.
//
// |AP|/|PC| = s/t
func (t1 Triangle) DivideAC(s, t float64) Vec {
	return t1.A.Add(t1.C.Subtract(t1.A).Scale(s / (s + t)))
}

// EnclosingSphere returns a sphere that the receiver triangle's vertices are
// not outside of.
func (t1 Triangle) EnclosingSphere() Sphere {
	O := t1.A.MidTo(t1.C)
	OA := O.To(t1.A)
	OB := O.To(t1.B)
	OADotOA := OA.MagSqd()
	OBDotOB := OB.MagSqd()
	var modSqd float64
	if OADotOA > OBDotOB {
		modSqd = OADotOA
	} else {
		modSqd = OBDotOB
	}
	return Sphere{C: O, R: math.Sqrt(modSqd)}
}

// Plane returns a new plane which coincides with the receiver triangle's
// intrinsic plane.
func (t1 Triangle) Plane() Plane {
	ab := t1.A.To(t1.B)
	ac := t1.A.To(t1.C)
	return Plane{P: t1.A, Normal: ab.Cross(ac).Unit()}
}

// Scale the triangle about its centroid.
func (t1 Triangle) Scale(k float64) Triangle {
	cen := t1.Centroid()
	return Triangle{
		A: cen.Add(cen.To(t1.A).Scale(k)),
		B: cen.Add(cen.To(t1.B).Scale(k)),
		C: cen.Add(cen.To(t1.C).Scale(k)),
	}
}

// Annulus2 is an annulus which is a circular ring shaped region enclosed by two
// concentric circles.
type Annulus2 struct {
	// The position vector of the circles' centre.
	C Vec2
	// The radius of the inner circle.
	R1 float64
	// The radius of the outer circle.
	R2 float64
}

// Bez2 is a 2D quadratic Bezier curve.
type Bez2 Triangle2

// Circle2 is a circle in 2D space defined by its centre and radius.
type Circle2 struct {
	// The position vector of the circle centre.
	C Vec2
	// The radius of the circle.
	R float64
}

// Line2 is a line in 2D space defined by any point on the line and the
// direction of the line.
type Line2 struct {
	// Any position vector on the line.
	P Vec2
	// The unit vector in the direction of the line.
	Dir Vec2
}

// LineSeg2 is a line segment in 2D space defined by two points.
type LineSeg2 struct {
	P1, P2 Vec2
}

// Triangle2 is a triangle in 2D space defined by three vertices.
type Triangle2 struct {
	// The triangle's vertices.
	A, B, C Vec2
}

// LineIntersection returns the non-zero length segments, if any, of the given
// line which overlap the receiver annulus.
//
// Return the number of line segments which can be 0, 1, or 2.
func (a1 Annulus2) LineIntersection(l Line2) ([2]LineSeg2, int) {
	outer, ok := Circle2{C: a1.C, R: a1.R2}.LineIntersections(l)
	if !ok || outer[0] == outer[1] {
		return [2]LineSeg2{}, 0
	}

	inner, ok := Circle2{C: a1.C, R: a1.R1}.LineIntersections(l)
	if !ok || inner[0] == inner[1] {
		return [2]LineSeg2{{P1: outer[0], P2: outer[1]}}, 1
	}

	return [2]LineSeg2{
		{P1: outer[0], P2: inner[0]},
		{P1: inner[1], P2: outer[1]},
	}, 2
}

// LineSegIntersection returns the non-zero length segments, if any, of the
// given line segment which overlap the receiver annulus.
//
// Return the number of line segments which can be 0, 1, or 2.
func (a1 Annulus2) LineSegIntersection(seg LineSeg2) ([2]LineSeg2, int) {

	removeZeroLen := func(vs [2]LineSeg2, n int) ([2]LineSeg2, int) {
		switch n {
		case 1:
			if vs[0].Len() == 0 {
				return [2]LineSeg2{}, 0
			}
		case 2:
			l1, l2 := vs[0].Len(), vs[1].Len()
			switch {
			case l1 == 0 && l2 == 0:
				return [2]LineSeg2{}, 0
			case l1 == 0:
				return [2]LineSeg2{vs[1]}, 1
			case l2 == 0:
				return [2]LineSeg2{vs[0]}, 1
			}
		}
		return vs, n
	}

	vs, n := a1.LineIntersection(seg.Line())
	unit := interval.In{0, 1}

	switch n {

	case 1:
		in, ok := interval.In{
			seg.T(vs[0].P1), seg.T(vs[0].P2),
		}.AsOpenIntersection(unit)
		if !ok {
			return [2]LineSeg2{}, 0
		}
		return removeZeroLen(
			[2]LineSeg2{{P1: seg.AtT(in[0]), P2: seg.AtT(in[1])}}, 1)

	case 2:
		in1, ok1 := interval.In{
			seg.T(vs[0].P1), seg.T(vs[0].P2),
		}.AsOpenIntersection(unit)
		in2, ok2 := interval.In{
			seg.T(vs[1].P1), seg.T(vs[1].P2),
		}.AsOpenIntersection(unit)
		switch {
		case ok1 && ok2:
			return removeZeroLen([2]LineSeg2{
				{P1: seg.AtT(in1[0]), P2: seg.AtT(in1[1])},
				{P1: seg.AtT(in2[0]), P2: seg.AtT(in2[1])},
			}, 2)
		case ok1:
			return removeZeroLen(
				[2]LineSeg2{{P1: seg.AtT(in1[0]), P2: seg.AtT(in1[1])}}, 1)
		default:
			return removeZeroLen(
				[2]LineSeg2{{P1: seg.AtT(in2[0]), P2: seg.AtT(in2[1])}}, 1)
		}

	default:
		return [2]LineSeg2{}, 0
	}
}

// AtT returns the position vector on the receiver Bezier curve at the argument
// parameter. The argument parameter is valid in the range 0..1 inclusive.
func (b1 Bez2) AtT(t float64) Vec2 {
	coef0 := (1 - t) * (1 - t)
	coef1 := 2 * (1 - t) * t
	coef2 := t * t
	return Vec2{
		X: coef0*b1.A.X + coef1*b1.B.X + coef2*b1.C.X,
		Y: coef0*b1.A.Y + coef1*b1.B.Y + coef2*b1.C.Y,
	}
}

// LineIntersections with the circle.
func (c1 Circle2) LineIntersections(l Line2) ([2]Vec2, bool) {
	// Perpendicular to the line segment and passes through the circle centre.
	perp := Line2{P: c1.C, Dir: Vec2{X: -l.Dir.Y, Y: l.Dir.X}}
	p := l.ClosestPoint(perp)
	cp := c1.C.To(p)
	d := cp.Mag()
	switch {
	case d == c1.R:
		return [2]Vec2{p, p}, true
	case d < c1.R:
		y := math.Sqrt(c1.R*c1.R - d*d)
		return [2]Vec2{p.Add(l.Dir.Scale(-y)), p.Add(l.Dir.Scale(y))}, true
	}
	return [2]Vec2{}, false
}

// RandPoints anywhere on the surface of the circle.
func (c1 Circle2) RandPoints(n int) []Vec2 {
	vp := make([]Vec2, n)
	for i := 0; i < n; i++ {
		a, r := rdm.Float64()*TwoPi, rdm.Float64()*c1.R
		s, c := math.Sincos(a)
		vp[i] = c1.C.Add(Vec2{X: r * c, Y: r * s})
	}
	return vp
}

// Closest returns the closest points of approach between the receiver line and
// the argument line.
//
// The first return value is a point on the receiver line. The second return
// value is a point on the argument line. The returned bool value is false if
// there is no unique solution.
func (l1 Line2) Closest(l2 Line2) (Vec2, Vec2, bool) {
	// https://gist.github.com/fospathi/27facb61439d3f5f144dc992066f1bdc
	p := l1.P.To(l2.P)
	a1 := l1.Dir.Dot(l1.Dir)
	b1 := -l1.Dir.Dot(l2.Dir)
	c1 := -p.Dot(l1.Dir)
	a2 := b1
	b2 := l2.Dir.Dot(l2.Dir)
	c2 := p.Dot(l2.Dir)
	a, b, ok := SolveSLE2(a1, b1, c1, a2, b2, c2)
	if !ok {
		return Vec2{}, Vec2{}, false
	}
	return l1.P.Add(l1.Dir.Scale(a)), l2.P.Add(l2.Dir.Scale(b)), true
}

// ClosestApproach on the line to the given point.
func (l1 Line2) ClosestApproach(p Vec2) Vec2 {
	perp := Line2{P: p, Dir: Vec2{X: -l1.Dir.Y, Y: l1.Dir.X}}
	return l1.ClosestPoint(perp)
}

// ClosestPoint returns the closest point on the receiver line to the argument
// line. This function assumes that a unique solution definitely exists.
func (l1 Line2) ClosestPoint(l2 Line2) Vec2 {
	// https://gist.github.com/fospathi/27facb61439d3f5f144dc992066f1bdc
	p := l1.P.To(l2.P)
	a1 := l1.Dir.Dot(l1.Dir)
	b1 := -l1.Dir.Dot(l2.Dir)
	c1 := -p.Dot(l1.Dir)
	a2 := b1
	b2 := l2.Dir.Dot(l2.Dir)
	c2 := p.Dot(l2.Dir)
	a, _, _ := SolveSLE2(a1, b1, c1, a2, b2, c2)
	return l1.P.Add(l1.Dir.Scale(a))
}

// AtT maps a parameterisation of a point on the receiver segment's extended
// line back to a point on the line.
func (ls1 LineSeg2) AtT(t float64) Vec2 {
	return ls1.P1.Add(ls1.P1.To(ls1.P2).Scale(t))
}

// Canonical undirected form of the line segment.
func (ls1 LineSeg2) Canonical() LineSeg2 {
	switch {
	case ls1.P2.X < ls1.P1.X:
		return LineSeg2{P1: ls1.P2, P2: ls1.P1}
	case ls1.P2.X == ls1.P1.X:
		switch {
		case ls1.P2.Y < ls1.P1.Y:
			return LineSeg2{P1: ls1.P2, P2: ls1.P1}
		}
	}
	return ls1
}

// DistTo returns the shortest distance from the receiver line segment to the
// argument point.
func (ls1 LineSeg2) DistTo(P Vec2) float64 {
	d := ls1.P1.DistTo(ls1.P2)
	u := ls1.P1.To(ls1.P2).Unit()
	P1P := ls1.P1.To(P)
	P2P := ls1.P2.To(P)
	p1p := P1P.Dot(u)
	if p1p > d {
		return P2P.Mag()
	} else if p1p < 0 {
		return P1P.Mag()
	}
	return math.Sqrt(P1P.MagSqd() - p1p*p1p)
}

// Higher dimension triangle with the new dimension set to the given value.
func (ls1 LineSeg2) Higher(z float64) LineSeg {
	return LineSeg{P1: ls1.P1.Higher(z), P2: ls1.P2.Higher(z)}
}

// Len is the length of the line segment.
func (ls1 LineSeg2) Len() float64 {
	return ls1.P1.DistTo(ls1.P2)
}

// Line through the line segment.
func (ls1 LineSeg2) Line() Line2 {
	return Line2{P: ls1.P1, Dir: ls1.P1.To(ls1.P2).Unit()}
}

// PerpBis returns the perpendicular bisector of the line segment.
func (ls1 LineSeg2) PerpBis() Line2 {
	return Line2{P: ls1.P1.MidTo(ls1.P2), Dir: ls1.P1.To(ls1.P2).Normal()}
}

// Right reports whether the given point is to the right of the extended line
// through the line segment.
//
// It is the implied direction of the line segment that gives meaning to the
// concepts of left and right in this case.
func (ls1 LineSeg2) Right(p Vec2) bool {
	v1 := ls1.P1.To(p)
	v2 := ls1.P1.To(ls1.P2)
	return Vec{X: v1.X, Y: v1.Y, Z: 0}.Cross(Vec{X: v2.X, Y: v2.Y, Z: 0}).Z > 0
}

// T is a parameterisation of the position of the given point upon the extended
// line of the receiver line segment.
//
// For t = 0, the closest point is the segment's first point. For t = 1, then
// the closest point of approach is the segment's second point.
//
//	     t ~ -0.5       t ~ 0.5                 t ~ 2
//	       *              *                       *
//
//	- - - - - - - P1 - - - - - - P2 - - - - - - - -
//
//
//	              *              *
//	            t = 0          t = 1
func (ls1 LineSeg2) T(p Vec2) float64 {
	ln := ls1.Len()
	if ln == 0 {
		return 0
	}
	l := ls1.Line()
	a := l.ClosestApproach(p)
	t := ls1.P1.To(a).Mag() / ln
	if l.Dir.Dot(ls1.P1.To(p)) < 0 {
		return -t
	}
	return t
}

// Anticlockwise triangle with the same vertices.
func (t1 Triangle2) Anticlockwise() Triangle2 {
	if !t1.Winding() {
		t1.B, t1.C = t1.C, t1.B
	}
	return t1
}

// Canonical form of the triangle.
func (t1 Triangle2) Canonical() Triangle2 {
	vp := [3]Vec2{t1.A, t1.B, t1.C}
	x := Vec2{X: 1, Y: 0}
	slices.SortFunc(vp[:], func(a, b Vec2) int {
		if dA, dB := a.MagSqd(), b.MagSqd(); dA == dB {
			return cmp.Compare(x.Anticlockwise(a), x.Anticlockwise(b))
		} else {
			return cmp.Compare(dA, dB)
		}
	})
	return Triangle2{A: vp[0], B: vp[1], C: vp[2]}.Anticlockwise()
}

// Circumcircle returns the unique circle that circumscribes the receiver
// triangle's vertices.
func (t1 Triangle2) Circumcircle() Circle2 {
	perp1 := LineSeg2{t1.A, t1.B}.PerpBis()
	perp2 := LineSeg2{t1.A, t1.C}.PerpBis()
	o := perp1.ClosestPoint(perp2)
	return Circle2{C: o, R: o.DistTo(t1.A)}
}

// CircumcircleContains reports whether the given point is inside the bounds of
// the right handed triangle's circumcircle.
//
// The triangle's vertices shall be in anticlockwise order.
func (t1 Triangle2) CircumcircleContains(p Vec2) bool {
	// The determinant of this matrix determines when a point is within a
	// triangle's circumcircle.
	//
	// https://en.wikipedia.org/wiki/Delaunay_triangulation#Algorithms
	return Mat4{
		t1.A.X, t1.B.X, t1.C.X, p.X,
		t1.A.Y, t1.B.Y, t1.C.Y, p.Y,

		t1.A.X*t1.A.X + t1.A.Y*t1.A.Y,
		t1.B.X*t1.B.X + t1.B.Y*t1.B.Y,
		t1.C.X*t1.C.X + t1.C.Y*t1.C.Y,
		p.X*p.X + p.Y*p.Y,

		1, 1, 1, 1,
	}.Det() > 0
}

// CoincidesWith reports whether the triangles share the same vertices.
func (t1 Triangle2) CoincidesWith(t2 Triangle2) bool {
	tI := [3]Vec2{t1.A, t1.B, t1.C}
	tJ := [3]Vec2{t2.A, t2.B, t2.C}
loop:
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if tI[i] == tJ[j] {
				continue loop
			}
		}
		return false
	}
	return true
}

// Edges of the triangle.
func (t1 Triangle2) Edges() [3]LineSeg2 {
	return [3]LineSeg2{
		{P1: t1.A, P2: t1.B}, {P1: t1.B, P2: t1.C}, {P1: t1.C, P2: t1.A},
	}
}

// Higher dimension triangle with the new dimension set to the given value.
func (t1 Triangle2) Higher(z float64) Triangle {
	return Triangle{A: t1.A.Higher(z), B: t1.B.Higher(z), C: t1.C.Higher(z)}
}

// SharesAnyVertexWith reports whether the triangles share at least one vertex.
func (t1 Triangle2) SharesAnyVertexWith(t2 Triangle2) bool {
	tI := [3]Vec2{t1.A, t1.B, t1.C}
	tJ := [3]Vec2{t2.A, t2.B, t2.C}
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if tI[i] == tJ[j] {
				return true
			}
		}
	}
	return false
}

// Vertices of the triangle.
func (t1 Triangle2) Vertices() [3]Vec2 {
	return [3]Vec2{t1.A, t1.B, t1.C}
}

// Winding returns true if the receiver triangle's vertices are in anticlockwise
// order around the triangle's interior.
//
// The viewpoint for defining the anticlockwise direction is that of a viewpoint
// somewhere on the positive Z-axis of a right handed coordinate system looking
// down on the triangle in the z=0 plane.
func (tr1 Triangle2) Winding() bool {
	return Vec{tr1.B.X - tr1.A.X, tr1.B.Y - tr1.A.Y, 0}.Cross(
		Vec{tr1.C.X - tr1.A.X, tr1.C.Y - tr1.A.Y, 0}).Z > 0
}

// AltitudeRatioC returns the signed ratio that is a measure of the
// perpendicular distance of the argument point from the receiver triangle's C
// vertex as measured along the altitude line through the C vertex to the
// extended line AB.
//
// Using the altitude line through vertex C, which is perpendicular to AB, as an
// axis with C at the origin, then if the returned ratio is:
//
// * 0: the argument point is vertically above or below the vertex C
//
// * 1: the argument point is somewhere on the extended line AB
//
// * between 0 and 1: the argument point is somewhere within, above or below the
// triangle
//
// * negative, the argument point is to the left of the C vertex
//
// * greater than 1, the argument point is to the right of the triangle's base
// AB
func (tr1 Triangle2) AltitudeRatioC(P Vec2) float64 {
	A := Vec{tr1.A.X, tr1.A.Y, 0}
	B := Vec{tr1.B.X, tr1.B.Y, 0}
	altitudeParallel := Vec{X: 0, Y: 0, Z: 1}.Cross(A.To(B))
	altitudeDir := Vec2{altitudeParallel.X, altitudeParallel.Y}.Unit()
	if !tr1.Winding() {
		altitudeDir = altitudeDir.Scale(-1)
	}
	return tr1.C.To(P).Dot(altitudeDir) / tr1.C.To(tr1.B).Dot(altitudeDir)
}

// AltitudeRatioB returns the signed ratio that is a measure of the
// perpendicular distance of the argument point from the receiver triangle's B
// vertex as measured along the altitude line through the B vertex to the
// extended line CA.
func (tr1 Triangle2) AltitudeRatioB(P Vec2) float64 {
	C := Vec{tr1.C.X, tr1.C.Y, 0}
	A := Vec{tr1.A.X, tr1.A.Y, 0}
	altitudeParallel := Vec{X: 0, Y: 0, Z: 1}.Cross(C.To(A))
	altitudeDir := Vec2{altitudeParallel.X, altitudeParallel.Y}.Unit()
	if !tr1.Winding() {
		altitudeDir = altitudeDir.Scale(-1)
	}
	return tr1.B.To(P).Dot(altitudeDir) / tr1.B.To(tr1.A).Dot(altitudeDir)
}

// AltitudeRatioA returns the signed ratio that is a measure of the
// perpendicular distance of the argument point from the receiver triangle's A
// vertex as measured along the altitude line through the A vertex to the
// extended line BC.
func (tr1 Triangle2) AltitudeRatioA(P Vec2) float64 {
	B := Vec{tr1.B.X, tr1.B.Y, 0}
	C := Vec{tr1.C.X, tr1.C.Y, 0}
	altitudeParallel := Vec{X: 0, Y: 0, Z: 1}.Cross(B.To(C))
	altitudeDir := Vec2{altitudeParallel.X, altitudeParallel.Y}.Unit()
	if !tr1.Winding() {
		altitudeDir = altitudeDir.Scale(-1)
	}
	return tr1.A.To(P).Dot(altitudeDir) / tr1.A.To(tr1.C).Dot(altitudeDir)
}
