package maf

import (
	"testing"
)

func TestAbsLen(t *testing.T) {
	want := 8.0
	got := AbsLen(-4, 4)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = AbsLen(4, -4)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 4.0
	got = AbsLen(-8, -4)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 4.0
	got = AbsLen(-4, -8)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestMinMax(t *testing.T) {
	want1, want2 := 1.0, 3.0

	got1, got2 := MinMax(1, 2, 3)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	got1, got2 = MinMax(1, 3, 2)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	got1, got2 = MinMax(2, 1, 3)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	got1, got2 = MinMax(2, 3, 1)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	got1, got2 = MinMax(3, 1, 2)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	got1, got2 = MinMax(3, 2, 1)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = 0.0, 2.0
	got1, got2 = MinMax(0, 0, 2)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	got1, got2 = MinMax(0, 2, 0)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	got1, got2 = MinMax(2, 0, 0)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
}

func TestMinMaxInt(t *testing.T) {
	want1, want2 := 0, 2
	func() {
		got1, got2 := MinMaxInt(0, 0, 2)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
	}()
}
