package maf

import (
	"math"
	"testing"

	"gitlab.com/fospathi/universal/tezd"
)

func BenchmarkBez_PlaneIntersections(b *testing.B) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, 0}, C: Vec{2, 0, 0}}
	p := Plane{P: Vec{0, 0.4, 0}, Normal: Vec{0, 1, 0}}
	for n := 0; n < b.N; n++ {
		bez.PlaneIntersections(p)
	}
}

func BenchmarkCircle_PlaneIntersections(b *testing.B) {
	c1 := Circle{C: Vec{0, 0.5, 0}, R: 1, Normal: Vec{0, 1, -1}.Unit()}
	p := Plane{P: Vec{}, Normal: PosY}
	for n := 0; n < b.N; n++ {
		c1.PlaneIntersections(p)
	}
}

func BenchmarkSphere_Overlaps(b *testing.B) {
	s1 := Sphere{C: Vec{1, 1, 1}, R: 1}
	s2 := Sphere{C: Vec{2, 1, 1}, R: 1}
	for n := 0; n < b.N; n++ {
		s1.InteriorsIntersect(s2)
	}
}

func BenchmarkTriangle2_InteriorIncludes(b *testing.B) {
	tr1 := Triangle2{Vec2{0, 0}, Vec2{-2, 2}, Vec2{2, 2}}
	P1 := Vec2{1, 1}
	for n := 0; n < b.N; n++ {
		tr1.InteriorIncludes(P1)
	}
}

func BenchmarkTriangle2_InteriorsIntersect(b *testing.B) {
	tr1 := Triangle2{Vec2{0, 0}, Vec2{-2, 2}, Vec2{2, 2}}
	tr2 := Triangle2{Vec2{-1, 1}, Vec2{1, 1}, Vec2{0, 2}}
	tr3 := Triangle2{Vec2{0, 1}, Vec2{-0.5, 3}, Vec2{0.5, 3}}
	tr4 := Triangle2{Vec2{-5, 5}, Vec2{5, 5}, Vec2{0, 10}}
	for n := 0; n < b.N; n++ {
		tr1.InteriorsIntersect(tr2) // 800
		tr1.InteriorsIntersect(tr3) // 37
		tr1.InteriorsIntersect(tr4) // 530
	}
	// 1370
}

func TestBez_PlaneIntersections(t *testing.T) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, -0.5}, C: Vec{2, 0, -1}}

	func() {
		p := Plane{P: Vec{0, 0.5, 0}, Normal: Vec{0, 1, 0}}
		want1, want2 := 0.5, 0.5
		got0, got1, got2 := bez.PlaneIntersections(p)
		if got0 != 2 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 2, got0)
		}
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}

		p = Plane{P: Vec{0, 0.51, 0}, Normal: Vec{0, 1, 0}}
		want1, want2 = math.NaN(), math.NaN()
		got0, got1, got2 = bez.PlaneIntersections(p)
		if got0 != 0 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 0, got0)
		}
		if !math.IsNaN(got1) || !math.IsNaN(got2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}

		p = Plane{P: Vec{0, -0.1, 0}, Normal: Vec{0, 1, 0}}
		want1, want2 = math.NaN(), math.NaN()
		got0, got1, got2 = bez.PlaneIntersections(p)
		if got0 != 0 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 0, got0)
		}
		if !math.IsNaN(got1) || !math.IsNaN(got2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}

		p = Plane{P: Vec{0, 0, 0}, Normal: Vec{0, 1, 0}}
		want1, want2 = 0, 1
		got0, got1, got2 = bez.PlaneIntersections(p)
		if got0 != 2 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 2, got0)
		}
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}

		p = Plane{P: Vec{0, 0, 0}, Normal: Vec{0, -1, 0}}
		want1, want2 = 0, 1
		got0, got1, got2 = bez.PlaneIntersections(p)
		if got0 != 2 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 2, got0)
		}
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}

		p = Plane{P: Vec{0, 0, 0}, Normal: Vec{1, 0, 0}}
		want1, want2 = 0, math.NaN()
		got0, got1, got2 = bez.PlaneIntersections(p)
		if got0 != 1 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 1, got0)
		}
		if want1 != got1 || !math.IsNaN(got2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}

		p = Plane{P: Vec{1, 0, 0}, Normal: Vec{1, 0, 0}}
		want1, want2 = 0.5, math.NaN()
		got0, got1, got2 = bez.PlaneIntersections(p)
		if got0 != 1 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 1, got0)
		}
		if want1 != got1 || !math.IsNaN(got2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
	}()

	bez = Bez{A: Vec{0, 0, 0}, B: Vec{-1, 1, -0.5}, C: Vec{-2, 0, -1}}
	func() {
		p := Plane{P: Vec{-1, 0.5, -0.5}, Normal: Vec{1, 0.1, -1}}
		want1, want2 := 0.5, math.NaN()
		got0, got1, got2 := bez.PlaneIntersections(p)
		if got0 != 1 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 1, got0)
		}
		if !tezd.CompareFloat64(want1, got1, 5) || !math.IsNaN(got2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
	}()

	bez = Bez{A: Vec{0, 0, 0}, B: Vec{0.5, 1, 0.0}, C: Vec{1, 0, 0}}
	func() {
		p := Plane{P: Vec{0.5, -0.5, 0}, Normal: Vec{1, -1, 0}}
		want1, want2 := 1.0, math.NaN()
		got0, got1, got2 := bez.PlaneIntersections(p)
		if got0 != 1 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 1, got0)
		}
		if !tezd.CompareFloat64(want1, got1, 5) || !math.IsNaN(got2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}

		p = Plane{P: Vec{1.5, 0, 0}, Normal: Vec{1, 0, 0}}
		want1, want2 = math.NaN(), math.NaN()
		got0, got1, got2 = bez.PlaneIntersections(p)
		if got0 != 0 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 0, got0)
		}
		if !math.IsNaN(got1) || !math.IsNaN(got2) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
	}()

	bez = Bez{A: Vec{-3, 0, 0}, B: Vec{0, 6, 0}, C: Vec{3, -3, 0}}
	func() {
		p := Plane{P: Vec{0, 1, 0}, Normal: Vec{1, 1, 0}.Unit()}
		want0, want1, want2 := 2, Vec{-1.233030277982336, 2.233030277982335, 0}, Vec{2.4330302779823367, -1.4330302779823374, 0}
		got0, t1, t2 := bez.PlaneIntersections(p)
		got1, got2 := bez.AtT(t1), bez.AtT(t2)
		if want0 != got0 || !CompareVec(want1, got1, 5) || !CompareVec(want2, got2, 5) {
			t.Errorf("\nWant0:\n%#v\nGot0:\n%#v\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want0, got0, want1, got1, want2, got2)
		}
	}()
}

func TestCircle_PlaneIntersections(t *testing.T) {
	c1 := Circle{C: Vec{}, R: 1, Normal: PosY}
	p := Plane{P: Vec{}, Normal: PosZ}
	want1, want2 := 2, []Vec{{-1.0, 0, 0}, {1, 0, 0}}
	got1, v1, v2 := c1.PlaneIntersections(p)
	got2 := []Vec{v1, v2}
	if want1 != got1 || !CompareVecs(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	c1 = Circle{C: Vec{}, R: 1, Normal: PosY}
	p = Plane{P: Vec{Z: 0.5}, Normal: PosZ}
	want1, want2 = 2, []Vec{{-0.866025, 0, 0.5}, {0.866025, 0, 0.5}}
	got1, v1, v2 = c1.PlaneIntersections(p)
	got2 = []Vec{v1, v2}
	if want1 != got1 || !CompareVecs(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	c1 = Circle{C: Vec{}, R: 1, Normal: PosY}
	p = Plane{P: Vec{Z: -0.5}, Normal: PosZ}
	want1, want2 = 2, []Vec{{-0.866025, 0, -0.5}, {0.866025, 0, -0.5}}
	got1, v1, v2 = c1.PlaneIntersections(p)
	got2 = []Vec{v1, v2}
	if want1 != got1 || !CompareVecs(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	c1 = Circle{C: Vec{}, R: 1, Normal: NegY}
	p = Plane{P: Vec{Z: 0.5}, Normal: PosZ}
	want1, want2 = 2, []Vec{{-0.866025, 0, 0.5}, {0.866025, 0, 0.5}}
	got1, v1, v2 = c1.PlaneIntersections(p)
	got2 = []Vec{v1, v2}
	if want1 != got1 || !CompareVecs(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	c1 = Circle{C: Vec{}, R: 1, Normal: NegY}
	p = Plane{P: Vec{Z: -0.5}, Normal: PosZ}
	want1, want2 = 2, []Vec{{-0.866025, 0, -0.5}, {0.866025, 0, -0.5}}
	got1, v1, v2 = c1.PlaneIntersections(p)
	got2 = []Vec{v1, v2}
	if want1 != got1 || !CompareVecs(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	c1 = Circle{C: Vec{}, R: 1, Normal: NegY}
	p = Plane{P: Vec{Z: 1}, Normal: PosZ}
	want1, want2 = 1, []Vec{{0, 0, 1}, {0, 0, 0}}
	got1, v1, v2 = c1.PlaneIntersections(p)
	got2 = []Vec{v1, v2}
	if want1 != got1 || !CompareVecs(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	c1 = Circle{C: Vec{}, R: 1, Normal: NegY}
	p = Plane{P: Vec{Z: 1}, Normal: PosY}
	want1, want2 = 0, []Vec{{0, 0, 0}, {0, 0, 0}}
	got1, v1, v2 = c1.PlaneIntersections(p)
	got2 = []Vec{v1, v2}
	if want1 != got1 || !CompareVecs(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	c1 = Circle{C: Vec{0, 0.5, 0}, R: 1, Normal: Vec{0, 1, -1}.Unit()}
	p = Plane{P: Vec{}, Normal: PosY}
	want1, want2 = 2, []Vec{{1 / math.Sqrt2, 0, -0.5}, {-1 / math.Sqrt2, 0, -0.5}}
	got1, v1, v2 = c1.PlaneIntersections(p)
	got2 = []Vec{v1, v2}
	if want1 != got1 || !CompareVecs(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
}

func TestLine_PlaneIntersection(t *testing.T) {
	var want, got Vec

	plane1 := Plane{P: Vec{1, 0, 0}, Normal: PosX}
	plane2 := Plane{P: Vec{-1, 0, 0}, Normal: PosX}
	for _, p := range []Vec{
		{-1, 0, 0}, {0, 0, 0}, {1, 0, 0},
		{-0.5, 0, 0}, {0.5, 0, 0},
		{-2, 0, 0}, {2, 0, 0},
	} {
		want = Vec{1, 0, 0}
		got = Line{Dir: PosX, P: p}.PlaneIntersection(plane1)
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
	for _, p := range []Vec{
		{-1, 0, 0}, {0, 0, 0}, {1, 0, 0},
		{-0.5, 0, 0}, {0.5, 0, 0},
		{-2, 0, 0}, {2, 0, 0},
	} {
		want = Vec{-1, 0, 0}
		got = Line{Dir: PosX, P: p}.PlaneIntersection(plane2)
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	plane1 = Plane{P: Vec{1, -1, 0}, Normal: PosX}
	plane2 = Plane{P: Vec{-1, 1, 0}, Normal: PosX}
	for _, p := range []Vec{
		{-1, 0, 0}, {0, 0, 0}, {1, 0, 0},
		{-0.5, 0, 0}, {0.5, 0, 0},
		{-2, 0, 0}, {2, 0, 0},
	} {
		want = Vec{1, 0, 0}
		got = Line{Dir: PosX, P: p}.PlaneIntersection(plane1)
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
	for _, p := range []Vec{
		{-1, 0, 0}, {0, 0, 0}, {1, 0, 0},
		{-0.5, 0, 0}, {0.5, 0, 0},
		{-2, 0, 0}, {2, 0, 0},
	} {
		want = Vec{-1, 0, 0}
		got = Line{Dir: PosX, P: p}.PlaneIntersection(plane2)
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	plane1 = Plane{P: Vec{1, 0, 0}, Normal: NegX}
	plane2 = Plane{P: Vec{-1, 0, 0}, Normal: NegX}
	for _, p := range []Vec{
		{-1, 0, 0}, {0, 0, 0}, {1, 0, 0},
		{-0.5, 0, 0}, {0.5, 0, 0},
		{-2, 0, 0}, {2, 0, 0},
	} {
		want = Vec{1, 0, 0}
		got = Line{Dir: PosX, P: p}.PlaneIntersection(plane1)
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
	for _, p := range []Vec{
		{-1, 0, 0}, {0, 0, 0}, {1, 0, 0},
		{-0.5, 0, 0}, {0.5, 0, 0},
		{-2, 0, 0}, {2, 0, 0},
	} {
		want = Vec{-1, 0, 0}
		got = Line{Dir: PosX, P: p}.PlaneIntersection(plane2)
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	plane1 = Plane{Normal: PosY}
	want = Vec{}
	got = Line{Dir: Vec{1, -1, 0}.Unit(), P: Vec{-1, 1, 0}}.PlaneIntersection(plane1)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestPlane_PlaneIntersection(t *testing.T) {
	p1 := Plane{Normal: PosX, P: Vec{0, 0, 0}}
	p2 := Plane{Normal: PosY, P: Vec{0, 0, 0}}
	l := Line{Dir: PosZ, P: Vec{0, 0, 0}}

	want := true
	got := CompareLine(l, p1.PlaneIntersection(p2), 1e-6, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	p1 = Plane{Normal: Vec{1, 1, 1}.Unit(), P: Vec{0, 0, 0}}
	p2 = Plane{Normal: Vec{-1, 1, -1}.Unit(), P: Vec{0, 0, 0}}
	l = Line{Dir: NegX.Add(PosZ).Unit(), P: Vec{0, 0, 0}}

	got = CompareLine(l, p1.PlaneIntersection(p2), 1e-6, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	l = Line{Dir: NegX.Add(PosZ).Unit(), P: Vec{0, 1, 0}}

	got = CompareLine(l, p1.PlaneIntersection(p2), 1e-6, 5)
	want = false
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestRay_PlaneIntersection(t *testing.T) {
	r1 := Ray{Dir: PosX}
	p1 := Plane{P: Vec{1, 0, 0}, Normal: PosX}
	p2 := Plane{P: Vec{-1, 0, 0}, Normal: PosX}
	want1, want2 := Vec{1, 0, 0}, true
	got1, got2 := r1.PlaneIntersection(p1)
	if !CompareVec(want1, got1, 5) || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
	want1, want2 = Vec{0, 0, 0}, false
	got1, got2 = r1.PlaneIntersection(p2)
	if !CompareVec(want1, got1, 5) || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	r1 = Ray{O: Vec{1, 1, 0}, Dir: NegX.Add(NegY).Unit()}
	p1 = Plane{P: Vec{0, 0, 0}, Normal: PosY}
	p2 = Plane{P: Vec{0, 2, 0}, Normal: PosY}
	want1, want2 = Vec{0, 0, 0}, true
	got1, got2 = r1.PlaneIntersection(p1)
	if !CompareVec(want1, got1, 5) || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
	want1, want2 = Vec{0, 0, 0}, false
	got1, got2 = r1.PlaneIntersection(p2)
	if !CompareVec(want1, got1, 5) || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	r1 = Ray{O: Vec{1, 1, 0}, Dir: NegX.Add(NegY).Unit()}
	p1 = Plane{P: Vec{0, 0, 0}, Normal: NegY}
	p2 = Plane{P: Vec{0, 2, 0}, Normal: NegY}
	want1, want2 = Vec{0, 0, 0}, true
	got1, got2 = r1.PlaneIntersection(p1)
	if !CompareVec(want1, got1, 5) || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
	want1, want2 = Vec{0, 0, 0}, false
	got1, got2 = r1.PlaneIntersection(p2)
	if !CompareVec(want1, got1, 5) || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	r1 = Ray{O: Vec{1, 1, 0}, Dir: NegX}
	p1 = Plane{P: Vec{0, 1, 0}, Normal: NegY}
	want1, want2 = Vec{0, 0, 0}, false
	got1, got2 = r1.PlaneIntersection(p1)
	if !CompareVec(want1, got1, 5) || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	r1 = Ray{O: Vec{1, 1, 0}, Dir: PosY}
	p1 = Plane{P: Vec{0, 1, 0}, Normal: NegY}
	want1, want2 = Vec{1, 1, 0}, true
	got1, got2 = r1.PlaneIntersection(p1)
	if !CompareVec(want1, got1, 5) || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
}

func TestSphere_InteriorsIntersect(t *testing.T) {
	s1 := Sphere{C: Vec{0, 0, 0}, R: 1}
	s2 := Sphere{C: Vec{1, 0, 0}, R: 1}
	want := true
	got := s1.InteriorsIntersect(s2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	s2 = Sphere{C: Vec{2, 0, 0}, R: 1}
	want = false
	got = s1.InteriorsIntersect(s2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestLineSeg2_InteriorsIntersect(t *testing.T) {
	ls1 := LineSeg2{P1: Vec2{-1, 0}, P2: Vec2{1, 0}}
	ls2 := LineSeg2{P1: Vec2{0, -1}, P2: Vec2{0, 1}}
	want := true
	got := ls1.InteriorsIntersect(ls2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	ls3 := LineSeg2{P1: Vec2{0, 1}, P2: Vec2{0, -1}}
	got = ls1.InteriorsIntersect(ls3)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	ls4 := LineSeg2{P1: Vec2{-1, -1}, P2: Vec2{1, 1}}
	got = ls4.InteriorsIntersect(ls3) && ls4.InteriorsIntersect(ls2) && ls4.InteriorsIntersect(ls1)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = false
	got = ls4.InteriorsIntersect(ls4) || ls3.InteriorsIntersect(ls3) || ls2.InteriorsIntersect(ls2) || ls1.InteriorsIntersect(ls1)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	ls5 := LineSeg2{P1: Vec2{0, 0}, P2: Vec2{0, 1}}
	ls6 := LineSeg2{P1: Vec2{0, 0}, P2: Vec2{1, 0}}
	got = ls6.InteriorsIntersect(ls5) || ls5.InteriorsIntersect(ls6) || ls4.InteriorsIntersect(ls5) || ls4.InteriorsIntersect(ls6)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = ls6.InteriorsIntersectAny(ls5, ls4, ls3, ls2, ls1)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = true
	ls7 := LineSeg2{P1: Vec2{0.3, -0.2}, P2: Vec2{0.7, 0.2}}
	got = ls6.InteriorsIntersectAny(ls5, ls4, ls3, ls2, ls7, ls1)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle2_InteriorIncludes(t *testing.T) {
	var got, want bool
	tr1 := Triangle2{Vec2{-1, -1}, Vec2{0, 1}, Vec2{1, -1}}

	want = false
	got = tr1.InteriorIncludes(tr1.A) && tr1.InteriorIncludes(tr1.B) && tr1.InteriorIncludes(tr1.C)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = tr1.InteriorIncludes(tr1.A.MidTo(tr1.B)) && tr1.InteriorIncludes(tr1.B.MidTo(tr1.C)) &&
		tr1.InteriorIncludes(tr1.C.MidTo(tr1.A))
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = tr1.InteriorIncludes(Vec2{-2, -2}) && tr1.InteriorIncludes(Vec2{0, 2}) && tr1.InteriorIncludes(Vec2{2, -2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = tr1.InteriorIncludes(Vec2{-2, 0}) && tr1.InteriorIncludes(Vec2{2, 0}) && tr1.InteriorIncludes(Vec2{0, -2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = true
	got = tr1.InteriorIncludes(Vec2{0, 0}) && tr1.InteriorIncludes(Vec2{0, 0.1}) &&
		tr1.InteriorIncludes(Vec2{-0.1, -0.1}) && tr1.InteriorIncludes(Vec2{0.1, 0.1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle2_ClosureIncludes(t *testing.T) {
	var got, want bool
	tr1 := Triangle2{Vec2{-1, -1}, Vec2{0, 1}, Vec2{1, -1}}

	want = true
	got = tr1.ClosureIncludes(tr1.A) && tr1.ClosureIncludes(tr1.B) && tr1.ClosureIncludes(tr1.C)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = tr1.ClosureIncludes(tr1.A.MidTo(tr1.B)) && tr1.ClosureIncludes(tr1.B.MidTo(tr1.C)) &&
		tr1.ClosureIncludes(tr1.C.MidTo(tr1.A))
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = false
	got = tr1.ClosureIncludes(Vec2{-2, -2}) && tr1.ClosureIncludes(Vec2{0, 2}) && tr1.ClosureIncludes(Vec2{2, -2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = tr1.ClosureIncludes(Vec2{-2, 0}) && tr1.ClosureIncludes(Vec2{2, 0}) && tr1.ClosureIncludes(Vec2{0, -2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = true
	got = tr1.ClosureIncludes(Vec2{0, 0}) && tr1.ClosureIncludes(Vec2{0, 0.1}) &&
		tr1.ClosureIncludes(Vec2{-0.1, -0.1}) && tr1.ClosureIncludes(Vec2{0.1, 0.1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = tr1.ClosureIncludesAny(Vec2{-2, -2}, tr1.A, Vec2{0, 2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = false
	got = tr1.ClosureIncludesAny(Vec2{-2, -2}, Vec2{0, 2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle2_InteriorsOverlap(t *testing.T) {
	tr1 := Triangle2{Vec2{-1, 0}, Vec2{0, 1}, Vec2{1, 0}}
	tr2 := Triangle2{Vec2{0, 0}, Vec2{0, 1}, Vec2{1, 0}}

	want := true
	got := tr1.InteriorsIntersect(tr1) && tr2.InteriorsIntersect(tr2) && tr1.InteriorsIntersect(tr2) && tr2.InteriorsIntersect(tr1)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tr3 := Triangle2{Vec2{0, -1}, Vec2{0, 2}, Vec2{3, 0}}
	got = tr3.InteriorsIntersect(tr1) && tr3.InteriorsIntersect(tr2) && tr1.InteriorsIntersect(tr3) && tr2.InteriorsIntersect(tr3)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = false
	tr4 := Triangle2{Vec2{0, 1}, Vec2{1, 0}, Vec2{1, 1}}
	got = tr4.InteriorsIntersect(tr1) && tr4.InteriorsIntersect(tr2) && tr4.InteriorsIntersect(tr3) &&
		tr1.InteriorsIntersect(tr4) && tr2.InteriorsIntersect(tr4) && tr3.InteriorsIntersect(tr4)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tr5 := Triangle2{Vec2{0, 0}, Vec2{-2, 2}, Vec2{2, 2}}
	tr6 := Triangle2{Vec2{-1, 1}, Vec2{1, 1}, Vec2{0, 2}}
	want = true
	got = tr5.InteriorsIntersect(tr6) && tr6.InteriorsIntersect(tr5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle2_InteriorOverlapsLineSeg(t *testing.T) {
	tr1 := Triangle2{Vec2{-1, 0}, Vec2{0, 1}, Vec2{1, 0}}
	tr2 := Triangle2{Vec2{-1, 0.5}, Vec2{1, 0.5}, Vec2{0, -0.5}}

	want := false
	got := tr1.InteriorsIntersectLineSeg(LineSeg2{P1: tr1.A, P2: tr1.B}) ||
		tr1.InteriorsIntersectLineSeg(LineSeg2{P1: tr1.B, P2: tr1.C}) ||
		tr1.InteriorsIntersectLineSeg(LineSeg2{P1: tr1.C, P2: tr1.A})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = tr1.InteriorsIntersectLineSeg(LineSeg2{P1: Vec2{2, 2}, P2: Vec2{3, 3}})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = true
	got = tr1.InteriorsIntersectLineSeg(LineSeg2{P1: tr2.A, P2: tr2.B}) ||
		tr1.InteriorsIntersectLineSeg(LineSeg2{P1: tr2.B, P2: tr2.C}) ||
		tr1.InteriorsIntersectLineSeg(LineSeg2{P1: tr2.C, P2: tr2.A})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = tr1.InteriorsIntersectLineSeg(LineSeg2{P1: Vec2{0, 0}, P2: Vec2{-0.5, 0.5}})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = tr1.InteriorsIntersectLineSeg(LineSeg2{P1: Vec2{0, 0}, P2: Vec2{-1.0, 1.0}})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = false
	got = tr1.InteriorsIntersectLineSeg(LineSeg2{P1: Vec2{-1.0, -1.0}, P2: Vec2{-0.5, -0.5}})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
