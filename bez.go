package maf

import (
	"math"

	"gitlab.com/fospathi/universal/interval"
)

// Bez is a 3D quadratic Bezier curve.
type Bez Triangle

// AngleB returns the positive angle subtended at the vertex B by the edge AC.
func (b1 Bez) AngleB() float64 {
	return b1.B.To(b1.A).Angle(b1.B.To(b1.C))
}

// AtT returns the position vector on the receiver Bezier curve at the argument
// parameter.
//
// The argument parameter is valid in the range 0..1 inclusive.
func (b1 Bez) AtT(t float64) Vec {
	coef0 := (1 - t) * (1 - t)
	coef1 := 2 * (1 - t) * t
	coef2 := t * t
	return Vec{
		X: coef0*b1.A.X + coef1*b1.B.X + coef2*b1.C.X,
		Y: coef0*b1.A.Y + coef1*b1.B.Y + coef2*b1.C.Y,
		Z: coef0*b1.A.Z + coef1*b1.B.Z + coef2*b1.C.Z}
}

// For a quadratic Bezier with control points A, B, C let
//   b0 = A to B
//   b1 = B to C
//   b2 = C to A
func bezierArcLength(b0, b1, b2 Vec) float64 {
	/*
		Geometric constraints on quadratic Bézier curves using minimal length and energy
		Ahn, Young Joon
		Hoffmann, Christoph
		Rosen, Paul
		https://www.sciencedirect.com/science/article/pii/S037704271300349X#s000020
	*/
	B0, B1, B2 := b0.Mag(), b1.Mag(), b2.Mag()
	commonTerm := func(alpha float64) float64 {
		return alpha*B0*B0 + (1-alpha)*B1*B1 - 0.25*B2*B2
	}
	ct14 := commonTerm(0.25)
	ct12 := commonTerm(0.5)
	ct34 := commonTerm(0.75)
	sqrtCt12 := math.Sqrt(ct12)
	return (ct14*B1+ct34*B0)/(2*ct12) +
		((b0.Cross(b1).Mag()*b0.Cross(b1).Mag())/(8*sqrtCt12*ct12))*
			(math.Log(ct14+B1*sqrtCt12)-math.Log(-ct34+B0*sqrtCt12))
}

// ArcLength returns the length of the receiver Bezier curve.
func (b1 Bez) ArcLength() float64 {
	return bezierArcLength(b1.A.To(b1.B), b1.B.To(b1.C), b1.C.To(b1.A))
}

// For a quadratic Bezier with control points A, B, C let
//   b0 = A to B
//   b1 = B to C
//   b2 = C to A
func bezierBendingEnergy(b0, b1, b2 Vec) float64 {
	/*
		Geometric constraints on quadratic Bézier curves using minimal length and energy
		Ahn, Young Joon
		Hoffmann, Christoph
		Rosen, Paul
		https://www.sciencedirect.com/science/article/pii/S037704271300349X#s000025
	*/
	B0, B1, B2 := b0.Mag(), b1.Mag(), b2.Mag()
	commonTerm := func(alpha float64) float64 {
		return alpha*B0*B0 + (1-alpha)*B1*B1 - 0.25*B2*B2
	}
	ct14 := commonTerm(0.25)
	ct12 := commonTerm(0.5)
	ct34 := commonTerm(0.75)
	A := ct14 * (3*ct12*B1*B1 - ct14*ct14) / (B1 * B1 * B1)
	B := ct34 * (3*ct12*B0*B0 - ct34*ct34) / (B0 * B0 * B0)
	return (2.0 / (3.0 * (b0.Cross(b1).Mag() * b0.Cross(b1).Mag()))) *
		(A + B)
}

// BendingEnergy returns the bending energy of the receiver Bezier curve.
func (b1 Bez) BendingEnergy() float64 {
	return bezierBendingEnergy(b1.A.To(b1.B), b1.B.To(b1.C), b1.C.To(b1.A))
}

// BezClosest is the closest point on a Bezier curve to some other point.
type BezClosest struct {
	// The distance between the points.
	D float64
	// The point on the Bezier curve corresponging to the parameter value.
	P Vec
	// The value of the quadratic Bezier curve's parameter, in the range 0 to 1,
	// which generates the closest point.
	//
	// In the case of a Bezier that has two closest points of approach to the
	// point, either parameter may be returned.
	T float64
}

// Solve the cubic equation that yields the possible values of t that solve the
// closest point of approach problem of the argument point to the argument
// Bezier curve.
func bezClosestRoots(r Bez, p Vec) (complex128, complex128, complex128) {
	// Let P be some point in the vicinity of a quadratic Bezier curve R(t)
	// where      R(t) = (x(t), y(t), z(t))
	// and        x(t) = (1-t)²xA + 2(1-t)(t)xB + t²xC
	// and similarly for y and z.
	// The distance between any curve point and P is:
	//      |R(t) - P| = √[(x(t) - xP)² + (y(t) - yP)² + (z(t) - zP)²]
	// To find the closest point of approach we can minimise |R(t) - P|² wrt to t.
	// Consider the x components:
	//    (x(t) - xP)² = ((1-t)²xA + 2(1-t)(t)xB + t²xC - xP)²
	// So
	//  d
	// -- (x(t) - xP)² = 2( (1-t)²xA + 2(1-t)(t)xB + t²xC - xP )( -2(1-t)xA - 2txB + 2(1-t)xB + 2txC )
	// dt
	//
	// Solving this for zero gives us a cubic equation in t.
	// Let 0 = coef3*t³ + coef2*t² + coef1*t + coef0, where the coefficients
	// also include the y and z terms analogous to those as deduced above for x.
	xA, xB, xC := r.A.X, r.B.X, r.C.X
	yA, yB, yC := r.A.Y, r.B.Y, r.C.Y
	zA, zB, zC := r.A.Z, r.B.Z, r.C.Z
	coef3 := (xA-2*xB+xC)*(xA-2*xB+xC) + (yA-2*yB+yC)*(yA-2*yB+yC) + (zA-2*zB+zC)*(zA-2*zB+zC)
	coef2 := 3*(xA-2*xB+xC)*(xB-xA) + 3*(yA-2*yB+yC)*(yB-yA) + 3*(zA-2*zB+zC)*(zB-zA)
	coef1 := (xA-2*xB+xC)*(xA-p.X) + 2*(xB-xA)*(xB-xA) +
		(yA-2*yB+yC)*(yA-p.Y) + 2*(yB-yA)*(yB-yA) +
		(zA-2*zB+zC)*(zA-p.Z) + 2*(zB-zA)*(zB-zA)
	coef0 := (xB-xA)*(xA-p.X) + (yB-yA)*(yA-p.Y) + (zB-zA)*(zA-p.Z)
	return Cubic{A: coef3, B: coef2, C: coef1, D: coef0}.Roots()
}

// Returns an approximate guess as to the value of the Bezier curve parameter t,
// in the range 0 to 1, that corresponds to the point on the argument Bezier
// curve that is the closest to the argument point.
func bezClosestT(b1 Bez, p Vec) float64 {
	const depth = 16
	in := interval.In{0, 1}
	var ld, rd float64
	for i := 0; i < depth; i++ {
		vin := in.Split(2)
		ld := b1.AtT(vin[0].Midpoint()).To(p).MagSqd()
		rd := b1.AtT(vin[1].Midpoint()).To(p).MagSqd()
		if ld < rd {
			in = vin[0]
		} else {
			in = vin[1]
		}
	}
	ld = b1.AtT(in[0]).To(p).MagSqd()
	rd = b1.AtT(in[1]).To(p).MagSqd()
	switch {
	case ld < rd:
		return in[0]
	case rd < ld:
		return in[1]
	default:
		return in.Midpoint()
	}
}

// Closest returns information about the closest point of approach on the
// receiver Bezier curve to the argument point.
//
// The receiver Bezier curve shall not have collinear control points. In the
// case of two equidistant closest points either one can be returned.
func (b1 Bez) Closest(p Vec) BezClosest {
	root1, root2, root3 := bezClosestRoots(b1, p)
	closest1, closest2, closest3 :=
		BezClosest{T: clampBezT(real(root1))},
		BezClosest{T: clampBezT(real(root2))},
		BezClosest{T: clampBezT(real(root3))}
	closest1.P, closest2.P, closest3.P =
		b1.AtT(closest1.T), b1.AtT(closest2.T), b1.AtT(closest3.T)
	closest := closest1
	closest.D = closest.P.To(p).MagSqd()
	if d := closest2.P.To(p).MagSqd(); d < closest.D {
		closest = closest2
		closest.D = d
	}
	if d := closest3.P.To(p).MagSqd(); d < closest.D {
		closest = closest3
		closest.D = d
	}
	closest.D = math.Sqrt(closest.D)
	if math.IsNaN(closest.T) {
		// Resort to a numerical approximation. This case is known to occur when
		// a collinear Bez is used with a midpoint-coinciding middle control
		// point. There may be other unknown cases that fail and require a
		// numerical approximation.
		t := bezClosestT(b1, p)
		return BezClosest{D: b1.AtT(t).DistTo(p), P: b1.AtT(t), T: t}
	}
	return closest
}

// ClosestPoints returns information about the one or two closest points of
// approach on the receiver Bezier curve to the argument point.
//
// The special case of two closest points, as opposed to a single point, can
// occur for example when
//
// * the control points are collinear. The two closest points coincide but have
// different values of the curve's parameter t
//
// * the control points are symmetrical about a line and the point lies on the
// line
//
// The returned integer value is the number of solutions and can be 1 or 2. If
// its value is 1 then the second BezClosest is identical to the first.
func (b1 Bez) ClosestPoints(p Vec) (int, BezClosest, BezClosest) {
	root1, root2, root3 := bezClosestRoots(b1, p)
	if math.IsNaN(real(root1)) {
		// Resort to a numerical approximation. This case is known to occur when
		// a collinear Bez is used with a midpoint-coinciding middle control
		// point. There may be other unknown cases that fail and require a
		// numerical approximation.
		t := bezClosestT(b1, p)
		closest := BezClosest{D: b1.AtT(t).DistTo(p), P: b1.AtT(t), T: t}
		return 1, closest, closest
	}
	closest1, closest2, closest3 :=
		BezClosest{T: clampBezT(real(root1))},
		BezClosest{T: clampBezT(real(root2))},
		BezClosest{T: clampBezT(real(root3))}
	closest1.P, closest2.P, closest3.P =
		b1.AtT(closest1.T), b1.AtT(closest2.T), b1.AtT(closest3.T)
	closest1.D, closest2.D, closest3.D =
		closest1.P.To(p).Mag(), closest2.P.To(p).Mag(), closest3.P.To(p).Mag()
	min, _ := MinMax(closest1.D, closest2.D, closest3.D)
	delta1, delta2, delta3 :=
		closest1.D-min, closest2.D-min, closest3.D-min
	const smallValue = 1e-10
	switch {
	case delta1 < smallValue && delta2 < smallValue:
		if abs(closest1.T-closest2.T) > smallValue {
			return 2, closest1, closest2
		}
	case delta1 < smallValue && delta3 < smallValue:
		if abs(closest1.T-closest3.T) > smallValue {
			return 2, closest1, closest3
		}
	case delta2 < smallValue && delta3 < smallValue:
		if abs(closest2.T-closest3.T) > smallValue {
			return 2, closest2, closest3
		}
	}
	switch min {
	case closest1.D:
		return 1, closest1, closest1
	case closest2.D:
		return 1, closest2, closest2
	default:
		return 1, closest3, closest3
	}
}

// IsCollinear returns whether the receiver Bezier is, to within the argument
// tolerance, a degenerate straight line case.
func (b1 Bez) IsCollinear(tolerance float64) bool {
	return b1.A.To(b1.B).Cross(b1.A.To(b1.C)).MagSqd() < tolerance
}

// Plane returns a new plane which coincides with the receiver Bezier curve.
func (b1 Bez) Plane() Plane {
	ab := b1.A.To(b1.B)
	ac := b1.A.To(b1.C)
	return Plane{P: b1.A, Normal: ab.Cross(ac).Unit()}
}

// Subdivide1 returns the result of dividing the receiver Bezier curve into two
// pieces at the argument parameter in the interval 0..1.
func (b1 Bez) Subdivide1(t float64) (Bez, Bez) {
	PAB := b1.A.Add(b1.A.To(b1.B).Scale(t))
	PBC := b1.B.Add(b1.B.To(b1.C).Scale(t))
	P := PAB.Add(PAB.To(PBC).Scale(t))
	return Bez{A: b1.A, B: PAB, C: P}, Bez{A: P, B: PBC, C: b1.C}
}

// Subdivide2 returns the result of dividing the receiver Bezier curve into
// three pieces at the two argument parameters in the interval 0..1.
func (b1 Bez) Subdivide2(t1, t2 float64) (Bez, Bez, Bez) {
	if t1 > t2 {
		t1, t2 = t2, t1
	}
	b2, b3 := b1.Subdivide1(t1)
	t := (t2 - t1) / (1 - t1)
	b4, b5 := b3.Subdivide1(t)
	return b2, b4, b5
}

// PlaneTangencyLocus returns the set of control point positions, for the middle
// control point B, that would make the receiver Bezier curve tangential to the
// argument plane.
//
// The start and end control points, A and C, of the receiver Bezier shall be on
// the same side of the argument plane.
//
// The returned set of control points will coincide with the intrinsic plane of
// the receiver Bezier curve and will be parallel to the argument plane.
func (b1 Bez) PlaneTangencyLocus(plane Plane) Line {
	/*
		Geometric constraints on quadratic Bézier curves using minimal length and energy
		Ahn, Young Joon
		Hoffmann, Christoph
		Rosen, Paul
		https://www.sciencedirect.com/science/article/pii/S037704271300349X#s000030
	*/
	z0, z2 := plane.Closest(b1.A), plane.Closest(b1.C)
	a, c := plane.DistTo(b1.A), plane.DistTo(b1.C)
	b := math.Sqrt(a * c)
	z1 := z0.DivTo(z2, a, c)
	return Line{
		P:   z1.Add(b1.A.To(z0).Unit().Scale(b)),
		Dir: z0.To(z2).Unit(),
	}
}

// CircleTangency returns the tangency information for the Bezier and the circle
// which is the intersection of the given sphere's surface with the plane of the
// receiver Bezier.
//
// The returned sets of control points will coincide with the intrinsic plane of
// the receiver Bezier curve.
//
// The distance of the receiver Bezier's plane from the argument sphere's centre
// shall be less than the sphere's radius, that is, they shall intersect forming
// a circle.
//
// For more details and special cases see
//  Geometric constraints on quadratic Bézier curves using minimal length and energy
//  Ahn, Young Joon
//  Hoffmann, Christoph
//  Rosen, Paul
//  https://www.sciencedirect.com/science/article/pii/S037704271300349X#s000040
func (b1 Bez) CircleTangency(sph Sphere) BezCircleTangency {
	plane := b1.Plane()
	circ := sph.Circle(plane)
	switch {
	case Line{P: b1.A, Dir: b1.A.To(b1.C).Unit()}.DistTo(circ.C) > circ.R:
		// The normal non-special case
		return tangencyLocusToCircleForNormalCase(plane, circ, b1.A, b1.C)
	}
	return BezCircleTangency{}
}

// Tangent returns the tangent line to the receiver Bezier curve at the argument
// parameter.
//
// The argument parameter is valid in the closed interval 0..1.
func (b1 Bez) Tangent(t float64) Line {
	ab := b1.A.To(b1.B)
	bc := b1.B.To(b1.C)
	p1 := b1.A.Add(ab.Scale(t))
	p2 := b1.B.Add(bc.Scale(t))
	return Line{P: b1.AtT(t), Dir: p1.To(p2).Unit()}
}

// BezCircleTangency is a solution to the problem of finding positions, for a
// quadratic Bezier curve's middle control point, that make the Bezier curve
// tangential to a coplanar circle.
//
// The positions are relative to the same frame that the Bezier and circle are
// defined in, the angles however are relative to the provided angle's basis.
type BezCircleTangency struct {
	// The fixed first and last control points of the quadratic Bezier curve.
	A, C Vec
	// The angles in the intervals and used by the locus/tangent functions are
	// relative to a hypothetical frame with this basis at the centre of the
	// circle.
	AnglesBasis Basis
	// The circle which is touched tangentially by the Bezier curve.
	Circle Circle
	// An interval of angle values, measured in radians, each of which
	// corresponds to a unique Bezier curve which makes a tangent with the
	// closer arc on the circle.
	Closer interval.In
	// An interval of angle values, measured in radians, each of which
	// corresponds to a unique Bezier curve which makes a tangent with the
	// farther arc on the circle.
	Farther interval.In
	// The middle control point of the Bezier curve, so that the curve is
	// tangential. Valid only for angle values contained in the intervals.
	B func(theta float64) Vec
	// The contact point/tangent position where the Bezier curve is a tangent to
	// the circle. Valid only for angle values contained in the intervals.
	T func(theta float64) Vec
	// A Non-zero value implies a special case.
	SpecialCase uint8
}

// TangentialBez returns a new tangential Bezier at the argument angle value.
func (tang BezCircleTangency) TangentialBez(theta float64) Bez {
	return Bez{A: tang.A, B: tang.B(theta), C: tang.C}
}

func tangencyLocusToCircleForNormalCase(
	plane Plane,
	circ Circle,
	b0 Vec,
	b2 Vec,
) BezCircleTangency {

	/*
		Geometric constraints on quadratic Bézier curves using minimal length and energy
		Ahn, Young Joon
		Hoffmann, Christoph
		Rosen, Paul
		https://www.sciencedirect.com/science/article/pii/S037704271300349X#s000040
	*/
	var closer0, closer2, farther0, farther2 Vec
	{
		tan1, tan2 := circ.TangentPositions(b0)
		if tan1.DistTo(b2) < tan2.DistTo(b2) {
			closer0, farther0 = tan1, tan2
		} else {
			closer0, farther0 = tan2, tan1
		}
		tan1, tan2 = circ.TangentPositions(b2)
		if tan1.DistTo(b0) < tan2.DistTo(b0) {
			closer2, farther2 = tan1, tan2
		} else {
			closer2, farther2 = tan2, tan1
		}
	}
	var x, y Vec
	{
		// Choose a positive X-axis direction that intersects the circle in the
		// farther arc.
		l0 := Line{P: b0, Dir: b0.To(farther0).Unit()}
		l2 := Line{P: b2, Dir: b2.To(farther2).Unit()}
		if l0.IsParallel(l2, SmallAngle) {
			x = b0.MidTo(b2).To(circ.C).Unit()
		} else {
			x = circ.C.To(l0.ClosestPoint(l2)).Unit()
		}
		y = circ.Normal.Cross(x)
	}
	circP := func(theta float64) Vec {
		return circ.C.Add(x.Scale(circ.R * math.Cos(theta)).Add(y.Scale(circ.R * math.Sin(theta))))
	}
	signedAngle := func(tan Vec) float64 {
		if ScalarTripleProduct(x, circ.C.To(tan), plane.Normal) < 0 {
			return x.Angle(circ.C.To(tan)) * -1
		}
		return x.Angle(circ.C.To(tan))
	}
	var closerInterval, fartherInterval interval.In
	{
		// signed angles in the range -Pi <= angle <= Pi
		theta2, theta1 := signedAngle(closer0), signedAngle(closer2)
		theta4, theta3 := signedAngle(farther0), signedAngle(farther2)
		closerInterval = interval.NewPi(theta1, theta2).NotContaining(0).Interval()
		fartherInterval = interval.NewPi(theta3, theta4).Containing(0).Interval()
	}

	b1 := func(theta float64) Vec {
		c := circP(theta)
		d0 := (1 / circ.R) * b0.To(c).Dot(circ.C.To(c))
		if d0 < 0 {
			d0 *= -1
		}
		d2 := (1 / circ.R) * b2.To(c).Dot(circ.C.To(c))
		if d2 < 0 {
			d2 *= -1
		}
		v0 := b0.To(c).Scale(math.Sqrt(d2 / d0))
		v2 := b2.To(c).Scale(math.Sqrt(d0 / d2))
		return c.Add(v0.Add(v2).Scale(0.5))
	}

	return BezCircleTangency{
		A: b0,
		C: b2,
		AnglesBasis: Basis{
			X: x,
			Y: y,
			Z: circ.Normal,
		},
		Circle:  circ,
		Closer:  closerInterval,
		Farther: fartherInterval,
		B:       b1,
		T:       circP,
	}
}

func clampBezT(t float64) float64 {
	if t < 0 {
		return 0
	}
	if t > 1 {
		return 1
	}
	return t
}
