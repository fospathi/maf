package maf

import (
	"math"

	"gitlab.com/fospathi/universal/interval"
)

// CosineRule figures out the angle C, measured in radians, in a triangle with
// side lengths a, b, and c.
//
// The side c subtends the angle C.
func CosineRule(a, b, c float64) float64 {
	//     c^2 = a^2 + b^2 - 2 a b cos(C)
	//
	//  cos(C) = a^2 + b^2 - c^2
	//           ---------------
	//                2 a b
	return math.Acos((a*a + b*b - c*c) / (2 * a * b))
}

// ModuloPi returns the trigonometrically equivalent angle to the argument
// angle, measured in radians, in the range -Pi to Pi.
func ModuloPi(angle float64) float64 {
	if angle >= -math.Pi && angle <= math.Pi {
		return angle
	}
	angle = math.Mod(angle, TwoPi)
	if angle < -math.Pi {
		angle += TwoPi
	} else if angle > math.Pi {
		angle -= TwoPi
	}
	return interval.PiIn.Clamp(angle)
}
