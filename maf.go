/*
Package maf provides maths functions and types.
*/
package maf

import (
	"math"
	"math/rand"
	"time"
)

const (
	// SmallAngle is an arbitrary small angle measured in radians.
	//
	// It is used in situations that need to take into account floating point
	// numerical approximations like testing for parallelism.
	SmallAngle = 1e-6

	// TwoPi is the angle subtended at the centre of a whole circle by its
	// perimeter, measured in radians.
	TwoPi = math.Pi * 2

	// HalfPi is the angle subtended at the centre of a quarter circle by its
	// arc, measured in radians.
	HalfPi = math.Pi / 2

	// QuartPi is the angle subtended at the centre of a 1/8th circle by its
	// arc, measured in radians.
	QuartPi = math.Pi / 4
)

var (
	// CosPiOver6 is the value of Cos(Pi/6).
	CosPiOver6 = math.Sqrt(3) / 2.0

	// TanSmallAngle is the value of the tan of a small angle.
	TanSmallAngle = math.Tan(SmallAngle)
)

var (
	// StdBasis is a standard basis with axis directions (1, 0, 0), (0, 1, 0),
	// (0, 0, 1).
	StdBasis = Basis{PosX, PosY, PosZ}

	// StdFrame is a standard frame with origin (0, 0, 0) and axis directions
	// (1, 0, 0), (0, 1, 0), (0, 0, 1).
	StdFrame = Frame{O: Zero, Axes: StdBasis}
)

var (
	// AffIdentity is the identity transformation.
	AffIdentity = Aff{1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}
)

var (
	// Zero is the zero length vector.
	Zero = Vec{X: 0, Y: 0, Z: 0}

	// NegZ is the unit direction vector of the negative Z-axis.
	NegZ = Vec{X: 0, Y: 0, Z: -1}

	// PosZ is the unit direction vector of the positive Z-axis.
	PosZ = Vec{X: 0, Y: 0, Z: 1}

	// NegY is the unit direction vector of the negative Y-axis.
	NegY = Vec{X: 0, Y: -1, Z: 0}

	// PosY is the unit direction vector of the positive Y-axis.
	PosY = Vec{X: 0, Y: 1, Z: 0}

	// NegX is the unit direction vector of the negative X-axis.
	NegX = Vec{X: -1, Y: 0, Z: 0}

	// PosX is the unit direction vector of the positive X-axis.
	PosX = Vec{X: 1, Y: 0, Z: 0}

	// NegXNegZ is the unit direction vector of the vector sum of the negative
	// X-axis and negative Z-axis.
	NegXNegZ = NegX.Add(NegZ).Unit()

	// NegXPosZ is the unit direction vector of the vector sum of the negative
	// X-axis and positive Z-axis.
	NegXPosZ = NegX.Add(PosZ).Unit()

	// PosXNegZ is the unit direction vector of the vector sum of the positive
	// X-axis and negative Z-axis.
	PosXNegZ = PosX.Add(NegZ).Unit()

	// PosXPosZ is the unit direction vector of the vector sum of the positive
	// X-axis and positive Z-axis.
	PosXPosZ = PosX.Add(PosZ).Unit()
)

var (
	// X0Plane is the plane through the origin which coincides with the Y and Z
	// axes.
	X0Plane = Plane{Normal: PosX}

	// Y0Plane is the plane through the origin which coincides with the Z and X
	// axes.
	Y0Plane = Plane{Normal: PosY}

	// Z0Plane is the plane through the origin which coincides with the X and Y
	// axes.
	Z0Plane = Plane{Normal: PosZ}
)

var (
	// XRotPi is a rotation around the X-axis by Pi radians. It is its own
	// inverse transformation.
	XRotPi = NewXRot(math.Pi)

	// YRotPi is a rotation around the Y-axis by Pi radians. It is its own
	// inverse transformation.
	YRotPi = NewYRot(math.Pi)

	// ZRotPi is a rotation around the Z-axis by Pi radians. It is its own
	// inverse transformation.
	ZRotPi = NewZRot(math.Pi)
)

var (
	// Z0Refl is a reflection transformation in the XY plane at z=0.
	Z0Refl = NewXYRefl()
)

// AbsLen returns the magnitude of the length between the argument values.
func AbsLen(a, b float64) float64 {
	if a < b {
		return b - a
	}
	return a - b
}

// Lowest returns the smallest of the two integer arguments.
func Lowest(n1, n2 int) int {
	if n1 < n2 {
		return n1
	}
	return n2
}

// MinMax returns the lowest and highest of three numbers.
func MinMax(n1, n2, n3 float64) (float64, float64) {
	min, max := n1, n1
	if min > n2 {
		min = n2
	}
	if min > n3 {
		min = n3
	}
	if max < n2 {
		max = n2
	}
	if max < n3 {
		max = n3
	}
	return min, max
}

// MinMaxInt returns the lowest and highest of three integers.
func MinMaxInt(n1, n2, n3 int) (int, int) {
	a, b := MinMax(float64(n1), float64(n2), float64(n3))
	return int(a), int(b)
}

// nan is the special float value NaN.
var nan = math.NaN()

// DegToRad converts the argument angle measured in degrees to radians.
func DegToRad(deg float64) float64 {
	return (deg / 360) * TwoPi
}

// RadToDeg converts the argument angle measured in radians to degrees.
func RadToDeg(rad float64) float64 {
	return (rad / TwoPi) * 360
}

func abs(v float64) float64 {
	if v < 0 {
		return -v
	}
	return v
}

var rdm = rand.New(rand.NewSource(time.Now().UnixNano()))
