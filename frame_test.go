package maf

import (
	"math"
	"testing"
)

func BenchmarkBasis_In(b *testing.B) {
	basis := Basis{Vec{1, 1, 0}, Vec{-1, 1, 0}, PosZ}
	for n := 0; n < b.N; n++ {
		basis.In()
	}
}

func TestBasis_In(t *testing.T) {
	b := StdBasis
	m := b.In()
	want := StdBasis
	got := Basis{m.Transform(b.X), m.Transform(b.Y), m.Transform(b.Z)}
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	b = Basis{PosZ, PosY, NegX}
	m = b.In()
	want = StdBasis
	got = Basis{m.Transform(b.X), m.Transform(b.Y), m.Transform(b.Z)}
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	b = Basis{PosX, PosY, NegZ}
	m = b.In()
	want = StdBasis
	got = Basis{m.Transform(b.X), m.Transform(b.Y), m.Transform(b.Z)}
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_In(t *testing.T) {
	f := Frame{Vec{1, 1, 1}, StdBasis}
	m := f.In()
	wants := []Vec{{-1, -1, -1}, {0, -1, -1}, {-1, 0, -1}, {-1, -1, 0}}
	gots := []Vec{m.Transform(Vec{0, 0, 0}), m.Transform(PosX), m.Transform(PosY), m.Transform(PosZ)}
	for i, want := range wants {
		got := gots[i]
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	f = Frame{Vec{-2, -2, -2}, Basis{NegX, PosY, PosZ}}
	m = f.In()
	wants = []Vec{{-2, 2, 2}, {-3, 2, 2}, {-2, 3, 2}, {-2, 2, 3}}
	gots = []Vec{m.Transform(Vec{0, 0, 0}), m.Transform(PosX), m.Transform(PosY), m.Transform(PosZ)}
	for i, want := range wants {
		got := gots[i]
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestBasis_Out(t *testing.T) {
	func() {
		b := StdBasis
		want := Mat3{1, 0, 0, 0, 1, 0, 0, 0, 1}
		got := b.Out()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}()

	func() {
		b := StdBasis
		m := b.Out()
		v := PosX
		want := PosX
		got := m.Transform(v)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		b = Basis{NegX, PosY, NegZ}
		m = b.Out()
		v = PosX
		want = NegX
		got = m.Transform(v)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		b = Basis{NegX, PosY, NegZ}
		m = b.Out()
		v = Vec{1, 1, 1}
		want = Vec{-1, 1, -1}
		got = m.Transform(v)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}()
}

func TestFrame_Out(t *testing.T) {
	f := Frame{Vec{1, 1, 1}, StdBasis}
	m := f.Out()
	wants := []Vec{{1, 1, 1}, {2, 1, 1}, {1, 2, 1}, {1, 1, 2}}
	gots := []Vec{m.Transform(Vec{0, 0, 0}), m.Transform(PosX), m.Transform(PosY), m.Transform(PosZ)}
	for i, want := range wants {
		got := gots[i]
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestBasis_InFrom(t *testing.T) {
	b1 := Basis{PosY, NegX, PosZ}
	b2 := Basis{NegZ, PosY, PosX}
	m := b1.InFrom(b2)
	want := Basis{NegZ, PosX, NegY}
	got := Basis{m.Transform(PosX), m.Transform(PosY), m.Transform(PosZ)}
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_InFrom(t *testing.T) {
	f1 := Frame{Vec{1, 1, 1}, Basis{NegZ, PosY, NegX}}
	f2 := Frame{Vec{-1, -1, -1}, StdBasis}
	m := f1.InFrom(f2)
	wants := []Vec{{2, -2, 2}, {2, -2, 1}, {2, -1, 2}, {1, -2, 2}}
	gots := []Vec{m.Transform(Vec{0, 0, 0}), m.Transform(PosX), m.Transform(PosY), m.Transform(PosZ)}
	for i, want := range wants {
		got := gots[i]
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestBasis_OutTo(t *testing.T) {
	b1 := Basis{PosY, NegX, PosZ}
	b2 := Basis{NegZ, PosY, PosX}
	m := b2.OutTo(b1)
	want := Basis{NegZ, PosX, NegY}
	got := Basis{m.Transform(PosX), m.Transform(PosY), m.Transform(PosZ)}
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_OutTo(t *testing.T) {
	f1 := Frame{Vec{1, 1, 1}, Basis{NegZ, PosY, NegX}}
	f2 := Frame{Vec{-1, -1, -1}, StdBasis}
	m := f1.OutTo(f2)
	wants := []Vec{{2, 2, 2}, {2, 2, 1}, {2, 3, 2}, {1, 2, 2}}
	gots := []Vec{m.Transform(Vec{0, 0, 0}), m.Transform(PosX), m.Transform(PosY), m.Transform(PosZ)}
	for i, want := range wants {
		got := gots[i]
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestFrame_PlotXY(t *testing.T) {
	f1 := NewFrame(Vec{1, 1, 1}, NegZ, PosX, NegY)

	want := Vec{2, 1, 0}
	got := f1.PlotXY(Vec2{1, 1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_View(t *testing.T) {
	f1 := NewFrame()
	target := Vec{1, 1, 0}
	f2 := f1.View(target)
	want := Frame{Vec{0, 0, 0}, Basis{PosZ, Vec{-1, 1, 0}.Unit(), Vec{-1, -1, 0}.Unit()}}
	got := f2
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	f1.O = Vec{1, 1, 0}
	target = Vec{-1, -1, 0}
	f2 = f1.View(target)
	want = Frame{Vec{1, 1, 0}, Basis{NegZ, Vec{-1, 1, 0}.Unit(), Vec{1, 1, 0}.Unit()}}
	got = f2
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	target = Vec{1, 3, 0}
	f2 = f1.View(target)
	want = Frame{Vec{1, 1, 0}, Basis{PosX, PosZ, NegY}}
	got = f2
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	target = PosX
	f2 = f1.View(target)
	want = Frame{Vec{1, 1, 0}, Basis{NegX, PosZ, PosY}}
	got = f2
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewFrame(t *testing.T) {
	want := Frame{Vec{0, 0, 0}, StdBasis}
	got := NewFrame()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Frame{Vec{1, 1, 1}, StdBasis}
	got = NewFrame(Vec{1, 1, 1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Frame{Vec{1, 1, 1}, Basis{NegX, PosY, PosZ}}
	got = NewFrame(Vec{1, 1, 1}, NegX)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Frame{Vec{1, 1, 1}, Basis{NegX, PosY, NegZ}}
	got = NewFrame(Vec{1, 1, 1}, NegX, PosY)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Frame{Vec{1, 1, 1}, Basis{NegZ, PosY, PosX}}
	got = NewFrame(Vec{1, 1, 1}, NegZ, PosY, PosX)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_Global(t *testing.T) {
	a := NewRot(Line{P: PosX, Dir: PosY}, math.Pi/2)
	got := Frame{Vec{1, 1, 1}, StdBasis}.Global(a)
	want := Frame{Vec{2, 1, 0}, Basis{NegZ, PosY, PosX}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	a = NewXRot(math.Pi / 2)
	got = Frame{Vec{1, 1, 1}, StdBasis}.Global(a)
	want = Frame{Vec{1, -1, 1}, Basis{PosX, PosZ, NegY}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestBasis_Local(t *testing.T) {
	a := NewXRot(HalfPi)
	got := StdBasis.Local(a)
	want := Basis{PosX, PosZ, NegY}
	if !CompareBasis(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	a = NewYRot(HalfPi).Multiply(NewXRot(HalfPi))
	got = StdBasis.Local(a)
	want = Basis{NegZ, PosX, NegY}
	if !CompareBasis(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	{
		want := Vec{1, -1, -1}
		got := a.Transform(Vec{1, 1, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	aff1, aff2 := NewXRot(HalfPi), NewYRot(HalfPi)
	b1, b2 := StdBasis.Local(aff1), StdBasis.Local(aff2)
	aff := b2.In().Multiply(b1.In()) // A change of basis transformation

	{
		want := Vec{1, 1, 1}
		got := aff.Transform(Vec{1, 1, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		want = Vec{1, -1, 0}
		got = aff.Transform(Vec{0, 1, -1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestFrame_Local(t *testing.T) {
	a := NewRot(Line{P: PosX, Dir: PosY}, math.Pi/2)
	got := Frame{Vec{1, 1, 1}, StdBasis}.Local(a)
	want := Frame{Vec{2, 1, 2}, Basis{NegZ, PosY, PosX}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	a = NewXRot(math.Pi / 2)
	got = Frame{Vec{1, 1, 1}, StdBasis}.Local(a)
	want = Frame{Vec{1, 1, 1}, Basis{PosX, PosZ, NegY}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalRot(t *testing.T) {
	l := Line{P: Vec{2, 0, 0}, Dir: PosY}
	got := Frame{Vec{2, 1, 2}, StdBasis}.LocalRot(l, HalfPi)
	want := Frame{Vec{4, 1, 4}, Basis{NegZ, PosY, PosX}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalRotBasis(t *testing.T) {
	got := Frame{Vec{2, 1, 2}, StdBasis}.LocalRotBasis(PosX, HalfPi)
	want := Frame{Vec{2, 1, 2}, Basis{PosX, PosZ, NegY}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalRotNegX(t *testing.T) {
	got := Frame{Vec{2, 1, 2}, StdBasis}.LocalRotNegX(HalfPi)
	want := Frame{Vec{2, 1, 2}, Basis{PosX, NegZ, PosY}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalRotNegY(t *testing.T) {
	got := Frame{Vec{2, 1, 2}, StdBasis}.LocalRotNegY(HalfPi)
	want := Frame{Vec{2, 1, 2}, Basis{PosZ, PosY, NegX}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalRotNegZ(t *testing.T) {
	got := Frame{Vec{2, 1, 2}, StdBasis}.LocalRotNegZ(HalfPi)
	want := Frame{Vec{2, 1, 2}, Basis{NegY, PosX, PosZ}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalRotPosX(t *testing.T) {
	got := Frame{Vec{2, 1, 2}, StdBasis}.LocalRotPosX(HalfPi)
	want := Frame{Vec{2, 1, 2}, Basis{PosX, PosZ, NegY}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalRotPosY(t *testing.T) {
	got := Frame{Vec{2, 1, 2}, StdBasis}.LocalRotPosY(HalfPi)
	want := Frame{Vec{2, 1, 2}, Basis{NegZ, PosY, PosX}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalRotPosZ(t *testing.T) {
	got := Frame{Vec{2, 1, 2}, StdBasis}.LocalRotPosZ(HalfPi)
	want := Frame{Vec{2, 1, 2}, Basis{PosY, NegX, PosZ}}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDis(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDis(Vec{X: 1, Y: 1, Z: 1})
	want := Frame{Vec{3, 2, 1}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisNegX(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisNegX(1)
	want := Frame{Vec{2, 1, 3}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisNegY(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisNegY(1)
	want := Frame{Vec{2, 0, 2}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisNegZ(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisNegZ(1)
	want := Frame{Vec{1, 1, 2}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisPosX(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisPosX(1)
	want := Frame{Vec{2, 1, 1}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisPosY(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisPosY(1)
	want := Frame{Vec{2, 2, 2}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisPosZ(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisPosZ(1)
	want := Frame{Vec{3, 1, 2}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisNegXNegZ(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisNegXNegZ(math.Sqrt2)
	want := Frame{Vec{1, 1, 3}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisNegXPosZ(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisNegXPosZ(math.Sqrt2)
	want := Frame{Vec{3, 1, 3}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisPosXNegZ(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisPosXNegZ(math.Sqrt2)
	want := Frame{Vec{1, 1, 1}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestFrame_LocalDisPosXPosZ(t *testing.T) {
	b := Basis{NegZ, PosY, PosX}
	got := Frame{Vec{2, 1, 2}, b}.LocalDisPosXPosZ(math.Sqrt2)
	want := Frame{Vec{3, 1, 1}, b}
	if !CompareFrame(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
