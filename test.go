package maf

import (
	"fmt"
	"strconv"

	"gitlab.com/fospathi/universal/tezd"
)

// CompareAff returns whether two affine transformations are approximately
// equal in an element-wise comparison.
func CompareAff(a1, a2 Aff, precision int) bool {
	return tezd.CompareFloat64s(a1[:], a2[:], precision)
}

// CompareBasis returns whether two basis' are approximately equal.
func CompareBasis(b1, b2 Basis, precision int) bool {
	return CompareVec(b1.X, b2.X, precision) &&
		CompareVec(b1.Y, b2.Y, precision) &&
		CompareVec(b1.Z, b2.Z, precision)
}

// CompareComplex returns whether two complex numbers are approximately equal.
func CompareComplex(c1, c2 complex128, precision int) bool {
	return tezd.CompareFloat64(real(c1), real(c2), precision) &&
		tezd.CompareFloat64(imag(c1), imag(c2), precision)
}

// CompareFrame returns whether two frames are approximately equal.
func CompareFrame(f1, f2 Frame, precision int) bool {
	return CompareVec(f1.O, f2.O, precision) &&
		CompareVec(f1.Axes.X, f2.Axes.X, precision) &&
		CompareVec(f1.Axes.Y, f2.Axes.Y, precision) &&
		CompareVec(f1.Axes.Z, f2.Axes.Z, precision)
}

// CompareLine reports whether two lines approximately coincide.
func CompareLine(l1, l2 Line, tolerance float64, precision int) bool {
	if !l1.IsParallel(l2, tolerance) {
		return false
	}
	if CompareVec(l1.P, l2.P, precision) {
		return true
	}
	var k float64 // Assume there is a k such that l2.P = l1.P + (k * l1.Dir).
	switch {
	case !tezd.CompareFloat64(0, l1.Dir.X, 5):
		k = (l2.P.X - l1.P.X) / l1.Dir.X
	case !tezd.CompareFloat64(0, l1.Dir.Y, 5):
		k = (l2.P.Y - l1.P.Y) / l1.Dir.Y
	case !tezd.CompareFloat64(0, l1.Dir.Z, 5):
		k = (l2.P.Z - l1.P.Z) / l1.Dir.Z
	}
	return CompareVec(l1.P.Add(l1.Dir.Scale(k)), l2.P, precision)
}

// CompareMat4 returns whether two matrices are approximately equal.
func CompareMat4(m1, m2 Mat4, precision int) bool {
	return tezd.CompareFloat64s(m1[:], m2[:], precision)
}

// CompareSeg reports whether two line segments are approximately equal and have
// the same direction.
func CompareSeg(s1, s2 LineSeg, precision int) bool {
	return CompareVec(s1.P1, s2.P1, precision) &&
		CompareVec(s1.P2, s2.P2, precision)
}

// CompareOrderedSegs reports whether two slices contain, in the same order,
// approximately equal line segments with the same directions.
func CompareOrderedSegs(s1, s2 []LineSeg, precision int) bool {
	if l := len(s1); l == len(s2) {
		for i := 0; i < l; i++ {
			if !CompareSeg(s1[i], s2[i], precision) {
				return false
			}
		}
		return true
	}
	return false
}

// CompareTriangle reports whether the given triangles approximately coincide.
func CompareTriangle(t1, t2 Triangle, precision int) bool {
	t1.A = RoundVec(t1.A, precision)
	t1.B = RoundVec(t1.B, precision)
	t1.C = RoundVec(t1.C, precision)
	t2.A = RoundVec(t2.A, precision)
	t2.B = RoundVec(t2.B, precision)
	t2.C = RoundVec(t2.C, precision)
	return t1.Coincide(t2)
}

// CompareTriangles reports whether two vector slices contain in any order the
// same approximately coinciding triangles.
func CompareTriangles(v1, v2 []Triangle, precision int) bool {
	if len(v1) == len(v2) {
		for i := 0; i < len(v1); i++ {
			matched := false
			for j := 0; j < len(v2); j++ {
				if CompareTriangle(v1[i], v2[j], precision) {
					v1 = append(v1[:i], v1[i+1:]...)
					v2 = append(v2[:j], v2[j+1:]...)
					matched = true
					i--
					break
				}
			}
			if !matched {
				return false
			}
		}
		return true
	}
	return false
}

// CompareOrderedTriangle reports whether the given triangles approximately
// coincide and label the vertices the same way.
func CompareOrderedTriangle(t1, t2 Triangle, precision int) bool {
	t1.A = RoundVec(t1.A, precision)
	t1.B = RoundVec(t1.B, precision)
	t1.C = RoundVec(t1.C, precision)
	t2.A = RoundVec(t2.A, precision)
	t2.B = RoundVec(t2.B, precision)
	t2.C = RoundVec(t2.C, precision)
	return t1 == t2
}

// CompareVec returns whether two vectors are approximately equal.
func CompareVec(v1, v2 Vec, precision int) bool {
	return tezd.CompareFloat64(v1.X, v2.X, precision) &&
		tezd.CompareFloat64(v1.Y, v2.Y, precision) &&
		tezd.CompareFloat64(v1.Z, v2.Z, precision)
}

// CompareVec2 returns whether two vectors are approximately equal.
func CompareVec2(v1, v2 Vec2, precision int) bool {
	return tezd.CompareFloat64(v1.X, v2.X, precision) &&
		tezd.CompareFloat64(v1.Y, v2.Y, precision)
}

// CompareVecs returns whether two vector slices contain in any order the same
// approximately equal vectors.
func CompareVecs(v1, v2 []Vec, precision int) bool {
	if len(v1) == len(v2) {
		for i := 0; i < len(v1); i++ {
			matched := false
			for j := 0; j < len(v2); j++ {
				if CompareVec(v1[i], v2[j], precision) {
					v1 = append(v1[:i], v1[i+1:]...)
					v2 = append(v2[:j], v2[j+1:]...)
					matched = true
					i--
					break
				}
			}
			if !matched {
				return false
			}
		}
		return true
	}
	return false
}

// CompareOrderedVecs returns whether two vector slices contain, in the same
// order, approximately equal vectors.
func CompareOrderedVecs(v1, v2 []Vec, precision int) bool {
	if l := len(v1); l == len(v2) {
		for i := 0; i < l; i++ {
			if !CompareVec(v1[i], v2[i], precision) {
				return false
			}
		}
		return true
	}
	return false
}

// Round to the given number of sig figs after the decimal point.
func Round(f float64, precision int) float64 {
	s := fmt.Sprintf("%.*f", precision, f)
	r, _ := strconv.ParseFloat(s, 64)
	return r
}

// RoundVec components to the given number of sig figs after the decimal point.
func RoundVec(v Vec, precision int) Vec {
	return Vec{
		X: Round(v.X, precision),
		Y: Round(v.Y, precision),
		Z: Round(v.Z, precision),
	}
}
