package maf

import (
	"math"
	"testing"
)

func BenchmarkAff_Multiply(b *testing.B) {
	left := Aff{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
	right := Aff{13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24}
	for n := 0; n < b.N; n++ {
		left.Multiply(right)
	}
}

func BenchmarkMat4_Multiply(b *testing.B) {
	left := Mat4{1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15, 4, 8, 12, 16}
	right := Mat4{17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32}
	for n := 0; n < b.N; n++ {
		left.Multiply(right)
	}
}

func BenchmarkNewRot(b *testing.B) {
	angle := math.Pi / 2
	line := Line{P: Vec{0, 0, 1}, Dir: Vec{0, 1, -1}.Unit()}
	for n := 0; n < b.N; n++ {
		NewRot(line, angle)
	}
}

func BenchmarkNewRot2(b *testing.B) {
	angle := math.Pi / 2
	line := Line{P: Vec{0, 0, 1}, Dir: Vec{0, 1, -1}.Unit()}
	for n := 0; n < b.N; n++ {
		newRot2(line, angle)
	}
}

func TestMat3_Det(t *testing.T) {
	m := Mat3{1, 4, 7, 2, 5, 8, 3, 6, 9}
	want := 0.0
	got := m.Det()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	m = Mat3{0, 4, 7, 2, 5, 8, 3, 6, 9}
	want = 3.0
	got = m.Det()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestMat4_Det(t *testing.T) {
	m := Mat4{1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15, 4, 8, 12, 16}
	want := 0.0
	got := m.Det()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	m = Mat4{1, 5, 9, 0, 2, 6, 0, 14, 3, 7, 11, 15, 4, 8, 12, 16}
	want = 520
	got = m.Det()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	m = Mat4{1, 5, 9, 0, 2, 6, 0, 14, 3, 0, 11, 15, 0, 8, 12, 16}
	want = -5464
	got = m.Det()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestAff_Inverse(t *testing.T) {
	// Translation.
	tr := NewTranslation(Vec{X: 1, Y: -1, Z: 1})
	want := AffIdentity
	got := tr.Inverse().Multiply(tr)
	if !CompareAff(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Rotations.
	rot := NewYRot(HalfPi)
	got = rot.Inverse().Multiply(rot)
	if !CompareAff(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	rot = NewZRot(HalfPi)
	got = rot.Inverse().Multiply(rot)
	if !CompareAff(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	rot = NewRot(
		Line{P: Vec{X: 1, Y: 0, Z: 0}, Dir: Vec{X: 1, Y: 1, Z: 1}},
		QuartPi,
	)
	got = rot.Inverse().Multiply(rot)
	if !CompareAff(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Compound.
	com := NewTranslation(Vec{X: 1, Y: -1, Z: 1}).
		Multiply(NewYRot(HalfPi)).
		Multiply(NewZRot(QuartPi))
	got = com.Inverse().Multiply(com)
	if !CompareAff(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestAff_Multiply(t *testing.T) {
	left := Aff{1, 5, 9, 2, 6, 10, 3, 7, 11, 4, 8, 12}
	right := Aff{10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21}
	want := Aff{68, 200, 332, 86, 254, 422, 104, 308, 512, 126, 370, 614}
	got := left.Multiply(right)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestMat3_Multiply(t *testing.T) {
	left := Mat3{1, 4, 7, 2, 5, 8, 3, 6, 9}
	right := Mat3{10, 11, 12, 13, 14, 15, 16, 17, 18}
	want := Mat3{68, 167, 266, 86, 212, 338, 104, 257, 410}
	got := left.Multiply(right)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestMat4_Multiply(t *testing.T) {
	left := Mat4{1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15, 4, 8, 12, 16}
	right := Mat4{17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32}
	want := Mat4{190, 486, 782, 1078, 230, 590, 950, 1310, 270, 694, 1118, 1542, 310, 798, 1286, 1774}
	got := left.Multiply(right)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestAff_Transform(t *testing.T) {
	m := Aff{1, 5, 9, 2, 6, 10, 3, 7, 11, 4, 8, 12}
	v := Vec{1, 2, 3}
	want := Vec{18, 46, 74}
	got := m.Transform(v)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestAff_TransformFrame(t *testing.T) {
	m := NewTranslation(Vec{X: 1, Y: 1, Z: 1}).Multiply(NewXRot(HalfPi))
	want := Frame{
		O: Vec{X: 1, Y: 1, Z: 1},
		Axes: Basis{
			X: Vec{X: 1, Y: 0, Z: 0},
			Y: Vec{X: 0, Y: 0, Z: 1},
			Z: Vec{X: 0, Y: -1, Z: 0},
		},
	}
	got := m.TransformFrame(StdFrame)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestMat3_Transform(t *testing.T) {
	m := Mat3{1, 4, 7, 2, 5, 8, 3, 6, 9}
	v := Vec{1, 2, 3}
	want := Vec{14, 32, 50}
	got := m.Transform(v)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestMat4_Transform(t *testing.T) {
	m := Mat4{1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15, 4, 8, 12, 16}
	v := Vec4{1, 2, 3, 4}
	want := Vec4{30, 70, 110, 150}
	got := m.Transform(v)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestMat3_Transpose(t *testing.T) {
	want := Mat3{1, 2, 3, 4, 5, 6, 7, 8, 9}
	got := Mat3{1, 4, 7, 2, 5, 8, 3, 6, 9}.Transpose()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewXRot(t *testing.T) {
	angle := math.Pi / 4
	p1 := Vec{0, 1, 0}
	a := NewXRot(angle)

	want := Vec{0, 1 / math.Sqrt2, 1 / math.Sqrt2}
	got := a.Transform(p1)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	p2 := got
	a = NewXRot(-angle)

	want = p1
	got = a.Transform(p2)

	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewYRot(t *testing.T) {
	angle := math.Pi / 4
	p1 := Vec{0, 0, 1}
	a := NewYRot(angle)

	want := Vec{1 / math.Sqrt2, 0, 1 / math.Sqrt2}
	got := a.Transform(p1)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	p2 := got
	a = NewYRot(-angle)

	want = p1
	got = a.Transform(p2)

	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewZRot(t *testing.T) {
	angle := math.Pi / 4
	p1 := Vec{1, 0, 0}
	a := NewZRot(angle)

	want := Vec{1 / math.Sqrt2, 1 / math.Sqrt2, 0}
	got := a.Transform(p1)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	p2 := got
	a = NewZRot(-angle)

	want = p1
	got = a.Transform(p2)

	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewRot(t *testing.T) {
	var angle float64
	var line Line
	var a Aff
	var want, got Vec
	piOver6 := math.Pi / 6
	root3Over2 := math.Sqrt(3) / 2

	func() {
		// Point on line

		angle = math.Pi / 2
		line = Line{P: Vec{0, 0, 1}, Dir: Vec{0, 1, -1}.Unit()}
		a = NewRot(line, angle)
		want = Vec{0, 0, 1}
		got = a.Transform(Vec{0, 0, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		want = Vec{0, 0, 1}.Add(line.Dir)
		got = a.Transform(Vec{0, 0, 1}.Add(line.Dir))
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}()

	func() {
		// Arbitrary line

		angle = math.Pi / 2
		line = Line{P: Vec{0, 0, 1}, Dir: Vec{0, 1, -1}.Unit()}
		a = NewRot(line, angle)
		want = Vec{1, 0, 1}
		got = a.Transform(Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit()))
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit())
		got = a.Transform(Vec{1, 0, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = math.Pi
		a = NewRot(line, angle)
		want = Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit().Scale(-1))
		got = a.Transform(Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit()))
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		want = Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit())
		got = a.Transform(Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit().Scale(-1)))
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = math.Pi / 6
		line = Line{P: Vec{3, 3, 3}, Dir: Vec{-1, -1, -1}.Unit()}
		a = NewRot(line, angle)
		want = Vec{0.9107, -0.244, 0.3333}
		got = a.Transform(Vec{1, 0, 0})
		if !CompareVec(want, got, 3) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{1, 0, 0}
		got = a.Transform(Vec{0.9107, -0.244, 0.3333})
		if !CompareVec(want, got, 3) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = math.Pi / 6
		line = Line{P: Vec{3, 2, 1}, Dir: Vec{1, -1, -1}.Unit()}
		a = NewRot(line, angle)
		want = Vec{1.0239, 1.0893, -1.0654}
		got = a.Transform(Vec{1, 0, 0})
		if !CompareVec(want, got, 3) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{1, 0, 0}
		got = a.Transform(Vec{1.0239, 1.0893, -1.0654})
		if !CompareVec(want, got, 3) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}()

	func() {
		// Parallel to Z-axis

		angle = piOver6
		line = Line{P: Vec{0, 0, 0}, Dir: Vec{0, 0, 1}}
		a = NewRot(line, angle)
		want = Vec{root3Over2, 0.5, 1}
		got = a.Transform(Vec{1, 0, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{1, 0, 1}
		got = a.Transform(Vec{root3Over2, 0.5, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = 5 * piOver6
		a = NewRot(line, angle)
		want = Vec{-root3Over2, 0.5, 1}
		got = a.Transform(Vec{1, 0, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{1, 0, 1}
		got = a.Transform(Vec{-root3Over2, 0.5, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = piOver6
		line = Line{P: Vec{0, 0, 0}, Dir: Vec{0, 0, -1}}
		a = NewRot(line, angle)
		want = Vec{root3Over2, -0.5, 1}
		got = a.Transform(Vec{1, 0, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{1, 0, 1}
		got = a.Transform(Vec{root3Over2, -0.5, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}()

	func() {
		// Parallel to Y-axis

		angle = piOver6
		line = Line{P: Vec{0, 0, 0}, Dir: Vec{0, 1, 0}}
		a = NewRot(line, angle)
		want = Vec{0.5, 1, root3Over2}
		got = a.Transform(Vec{0, 1, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{0, 1, 1}
		got = a.Transform(Vec{0.5, 1, root3Over2})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = 5 * piOver6
		a = NewRot(line, angle)
		want = Vec{0.5, 1, -root3Over2}
		got = a.Transform(Vec{0, 1, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{0, 1, 1}
		got = a.Transform(Vec{0.5, 1, -root3Over2})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = piOver6
		line = Line{P: Vec{0, 0, 0}, Dir: Vec{0, -1, 0}}
		a = NewRot(line, angle)
		want = Vec{-0.5, 1, root3Over2}
		got = a.Transform(Vec{0, 1, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{0, 1, 1}
		got = a.Transform(Vec{-0.5, 1, root3Over2})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}()

	func() {
		// Parallel to X-axis

		angle = piOver6
		line = Line{P: Vec{0, 0, 0}, Dir: Vec{1, 0, 0}}
		a = NewRot(line, angle)
		want = Vec{1, -0.5, root3Over2}
		got = a.Transform(Vec{1, 0, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{1, 0, 1}
		got = a.Transform(Vec{1, -0.5, root3Over2})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = 5 * piOver6
		a = NewRot(line, angle)
		want = Vec{1, -0.5, -root3Over2}
		got = a.Transform(Vec{1, 0, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{1, 0, 1}
		got = a.Transform(Vec{1, -0.5, -root3Over2})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = piOver6
		line = Line{P: Vec{0, 0, 0}, Dir: Vec{-1, 0, 0}}
		a = NewRot(line, angle)
		want = Vec{1, 0.5, root3Over2}
		got = a.Transform(Vec{1, 0, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{1, 0, 1}
		got = a.Transform(Vec{1, 0.5, root3Over2})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = piOver6
		line = Line{P: Vec{2, 2, 2}, Dir: Vec{1, 0, 0}}
		a = NewRot(line, angle)
		want = Vec{-3, 2 + 0.5, 2 - root3Over2}
		got = a.Transform(Vec{-3, 2, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = NewRot(line, angle)
		want = Vec{-3, 2, 1}
		got = a.Transform(Vec{-3, 2 + 0.5, 2 - root3Over2})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}()
}

func TestNewRot2(t *testing.T) {
	var angle float64
	var line Line
	var a Aff
	var want, got Vec

	func() {
		// Arbitrary line

		angle = math.Pi / 2
		line = Line{P: Vec{0, 0, 1}, Dir: Vec{0, 1, -1}.Unit()}
		a = newRot2(line, angle)
		want = Vec{1, 0, 1}
		got = a.Transform(Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit()))
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = newRot2(line, angle)
		want = Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit())
		got = a.Transform(Vec{1, 0, 1})
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = math.Pi
		a = newRot2(line, angle)
		want = Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit().Scale(-1))
		got = a.Transform(Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit()))
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		want = Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit())
		got = a.Transform(Vec{0, 0, 1}.Add(Vec{0, 1, 1}.Unit().Scale(-1)))
		if !CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = math.Pi / 6
		line = Line{P: Vec{3, 3, 3}, Dir: Vec{-1, -1, -1}.Unit()}
		a = newRot2(line, angle)
		want = Vec{0.9107, -0.244, 0.3333}
		got = a.Transform(Vec{1, 0, 0})
		if !CompareVec(want, got, 3) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = newRot2(line, angle)
		want = Vec{1, 0, 0}
		got = a.Transform(Vec{0.9107, -0.244, 0.3333})
		if !CompareVec(want, got, 3) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = math.Pi / 6
		line = Line{P: Vec{3, 2, 1}, Dir: Vec{1, -1, -1}.Unit()}
		a = newRot2(line, angle)
		want = Vec{1.0239, 1.0893, -1.0654}
		got = a.Transform(Vec{1, 0, 0})
		if !CompareVec(want, got, 3) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		angle = -angle
		a = newRot2(line, angle)
		want = Vec{1, 0, 0}
		got = a.Transform(Vec{1.0239, 1.0893, -1.0654})
		if !CompareVec(want, got, 3) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}()
}

func TestNewXZRefl(t *testing.T) {
	a := NewXZRefl()
	want := Vec{-1, 0, -1}
	got := a.Transform(Vec{-1, 0, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{-1, -2, -1}
	got = a.Transform(Vec{-1, 2, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{2, -1, -1}
	got = a.Transform(Vec{2, 1, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewYZRef(t *testing.T) {
	a := NewYZRefl()
	want := Vec{0, -1, -1}
	got := a.Transform(Vec{0, -1, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{-2, -1, -1}
	got = a.Transform(Vec{2, -1, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{2, -1, -1}
	got = a.Transform(Vec{-2, -1, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	a = NewYZReflAt(3)
	want = Vec{3, -1, -1}
	got = a.Transform(Vec{3, -1, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{0, -1, -1}
	got = a.Transform(Vec{6, -1, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{7, -1, -1}
	got = a.Transform(Vec{-1, -1, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestNewRef(t *testing.T) {

	// Reflection in a YZ plane
	yz := Plane{P: Vec{X: 0, Y: 0, Z: 0}, Normal: Vec{X: 1, Y: 0, Z: 0}}
	a := NewRefl(yz)
	want := Vec{0, -1, -1}
	got := a.Transform(Vec{0, -1, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{-2, -1, -1}
	got = a.Transform(Vec{2, -1, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	yz = Plane{P: Vec{X: 0, Y: 0, Z: 0}, Normal: Vec{X: -1, Y: 0, Z: 0}}
	a = NewRefl(yz)
	want = Vec{0, -1, -1}
	got = a.Transform(Vec{0, -1, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{-2, -1, -1}
	got = a.Transform(Vec{2, -1, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Reflection in a ZX plane
	zx := Plane{P: Vec{X: 2, Y: 2, Z: 2}, Normal: Vec{X: 0, Y: 1, Z: 0}}
	a = NewRefl(zx)
	want = Vec{-1, 2, -1}
	got = a.Transform(Vec{-1, 2, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{-1, 5, -1}
	got = a.Transform(Vec{-1, -1, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{-1, -1, -1}
	got = a.Transform(Vec{-1, 5, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	zx = Plane{P: Vec{X: 2, Y: 2, Z: 2}, Normal: Vec{X: 0, Y: -1, Z: 0}}
	a = NewRefl(zx)
	want = Vec{-1, 2, -1}
	got = a.Transform(Vec{-1, 2, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{-1, 5, -1}
	got = a.Transform(Vec{-1, -1, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{-1, -1, -1}
	got = a.Transform(Vec{-1, 5, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Reflection in an XY plane
	xy := Plane{P: Vec{X: -2, Y: -2, Z: -2}, Normal: Vec{X: 0, Y: 0, Z: 1}}
	a = NewRefl(xy)
	want = Vec{1, 1, -2}
	got = a.Transform(Vec{1, 1, -2})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{1, 1, -5}
	got = a.Transform(Vec{1, 1, 1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{1, 1, 1}
	got = a.Transform(Vec{1, 1, -5})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	xy = Plane{P: Vec{X: -2, Y: -2, Z: -2}, Normal: Vec{X: 0, Y: 0, Z: -1}}
	a = NewRefl(xy)
	want = Vec{1, 1, -2}
	got = a.Transform(Vec{1, 1, -2})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{1, 1, -5}
	got = a.Transform(Vec{1, 1, 1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{1, 1, 1}
	got = a.Transform(Vec{1, 1, -5})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Reflection in an arbitrary plane
	p := Plane{P: Vec{X: 1, Y: 1, Z: 1}, Normal: Vec{X: 1, Y: 1, Z: 1}.Unit()}
	a = NewRefl(p)
	want = Vec{1, 1, 1}
	got = a.Transform(Vec{1, 1, 1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{0, 0, 0}
	got = a.Transform(Vec{2, 2, 2})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{2, 2, 2}
	got = a.Transform(Vec{0, 0, 0})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	p = Plane{P: Vec{X: 1, Y: 1, Z: 1}, Normal: Vec{X: -1, Y: -1, Z: -1}.Unit()}
	a = NewRefl(p)
	want = Vec{1, 1, 1}
	got = a.Transform(Vec{1, 1, 1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{0, 0, 0}
	got = a.Transform(Vec{2, 2, 2})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	want = Vec{2, 2, 2}
	got = a.Transform(Vec{0, 0, 0})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
