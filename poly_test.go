package maf

import (
	"testing"

	"gitlab.com/fospathi/universal/tezd"
)

func TestPoly_AtX(t *testing.T) {
	p := Poly{1, 2, 3}

	want := 2.0
	got := p.At(-1)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 1.0
	got = p.At(0)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 6.0
	got = p.At(1)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestPoly_Derivative(t *testing.T) {
	p := Poly{1, 2, 3}

	want := Poly{2, 6}
	got := p.Derivative()
	if !tezd.CompareFloat64s(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Poly{0}
	got = Poly{3}.Derivative()
	if !tezd.CompareFloat64s(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	got = Poly{}.Derivative()
	if !tezd.CompareFloat64s(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestPoly_Integral(t *testing.T) {
	acceleration := Poly{6, -6, 12}
	velocity := acceleration.Integral(-8, 0)
	displacement := velocity.Integral(4, 0)

	want := Poly{-8, 6, -3, 4}
	got := velocity
	if !tezd.CompareFloat64s(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Poly{4, -8, 3, -1, 1}
	got = displacement
	if !tezd.CompareFloat64s(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Poly{4}
	got = Poly{}.Integral(4, 0)
	if !tezd.CompareFloat64s(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
