package maf

import (
	"fmt"
	"math"
)

// Aff is a 3x4 affine transformation matrix.
// Elements are stored in column order.
type Aff [12]float64

// Mat2 is a square 2x2 matrix.
// Elements are stored in column order.
type Mat2 [4]float64

// Mat3 is a square 3x3 matrix.
// Elements are stored in column order.
type Mat3 [9]float64

// Mat4 is a square 4x4 matrix.
// Elements are stored in column order.
type Mat4 [16]float64

// OpenGLMat4 is a square 4x4 matrix with openGL sized floats.
// Elements are stored in column order.
type OpenGLMat4 [16]float32

// OpenGLMat4x3 is a 4x3 matrix (4 columns and 3 rows) with openGL sized floats.
// Elements are stored in column order.
type OpenGLMat4x3 [12]float32

// CSS transform property value.
//
// https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/matrix3d.
func (a1 Aff) CSS() string {
	return fmt.Sprintf(
		" matrix3d("+
			"%v, %v, %v, 0, "+
			"%v, %v, %v, 0, "+
			"%v, %v, %v, 0, "+
			"%v, %v, %v, 1) ",
		a1[0], a1[1], a1[2],
		a1[3], a1[4], a1[5],
		a1[6], a1[7], a1[8],
		a1[9], a1[10], a1[11],
	)
}

// CSS transform property value.
//
// https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/matrix3d.
func (m1 Mat3) CSS() string {
	return fmt.Sprintf(
		" matrix3d("+
			"%v, %v, %v, 0, "+
			"%v, %v, %v, 0, "+
			"%v, %v, %v, 0, "+
			"0, 0, 0, 1) ",
		m1[0], m1[1], m1[2],
		m1[3], m1[4], m1[5],
		m1[6], m1[7], m1[8],
	)
}

// CSS transform property value.
//
// https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/matrix3d.
func (m1 Mat4) CSS() string {
	return fmt.Sprintf(
		" matrix3d("+
			"%v, %v, %v, %v, "+
			"%v, %v, %v, %v, "+
			"%v, %v, %v, %v, "+
			"%v, %v, %v, %v) ",
		m1[0], m1[1], m1[2], m1[3],
		m1[4], m1[5], m1[6], m1[7],
		m1[8], m1[9], m1[10], m1[11],
		m1[12], m1[13], m1[14], m1[15],
	)
}

// Det is the determinant.
func (m1 Mat3) Det() float64 {
	// 0 3 6
	// 1 4 7
	// 2 5 8
	var ( // Co-factors.
		c0 = +(m1[4]*m1[8] - m1[5]*m1[7])
		c3 = -(m1[1]*m1[8] - m1[2]*m1[7])
		c6 = +(m1[1]*m1[5] - m1[2]*m1[4])
	)
	return m1[0]*c0 + m1[3]*c3 + m1[6]*c6
}

// Det is the determinant.
func (m1 Mat4) Det() float64 {
	return m1[1]*m1[11]*m1[14]*m1[4] - m1[1]*m1[10]*m1[15]*m1[4] -
		m1[11]*m1[13]*m1[2]*m1[4] + m1[10]*m1[13]*m1[3]*m1[4] -
		m1[0]*m1[11]*m1[14]*m1[5] + m1[0]*m1[10]*m1[15]*m1[5] +
		m1[11]*m1[12]*m1[2]*m1[5] - m1[10]*m1[12]*m1[3]*m1[5] -
		m1[1]*m1[11]*m1[12]*m1[6] + m1[0]*m1[11]*m1[13]*m1[6] +
		m1[1]*m1[10]*m1[12]*m1[7] - m1[0]*m1[10]*m1[13]*m1[7] -
		m1[15]*m1[2]*m1[5]*m1[8] + m1[14]*m1[3]*m1[5]*m1[8] +
		m1[1]*m1[15]*m1[6]*m1[8] - m1[13]*m1[3]*m1[6]*m1[8] -
		m1[1]*m1[14]*m1[7]*m1[8] + m1[13]*m1[2]*m1[7]*m1[8] +
		m1[15]*m1[2]*m1[4]*m1[9] - m1[14]*m1[3]*m1[4]*m1[9] -
		m1[0]*m1[15]*m1[6]*m1[9] + m1[12]*m1[3]*m1[6]*m1[9] +
		m1[0]*m1[14]*m1[7]*m1[9] - m1[12]*m1[2]*m1[7]*m1[9]
}

// Inverse of the matrix.
//
// If the inverse does not exist the result is undefined.
func (a1 Aff) Inverse() Aff {
	// https://en.wikipedia.org/wiki/Affine_transformation#Groups

	ai := Mat3(a1[:9]).Inverse()
	b := ai.Transform(Vec{X: a1[9], Y: a1[10], Z: a1[11]}).Scale(-1)
	return Aff{
		ai[0], ai[1], ai[2],
		ai[3], ai[4], ai[5],
		ai[6], ai[7], ai[8],
		b.X, b.Y, b.Z,
	}
}

// Inverse of the matrix.
//
// If the inverse does not exist, return the identity matrix.
func (m1 Mat3) Inverse() Mat3 {
	// https://www.mathcentre.ac.uk/resources/uploaded/sigma-matrices11-2009-1.pdf

	// 0 3 6
	// 1 4 7
	// 2 5 8
	var ( // Co-factors.
		c0 = +(m1[4]*m1[8] - m1[5]*m1[7])
		c1 = -(m1[3]*m1[8] - m1[5]*m1[6])
		c2 = +(m1[3]*m1[7] - m1[4]*m1[6])
		c3 = -(m1[1]*m1[8] - m1[2]*m1[7])
		c4 = +(m1[0]*m1[8] - m1[2]*m1[6])
		c5 = -(m1[0]*m1[7] - m1[1]*m1[6])
		c6 = +(m1[1]*m1[5] - m1[2]*m1[4])
		c7 = -(m1[0]*m1[5] - m1[2]*m1[3])
		c8 = +(m1[0]*m1[4] - m1[1]*m1[3])
	)

	d := (m1[0]*c0 + m1[3]*c3 + m1[6]*c6)
	if d == 0 {
		return Mat3{
			1, 0, 0,
			0, 1, 0,
			0, 0, 1,
		}
	}

	s := 1 / (m1[0]*c0 + m1[3]*c3 + m1[6]*c6)

	// Scaled transpose of the adjoint matrix.
	return Mat3{
		s * c0, s * c3, s * c6,
		s * c1, s * c4, s * c7,
		s * c2, s * c5, s * c8,
	}
}

// Multiply multiplies its receiver matrix with the argument matrix and returns
// the resulting matrix. The receiver matrix is the left hand side operand of
// the matrix multiplication. The argument matrix is the right hand side operand
// of the matrix multiplication.
func (a1 Aff) Multiply(a2 Aff) (affine Aff) {
	return Aff{a1[0]*a2[0] + a1[3]*a2[1] + a1[6]*a2[2],
		a1[1]*a2[0] + a1[4]*a2[1] + a1[7]*a2[2],
		a1[2]*a2[0] + a1[5]*a2[1] + a1[8]*a2[2],
		a1[0]*a2[3] + a1[3]*a2[4] + a1[6]*a2[5],
		a1[1]*a2[3] + a1[4]*a2[4] + a1[7]*a2[5],
		a1[2]*a2[3] + a1[5]*a2[4] + a1[8]*a2[5],
		a1[0]*a2[6] + a1[3]*a2[7] + a1[6]*a2[8],
		a1[1]*a2[6] + a1[4]*a2[7] + a1[7]*a2[8],
		a1[2]*a2[6] + a1[5]*a2[7] + a1[8]*a2[8],
		a1[0]*a2[9] + a1[3]*a2[10] + a1[6]*a2[11] + a1[9],
		a1[1]*a2[9] + a1[4]*a2[10] + a1[7]*a2[11] + a1[10],
		a1[2]*a2[9] + a1[5]*a2[10] + a1[8]*a2[11] + a1[11]}
}

// Multiply multiplies its receiver matrix with the argument matrix and returns
// the resulting matrix. The receiver matrix is the left hand side operand of
// the matrix multiplication. The argument matrix is the right hand side operand
// of the matrix multiplication.
func (m1 Mat3) Multiply(m2 Mat3) Mat3 {
	return Mat3{
		m1[0]*m2[0] + m1[3]*m2[1] + m1[6]*m2[2],
		m1[1]*m2[0] + m1[4]*m2[1] + m1[7]*m2[2],
		m1[2]*m2[0] + m1[5]*m2[1] + m1[8]*m2[2],
		m1[0]*m2[3] + m1[3]*m2[4] + m1[6]*m2[5],
		m1[1]*m2[3] + m1[4]*m2[4] + m1[7]*m2[5],
		m1[2]*m2[3] + m1[5]*m2[4] + m1[8]*m2[5],
		m1[0]*m2[6] + m1[3]*m2[7] + m1[6]*m2[8],
		m1[1]*m2[6] + m1[4]*m2[7] + m1[7]*m2[8],
		m1[2]*m2[6] + m1[5]*m2[7] + m1[8]*m2[8]}
}

// Multiply multiplies its receiver matrix with the argument matrix and returns
// the resulting matrix. The receiver matrix is the left hand side operand of
// the matrix multiplication. The argument matrix is the right hand side operand
// of the matrix multiplication.
func (m1 Mat4) Multiply(m2 Mat4) Mat4 {
	return Mat4{
		m1[0]*m2[0] + m1[4]*m2[1] + m1[8]*m2[2] + m1[12]*m2[3],
		m1[1]*m2[0] + m1[5]*m2[1] + m1[9]*m2[2] + m1[13]*m2[3],
		m1[2]*m2[0] + m1[6]*m2[1] + m1[10]*m2[2] + m1[14]*m2[3],
		m1[3]*m2[0] + m1[7]*m2[1] + m1[11]*m2[2] + m1[15]*m2[3],
		m1[0]*m2[4] + m1[4]*m2[5] + m1[8]*m2[6] + m1[12]*m2[7],
		m1[1]*m2[4] + m1[5]*m2[5] + m1[9]*m2[6] + m1[13]*m2[7],
		m1[2]*m2[4] + m1[6]*m2[5] + m1[10]*m2[6] + m1[14]*m2[7],
		m1[3]*m2[4] + m1[7]*m2[5] + m1[11]*m2[6] + m1[15]*m2[7],
		m1[0]*m2[8] + m1[4]*m2[9] + m1[8]*m2[10] + m1[12]*m2[11],
		m1[1]*m2[8] + m1[5]*m2[9] + m1[9]*m2[10] + m1[13]*m2[11],
		m1[2]*m2[8] + m1[6]*m2[9] + m1[10]*m2[10] + m1[14]*m2[11],
		m1[3]*m2[8] + m1[7]*m2[9] + m1[11]*m2[10] + m1[15]*m2[11],
		m1[0]*m2[12] + m1[4]*m2[13] + m1[8]*m2[14] + m1[12]*m2[15],
		m1[1]*m2[12] + m1[5]*m2[13] + m1[9]*m2[14] + m1[13]*m2[15],
		m1[2]*m2[12] + m1[6]*m2[13] + m1[10]*m2[14] + m1[14]*m2[15],
		m1[3]*m2[12] + m1[7]*m2[13] + m1[11]*m2[14] + m1[15]*m2[15]}
}

// RowMajorSlice expresses the receiver matrix in a new slice with the elements
// in row-major order.
func (m1 Mat4) RowMajorSlice() []float64 {
	return []float64{
		m1[0], m1[4], m1[8], m1[12],
		m1[1], m1[5], m1[9], m1[13],
		m1[2], m1[6], m1[10], m1[14],
		m1[3], m1[7], m1[11], m1[15],
	}
}

// Transform multiplies the receiver matrix with the argument vector and returns
// the resulting vector. The argument vector is an augmented vector with an
// implied 1 as the fourth element.
func (a1 Aff) Transform(v Vec) Vec {
	return Vec{
		a1[0]*v.X + a1[3]*v.Y + a1[6]*v.Z + a1[9],
		a1[1]*v.X + a1[4]*v.Y + a1[7]*v.Z + a1[10],
		a1[2]*v.X + a1[5]*v.Y + a1[8]*v.Z + a1[11]}
}

// TransformBez multiplies the receiver matrix with the vertices of the argument
// Bezier and returns the resulting Bezier.
func (a1 Aff) TransformBez(b Bez) Bez {
	return Bez{
		A: Vec{
			a1[0]*b.A.X + a1[3]*b.A.Y + a1[6]*b.A.Z + a1[9],
			a1[1]*b.A.X + a1[4]*b.A.Y + a1[7]*b.A.Z + a1[10],
			a1[2]*b.A.X + a1[5]*b.A.Y + a1[8]*b.A.Z + a1[11]},
		B: Vec{
			a1[0]*b.B.X + a1[3]*b.B.Y + a1[6]*b.B.Z + a1[9],
			a1[1]*b.B.X + a1[4]*b.B.Y + a1[7]*b.B.Z + a1[10],
			a1[2]*b.B.X + a1[5]*b.B.Y + a1[8]*b.B.Z + a1[11]},
		C: Vec{
			a1[0]*b.C.X + a1[3]*b.C.Y + a1[6]*b.C.Z + a1[9],
			a1[1]*b.C.X + a1[4]*b.C.Y + a1[7]*b.C.Z + a1[10],
			a1[2]*b.C.X + a1[5]*b.C.Y + a1[8]*b.C.Z + a1[11]},
	}
}

// TransformFrame applies the receiver transformation to the given frame.
func (a1 Aff) TransformFrame(fr Frame) Frame {
	o := a1.Transform(fr.O)
	return Frame{
		O: o,
		Axes: Basis{
			X: o.To(a1.Transform(PosX)),
			Y: o.To(a1.Transform(PosY)),
			Z: o.To(a1.Transform(PosZ)),
		},
	}
}

// TransformRay multiplies the receiver matrix with the argument ray.
//
// The direction vector of the returned ray is a unit vector.
func (a1 Aff) TransformRay(r Ray) Ray {
	o := a1.Transform(r.O)
	p := a1.Transform(r.O.Add(r.Dir))
	return Ray{O: o, Dir: o.To(p).Unit()}
}

// Transform multiplies the receiver matrix with the argument vector and returns
// the resulting vector.
func (m1 Mat2) Transform(v Vec2) Vec2 {
	return Vec2{m1[0]*v.X + m1[2]*v.Y, m1[1]*v.X + m1[3]*v.Y}
}

// Transform multiplies the receiver matrix with the argument vector and returns
// the resulting vector.
func (m1 Mat3) Transform(v Vec) Vec {
	return Vec{
		m1[0]*v.X + m1[3]*v.Y + m1[6]*v.Z,
		m1[1]*v.X + m1[4]*v.Y + m1[7]*v.Z,
		m1[2]*v.X + m1[5]*v.Y + m1[8]*v.Z}
}

// Transform multiplies the receiver matrix with the argument vector and returns
// the resulting vector.
func (m1 Mat4) Transform(v Vec4) Vec4 {
	return Vec4{
		m1[0]*v.X + m1[4]*v.Y + m1[8]*v.Z + m1[12]*v.W,
		m1[1]*v.X + m1[5]*v.Y + m1[9]*v.Z + m1[13]*v.W,
		m1[2]*v.X + m1[6]*v.Y + m1[10]*v.Z + m1[14]*v.W,
		m1[3]*v.X + m1[7]*v.Y + m1[11]*v.Z + m1[15]*v.W}
}

// Transpose returns the transpose of the receiver matrix.
//
// The ith rows in the receiver matrix are the ith columns of the returned
// matrix.
func (m1 Mat3) Transpose() Mat3 {
	return Mat3{m1[0], m1[3], m1[6], m1[1], m1[4], m1[7], m1[2], m1[5], m1[8]}
}

// ToAff returns the equivalent Affine matrix of the receiver matrix.
func (m1 Mat3) ToAff() Aff {
	return Aff{
		m1[0], m1[1], m1[2],
		m1[3], m1[4], m1[5],
		m1[6], m1[7], m1[8],
		0, 0, 0,
	}
}

// ToMat3 returns the equivalent 3x3 matrix of the receiver matrix.
//
// The translation components are dropped.
func (a1 Aff) ToMat3() Mat3 {
	return Mat3{
		a1[0], a1[1], a1[2],
		a1[3], a1[4], a1[5],
		a1[6], a1[7], a1[8],
	}
}

// ToMat4 returns the equivalent 4x4 matrix of the receiver matrix.
func (a1 Aff) ToMat4() Mat4 {
	return Mat4{
		a1[0], a1[1], a1[2], 0,
		a1[3], a1[4], a1[5], 0,
		a1[6], a1[7], a1[8], 0,
		a1[9], a1[10], a1[11], 1,
	}
}

// ToOpenGLMat4 returns the equivalent OpenGL float32 format matrix of the
// receiver matrix.
func (m1 Mat4) ToOpenGLMat4() OpenGLMat4 {
	return OpenGLMat4{
		float32(m1[0]), float32(m1[1]), float32(m1[2]), float32(m1[3]),
		float32(m1[4]), float32(m1[5]), float32(m1[6]), float32(m1[7]),
		float32(m1[8]), float32(m1[9]), float32(m1[10]), float32(m1[11]),
		float32(m1[12]), float32(m1[13]), float32(m1[14]), float32(m1[15]),
	}
}

// ToOpenGLMat4x3 returns the equivalent OpenGL float32 format matrix of the
// receiver matrix.
func (a1 Aff) ToOpenGLMat4x3() OpenGLMat4x3 {
	return OpenGLMat4x3{
		float32(a1[0]), float32(a1[1]), float32(a1[2]),
		float32(a1[3]), float32(a1[4]), float32(a1[5]),
		float32(a1[6]), float32(a1[7]), float32(a1[8]),
		float32(a1[9]), float32(a1[10]), float32(a1[11]),
	}
}

// NewTranslation constructs a new translation transformation using the argument
// as the translation.
func NewTranslation(t Vec) Aff {
	return Aff{1, 0, 0, 0, 1, 0, 0, 0, 1, t.X, t.Y, t.Z}
}

// New2DRotO constructs a new rotation around the origin in the XY-plane.
//
// The argument specifies the angle of rotation measured in radians.
//
// A positive rotation angle indicates an anticlockwise rotation.
func New2DRotO(angle float64) Mat2 {
	s, c := math.Sincos(angle)
	return Mat2{c, s, -s, c}
}

// NewXRot constructs a new rotation around the X-axis.
//
// The argument specifies the angle of rotation measured in radians.
//
// Suppose the X-axis was pointing out of the screen towards you, then a
// positive rotation angle indicates an anticlockwise rotation.
func NewXRot(angle float64) Aff {
	s, c := math.Sincos(angle)
	return Aff{
		1, 0, 0,
		0, c, s,
		0, -s, c}
}

// NewYRot constructs a new rotation around the Y-axis.
//
// The argument specifies the angle of rotation measured in radians.
//
// Suppose the Y-axis was pointing out of the screen towards you, then a
// positive rotation angle indicates an anticlockwise rotation.
func NewYRot(angle float64) Aff {
	s, c := math.Sincos(angle)
	return Aff{
		c, 0, -s,
		0, 1, 0,
		s, 0, c}
}

// NewZRot constructs a new rotation around the Z-axis.
//
// The argument specifies the angle of rotation measured in radians.
//
// Suppose the Z-axis was pointing out of the screen towards you, then a
// positive rotation angle indicates an anticlockwise rotation.
func NewZRot(angle float64) Aff {
	s, c := math.Sincos(angle)
	return Aff{
		c, s, 0,
		-s, c, 0,
		0, 0, 1}
}

// newRot2 is only for testing/benchmarking. See NewRot.
func newRot2(l Line, angle float64) Aff {
	z := -math.Atan2(l.Dir.Y, l.Dir.X)
	y := -math.Atan2(math.Sqrt(l.Dir.X*l.Dir.X+l.Dir.Y*l.Dir.Y), l.Dir.Z)
	A := Aff{1, 0, 0, 0, 1, 0, 0, 0, 1, -l.P.X, -l.P.Y, -l.P.Z}
	B := NewZRot(z)
	C := NewYRot(y)
	D := NewZRot(angle)
	CUndo := NewYRot(-y)
	BUndo := NewZRot(-z)
	AUndo := Aff{1, 0, 0, 0, 1, 0, 0, 0, 1, l.P.X, l.P.Y, l.P.Z}

	tmp := AUndo.Multiply(BUndo)
	tmp = tmp.Multiply(CUndo)
	tmp = tmp.Multiply(D)
	tmp = tmp.Multiply(C)
	tmp = tmp.Multiply(B)
	tmp = tmp.Multiply(A)
	return tmp
}

// NewORot constructs a new rotation around a line through the origin.
//
// The argument direction vector is parallel to the line. The argument angle is
// measured in radians.
func NewORot(v Vec, angle float64) Aff {
	// https://gist.github.com/fospathi/1bfbc5d5a1f1df588aa31732a8cd451e
	S, C := math.Sincos(angle)
	z := -math.Atan2(v.Y, v.X)
	y := -math.Atan2(math.Sqrt(v.X*v.X+v.Y*v.Y), v.Z)
	sy, cy := math.Sincos(y)
	sz, cz := math.Sincos(z)
	m00 := C*(cy*cy*cz*cz+sz*sz) + cz*cz*sy*sy
	m01 := C*cz*sz*(1-cy*cy) - S*cy*(cz*cz+sz*sz) - cz*sy*sy*sz
	m02 := C*cy*cz*sy + S*sy*sz - cy*cz*sy
	m10 := C*cz*sz*(1-cy*cy) + S*cy*(cz*cz+sz*sz) - cz*sy*sy*sz
	m11 := C*(cy*cy*sz*sz+cz*cz) + sy*sy*sz*sz
	m12 := -C*cy*sy*sz + S*cz*sy + cy*sy*sz
	m20 := C*cy*cz*sy - S*sy*sz - cy*cz*sy
	m21 := -C*cy*sy*sz - S*cz*sy + cy*sy*sz
	m22 := C*sy*sy + cy*cy
	return Aff{
		m00, m10, m20,
		m01, m11, m21,
		m02, m12, m22,
		0, 0, 0,
	}
}

// NewRot constructs a new rotation around a line.
//
// The angle argument specifies the angle of rotation measured in radians.
//
// Suppose the line's direction vector was pointing out of the screen towards
// you, then a positive rotation angle indicates an anticlockwise rotation.
func NewRot(l Line, angle float64) Aff {
	// https://gist.github.com/fospathi/1bfbc5d5a1f1df588aa31732a8cd451e
	S, C := math.Sincos(angle)
	z := -math.Atan2(l.Dir.Y, l.Dir.X)
	y := -math.Atan2(math.Sqrt(l.Dir.X*l.Dir.X+l.Dir.Y*l.Dir.Y), l.Dir.Z)
	sy, cy := math.Sincos(y)
	sz, cz := math.Sincos(z)
	m00 := C*(cy*cy*cz*cz+sz*sz) + cz*cz*sy*sy
	m01 := C*cz*sz*(1-cy*cy) - S*cy*(cz*cz+sz*sz) - cz*sy*sy*sz
	m02 := C*cy*cz*sy + S*sy*sz - cy*cz*sy
	m10 := C*cz*sz*(1-cy*cy) + S*cy*(cz*cz+sz*sz) - cz*sy*sy*sz
	m11 := C*(cy*cy*sz*sz+cz*cz) + sy*sy*sz*sz
	m12 := -C*cy*sy*sz + S*cz*sy + cy*sy*sz
	m20 := C*cy*cz*sy - S*sy*sz - cy*cz*sy
	m21 := -C*cy*sy*sz - S*cz*sy + cy*sy*sz
	m22 := C*sy*sy + cy*cy
	return Aff{
		m00, m10, m20,
		m01, m11, m21,
		m02, m12, m22,
		-l.P.X*m00 - l.P.Y*m01 - l.P.Z*m02 + l.P.X,
		-l.P.X*m10 - l.P.Y*m11 - l.P.Z*m12 + l.P.Y,
		-l.P.X*m20 - l.P.Y*m21 - l.P.Z*m22 + l.P.Z,
	}
}

// NewXYRefl constructs a new reflection transformation in the XY plane at z=0.
func NewXYRefl() Aff {
	return Aff{1, 0, 0, 0, 1, 0, 0, 0, -1, 0, 0, 0}
}

// NewXZRefl constructs a new reflection transformation in the XZ plane at y=0.
func NewXZRefl() Aff {
	return Aff{1, 0, 0, 0, -1, 0, 0, 0, 1, 0, 0, 0}
}

// NewYZRefl constructs a new reflection transformation in the YZ plane at x=0.
func NewYZRefl() Aff {
	return Aff{-1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}
}

// NewYZReflAt constructs a new reflection transformation in the YZ plane given
// by x = argument value.
func NewYZReflAt(x float64) Aff {
	return Aff{-1, 0, 0, 0, 1, 0, 0, 0, 1, 2 * x, 0, 0}
}

// NewRefl constructs a new reflection transformation in the argument plane.
func NewRefl(p Plane) Aff {
	// https://gist.github.com/fospathi/2abd97ebe8b80c21dcd1e27cdb3de1b6
	z := -math.Atan2(p.Normal.Y, p.Normal.X)
	y := -math.Atan2(math.Sqrt(p.Normal.X*p.Normal.X+p.Normal.Y*p.Normal.Y), p.Normal.Z)
	sy, cy := math.Sincos(y)
	sz, cz := math.Sincos(z)
	m00 := cy*cy*cz*cz + sz*sz - cz*cz*sy*sy
	m01 := cz*sz*(1-cy*cy) + cz*sy*sy*sz
	m02 := 2 * cy * cz * sy
	m10 := cz*sz*(1-cy*cy) + cz*sy*sy*sz
	m11 := cy*cy*sz*sz + cz*cz - sy*sy*sz*sz
	m12 := -2 * cy * sy * sz
	m20 := 2 * cy * cz * sy
	m21 := -2 * cy * sy * sz
	m22 := sy*sy - cy*cy
	return Aff{
		m00, m10, m20,
		m01, m11, m21,
		m02, m12, m22,
		-p.P.X*m00 - p.P.Y*m01 - p.P.Z*m02 + p.P.X,
		-p.P.X*m10 - p.P.Y*m11 - p.P.Z*m12 + p.P.Y,
		-p.P.X*m20 - p.P.Y*m21 - p.P.Z*m22 + p.P.Z,
	}
}
