package maf

import "math"

// EllipsePerimeter approximation for the ellipse with semi-major axis length a
// and semi-minor axis length b.
func EllipsePerimeter(a, b float64) float64 {
	// One possible expression for the perimeter of an ellipse is the infinite
	// sum:
	//
	//                  ∞
	//   p = π (a + b)  ∑   [ (0.5)² hⁿ ]
	//                  n=0   ( n )
	//
	//   where (0.5) is the fractional binomial coefficient and h = (a - b)²
	//         ( n )                                                --------
	//                                                              (a + b)²
	//
	// Fractional binomial coefficients:
	// https://math.stackexchange.com/questions/340124/binomial-coefficients-1-2-choose-k

	const iterations = 10
	h := ((a - b) * (a - b)) / ((a + b) * (a + b))

	x := 0.5
	xFactor := x      // Tracks the latest factor in the binomial coefficient numerator.
	xProduct := x     // Tracks the binomial coefficient numerator.
	nFactorial := 1.0 // Tracks the binomial coefficient denominator.
	hPower := h       // Tracks the nth power of h.
	sum := 1.0        // First n=0 term in the expansion is 1.0.
	for n := 1; n <= iterations; n++ {
		binCoef := xProduct / nFactorial
		nthTerm := binCoef * binCoef * hPower
		sum += nthTerm

		hPower *= h
		xFactor -= 1.0
		xProduct *= xFactor
		nFactorial *= float64(n + 1)
	}
	return math.Pi * (a + b) * sum
}
