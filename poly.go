package maf

import "math"

// Poly is a single variable polynomial with real coefficients.
//
// The degree of the polynomial is one less than the length of the slice.
//
// The elements of the slice are the coefficients of the constituent monomial terms. The index of the
// coefficient in the slice is the power of the monomial term to which the coefficient corresponds.
type Poly []float64

// At evaluates the polynomial as a function at the argument polynomial variable value.
func (p Poly) At(x float64) float64 {
	var evaluation float64
	for i := len(p) - 1; i >= 0; i-- {
		coef := p[i]
		evaluation += coef * math.Pow(x, float64(i))
	}
	return evaluation
}

// Derivative returns the result of differentiating the receiver polynomial with respect to the variable.
//
// Special cases are:
//  The derivative of an empty polynomial is the zero polynomial.
//  The derivative of a constant polynomial is the zero polynomial.
func (p Poly) Derivative() Poly {
	switch l := len(p); l {
	case 0:
		fallthrough
	case 1:
		return Poly{0}
	default:
		result := make(Poly, l-1)
		for i := 0; i < l-1; i++ {
			result[i] = p[i+1] * float64(i+1)
		}
		return result
	}
}

// Integral returns the result of integrating the receiver polynomial with respect to the variable.
//
// In order that the constant of integration can be found the required arguments are the value of the
// integrated polynomial for a particular value of the variable.
func (p Poly) Integral(integral, variable float64) Poly {
	l := len(p) + 1
	result := make(Poly, l)
	var evaluation float64
	for i := l - 1; i > 0; i-- {
		coef := p[i-1] / float64(i)
		result[i] = coef
		evaluation += coef * math.Pow(variable, float64(i))
	}
	result[0] = integral - evaluation
	return result
}
