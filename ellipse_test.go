package maf

import (
	"math"
	"testing"

	"gitlab.com/fospathi/universal/tezd"
)

func TestEllipsePerimeter(t *testing.T) {
	{
		want := 51.05400
		got := EllipsePerimeter(10, 6)
		if !tezd.CompareFloat64(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := 10 * math.Pi
		got := EllipsePerimeter(5, 5)
		if !tezd.CompareFloat64(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
