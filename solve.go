package maf

import (
	"math"
	"math/cmplx"
)

// SolveSLE2 returns the solution of a simultaneous linear equation system with
// two variables:
//
//  a1*x + b1*y + c1 = 0
//  a2*x + b2*y + c2 = 0
//
// The bool return value is false if no unique solution can be found.
func SolveSLE2(a1, b1, c1, a2, b2, c2 float64) (x float64, y float64, ok bool) {
	xDenominator := a2*b1 - a1*b2
	switch {
	case xDenominator == 0:
		return
	case b1 != 0:
		x = (b2*c1 - b1*c2) / xDenominator
		y = (-c1 - a1*x) / b1
	case b2 != 0:
		x = (b2*c1 - b1*c2) / xDenominator
		y = (-c2 - a2*x) / b2
	default:
		return
	}
	return x, y, true
}

// CubeRootsOfOne are the three possible complex solutions of z = 1^(1/3).
//
// The order of the roots is anticlockwise around the origin starting from the
// real solution.
var CubeRootsOfOne = [3]complex128{
	1 + 0i,
	complex(-0.5, CosPiOver6),
	complex(-0.5, -CosPiOver6),
}

var cubeRootsOfOneSqd = [3]complex128{
	1 + 0i,
	CubeRootsOfOne[1] * CubeRootsOfOne[1],
	CubeRootsOfOne[2] * CubeRootsOfOne[2],
}

// Quadratic is a quadratic equation with real coefficients.
type Quadratic struct {
	// The real coefficients of the cubic equation Ax² + Bx + C = 0.
	//
	// A shall be be non-zero.
	A, B, C float64
}

// Roots returns the complex values which are the solutions to the receiver
// quadratic equation.
func (q Quadratic) Roots() (complex128, complex128) {
	discriminant := q.B*q.B - 4*q.A*q.C
	if discriminant < 0 {
		return complex(-q.B/(2*q.A), math.Sqrt(-discriminant)/(2*q.A)),
			complex(-q.B/(2*q.A), -math.Sqrt(-discriminant)/(2*q.A))
	}
	return complex((-q.B+math.Sqrt(discriminant))/(2*q.A), 0),
		complex((-q.B+math.Sqrt(discriminant))/(2*q.A), 0)
}

// Root returns a single complex value which is a solution to the receiver
// quadratic equation.
//
// The value returned is derived from the plus option in the quadratic formula:
// (-b + sqrt(b*b-4ac))/2a
func (q Quadratic) Root() complex128 {
	discriminant := q.B*q.B - 4*q.A*q.C
	if discriminant < 0 {
		return complex(-q.B/(2*q.A), math.Sqrt(-discriminant)/(2*q.A))
	}
	return complex((-q.B+math.Sqrt(discriminant))/(2*q.A), 0)
}

// Cubic is a cubic equation with real coefficients.
type Cubic struct {
	// The real coefficients of the cubic equation Ax³ + Bx² + Cx + D = 0.
	//
	// A shall be non-zero.
	A, B, C, D float64
}

// Roots returns the complex values which are the solutions to the receiver
// cubic equation.
func (c Cubic) Roots() (complex128, complex128, complex128) {
	// https://en.wikipedia.org/wiki/Cubic_function#Vieta%27s_substitution
	p := complex((3*c.A*c.C-c.B*c.B)/(3*c.A*c.A), 0)
	q := complex((2*c.B*c.B*c.B-9*c.A*c.B*c.C+27*c.A*c.A*c.D)/(27*c.A*c.A*c.A), 0)
	if p == 0 && q == 0 {
		return 0, 0, 0
	}
	Q := Quadratic{1, real(q), -real(p*p*p) / 27.0}
	w1, w2, w3 := CubeRoots(Q.Root())
	t1 := w1 - p/(3*w1)
	t2 := w2 - p/(3*w2)
	t3 := w3 - p/(3*w3)
	k := complex(c.B/(3*c.A), 0)
	return t1 - k, t2 - k, t3 - k
}

// CubeRoots returns the cube roots of a complex number.
func CubeRoots(c complex128) (complex128, complex128, complex128) {
	x := real(c)
	y := imag(c)
	r := cmplx.Rect(math.Cbrt(math.Sqrt(x*x+y*y)), math.Atan2(y, x)/3)
	return r, r * CubeRootsOfOne[1], r * CubeRootsOfOne[2]
}

// NewtonsMethod returns an approximate value of a root of a function.
//
// The two argument functions required are the function and its derivative
// function. The argument initial guess should be your closest guess to the
// desired root.
//
// The algorithm will halt when the magnitude of the change between a guess and
// its next guess is reduced below the argument tolerance or when the number of
// iterations of the algorithm reaches the argument limit.
func NewtonsMethod(
	f func(float64) float64,
	d func(float64) float64,
	guess float64,
	tolerance float64,
	maxIterations int,
) float64 {

	nextGuess := func(x float64) float64 {
		return x - f(x)/d(x)
	}
	current := guess
	if f(current) != 0 {
		for i := 0; i < maxIterations; i++ {
			delta := nextGuess(current) - current
			current += delta
			if math.Abs(delta) <= tolerance {
				break
			}
		}
	}
	return current
}
