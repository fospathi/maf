package maf

import (
	"math"
	"testing"

	"gitlab.com/fospathi/universal/tezd"
)

func BenchmarkMath_Cos(b *testing.B) {
	for n := 0; n < b.N; n++ {
		math.Cos(0.1234)
		math.Cos(0.4321)
		math.Cos(-27.3)
	}
}

func TestCosineRule(t *testing.T) {
	want := HalfPi
	got := CosineRule(3, 4, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestModuloPi(t *testing.T) {
	const pi = math.Pi
	a60 := pi / 3
	a120 := 2 * pi / 3
	va := []float64{a60, a120, -a60, -a120}
	for _, a := range va {
		want1, want2 := math.Cos(a), math.Sin(a)
		got1, got2 := math.Cos(ModuloPi(a)), math.Sin(ModuloPi(a))
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}

	va = []float64{
		TwoPi + a60, TwoPi + a120, -TwoPi - a60, -TwoPi - a120,
		2*TwoPi + a60, 2*TwoPi + a120, -2*TwoPi - a60, -2*TwoPi - a120,
		2*TwoPi + 2*a120, -2*TwoPi - 2*a120,
		3 * TwoPi, -3 * TwoPi,
		3*TwoPi + a60, 3*TwoPi + a120, -3*TwoPi - a60, -3*TwoPi - a120,
		3*TwoPi + 3*a120, -3*TwoPi - 3*a120,
	}
	for _, a := range va {
		want1, want2 := math.Cos(a), math.Sin(a)
		got1, got2 := math.Cos(ModuloPi(a)), math.Sin(ModuloPi(a))
		if !tezd.CompareFloat64(want1, got1, 5) ||
			!tezd.CompareFloat64(want2, got2, 5) {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}
}
