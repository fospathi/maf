module gitlab.com/fospathi/maf

go 1.21

require gitlab.com/fospathi/universal v0.0.0-20230605055348-86065fb86769

replace gitlab.com/fospathi/universal => ../universal
