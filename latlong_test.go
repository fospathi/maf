package maf

import (
	"testing"

	"gitlab.com/fospathi/universal/tezd"
)

func Benchmark_EquiangularLatitudes(b *testing.B) {
	nLats := 10
	for n := 0; n < b.N; n++ {
		EquiangularLatitudes(1.5, nLats)
	}
}

func Benchmark_EquidistantLatitudes(b *testing.B) {
	nLats := 10
	for n := 0; n < b.N; n++ {
		EquidistantLatitudes(1.5, nLats)
	}
}

func Test_EquiangularLatitudes(t *testing.T) {
	{
		got := EquiangularLatitudes(1, 1)
		want := []LatHR{{0, 1}}
		if len(got) != len(want) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		for i := 0; i < len(got); i++ {
			if !tezd.CompareFloat64(got[i].H, want[i].H, 5) || !tezd.CompareFloat64(got[i].R, want[i].R, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}

	{
		got := EquiangularLatitudes(1, 2)
		want := []LatHR{{-0.5, 0.866025}, {0.5, 0.866025}}
		if len(got) != len(want) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		for i := 0; i < len(got); i++ {
			if !tezd.CompareFloat64(got[i].H, want[i].H, 5) || !tezd.CompareFloat64(got[i].R, want[i].R, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}

	{
		got := EquiangularLatitudes(1, 3)
		want := []LatHR{{-0.707107, 0.707107}, {0, 1.0}, {0.707107, 0.707107}}
		if len(got) != len(want) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		for i := 0; i < len(got); i++ {
			if !tezd.CompareFloat64(got[i].H, want[i].H, 5) || !tezd.CompareFloat64(got[i].R, want[i].R, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}

	{
		got := EquiangularLatitudes(2, 2)
		want := []LatHR{{-1.0, 1.732051}, {1.0, 1.732051}}
		if len(got) != len(want) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		for i := 0; i < len(got); i++ {
			if !tezd.CompareFloat64(got[i].H, want[i].H, 5) || !tezd.CompareFloat64(got[i].R, want[i].R, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}
}

func Test_EquidistantLatitudes(t *testing.T) {
	{
		got := EquidistantLatitudes(1, 1)
		want := []LatHR{{0, 1}}
		if len(got) != len(want) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		for i := 0; i < len(got); i++ {
			if !tezd.CompareFloat64(got[i].H, want[i].H, 5) || !tezd.CompareFloat64(got[i].R, want[i].R, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}

	{
		got := EquidistantLatitudes(1, 2)
		want := []LatHR{{-1.0 / 3.0, 0.94281}, {1.0 / 3.0, 0.94281}}
		if len(got) != len(want) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		for i := 0; i < len(got); i++ {
			if !tezd.CompareFloat64(got[i].H, want[i].H, 5) || !tezd.CompareFloat64(got[i].R, want[i].R, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}

	{
		got := EquidistantLatitudes(1, 3)
		want := []LatHR{{-0.5, 0.866025}, {0, 1.0}, {0.5, 0.866025}}
		if len(got) != len(want) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		for i := 0; i < len(got); i++ {
			if !tezd.CompareFloat64(got[i].H, want[i].H, 5) || !tezd.CompareFloat64(got[i].R, want[i].R, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}

	{
		got := EquidistantLatitudes(2, 3)
		want := []LatHR{{-1.0, 1.73205}, {0, 2.0}, {1.0, 1.73205}}
		if len(got) != len(want) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		for i := 0; i < len(got); i++ {
			if !tezd.CompareFloat64(got[i].H, want[i].H, 5) || !tezd.CompareFloat64(got[i].R, want[i].R, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}
}
