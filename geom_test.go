package maf

import (
	"math"
	"testing"

	"gitlab.com/fospathi/universal/tezd"
)

func BenchmarkTriangle_EnclosingSphere(b *testing.B) {
	tri := Triangle{A: Vec{2.1, 1.3, 1.1}, B: Vec{1, 2, 1.1}, C: Vec{0.1, 1, 1.1}}
	for n := 0; n < b.N; n++ {
		tri.EnclosingSphere()
	}
}

func BenchmarkLineSeg2_DistanceTo(b *testing.B) {
	ls := LineSeg2{Vec2{-1, -1}, Vec2{1, 1}}
	p := Vec2{-0.1, -0.2}
	for n := 0; n < b.N; n++ {
		ls.DistTo(p)
	}
}

func TestCircle_RandPoints(t *testing.T) {
	const n, r = 50, 1.0
	o := Zero
	c := Circle{C: o, R: r, Normal: PosX}
	pl := Plane{P: o, Normal: PosX}
	vp := c.RandPoints(n)

	// Test the points are distinct.
	sp := map[Vec]struct{}{}
	for _, p := range vp {
		sp[p] = struct{}{}
	}
	if l := len(sp); l != n {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", n, l)
	}

	// Test the distance from the centre.
	for _, p := range vp {
		if p.DistTo(o) > r {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", "inside", "outside")
		}
	}

	// Test the distance from the plane.
	for _, p := range vp {
		if d := pl.DistTo(p); d > 1e-6 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", 0, d)
		}
	}
}

func TestLine_Closest(t *testing.T) {
	l := Line{P: Vec{0, 1, 0}, Dir: Vec{1, 0, 0}}
	p := Vec{X: 3, Y: -1, Z: -1}

	want := Vec{X: 3, Y: 1, Z: 0}
	got := l.Closest(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	p = Vec{X: -3, Y: -1, Z: -1}

	want = Vec{X: -3, Y: 1, Z: 0}
	got = l.Closest(p)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestLineSeg_Closest(t *testing.T) {
	ls := LineSeg{P1: Vec{-1, 1, 0}, P2: Vec{1, 1, 0}}
	p1, p2, p3 := Vec{-1, 1, 0}, Vec{1, 1, 0}, Vec{0, 1, 0}
	want1, want2, want3 := p1, p2, p3
	got1, got2, got3 := ls.Closest(p1), ls.Closest(p2), ls.Closest(p3)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	p1, p2, p3 = Vec{-2, 1, 0}, Vec{2, 1, 0}, Vec{0, 2, 0}
	want1, want2, want3 = Vec{-1, 1, 0}, Vec{1, 1, 0}, Vec{0, 1, 0}
	got1, got2, got3 = ls.Closest(p1), ls.Closest(p2), ls.Closest(p3)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}

func TestPlane_Closest(t *testing.T) {
	pl := Plane{
		P:      Vec{1, 1, 1},
		Normal: Vec{0, 0, 1},
	}

	want := Vec{2, 2, 1}
	got := pl.Closest(Vec{2, 2, 2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Vec{3, 3, 1}
	got = pl.Closest(Vec{3, 3, 1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Vec{-2, -2, 1}
	got = pl.Closest(Vec{-2, -2, -2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestPlane_DistTo(t *testing.T) {
	pl := Plane{P: Vec{1, 1, 1}, Normal: Vec{0, 0, 1}}

	want := 0.0
	got := pl.DistTo(Vec{0, 0, 1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 1.0
	got = pl.DistTo(Vec{-1, -1, 2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 2.0
	got = pl.DistTo(Vec{-1, -1, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestPlane_SignedDistTo(t *testing.T) {
	pl := Plane{P: Vec{1, 1, 1}, Normal: Vec{0, 0, 1}}

	want := 0.0
	got := pl.SignedDistTo(Vec{0, 0, 1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 1.0
	got = pl.SignedDistTo(Vec{-1, -1, 2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = -2.0
	got = pl.SignedDistTo(Vec{-1, -1, -1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle_BisectorAC(t *testing.T) {
	tri := Triangle{A: Vec{-1, 0, 0}, B: Vec{0, -1, 0}, C: Vec{1, 0, 0}}
	want := Vec{0, 0, 0}
	got := tri.BisectorAC()
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tri = Triangle{A: Vec{1, 0, 0}, B: Vec{0, 0, 0}, C: Vec{1, 1, 0}}
	want = Vec{1, math.Tan(math.Pi / 8), 0}
	got = tri.BisectorAC()
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle_Centroid(t *testing.T) {
	tri := Triangle{A: Vec{1, 2, 1}, B: Vec{3, 4, 1}, C: Vec{5, 6, 1}}
	want := Vec{3, 4, 1}
	got := tri.Centroid()
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle_Circumcircle(t *testing.T) {
	tri := Triangle{A: Vec{-1, 0, 0}, B: Vec{1, 0, 0}, C: Vec{0, 1, 0}}
	want := Circle{C: Vec{0, 0, 0}, R: 1, Normal: Vec{0, 0, 1}}
	got := tri.Circumcircle()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tri = Triangle{A: Vec{-2, 0, 0}, B: Vec{2, 0, 0}, C: Vec{0, 2, 0}}
	want = Circle{C: Vec{0, 0, 0}, R: 2, Normal: Vec{0, 0, 1}}
	got = tri.Circumcircle()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tri = Triangle{A: Vec{-3, 2, -3}, B: Vec{3, 2, -3}, C: Vec{0, 2, -6}}
	want = Circle{C: Vec{0, 2, -3}, R: 3, Normal: Vec{0, 1, 0}}
	got = tri.Circumcircle()
	if !CompareVec(want.C, got.C, 5) ||
		want.Normal != got.Normal ||
		want.R != got.R {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle_DivideAC(t *testing.T) {
	tri := Triangle{A: Vec{-1, 0, 0}, B: Vec{0, -1, 0}, C: Vec{1, 0, 0}}
	want := Vec{-0.5, 0, 0}
	got := tri.DivideAC(1, 3)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Vec{0, 0, 0}
	got = tri.DivideAC(2, 2)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Vec{0.5, 0, 0}
	got = tri.DivideAC(3, 1)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Vec{-1, 0, 0}
	got = tri.DivideAC(0, 1)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Vec{1, 0, 0}
	got = tri.DivideAC(1, 0)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle_EnclosingSphere(t *testing.T) {
	tri := Triangle{A: Vec{-1, 1, 1}, B: Vec{0, 1.5, 0}, C: Vec{1, 1, -1}}
	want := Sphere{C: Vec{0, 1, 0}, R: math.Sqrt(2)}
	got := tri.EnclosingSphere()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tri = Triangle{A: Vec{-1, 1, 0}, B: Vec{0, 3, 0}, C: Vec{1, 1, 0}}
	want = Sphere{C: Vec{0, 1, 0}, R: 2}
	got = tri.EnclosingSphere()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle_Scale(t *testing.T) {
	tri := Triangle{A: Vec{0, 0, 0}, B: Vec{1, 2, 0}, C: Vec{2, 1, 0}}
	want := Vec{X: 1, Y: 1, Z: 0}
	got := tri.Centroid()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tri = Triangle{A: Vec{0, 0, 0}, B: Vec{1, 2, 0}, C: Vec{2, 1, 0}}
	{
		want := tri.Scale(2).Centroid()
		got := tri.Centroid()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		want = tri.Scale(0.5).Centroid()
		got = tri.Centroid()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	want = Vec{X: 1, Y: 4, Z: 0}
	got = tri.Scale(3).B
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestLine_ClosestPoints(t *testing.T) {
	l1 := Line{P: Vec{0, 0, 0}, Dir: Vec{0, 1, 0}}
	l2 := Line{P: Vec{0, 0, 0}, Dir: Vec{1, 0, 0}}
	want1, want2, want3 := Vec{0, 0, 0}, Vec{0, 0, 0}, true
	got1, got2, got3 := l1.ClosestPoints(l2)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	l1 = Line{P: Vec{0, 0, 0}, Dir: Vec{0, 1, 0}}
	l2 = Line{P: Vec{0, 0, 1}, Dir: Vec{1, 0, 0}}
	want1, want2, want3 = Vec{0, 0, 0}, Vec{0, 0, 1}, true
	got1, got2, got3 = l1.ClosestPoints(l2)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	l1 = Line{P: Vec{0, 0, 0}, Dir: Vec{0, 1, 1}.Unit()}
	l2 = Line{P: Vec{2, 0, 0}, Dir: Vec{0, 1, -1}.Unit()}
	want1, want2, want3 = Vec{0, 0, 0}, Vec{2, 0, 0}, true
	got1, got2, got3 = l1.ClosestPoints(l2)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	l1 = Line{P: Vec{0, 0, -2}, Dir: Vec{0, 0, 2}.Unit()}
	l2 = Line{P: Vec{1, 2, 0}, Dir: Vec{0, 2, 0}.Unit()}
	want1, want2, want3 = Vec{0, 0, 0}, Vec{1, 0, 0}, true
	got1, got2, got3 = l1.ClosestPoints(l2)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	l1 = Line{P: Vec{1, 1, 1}, Dir: Vec{0, 1, 0}}
	l2 = Line{P: Vec{1, 1, 1}, Dir: Vec{0, 1, 0}}
	want1, want2, want3 = Vec{0, 0, 0}, Vec{0, 0, 0}, false
	got1, got2, got3 = l1.ClosestPoints(l2)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	l1 = Line{P: Vec{1, 1, 1}, Dir: Vec{0, 1, 0}}
	l2 = Line{P: Vec{0, 0, 0}, Dir: Vec{0, 1, 0}}
	want1, want2, want3 = Vec{0, 0, 0}, Vec{0, 0, 0}, false
	got1, got2, got3 = l1.ClosestPoints(l2)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}

func TestLine_DistTo(t *testing.T) {
	l1 := Line{P: Vec{0, 0, 0}, Dir: Vec{0, 1, 0}}
	p1, p2, p3 := Vec{0, 0, 0}, Vec{1, 0, 0}, Vec{2, 3, 0}
	want1, want2, want3 := 0.0, 1.0, 2.0
	got1, got2, got3 := l1.DistTo(p1), l1.DistTo(p2), l1.DistTo(p3)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	p1, p2, p3 = Vec{0, -1, 0}, Vec{1, -1, 0}, Vec{2, -3, 0}
	want1, want2, want3 = 0.0, 1.0, 2.0
	got1, got2, got3 = l1.DistTo(p1), l1.DistTo(p2), l1.DistTo(p3)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}

func TestLine_IsParallel(t *testing.T) {
	l1 := Line{P: Vec{0, 0, 0}, Dir: Vec{0, 1, 0}}
	l2 := Line{P: Vec{0, 0, 0}, Dir: Vec{0, -1, 0}}
	l3 := Line{P: Vec{0, 0, 0}, Dir: Vec{0.001, -1, 0}}
	l4 := Line{P: Vec{1, 1, 1}, Dir: Vec{0.5, 0, 0.5}}

	want := true
	got := l1.IsParallel(l1, 0) && l2.IsParallel(l1, 0) && l3.IsParallel(l1, 0.01)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = false
	got = l3.IsParallel(l1, 0) || l4.IsParallel(l1, 0.1)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestAnnulus2_LineIntersection(t *testing.T) {
	a := Annulus2{C: Vec2{1, 1}, R1: 1, R2: 2}

	// Test no intersection.

	vls, n := a.LineIntersection(Line2{P: Vec2{0, 3}, Dir: Vec2{1, 0}})

	want1, want2 := [2]LineSeg2{}, 0
	got1, got2 := vls, n
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
			want1, got1, want2, got2)
	}

	// Test four intersections giving two line segments.

	vls, n = a.LineIntersection(Line2{P: Vec2{0, 1}, Dir: Vec2{1, 0}})

	want1, want2 = [2]LineSeg2{
		{P1: Vec2{X: -1, Y: 1}, P2: Vec2{X: 0, Y: 1}},
		{P1: Vec2{X: 2, Y: 1}, P2: Vec2{X: 3, Y: 1}},
	}, 2
	got1, got2 = vls, n
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
			want1, got1, want2, got2)
	}
}

func TestCircle2_LineIntersections(t *testing.T) {
	c := Circle2{C: Vec2{1, 1}, R: 1}

	// Test no intersection.

	l := Line2{P: Vec2{1, 3}, Dir: Vec2{1, 0}}
	_, ok := c.LineIntersections(l)
	want, got := false, ok
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Test tangent intersections.

	l = Line2{P: Vec2{0, 2}, Dir: Vec2{1, 0}}
	vp, ok := c.LineIntersections(l)
	want1, want2 := [2]Vec2{{1, 2}, {1, 2}}, true
	got1, got2 := vp, ok
	if want1 != got1 || want2 != got2 {
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}

	l = Line2{P: Vec2{2, 0}, Dir: Vec2{0, -1}}
	vp, ok = c.LineIntersections(l)
	want1, want2 = [2]Vec2{{2, 1}, {2, 1}}, true
	got1, got2 = vp, ok
	if want1 != got1 || want2 != got2 {
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}

	// Test two intersections.

	l = Line2{P: Vec2{-3, 1}, Dir: Vec2{1, 0}}
	vp, ok = c.LineIntersections(l)
	want1, want2 = [2]Vec2{{0, 1}, {2, 1}}, true
	got1, got2 = vp, ok
	if want1 != got1 || want2 != got2 {
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n",
				want1, got1, want2, got2)
		}
	}
}

func TestCircle2_RandPoints(t *testing.T) {
	const n, r = 50, 1.0
	o := Vec2{1, 1}
	c := Circle2{C: o, R: r}
	vp := c.RandPoints(n)

	// Test the points are distinct.
	sp := map[Vec2]struct{}{}
	for _, p := range vp {
		sp[p] = struct{}{}
	}
	if l := len(sp); l != n {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", n, l)
	}

	// Test the distance from the centre.
	for _, p := range vp {
		if p.DistTo(o) > r {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", "inside", "outside")
		}
	}
}

func TestLineSeg2_Canonical(t *testing.T) {
	ls1 := LineSeg2{P1: Vec2{-1, 0}, P2: Vec2{1, 1}}
	ls2 := LineSeg2{P1: Vec2{1, 1}, P2: Vec2{-1, 0}}

	want, got := ls1, ls2.Canonical()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	ls1 = LineSeg2{P1: Vec2{0, 0}, P2: Vec2{0, 1}}
	ls2 = LineSeg2{P1: Vec2{0, 1}, P2: Vec2{0, 0}}

	want, got = ls1, ls2.Canonical()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestLineSeg2_DistanceTo(t *testing.T) {
	ls1 := LineSeg2{P1: Vec2{-1, 1}, P2: Vec2{1, 1}}
	p1, p2, p3 := Vec2{-1, 1}, Vec2{1, 1}, Vec2{0, 1}
	want1, want2, want3 := 0.0, 0.0, 0.0
	got1, got2, got3 := ls1.DistTo(p1), ls1.DistTo(p2), ls1.DistTo(p3)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	p1, p2, p3 = Vec2{-1, 2}, Vec2{-1, 0}, Vec2{-2, 1}
	want1, want2, want3 = 1.0, 1.0, 1.0
	got1, got2, got3 = ls1.DistTo(p1), ls1.DistTo(p2), ls1.DistTo(p3)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	p1, p2, p3 = Vec2{2, 1}, Vec2{1, 0}, Vec2{1, 2}
	want1, want2, want3 = 1.0, 1.0, 1.0
	got1, got2, got3 = ls1.DistTo(p1), ls1.DistTo(p2), ls1.DistTo(p3)
	if !tezd.CompareFloat64(want1, got1, 5) || !tezd.CompareFloat64(want2, got2, 5) || !tezd.CompareFloat64(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	p1, p2, p3 = Vec2{0.5, -1}, Vec2{2, 2}, Vec2{0.5, 3}
	want1, want2, want3 = 2.0, math.Sqrt2, 2
	got1, got2, got3 = ls1.DistTo(p1), ls1.DistTo(p2), ls1.DistTo(p3)
	if !tezd.CompareFloat64(want1, got1, 5) || !tezd.CompareFloat64(want2, got2, 5) || !tezd.CompareFloat64(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}

func TestLineSeg2_Right(t *testing.T) {
	ls1 := LineSeg2{P1: Vec2{1, 1}, P2: Vec2{1, 2}}

	want, got := true, ls1.Right(Vec2{2, 1}) &&
		ls1.Right(Vec2{2, -10}) &&
		ls1.Right(Vec2{2, 10}) &&
		!ls1.Right(Vec2{0, 1}) &&
		!ls1.Right(Vec2{0, -10}) &&
		!ls1.Right(Vec2{0, 10}) &&
		!ls1.Right(Vec2{1, 1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestLineSeg2_T(t *testing.T) {
	ls1 := LineSeg2{P1: Vec2{1, 1}, P2: Vec2{1, 2}}

	want, got := -1.0, ls1.T(Vec2{X: 0, Y: 0})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = 0.0, ls1.T(Vec2{X: 0, Y: 1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = 0.0, ls1.T(Vec2{X: 1, Y: 1})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = 0.5, ls1.T(Vec2{X: 1, Y: 1.5})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = 1.0, ls1.T(Vec2{X: 1, Y: 2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = 1.0, ls1.T(Vec2{X: 2, Y: 2})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = 2.0, ls1.T(Vec2{X: 2, Y: 3})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle2_Canonical(t *testing.T) {
	tr := Triangle2{
		Vec2{5, 0},
		Vec2{0, 5},
		Vec2{1, 1},
	}

	want, got := Triangle2{
		Vec2{1, 1},
		Vec2{5, 0},
		Vec2{0, 5},
	}, tr.Canonical()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tr = Triangle2{
		Vec2{0, 5},
		Vec2{5, 0},
		Vec2{1, 1},
	}

	want, got = Triangle2{
		Vec2{1, 1},
		Vec2{5, 0},
		Vec2{0, 5},
	}, tr.Canonical()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tr = Triangle2{
		Vec2{-1, 0},
		Vec2{1, 0},
		Vec2{0, 3},
	}

	want, got = Triangle2{
		Vec2{1, 0},
		Vec2{0, 3},
		Vec2{-1, 0},
	}, tr.Canonical()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tr = Triangle2{
		Vec2{1, 0},
		Vec2{-1, 0},
		Vec2{0, 3},
	}

	want, got = Triangle2{
		Vec2{1, 0},
		Vec2{0, 3},
		Vec2{-1, 0},
	}, tr.Canonical()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tr = Triangle2{
		Vec2{0, 3},
		Vec2{1, 0},
		Vec2{-1, 0},
	}

	want, got = Triangle2{
		Vec2{1, 0},
		Vec2{0, 3},
		Vec2{-1, 0},
	}, tr.Canonical()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle2_Circumcircle(t *testing.T) {
	cc := Triangle2{
		Vec2{1, 0},
		Vec2{0, 1},
		Vec2{math.Cos(math.Pi / 3), math.Sin(math.Pi / 3)},
	}.Circumcircle()

	{
		want, got := Vec2{0, 0}, cc.C
		if want.DistTo(got) > 1e-6 {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want, got := 1.0, cc.R
		if !tezd.CompareFloat64(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestTriangle2_CoincidesWith(t *testing.T) {
	tr := Triangle2{
		Vec2{5, 0},
		Vec2{0, 5},
		Vec2{1, 1},
	}

	want, got := true, tr.CoincidesWith(Triangle2{
		Vec2{1, 1},
		Vec2{5, 0},
		Vec2{0, 5},
	}) && !tr.CoincidesWith(Triangle2{
		Vec2{0, 0},
		Vec2{5, 0},
		Vec2{0, 5},
	})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle2_CircumcircleContains(t *testing.T) {
	tr := Triangle2{
		Vec2{1, 0},
		Vec2{0, 1},
		Vec2{math.Cos(math.Pi / 3), math.Sin(math.Pi / 3)},
	}.Anticlockwise()

	want, got := true, tr.CircumcircleContains(Vec2{0.5, 0.5}) &&
		tr.CircumcircleContains(Vec2{-0.5, -0.5}) &&
		tr.CircumcircleContains(Vec2{0.5, -0.5}) &&
		tr.CircumcircleContains(Vec2{-0.5, 0.5}) &&
		tr.CircumcircleContains(Vec2{0, 0})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want, got = false, tr.CircumcircleContains(Vec2{1.5, 1.5}) ||
		tr.CircumcircleContains(Vec2{-1.5, -1.5}) ||
		tr.CircumcircleContains(Vec2{1.5, -1.5}) ||
		tr.CircumcircleContains(Vec2{-1.5, 1.5})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

}

func TestTriangle2_Winding(t *testing.T) {
	tr1 := Triangle2{Vec2{0, 0}, Vec2{1, 0}, Vec2{0, 1}}
	want := true
	got := tr1.Winding()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	tr2 := Triangle2{Vec2{0, 0}, Vec2{0, 1}, Vec2{1, 0}}
	want = false
	got = tr2.Winding()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestTriangle2_AltitudeRatioC(t *testing.T) {
	tr1 := Triangle2{Vec2{0, 0}, Vec2{1, 0}, Vec2{0, 1}}

	want := true
	got := tr1.Winding()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want1, want2, want3 := 0.0, 0.0, 0.0
	got1, got2, got3 := tr1.AltitudeRatioC(Vec2{0, 1}), tr1.AltitudeRatioC(Vec2{-1, 1}), tr1.AltitudeRatioC(Vec2{1, 1})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 1.0, 1.0, 1.0
	got1, got2, got3 = tr1.AltitudeRatioC(Vec2{0, 0}), tr1.AltitudeRatioC(Vec2{-2, 0}), tr1.AltitudeRatioC(Vec2{2, 0})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 0.5, 0.5, 0.5
	got1, got2, got3 = tr1.AltitudeRatioC(Vec2{0, 0.5}), tr1.AltitudeRatioC(Vec2{-2, 0.5}), tr1.AltitudeRatioC(Vec2{2, 0.5})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = -1.0, -1.0, -1.0
	got1, got2, got3 = tr1.AltitudeRatioC(Vec2{0, 2}), tr1.AltitudeRatioC(Vec2{-2, 2}), tr1.AltitudeRatioC(Vec2{2, 2})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	tr1 = Triangle2{Vec2{1, 0}, Vec2{0, 0}, Vec2{0, 1}}

	want = false
	got = tr1.Winding()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want1, want2, want3 = 0.0, 0.0, 0.0
	got1, got2, got3 = tr1.AltitudeRatioC(Vec2{0, 1}), tr1.AltitudeRatioC(Vec2{-1, 1}), tr1.AltitudeRatioC(Vec2{1, 1})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 1.0, 1.0, 1.0
	got1, got2, got3 = tr1.AltitudeRatioC(Vec2{0, 0}), tr1.AltitudeRatioC(Vec2{-2, 0}), tr1.AltitudeRatioC(Vec2{2, 0})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 0.5, 0.5, 0.5
	got1, got2, got3 = tr1.AltitudeRatioC(Vec2{0, 0.5}), tr1.AltitudeRatioC(Vec2{-2, 0.5}), tr1.AltitudeRatioC(Vec2{2, 0.5})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = -1.0, -1.0, -1.0
	got1, got2, got3 = tr1.AltitudeRatioC(Vec2{0, 2}), tr1.AltitudeRatioC(Vec2{-2, 2}), tr1.AltitudeRatioC(Vec2{2, 2})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	tr1 = Triangle2{Vec2{1, 1}, Vec2{1, 2}, Vec2{2, 1.5}}

	want1, want2, want3 = -1.0, 0.5, 0.0
	got1, got2, got3 = tr1.AltitudeRatioC(Vec2{3, 5}), tr1.AltitudeRatioC(Vec2{1.5, -7}), tr1.AltitudeRatioC(Vec2{2, 10})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	tr1 = Triangle2{Vec2{1, 2}, Vec2{1, 1}, Vec2{2, 1.5}}

	want1, want2, want3 = -1.0, 0.5, 0.0
	got1, got2, got3 = tr1.AltitudeRatioC(Vec2{3, 5}), tr1.AltitudeRatioC(Vec2{1.5, -7}), tr1.AltitudeRatioC(Vec2{2, 10})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	tr1 = Triangle2{Vec2{-2, -2}, Vec2{0, 2}, Vec2{2, -2}}

	want1, want2, want3 = -1.0, 0.5, 0.0
	got1, got2, got3 = tr1.AltitudeRatioB(Vec2{-1, 6}), tr1.AltitudeRatioB(Vec2{1.5, 0}), tr1.AltitudeRatioB(Vec2{5, 2})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	tr1 = Triangle2{Vec2{2, -2}, Vec2{0, 2}, Vec2{-2, -2}}

	want1, want2, want3 = -1.0, 0.5, 0.0
	got1, got2, got3 = tr1.AltitudeRatioB(Vec2{-1, 6}), tr1.AltitudeRatioB(Vec2{1.5, 0}), tr1.AltitudeRatioB(Vec2{5, 2})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	tr1 = Triangle2{Vec2{0, -1}, Vec2{3, 2}, Vec2{3, 1}}

	want1, want2, want3 = -1.0, 0.5, 0.0
	got1, got2, got3 = tr1.AltitudeRatioA(Vec2{-3, 7}), tr1.AltitudeRatioA(Vec2{1.5, 0}), tr1.AltitudeRatioA(Vec2{0, -1})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	tr1 = Triangle2{Vec2{0, -1}, Vec2{3, 1}, Vec2{3, 2}}

	want1, want2, want3 = -1.0, 0.5, 0.0
	got1, got2, got3 = tr1.AltitudeRatioA(Vec2{-3, 7}), tr1.AltitudeRatioA(Vec2{1.5, 0}), tr1.AltitudeRatioA(Vec2{0, -1})
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}
