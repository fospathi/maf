package maf

import (
	"math"
	"math/rand"
	"testing"
	"time"

	"gitlab.com/fospathi/universal/interval"
	"gitlab.com/fospathi/universal/tezd"
)

func BenchmarkBez_ArcLength(b *testing.B) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, -0.5}, C: Vec{2, 0, -1}}
	for n := 0; n < b.N; n++ {
		bez.ArcLength()
	}
}

func BenchmarkBez_AtT(b *testing.B) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, -0.5}, C: Vec{2, 0, -1}}
	for n := 0; n < b.N; n++ {
		bez.AtT(0.5)
	}
}

func BenchmarkBez_Subdivide1(b *testing.B) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, 0}, C: Vec{2, 0, 0}}
	for n := 0; n < b.N; n++ {
		bez.Subdivide1(0.5)
	}
}

func BenchmarkBez_Subdivide2(b *testing.B) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, 0}, C: Vec{2, 0, 0}}
	for n := 0; n < b.N; n++ {
		bez.Subdivide2(0.3, 0.6)
	}
}

func TestBez_ArcLength(t *testing.T) {
	approx := func(b Bez, divs int) float64 {
		delta := 1 / float64(divs)
		l := 0.0
		for i := 1.0; i < float64(divs); i++ {
			t1, t2 := delta*float64(i-1), delta*i
			l += b.AtT(t1).To(b.AtT(t2)).Mag()
		}
		return l
	}

	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, 0}, C: Vec{2, 0, 0}}
	want := approx(bez, 5000)
	got := bez.ArcLength()
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{0, 0, 0}, B: Vec{1, 20, 0}, C: Vec{2, 0, 0}}
	want = approx(bez, 5000)
	got = bez.ArcLength()
	if !tezd.CompareFloat64(want, got, 1) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{1, 1, 1}, B: Vec{1, -30, 0}, C: Vec{5, -1, -1}}
	want = approx(bez, 30000)
	got = bez.ArcLength()
	if !tezd.CompareFloat64(want, got, 2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestBez_AtT(t *testing.T) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, -0.5}, C: Vec{2, 0, -1}}
	want := Vec{1, 0.5, -0.5}
	got := bez.AtT(0.5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{-1, 1, 0}, C: Vec{0.5, 0.5, 0}}
	want = Vec{-0.90625, 0.40625, 0}
	got = bez.AtT(0.25)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestBez_BendingEnergy(t *testing.T) {
	bez := Bez{A: Vec{-1, 0, 0}, B: Vec{0, 4.5, 0}, C: Vec{1, 0, 0}}
	want := 3.0
	got := bez.BendingEnergy()
	if !tezd.CompareFloat64(want, got, 1) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 3, 0}, C: Vec{1, 0, 0}}
	want = 2.0
	got = bez.BendingEnergy()
	if !tezd.CompareFloat64(want, got, 1) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 1.5, 0}, C: Vec{1, 0, 0}}
	want = 1.0
	got = bez.BendingEnergy()
	if !tezd.CompareFloat64(want, got, 1) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 0.3, 0}, C: Vec{1, 0, 0}}
	want = 0.1
	got = bez.BendingEnergy()
	if !tezd.CompareFloat64(want, got, 1) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestBez_Closest(t *testing.T) {
	bez := Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	want := 0.5
	got := bez.Closest(Vec{X: 0, Y: 2.5, Z: 0}).T
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	want = 0.0
	got = bez.Closest(Vec{X: -2, Y: -2, Z: 0}).T
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	want = 1.0
	got = bez.Closest(Vec{X: 2, Y: -2, Z: 0}).T
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{0, 0, 0}, B: Vec{0.5, 0.000001, 0}, C: Vec{1, 0, 0}}
	want = 0.75
	got = bez.Closest(Vec{X: 0.75, Y: 0, Z: 0}).T
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	{
		// Special linear cases where the midpoint of the endpoints coincides with the middle control point.
		bez = Bez{A: Vec{0, 0, 0}, B: Vec{1.0, 0, 0}, C: Vec{2, 0, 0}}
		want = 0.5
		got = bez.Closest(Vec{X: 1, Y: 0, Z: 0}).T
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		bez = Bez{A: Vec{-1, 0, 3}, B: Vec{0, 0, 3}, C: Vec{1, 0, 3}}
		want = 3.0
		got = bez.Closest(Vec{X: 0.5, Y: 0, Z: 0}).P.DistTo(Vec{X: 0.5, Y: 0, Z: 0})
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p := Vec{X: -0.5, Y: 0, Z: 0}
	want = bezClosestT(bez, p)
	got = bez.Closest(p).T
	if !tezd.CompareFloat64(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p = Vec{X: 0.5, Y: 0.2, Z: 0}
	want = bezClosestT(bez, p)
	got = bez.Closest(p).T
	if !tezd.CompareFloat64(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p = Vec{X: 100, Y: 1, Z: 3}
	want = bezClosestT(bez, p)
	got = bez.Closest(p).T
	if !tezd.CompareFloat64(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	bez = Bez{A: Vec{0, 0, 0}, B: Vec{0, 2, 0}, C: Vec{0, 0, 0}}
	p = Vec{X: 0.0001, Y: 0, Z: 0}
	want = bezClosestT(bez, p)
	got = bez.Closest(p).T
	if !tezd.CompareFloat64(want, got, 3) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestBez_ClosestPoints(t *testing.T) {
	testTwoClosest := func(bez Bez, p Vec) {
		n, c1, c2 := bez.ClosestPoints(p)
		want1, want2, want3 := bez.AtT(c1.T), bez.AtT(c2.T), 2
		got1, got2, got3 := c1.P, c2.P, n
		if !CompareVec(want1, got1, 5) || !CompareVec(want2, got2, 5) || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
				want1, got1, want2, got2, want3, got3)
		}
		want := false
		got := tezd.CompareFloat64(c1.T, c2.T, 2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	bez := Bez{A: Vec{0, 0, 0}, B: Vec{0, 3, 0}, C: Vec{0, 1, 0}}
	p := Vec{X: 0, Y: 1.3, Z: 0}
	testTwoClosest(bez, p)

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p = Vec{X: 0, Y: -1, Z: 0}
	testTwoClosest(bez, p)
	{
		_, c1, c2 := bez.ClosestPoints(p)
		wants := map[float64]struct{}{
			0.0: {},
			1.0: {},
		}
		delete(wants, c1.T)
		delete(wants, c2.T)
		want := 0
		got := len(wants)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	bez = Bez{A: Vec{0, 0, 1}, B: Vec{-20, 0, 0}, C: Vec{0, 0, -1}}
	p = Vec{X: -4, Y: -1, Z: 0}
	testTwoClosest(bez, p)
	{
		_, c1, c2 := bez.ClosestPoints(p)
		want := true
		got := tezd.CompareFloat64(c1.T, 1-c2.T, 5)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	bez = Bez{A: Vec{0, 0, 0}, B: Vec{0, 2, 0}, C: Vec{0, 0, 0}}
	p = Vec{X: 0.0001, Y: 0, Z: 0}
	testTwoClosest(bez, p)
	{
		_, c1, c2 := bez.ClosestPoints(p)
		wants := map[float64]struct{}{
			0.0: {},
			1.0: {},
		}
		delete(wants, c1.T)
		delete(wants, c2.T)
		want := 0
		got := len(wants)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Single solution cases
	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p = Vec{X: 0, Y: 2.5, Z: 0}
	n, c1, _ := bez.ClosestPoints(p)
	want1, want2 := 1, 0.5
	got1, got2 := n, c1.T
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p = Vec{X: -2, Y: -2, Z: 0}
	n, c1, _ = bez.ClosestPoints(p)
	want1, want2 = 1, 0.0
	got1, got2 = n, c1.T
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p = Vec{X: 2, Y: -2, Z: 0}
	n, c1, _ = bez.ClosestPoints(p)
	want1, want2 = 1, 1.0
	got1, got2 = n, c1.T
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	{
		bez = Bez{A: Vec{0, 0, 0}, B: Vec{0.5, 0.000001, 0}, C: Vec{1, 0, 0}}
		p = Vec{X: 0.75, Y: 0, Z: 0}
		_, c1, _ = bez.ClosestPoints(p)
		want := 0.75
		got := c1.T
		if !tezd.CompareFloat64(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		// Special linear cases where the midpoint of the endpoints coincides with the middle control point.
		bez = Bez{A: Vec{0, 0, 0}, B: Vec{1.0, 0, 0}, C: Vec{2, 0, 0}}
		p = Vec{X: 1, Y: 0, Z: 0}
		_, c1, _ = bez.ClosestPoints(p)
		want := 0.5
		got := c1.T
		if !tezd.CompareFloat64(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		bez = Bez{A: Vec{-1, 0, 3}, B: Vec{0, 0, 3}, C: Vec{1, 0, 3}}
		p = Vec{X: 0.5, Y: 0, Z: 0}
		_, c1, _ = bez.ClosestPoints(p)
		want = 3.0
		got = c1.P.DistTo(Vec{X: 0.5, Y: 0, Z: 0})
		if !tezd.CompareFloat64(want, got, 2) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p = Vec{X: -0.5, Y: 0, Z: 0}
	n, c1, _ = bez.ClosestPoints(p)
	want1, want2 = 1, bez.Closest(p).T
	got1, got2 = n, c1.T
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p = Vec{X: 0.5, Y: 0.2, Z: 0}
	n, c1, _ = bez.ClosestPoints(p)
	want1, want2 = 1, bez.Closest(p).T
	got1, got2 = n, c1.T
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	bez = Bez{A: Vec{-1, 0, 0}, B: Vec{0, 2, 0}, C: Vec{1, 0, 0}}
	p = Vec{X: 100, Y: 1, Z: 3}
	n, c1, _ = bez.ClosestPoints(p)
	want1, want2 = 1, bez.Closest(p).T
	got1, got2 = n, c1.T
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	bez = Bez{A: Vec{0, 0, 0}, B: Vec{0, 5, 0}, C: Vec{0, 3, 0}}
	p = Vec{X: 0, Y: 1, Z: 0}
	n, c1, _ = bez.ClosestPoints(p)
	want1, want2 = 1, bez.Closest(p).T
	got1, got2 = n, c1.T
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 5) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
}

func TestBez_Plane(t *testing.T) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, 0}, C: Vec{2, 0, 0}}
	a := bez.Plane().Normal.Angle(Vec{0, 0, 1})
	want := true
	got := tezd.CompareFloat64(a, math.Pi, 5) || tezd.CompareFloat64(a, 0, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestBez_Subdivide1(t *testing.T) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, -0.5}, C: Vec{2, 0, -1}}
	want1 := Bez{A: Vec{0, 0, 0}, B: Vec{0.5, 0.5, -0.25}, C: Vec{1, 0.5, -0.5}}
	want2 := Bez{A: Vec{1, 0.5, -0.5}, B: Vec{1.5, 0.5, -0.75}, C: Vec{2, 0, -1}}
	got1, got2 := bez.Subdivide1(0.5)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
}

func TestBez_Subdivide2(t *testing.T) {
	bez := Bez{A: Vec{0, 0, 0}, B: Vec{1, 1, -0.5}, C: Vec{2, 0, -1}}
	want := Bez{A: Vec{1, 0.5, -0.5}, B: Vec{1.5, 0.5, -0.75}, C: Vec{2, 0, -1}}
	_, _, got := bez.Subdivide2(0.25, 0.5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Bez{A: Vec{0, 0, 0}, B: Vec{0.5, 0.5, -0.25}, C: Vec{1, 0.5, -0.5}}
	got, _, _ = bez.Subdivide2(0.75, 0.5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	intersection, _, _ := bez.Tangent(0.25).ClosestPoints(bez.Tangent(0.75))
	want = Bez{A: bez.AtT(0.25), B: intersection, C: bez.AtT(0.75)}
	_, got, _ = bez.Subdivide2(0.25, 0.75)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestBez_PlaneTangencyLocus(t *testing.T) {
	closest := func(b Bez, p Plane) float64 {
		const divs = 200.0
		tDelta := 1 / divs
		closest := math.Inf(0)
		for i := 0; i < divs; i++ {
			if d := p.DistTo(b.AtT(float64(i) * tDelta)); d < closest {
				closest = d
			}
		}
		return closest
	}
	bez := Bez{A: Vec{1, 1, 0}, B: Vec{2, 2 * math.Pi, 0}, C: Vec{3, 1, 0}}
	plane := Plane{P: Vec{0, 2, 0}, Normal: Vec{0, 1, 0}}
	rdm := rand.New(rand.NewSource(time.Now().UnixNano()))

	const tolerance = 0.0001
	locus := bez.PlaneTangencyLocus(plane)
	for i := 0; i < 50; i++ {
		middle := locus.P.Add(locus.Dir.Scale(float64(rdm.Uint32() % 100)))
		b := Bez{A: bez.A, B: middle, C: bez.C}
		gap := closest(b, plane)
		want := true
		got := gap < tolerance
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

}

func TestBez_CircleTangency(t *testing.T) {
	bez := Bez{A: Vec{-2, 1, 0}, B: Vec{0, 0, 0}, C: Vec{-2, -1, 0}}
	sph := Sphere{C: Vec{}, R: 1}
	tl := bez.CircleTangency(sph)

	want := interval.In{-math.Pi / 2, math.Pi / 2}
	got := tl.Farther
	if !tezd.CompareIntervals(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
