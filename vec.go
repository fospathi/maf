package maf

import (
	"math"
)

// Vec2 is a two-dimensional position/direction vector.
type Vec2 struct {
	X, Y float64
}

// Vec is a three-dimensional position/direction vector.
type Vec struct {
	X, Y, Z float64
}

// Vec4 is a four-dimensional position/direction vector.
type Vec4 struct {
	X, Y, Z, W float64
}

// ScalarTripleProduct returns the signed volume of the parallelpiped spanned by
// three vectors.
//
// volume = a⋅(b×c) == c⋅(a×b) == b⋅(c×a) and a⋅(b×c) = (a×b)⋅c
//
// The returned value is zero if all three vectors are coplanar or one of them
// is zero.
//
// The returned value is < 0 for a left-handed set and > 0 for a right-handed
// set.
func ScalarTripleProduct(a, b, c Vec) float64 {
	return a.X*(b.Y*c.Z-c.Y*b.Z) - a.Y*(b.X*c.Z-c.X*b.Z) + a.Z*(b.X*c.Y-c.X*b.Y)
}

// Lower dimension vector.
func (v1 Vec4) Lower() Vec {
	return Vec{X: v1.X, Y: v1.Y, Z: v1.Z}
}

// AcuteAngle returns the positive angle, measured in radians, in the range
// 0..Pi/2 inclusive between the directional line implied by the receiver vector
// and the directional line implied by the argument vector.
func (v1 Vec) AcuteAngle(v2 Vec) (rads float64) {
	a := v1.Angle(v2)
	if a > math.Pi/2 {
		return math.Pi - a
	}
	return a
}

// Angle returns the positive angle, measured in radians, between the receiver
// vector and the argument vector.
//
// Special cases are:
//   - either vector is the zero vector: returns NaN
//   - either vector has an Inf or NaN element: returns NaN
func (v1 Vec) Angle(v2 Vec) (rads float64) {
	cosAngle := (v1.X*v2.X + v1.Y*v2.Y + v1.Z*v2.Z) /
		math.Sqrt((v1.X*v1.X+v1.Y*v1.Y+v1.Z*v1.Z)*(v2.X*v2.X+v2.Y*v2.Y+v2.Z*v2.Z))
	rads = math.Acos(cosAngle)
	if math.IsNaN(rads) {
		if v1.isProper() && v2.isProper() {
			if cosAngle <= -1 {
				rads = math.Pi
			} else {
				rads = 0
			}
		}
	}
	return rads
}

// Add adds the receiver vector to the argument vector and returns the resulting
// vector.
func (v1 Vec) Add(v2 Vec) Vec {
	return Vec{v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z}
}

// Cross returns the vector product of the receiver vector and the argument
// vector.
func (v1 Vec) Cross(v2 Vec) Vec {
	return Vec{v1.Y*v2.Z - v1.Z*v2.Y, v1.Z*v2.X - v1.X*v2.Z, v1.X*v2.Y - v1.Y*v2.X}
}

// DistTo returns the distance between the receiver position vector and the
// argument position vector.
func (v1 Vec) DistTo(v2 Vec) float64 {
	x, y, z := v1.X-v2.X, v1.Y-v2.Y, v1.Z-v2.Z
	return math.Sqrt(x*x + y*y + z*z)
}

// DistToSqd returns the square of the distance between the receiver position
// vector and the argument position vector.
func (v1 Vec) DistToSqd(v2 Vec) float64 {
	x, y, z := v1.X-v2.X, v1.Y-v2.Y, v1.Z-v2.Z
	return x*x + y*y + z*z
}

// DivTo returns the position vector that divides the line segment from the
// receiver to the argument vector in the ratio a to b.
func (v1 Vec) DivTo(v2 Vec, a float64, b float64) Vec {
	// p = v1 + (a/(a+b))(v2-v1)
	//   = v1(b/(a+b)) + v2(a/(a+b))
	return v1.Scale(b / (a + b)).Add(v2.Scale(a / (a + b)))
}

// Dot returns the dot product of the receiver vector and the argument vector.
func (v1 Vec) Dot(v2 Vec) float64 {
	return v1.X*v2.X + v1.Y*v2.Y + v1.Z*v2.Z
}

// Higher dimension vector with the new dimension set to the given value.
func (v1 Vec) Higher(w float64) Vec4 {
	return Vec4{X: v1.X, Y: v1.Y, Z: v1.Z, W: w}
}

// IsParallel returns whether the positive angle between the receiver and
// argument vector's directions is either zero or Pi to within the given
// positive tolerance argument, measured in radians.
func (v1 Vec) IsParallel(v2 Vec, tolerance float64) bool {
	a := v1.Angle(v2)
	return a <= tolerance || a >= math.Pi-tolerance
}

// IsPerpendicular returns whether the positive angle between the receiver and
// argument vector's directions is Pi/2 to within the given positive tolerance
// argument, measured in radians.
func (v1 Vec) IsPerpendicular(v2 Vec, tolerance float64) bool {
	a := v1.Angle(v2)
	return a >= (HalfPi-tolerance) && a <= (HalfPi+tolerance)
}

// Lower dimension vector.
func (v1 Vec) Lower() Vec2 {
	return Vec2{X: v1.X, Y: v1.Y}
}

// Mag returns the length of the receiver vector.
func (v1 Vec) Mag() float64 {
	return math.Sqrt(v1.X*v1.X + v1.Y*v1.Y + v1.Z*v1.Z)
}

// MagSqd returns the square of the length of the receiver vector.
func (v1 Vec) MagSqd() float64 {
	return v1.X*v1.X + v1.Y*v1.Y + v1.Z*v1.Z
}

// Map returns the vector whose components are obtained by applying the argument
// function to each of the receiver vector's components.
func (v1 Vec) Map(f func(float64) float64) Vec {
	return Vec{X: f(v1.X), Y: f(v1.Y), Z: f(v1.Z)}
}

// MidTo adds the receiver vector to the argument vector, scales the result by
// 0.5, and returns the resulting vector.
func (v1 Vec) MidTo(v2 Vec) Vec {
	return Vec{(v1.X + v2.X) * 0.5, (v1.Y + v2.Y) * 0.5, (v1.Z + v2.Z) * 0.5}
}

// Opposite returns a vector with the same magnitude as the receiver vector but
// with the opposite direction.
func (v1 Vec) Opposite() Vec {
	return Vec{-1 * v1.X, -1 * v1.Y, -1 * v1.Z}
}

// Perpendicular returns a unit vector which is normal to the receiver vector.
func (v1 Vec) Perpendicular() Vec {
	var nonPar Vec
	dX := v1.Dot(PosX)
	dY := v1.Dot(PosY)
	if dX*dX < dY*dY {
		nonPar = PosX
	} else {
		nonPar = PosY
	}
	return v1.Cross(nonPar).Unit()
}

// PlaneResolution is the orthogonal projection of the receiver vector onto the
// argument plane.
func (v1 Vec) PlaneResolution(p Plane) Vec {
	return Plane{Normal: p.Normal}.Closest(v1)
}

// Resolution is the orthogonal projection of the receiver vector onto a
// straight line parallel to the argument vector.
func (v1 Vec) Resolution(v2 Vec) Vec {
	unit := v2.Unit()
	return unit.Scale(v1.Dot(unit))
}

// Scale returns the scalar multiple of the receiver vector.
//
// The magnitude of the returned vector is the product of the receiver vector's
// magnitude and the argument.
func (v1 Vec) Scale(k float64) Vec {
	return Vec{v1.X * k, v1.Y * k, v1.Z * k}
}

// Subtract subtracts the receiver vector from the argument vector and returns
// the resulting vector.
func (v1 Vec) Subtract(v2 Vec) Vec {
	return Vec{v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z}
}

// To returns the translation vector that if added to the receiver vector would
// yield the argument vector.
func (v1 Vec) To(v2 Vec) Vec {
	return Vec{v2.X - v1.X, v2.Y - v1.Y, v2.Z - v1.Z}
}

// Unit returns a vector which has the same direction as the receiver vector and
// a magnitude of 1. The receiver vector shall have non-zero magnitude.
func (v1 Vec) Unit() Vec {
	divisor := 1 / math.Sqrt(v1.X*v1.X+v1.Y*v1.Y+v1.Z*v1.Z)
	return Vec{v1.X * divisor, v1.Y * divisor, v1.Z * divisor}
}

func (v1 Vec) hasInfOrNaN() bool {
	return math.IsInf(v1.X, 0) || math.IsNaN(v1.X) ||
		math.IsInf(v1.Y, 0) || math.IsNaN(v1.Y) ||
		math.IsInf(v1.Z, 0) || math.IsNaN(v1.Z)
}

func (v1 Vec) isProper() bool {
	return !v1.hasInfOrNaN() && !v1.isZero()
}

func (v1 Vec) isZero() bool {
	return v1 == Vec{}
}

// Add adds the receiver vector to the argument vector and returns the resulting
// vector.
func (v1 Vec2) Add(v2 Vec2) Vec2 {
	return Vec2{v1.X + v2.X, v1.Y + v2.Y}
}

// Angle returns the positive angle, measured in radians, between the receiver
// vector and the argument vector.
//
// Special cases are:
//
//	either vector is the zero vector = NaN
//	either vector has an Inf or NaN element = NaN
func (v1 Vec2) Angle(v2 Vec2) (rads float64) {
	cosAngle := (v1.X*v2.X + v1.Y*v2.Y) /
		math.Sqrt((v1.X*v1.X+v1.Y*v1.Y)*(v2.X*v2.X+v2.Y*v2.Y))
	rads = math.Acos(cosAngle)
	if math.IsNaN(rads) {
		if v1.isProper() && v2.isProper() {
			if cosAngle <= -1 {
				rads = math.Pi
			} else {
				rads = 0
			}
		}
	}
	return rads
}

// Anticlockwise positive angle, measured in radians, between the receiver
// vector and the argument vector.
func (v1 Vec2) Anticlockwise(v2 Vec2) float64 {
	a := v1.Angle(v2)
	if a == 0 || a == math.Pi {
		return a
	}
	if v1.Higher(0).Cross(v2.Higher(0)).Z > 0 {
		return a
	}
	return TwoPi - a
}

// Clockwise positive angle, measured in radians, between the receiver vector
// and the argument vector.
func (v1 Vec2) Clockwise(v2 Vec2) float64 {
	a := v1.Angle(v2)
	if a == 0 || a == math.Pi {
		return a
	}
	if v1.Higher(0).Cross(v2.Higher(0)).Z > 0 {
		return TwoPi - a
	}
	return a
}

// CosAngle returns the cosine of the angle between the receiver vector and the
// argument vector.
func (v1 Vec2) CosAngle(v2 Vec2) float64 {
	return (v1.X*v2.X + v1.Y*v2.Y) /
		math.Sqrt((v1.X*v1.X+v1.Y*v1.Y)*(v2.X*v2.X+v2.Y*v2.Y))
}

// DistTo returns the distance between the receiver position vector and the
// argument position vector.
func (v1 Vec2) DistTo(v2 Vec2) float64 {
	x, y := v1.X-v2.X, v1.Y-v2.Y
	return math.Sqrt(x*x + y*y)
}

// DistToSqd returns the square of the distance between the receiver position
// vector and the argument position vector.
func (v1 Vec2) DistToSqd(v2 Vec2) float64 {
	x, y := v1.X-v2.X, v1.Y-v2.Y
	return x*x + y*y
}

// Dot returns the dot product of the receiver vector and the argument vector.
func (v1 Vec2) Dot(v2 Vec2) float64 {
	return v1.X*v2.X + v1.Y*v2.Y
}

// Higher dimension vector with the new dimension set to the given value.
func (v1 Vec2) Higher(z float64) Vec {
	return Vec{X: v1.X, Y: v1.Y, Z: z}
}

// IsParallel returns whether the positive angle between the receiver and
// argument vector's directions is either zero or Pi to within the given
// positive tolerance argument, measured in radians.
func (v1 Vec2) IsParallel(v2 Vec2, tolerance float64) bool {
	a := v1.Angle(v2)
	return a <= tolerance || a >= math.Pi-tolerance
}

// Mag returns the length of the receiver vector.
func (v1 Vec2) Mag() float64 {
	return math.Sqrt(v1.X*v1.X + v1.Y*v1.Y)
}

// MagSqd returns the square of the length of the receiver vector.
func (v1 Vec2) MagSqd() float64 {
	return v1.X*v1.X + v1.Y*v1.Y
}

// MidTo adds the receiver vector to the argument vector, scales the result by
// 0.5, and returns the resulting vector.
func (v1 Vec2) MidTo(v2 Vec2) Vec2 {
	return Vec2{(v1.X + v2.X) * 0.5, (v1.Y + v2.Y) * 0.5}
}

// Normal returns a unit vector which has the direction of the result of
// rotating the receiver vector anticlockwise by Pi/2 radians.
func (v1 Vec2) Normal() Vec2 {
	scale := 1 / math.Sqrt(v1.X*v1.X+v1.Y*v1.Y)
	return Vec2{-v1.Y * scale, v1.X * scale}
}

// Scale returns the scalar multiple of the receiver vector.
//
// The magnitude of the returned vector is the product of the receiver vector's
// magnitude and the argument.
func (v1 Vec2) Scale(k float64) Vec2 {
	return Vec2{v1.X * k, v1.Y * k}
}

// To returns the translation vector that if added to the receiver vector would
// yield the argument vector.
func (v1 Vec2) To(v2 Vec2) Vec2 {
	return Vec2{v2.X - v1.X, v2.Y - v1.Y}
}

// Unit returns a vector which has the same direction as the receiver vector and
// a magnitude of 1. The receiver vector shall have non-zero magnitude.
func (v1 Vec2) Unit() Vec2 {
	divisor := 1 / math.Sqrt(v1.X*v1.X+v1.Y*v1.Y)
	return Vec2{v1.X * divisor, v1.Y * divisor}
}

// Vec returns the three dimensional equivalent of the receiver vector.
func (v1 Vec2) Vec() Vec {
	return Vec{v1.X, v1.Y, 0}
}

func (v1 Vec2) hasInfOrNaN() bool {
	return math.IsInf(v1.X, 0) ||
		math.IsNaN(v1.X) ||
		math.IsInf(v1.Y, 0) ||
		math.IsNaN(v1.Y)
}

func (v1 Vec2) isProper() bool {
	return !v1.hasInfOrNaN() && !v1.isZero()
}

func (v1 Vec2) isZero() bool {
	return v1 == Vec2{}
}
