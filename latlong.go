package maf

import (
	"math"
)

// LatHR specifies a latitudinal circle on a sphere.
type LatHR struct {
	// The latitudinal circle's signed perpendicular distance from the sphere's
	// equatorial plane.
	H float64
	// The latitudinal circle's radius around the pole axis.
	R float64
}

// EquiangularLatitudes returns for a sphere with the argument radius the height
// and radius of latitudinal circles (separated by equal angles subtended at the
// sphere's centre) beginning with the bottom most latitude.
func EquiangularLatitudes(R float64, n int) []LatHR {
	result := make([]LatHR, n)

	//                    G
	//                    |
	//            - - - - | - - - - F           A cross section of a unit sphere
	//                    |         /           with 5 latitudinal circles.
	//                    |       /
	//      - - - - - - - | H - / - - - -,E     The circular arcs AB, BC, CD,...
	//                    |   /    ,            subtend equal angles at O.
	//                    | /,
	//    - - - - - - - - | - - - - - - - - D
	//                    | O
	//                    |
	//      - - - - - - - | - - - - - - - C
	//                    |
	//                    |
	//            - - - - | - - - - B
	//                    |
	//                    A
	//
	// Angle AOB = BOC = COD = DOE = EOF = FOG
	//
	// For a unit sphere length OE = 1
	//     by definition length OH = h
	//                   angle OHE = right angle
	//              thus length HE = √(1 - h²)
	//
	delta := math.Pi / float64(n+1)
	// TODO take advantage of the symmetrical nature of this data.
	for i := 0; i < n; i++ {
		h := math.Sin(-math.Pi/2 + float64(i+1)*delta)
		r := math.Sqrt(1 - h*h)
		result[i] = LatHR{H: h * R, R: r * R} // Scale the unit sphere by R.
	}
	if n > 0 && n%2 != 0 {
		// By definition a central latitude has exactly zero h.
		result[(n-1)/2].H = 0
	}
	return result
}

// EquidistantLatitudes returns for a sphere with the argument radius the height
// and radius of latitudinal circles (equidistantly spaced along the axis)
// beginning with the bottom most latitude.
func EquidistantLatitudes(R float64, n int) []LatHR {
	result := make([]LatHR, n)

	//
	//                     . .
	//                     |      .
	//                     |         .
	//           - - - - - | - - - - - .       A cross section of a unit sphere
	//                     |            .      with 3 latitudinal circles.
	//                     | O           .
	//       - - - - - - - | - - - - - - -     The perpendicular gap between the
	//                     |             .     circles has length 2 / (3 + 1).
	//                     | H        P .
	//           - - - - - | - - - - - .
	//                     |       .
	//                     |. .
	//
	// For a unit sphere length OP = 1
	//     by definition length OH = h
	//                   angle OHP = right angle
	//              thus length PH = √(1 - h²)
	//
	delta := 2.0 / float64(n+1)
	for i := 0; i < n; i++ {
		h := -1 + float64(i+1)*delta
		r := math.Sqrt(1 - h*h)
		result[i] = LatHR{H: h * R, R: r * R} // Scale the unit sphere by R.
	}
	if n > 0 && n%2 != 0 {
		// By definition a central latitude has exactly zero h.
		result[(n-1)/2].H = 0
	}
	return result
}
