package kinem_test

import (
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
)

func TestCircular(t *testing.T) {
	r := 2.0
	w := maf.HalfPi
	cir := kinem.Circular{R: r, W: w}

	{
		want := maf.Vec{Y: 2}
		got := cir.Displacement(1)
		if !maf.CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		// v = rw in circular motion.
		want := maf.Vec{X: -r * w}
		got := cir.Velocity(1)
		if !maf.CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		// a = vv/r in circular motion.
		v := r * w
		want := maf.Vec{Y: -v * v / r}
		got := cir.Acceleration(1)
		if !maf.CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
