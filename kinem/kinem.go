package kinem

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// A Motion of a particle.
type Motion interface {
	Displacement(t float64) maf.Vec
	Velocity(t float64) maf.Vec
	Acceleration(t float64) maf.Vec
}

// Components of a vector property of a rigid bodies motion.
type Components struct {
	Rotational    maf.Vec
	Translational maf.Vec
}

// A Particle of a rigid body.
type Particle struct {
	// The fixed angular displacement of the particle measured from some
	// reference position.
	Angle float64
	// The fixed perpendicular distance of the particle from the rotation axis.
	Distance float64
}

// Curvature is the magnitude of the curvature for the trace of the motion at
// the given parameter value.
func Curvature(mot Motion, t float64) float64 {
	v, a := mot.Velocity(t), mot.Acceleration(t)
	vMag := v.Mag()
	// k = | v × a |
	//     ---------
	//       |v|^3
	return v.Cross(a).Mag() / (vMag * vMag * vMag)
}

// Frenet frame at the given parameter value on the trace of the given motion.
//
// The returned orthonormal basis has the X-axis as the velocity, the Y-axis as
// the normal, and the Z-axis as the binormal, at t.
func Frenet(mot Motion, t float64) maf.Basis {
	v, a := mot.Velocity(t), mot.Acceleration(t)
	tan := v.Unit()
	n := v.Cross(a).Cross(v).Unit()
	return maf.Basis{X: tan, Y: n, Z: tan.Cross(n)}
}

// A Displacementer is a motion with a known displacement at a given parameter
// value.
type Displacementer interface {
	Displacement(t float64) maf.Vec
}

// NumericalVelocity is a rough approximation of the given motion's velocity at
// the given parameter value.
func NumericalVelocity(mot Displacementer, t float64) maf.Vec {
	f := mot.Displacement

	// An approximation for the first derivative is
	//
	//   f'(x) ~= f(x+h) - f(x-h)
	//            ---------------
	//                   2h
	return f(t + h).Subtract(f(t - h)).Scale(1 / (2 * h))
}

// NumericalAcceleration is a very rough approximation of the given motion's
// acceleration at the given parameter value.
func NumericalAcceleration(mot Displacementer, t float64) maf.Vec {
	f := mot.Displacement
	h := math.Sqrt(h) // Use a larger h so that the h*h denominator isn't too small.

	// An approximation for the second derivative is
	//
	//   f''(x) ~= f(x+h) - 2f(x) + f(x+h)
	//             -----------------------
	//                      h * h
	return (f(t + h).Add(f(t).Scale(-2)).Add(f(t - h))).Scale(1 / (h * h))
}

// A Velocityer is a motion with a known velocity at a given parameter value.
type Velocityer interface {
	Velocity(t float64) maf.Vec
}

// NumericalAccel is a rough approximation of the given motion's acceleration at
// the given parameter value.
func NumericalAccel(mot Velocityer, t float64) maf.Vec {
	f := mot.Velocity

	// An approximation for the first derivative is
	//
	//   f'(v) ~= f(v+h) - f(v-h)
	//            ---------------
	//                   2h
	//
	// The first derivative of the velocity is the acceleration.
	return f(t + h).Subtract(f(t - h)).Scale(1 / (2 * h))
}

// The small variable increment for numerical approximations.
//
// The value of h is a balance between increment size (smaller is better) and
// significant digits (more is better).
const h = 1e-8
