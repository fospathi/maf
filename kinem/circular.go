package kinem

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// Circular is a uniform circular motion.
//
// The particle starts on the positive X-axis and is constrained to the
// XY-plane.
type Circular struct {
	// The radius of the circular motion.
	R float64
	// The constant angular velocity of the circular motion measured in radians
	// per unit time.
	W float64
}

// Displacement of the motion from the origin at the given motion parameter.
func (mot Circular) Displacement(t float64) maf.Vec {
	s, c := math.Sincos(mot.W * t)
	return maf.Vec{X: mot.R * c, Y: mot.R * s}
}

// Velocity of the motion at the given motion parameter.
func (mot Circular) Velocity(t float64) maf.Vec {
	s, c := math.Sincos(mot.W * t)
	return maf.Vec{X: -mot.W * mot.R * s, Y: mot.W * mot.R * c}
}

// Acceleration of the motion at the given motion parameter.
func (mot Circular) Acceleration(t float64) maf.Vec {
	s, c := math.Sincos(mot.W * t)
	// Let i and j be unit vectors in the X and Y-axis directions respectively:
	//   s(t) =    r*cos(wt) i +   r*sin(wt) j
	//  s'(t) =  -wr*sin(wt) i +  wr*cos(wt) j
	// s''(t) = -wwr*cos(wt) i - wwr*sin(wt) j
	return maf.Vec{
		X: -mot.W * mot.W * mot.R * c,
		Y: -mot.W * mot.W * mot.R * s,
	}
}
