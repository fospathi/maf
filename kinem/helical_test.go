package kinem_test

import (
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
)

func TestHelical(t *testing.T) {
	p := 1.0
	r := 2.0
	w := maf.HalfPi
	hel := kinem.Helical{P: p, R: r, W: w}

	period := maf.TwoPi / w
	vz := p / period

	{
		want := maf.Vec{Y: 2, Z: vz}
		got := hel.Displacement(1)
		if !maf.CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		want = maf.Vec{X: 2, Z: p}
		got = hel.Displacement(period)
		if !maf.CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := kinem.NumericalVelocity(hel, 1)
		got := hel.Velocity(1)
		if !maf.CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := kinem.NumericalAcceleration(hel, 1)
		got := hel.Acceleration(1)
		if !maf.CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		want = kinem.NumericalAccel(hel, 1)
		if !maf.CompareVec(want, got, 5) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
