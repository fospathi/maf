package kinem

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// Archimedean is the spiral motion of a particle moving along a radial path on
// a rotating turntable.
//
// The particle moves outwards from the centre and the turntable rotates
// anticlockwise around the Z-axis with the radial line starting at the positive
// X-axis.
type Archimedean struct {
	// Initial displacement from the origin along the positive X-axis.
	D float64
	// The magnitude of the constant radial speed component of the particle's
	// motion.
	V float64
	// The magnitude of the constant angular velocity component of the
	// particle's motion (measured in radians per time unit).
	W float64
}

// Displacement of the motion from the origin at the given motion parameter.
func (mot Archimedean) Displacement(t float64) maf.Vec {
	s, c := math.Sincos(mot.W * t)
	r := mot.D + mot.V*t
	return maf.Vec{X: r * c, Y: r * s}
}

// Polar method
// ============
//
// The general polar formula for velocity and acceleration are
//
//                .  ^              .  ^
//   v =         (r) r +         (r θ) θ
//
//
//        ‥     . .  ^      ‥     . .  ^
//   a = (r - r θ θ) r + (r θ + 2 r θ) θ
//
// where
//
//   ^
//   r =  cos(0) i + sin(0) j
//
//   ^
//   θ = -sin(0) i + cos(0) j
//
// Let r = d + st and θ = wt
//
// Then, for an Archimedean spiral
//   .               ‥
//   r = s           r = 0
//
//   .               ‥
//   θ = w           θ = 0
//
// Thus, for an Archimedean spiral
//
//   v = s [cos(0) i + sin(0) j] + rw [-sin(0) i + cos(0) j]
//     = (s cos(0) - rw sin(0)) i + (s sin(0) + rw cos(0)) j
//
//   a = (-rww) [cos(0) i + sin(0) j] + 2sw [-sin(0) i + cos(0) j]

// Cartesian method
// ================
//
// Note that v is the scalar constant straight line radial velocity
//           w is the scalar constant angular velocity
//           θ = wt       (angle)
//           r = d+vt     (radius)
//
//   x(t) = r cos(θ)
//   x(t) = (d+vt) cos(wt)                           | r cos
//  x'(t) = v cos(wt) - w (d+vt) sin(wt)             | v cos - w r sin
// x''(t) = -2vw sin(wt) - w² (d+vt) cos(wt)         | -2 v w sin - w w r cos
//
//   y(t) = r sin(θ)
//   y(t) = (d+vt) sin(wt)                           | r sin
//  y'(t) = v sin(wt) + w (d+vt) cos(wt)             | v sin + w r cos
// y''(t) = 2vw cos(wt) - w² (d+vt) sin(wt)          | 2 v w cos - w w r sin

// Velocity of the motion at the given motion parameter.
func (mot Archimedean) Velocity(t float64) maf.Vec {
	s, c := math.Sincos(mot.W * t)
	r := mot.D + mot.V*t
	return maf.Vec{
		X: mot.V*c - mot.W*r*s,
		Y: mot.V*s + mot.W*r*c,
	}
}

// Acceleration of the motion at the given motion parameter.
func (mot Archimedean) Acceleration(t float64) maf.Vec {
	s, c := math.Sincos(mot.W * t)
	r := mot.D + mot.V*t
	return maf.Vec{
		X: -2*mot.V*mot.W*s - mot.W*mot.W*r*c,
		Y: 2*mot.V*mot.W*c - mot.W*mot.W*r*s,
	}
}
