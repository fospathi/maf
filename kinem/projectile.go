package kinem

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// G is an approximation of the acceleration due to gravity on the Earth's
// surface, measured in m/s².
const G = -9.80665

// Projectile motion constrained to the XY-plane and starting at the origin and
// subject to a constant acceleration parallel to the Y-axis.
type Projectile struct {
	// The acceleration. A positive acceleration is in the direction of the
	// positive Y-axis.
	A float64
	// The initial velocity.
	U maf.Vec
}

// Displacement of the motion from the origin at the given motion parameter.
func (mot Projectile) Displacement(t float64) maf.Vec {
	return maf.Vec{
		X: mot.U.X * t,
		Y: mot.U.Y*t + 0.5*mot.A*t*t,
	}
}

// Velocity of the motion at the given motion parameter.
func (mot Projectile) Velocity(t float64) maf.Vec {
	return maf.Vec{
		X: mot.U.X,
		Y: mot.U.Y + mot.A*t,
	}
}

// Acceleration of the motion at the given motion parameter.
func (mot Projectile) Acceleration(t float64) maf.Vec {
	return maf.Vec{X: 0, Y: mot.A}
}

// LaunchAngle of the projectile.
func (mot Projectile) LaunchAngle() float64 {
	return math.Atan2(mot.U.Y, mot.U.X)
}

// TurningPoint is the position in the trace of the motion at which the
// projectile changes direction.
//
// The motion's acceleration shall be non zero.
func (mot Projectile) TurningPoint() maf.Vec {
	// In the vertical sense:
	//   v = u + at, where v = 0, ==> t = -u/a
	return mot.Displacement(-mot.U.Y / mot.A)
}

// ProjectileTargeter finds projectile motions which are launched from the
// origin and that will hit a target in the XY-plane.
type ProjectileTargeter struct {
	// The acceleration. A positive acceleration is in the direction of the
	// positive Y-axis.
	A float64
	// The target to hit with a projectile launched from the origin. Shall not
	// coincide with the origin.
	Target maf.Vec
}

// FromLaunchAngle fixes the projectile's launch angle, measured in radians, and
// attempts to find a solution.
//
// The launch angle shall be confined to the range -Pi/2...Pi/2 which is
// relative to the positive or negative X-axis as appropriate.
//
// If the returned bool value is false then there is no solution.
//
//  Special cases
//
// In the special case where all of the following three conditions apply
//   1. the target is vertically above or below the origin
//   2. the launch angle is +Pi/2 or -Pi/2 respectively
//   3. the acceleration direction is down or up respectively
// then, of the infinite number of projectile solutions, the one chosen is that
// where the peak of the projectile's motion coincides with the target.
//
// In the special case where all of the following three conditions apply
//   1. the target is vertically above or below the origin
//   2. the launch angle is -Pi/2 or +Pi/2 respectively
//   3. the acceleration direction is up or down respectively
// then, it suffices to simply let the projectile fall under the motion's
// acceleration and the initial velocity of the returned projectile is zero.
func (pt ProjectileTargeter) FromLaunchAngle(la float64) (Projectile, bool) {
	if pt.Target.X < 0 {
		pt.Target.X = -pt.Target.X
		p, ok := pt.FromLaunchAngle(la)
		if ok {
			p.U.X = -p.U.X
		}
		return p, ok
	}

	if (la > maf.HalfPi-maf.SmallAngle) || (la < -maf.HalfPi+maf.SmallAngle) {
		if !pt.Target.IsParallel(maf.PosY, maf.SmallAngle) {
			return Projectile{}, false
		}
		var launchDir maf.Vec
		switch {
		case la > maf.HalfPi-maf.SmallAngle:
			launchDir = maf.PosY
		case la < -maf.HalfPi+maf.SmallAngle:
			launchDir = maf.NegY
		default:
			return Projectile{}, false
		}
		pointsAtTarget := launchDir.Dot(pt.Target) > 0
		canTurn := launchDir.Dot(maf.PosY.Scale(pt.A)) < 0
		if !(pointsAtTarget || canTurn) {
			return Projectile{}, false
		}
		if pointsAtTarget && canTurn {
			// Constant acceleration formula:
			//  v² = u² +2ax, where here v = 0.
			u := math.Sqrt(-2 * pt.A * pt.Target.Y)
			return Projectile{A: pt.A, U: launchDir.Scale(u)}, true
		}
		return Projectile{A: pt.A, U: maf.Vec{}}, true
	}

	// Parametric formula for parabolic motion:
	//   x = ut*cos(la), y = ut*sin(la)+0.5at²
	// Eliminate t and solve for u
	//   u = sqrt ( ax²(tan²(la)+1) )
	//            ( --------------- )
	//            ( 2 (y-x*tan(la)) )
	// Let x and y be the target's coordinates.
	s, c := math.Sincos(la)
	x, y, tan := pt.Target.X, pt.Target.Y, s/c
	denominator := 2 * (y - x*tan)
	if denominator == 0 {
		return Projectile{}, false
	}
	determinant := (pt.A * x * x * (tan*tan + 1)) / denominator
	if determinant < 0 {
		return Projectile{}, false
	}
	u := math.Sqrt(determinant)
	return Projectile{A: pt.A, U: maf.Vec{X: c, Y: s}.Scale(u)}, true
}

// FromSpeed fixes the projectile's initial positive speed and attempts to find
// a solution.
//
// The int return value is the number of found projectiles, either 0, 1, or 2,
// that coincide with the target and have the argument initial speed.
//
// In the case of two found projectiles with the target's X !~= 0 then the first
// of the returned projectiles launches closer to the +/- X-axis.
//
// In the case of two found projectiles, with the target's X ~= 0 and the
// acceleration and target displacement vectors in the same direction, the first
// of the returned projectiles will have the shortest time of flight to the
// target.
func (pt ProjectileTargeter) FromSpeed(u float64) (
	Projectile, Projectile, int,
) {
	if pt.Target.X < 0 {
		pt.Target.X = -pt.Target.X
		p1, p2, n := pt.FromSpeed(u)
		if n > 0 {
			p1.U.X = -p1.U.X
		}
		if n > 1 {
			p2.U.X = -p2.U.X
		}
		return p1, p2, n
	}

	if pt.Target.IsParallel(maf.PosY, maf.SmallAngle) {
		var (
			a    = maf.Vec{Y: pt.A}
			n    int
			tDir = maf.Vec{Y: math.Copysign(1.0, pt.Target.Y)}
		)

		if a.Dot(pt.Target) > 0 {
			if u == 0 {
				n = 1
			} else {
				n = 2
			}
		} else {
			// Vertically
			//  v² = u² + 2ay, where v = 0 at max height
			// Thus
			//   y = -u² / 2a
			if math.Abs(u*u/(2.0*pt.A)) < math.Abs(pt.Target.Y) {
				n = 0
			} else {
				n = 1
			}
		}

		switch n {
		case 0:
			return Projectile{}, Projectile{}, 0
		case 1:
			p := Projectile{A: pt.A, U: tDir.Scale(u)}
			return p, p, 1
		case 2:
			p1 := Projectile{A: pt.A, U: tDir.Scale(u)}
			p2 := p1
			p2.U.Y = -p2.U.Y
			return p1, p2, 2
		}
	}

	if u == 0 {
		return Projectile{}, Projectile{}, 0
	}

	// https://gist.github.com/fospathi/d370061ea1967c9adf485179c4fdaaf7
	//
	//  A = (ax²/(2u²))
	//  B = x
	//  C = A - y
	//
	//  θ = arctan( -B ± √(B² - 4AC) )
	//            ( ---------------- )
	//            (        2A        )
	x, y := pt.Target.X, pt.Target.Y
	A, B := pt.A*x*x/(2*u*u), x
	C := A - y
	det := B*B - 4*A*C
	if -maf.TanSmallAngle < det && det < maf.TanSmallAngle {
		// Only one solution when the quadratic equation's determinant is zero.
		θ := math.Atan(-B / (2 * A))
		s, c := math.Sincos(θ)
		U := maf.Vec{X: u * c, Y: u * s}
		p := Projectile{A: pt.A, U: U}
		return p, p, 1
	} else if det < 0 {
		return Projectile{}, Projectile{}, 0
	}
	sqrtDet := math.Sqrt(det)
	θ1, θ2 := math.Atan((-B+sqrtDet)/(2*A)), math.Atan((-B-sqrtDet)/(2*A))
	if math.Abs(θ2) < math.Abs(θ1) {
		θ1, θ2 = θ2, θ1
	}
	s, c := math.Sincos(θ1)
	u1 := maf.Vec{X: u * c, Y: u * s}
	p1 := Projectile{A: pt.A, U: u1}
	s, c = math.Sincos(θ2)
	u2 := maf.Vec{X: u * c, Y: u * s}
	p2 := Projectile{A: pt.A, U: u2}
	return p1, p2, 2
}
