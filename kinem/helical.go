package kinem

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// Helical is the helical motion of a particle moving along a circular path on a
// rising platform.
type Helical struct {
	// The pitch of the helix, that is, the distance travelled along the Z-axis
	// in one complete helix turn.
	P float64
	// The radius of the circular motion in the XY plane.
	R float64
	// The constant angular velocity of the circular motion in the XY plane
	// measured in radians per unit time.
	W float64
}

// Displacement of the motion from the origin at the given motion parameter.
func (mot Helical) Displacement(t float64) maf.Vec {
	s, c := math.Sincos(mot.W * t)
	vz := mot.P / (maf.TwoPi / mot.W)
	return maf.Vec{X: mot.R * c, Y: mot.R * s, Z: vz * t}
}

// Velocity of the motion at the given motion parameter.
func (mot Helical) Velocity(t float64) maf.Vec {
	s, c := math.Sincos(mot.W * t)
	vz := mot.P / (maf.TwoPi / mot.W)
	return maf.Vec{
		X: -mot.W * mot.R * s,
		Y: mot.W * mot.R * c,
		Z: vz,
	}
}

// Acceleration of the motion at the given motion parameter.
func (mot Helical) Acceleration(t float64) maf.Vec {
	s, c := math.Sincos(mot.W * t)
	// Let i, j, and k be unit vectors in the X, Y and Z-axis directions
	// respectively:
	//   s(t) =    r*cos(wt) i +   r*sin(wt) j + (wp/2π)t k
	//  s'(t) =  -wr*sin(wt) i +  wr*cos(wt) j +  (wp/2π) k
	// s''(t) = -wwr*cos(wt) i - wwr*sin(wt) j +        0 k
	return maf.Vec{
		X: -mot.W * mot.W * mot.R * c,
		Y: -mot.W * mot.W * mot.R * s,
	}
}
