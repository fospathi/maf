package kinem

import (
	"math"

	"gitlab.com/fospathi/maf"
)

// RollingWheel is the rolling motion of a non-slipping wheel rolling at
// constant velocity along the positive X-axis.
type RollingWheel struct {
	// Wheel radius.
	R float64
	// Wheel angular velocity. Clockwise (as in moving right) is positive.
	W float64
}

// Motion of the given particle on the receiver rolling wheel.
//
// The particle's angle is the initial clockwise angular displacement from the
// wheel radius which emits from the centre in the positive Y-axis direction.
func (rw RollingWheel) Motion(p Particle) RollingWheelParticle {
	return RollingWheelParticle{
		r:  rw.R,
		w:  rw.W,
		ρ:  p.Distance,
		θ1: p.Angle,
		u:  rw.R * rw.W,
		v:  math.Abs(p.Distance * rw.W),
	}
}

// RollingWheelParticle is the motion of a particle on a rolling wheel.
type RollingWheelParticle struct {
	r, w float64 // The wheel radius and clockwise-is-positive angular velocity.
	ρ    float64 // The particle distance from the axis of rotation.
	θ1   float64 // The initial angular offset from positive Y-axis.
	u    float64 // The wheel translational speed. (+/-).
	v    float64 // The particle rotational speed. ρ*w.
}

// Displacement of the motion from the origin at the given motion parameter.
func (mot RollingWheelParticle) Displacement(t float64) maf.Vec {
	s, c := math.Sincos(mot.θ1 + mot.w*t)
	return maf.Vec{
		X: mot.u*t + mot.ρ*s,
		Y: mot.r + mot.ρ*c,
	}
}

// Velocity of the motion at the given motion parameter.
func (mot RollingWheelParticle) Velocity(t float64) maf.Vec {
	s, c := math.Sincos(mot.θ1 + mot.w*t)
	return maf.Vec{
		X: mot.u + mot.v*c,
		Y: -mot.v * s,
	}
}

// Acceleration of the motion at the given motion parameter.
func (mot RollingWheelParticle) Acceleration(t float64) maf.Vec {
	s, c := math.Sincos(mot.θ1 + mot.w*t)
	return maf.Vec{
		X: s,
		Y: c,
	}.Scale(-mot.v * mot.w)
}

// Velocities of the motion at the given motion parameter.
func (mot RollingWheelParticle) Velocities(t float64) Components {
	s, c := math.Sincos(mot.θ1 + mot.w*t)
	return Components{
		Rotational:    maf.Vec{X: c, Y: -s}.Scale(mot.v),
		Translational: maf.Vec{X: mot.u},
	}
}

// Accelerations of the motion at the given motion parameter.
func (mot RollingWheelParticle) Accelerations(t float64) Components {
	s, c := math.Sincos(mot.θ1 + mot.w*t)
	return Components{
		Rotational:    maf.Vec{X: s, Y: c}.Scale(-mot.v * mot.w),
		Translational: maf.Vec{},
	}
}
