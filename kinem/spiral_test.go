package kinem_test

import (
	"math"
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
	"gitlab.com/fospathi/universal/tezd"
)

func TestArchimedean(t *testing.T) {
	param := 0.5
	d := 3.0
	v := 1.0
	w := maf.HalfPi

	ar := kinem.Archimedean{D: d, V: v, W: w}

	{
		{
			want := maf.Vec{X: 3}
			got := ar.Displacement(0)
			if want != got {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}

		{
			// polar equation: r = a + b * theta
			// in this case b = v / w
			//              a = d
			theta := w * param
			a := d
			b := v / w
			r := a + b*theta

			dis := ar.Displacement(param)
			want := dis.Mag()
			got := r
			if !tezd.CompareFloat64(want, got, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}

			want = math.Atan2(dis.Y, dis.X)
			got = w * param
			if !tezd.CompareFloat64(want, got, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}

	{
		vel := ar.Velocity(param)

		{
			l := d + v*param
			want := math.Sqrt(v*v + w*w*l*l) // |velocity| = sqrt(v*v + w*w*(d+vt)*(d+vt))
			got := vel.Mag()
			if !tezd.CompareFloat64(want, got, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}

		{
			// polar equation: r = a + b * theta
			// in this case b = v / w
			//              a = d
			theta := w * param
			a := d
			b := v / w
			r := a + b*theta

			s, c := math.Sincos(theta)
			// General derivative with polar coordinates:
			// dy/dx = (dr/dθ * sin(θ) + r cos(θ)) / (dr/dθ * cos(θ) − r sin(θ))
			dy_dx := (b*s + r*c) / (b*c - r*s)

			// At this angle (45 degrees) in the first quadrant the spiral is
			// progressing to the left (==> negative X direction) and upwards so
			// the Cartesian slope (dy/dx) will be negative. So to match the
			// velocities' direction at this point invert the slope sign.
			want := vel.Unit()
			got := maf.Vec{X: -1, Y: -dy_dx}.Unit()
			if !maf.CompareVec(want, got, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}

		{
			want := vel
			got := kinem.NumericalVelocity(ar, param)
			if !maf.CompareVec(want, got, 3) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}

		acc := ar.Acceleration(param)

		{
			want := acc
			got := kinem.NumericalAccel(ar, param)
			if !maf.CompareVec(want, got, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}

			got = kinem.NumericalAcceleration(ar, param)
			if !maf.CompareVec(want, got, 5) {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
		}
	}
}
