package kinem_test

import (
	"math"
	"testing"

	"gitlab.com/fospathi/maf"
	"gitlab.com/fospathi/maf/kinem"
	"gitlab.com/fospathi/universal/tezd"
)

func TestProjectileTargeter_FromLaunchAngle(t *testing.T) {
	// Aiming straight up in the positive Y-axis direction.

	tgr := kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 0, Y: 1, Z: 0},
	}
	p, ok := tgr.FromLaunchAngle(maf.HalfPi)
	want1, want2 := true, maf.Vec{X: 0, Y: math.Sqrt(20), Z: 0}
	got1, got2 := ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 0, Y: -3, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(maf.HalfPi)
	want1, want2 = true, maf.Vec{X: 0, Y: 0, Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: 0, Y: 1, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(maf.HalfPi)
	want1, want2 = true, maf.Vec{X: 0, Y: 0, Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: 0, Y: -3, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(maf.HalfPi)
	want1, want2 = false, maf.Vec{X: 0, Y: 0, Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	// Aiming straight down in the negative Y-axis direction.

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 0, Y: 1, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(-maf.HalfPi)
	want1, want2 = false, maf.Vec{X: 0, Y: 0, Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 0, Y: -3, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(-maf.HalfPi)
	want1, want2 = true, maf.Vec{X: 0, Y: 0, Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: 0, Y: 1, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(-maf.HalfPi)
	want1, want2 = true, maf.Vec{X: 0, Y: 0, Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: 0, Y: -1, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(-maf.HalfPi)
	want1, want2 = true, maf.Vec{X: 0, Y: -math.Sqrt(20), Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	// General

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 5, Y: 6, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(-maf.HalfPi)
	want1, want2 = false, maf.Vec{X: 0, Y: 0, Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 2, Y: 10, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(maf.QuartPi)
	want1, want2 = false, maf.Vec{X: 0, Y: 0, Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: -2, Y: 10, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(maf.QuartPi)
	want1, want2 = false, maf.Vec{X: 0, Y: 0, Z: 0}
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 3, Y: 0, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(maf.QuartPi)
	want1, want2 = true, maf.Vec{X: 1, Y: 1, Z: 0}.Unit().Scale(math.Sqrt(30)) // u=sqrt(d*g) at Pi/4
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: -3, Y: 0, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(maf.QuartPi)
	want1, want2 = true, maf.Vec{X: -1, Y: 1, Z: 0}.Unit().Scale(math.Sqrt(30)) // u=sqrt(d*g) at Pi/4
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 4.8145, Y: 3.2, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(math.Pi * 53 / 180)
	want1, want2 = true, maf.Vec{
		X: math.Cos(math.Pi * 53 / 180),
		Y: math.Sin(math.Pi * 53 / 180),
		Z: 0}.Unit().Scale(10)
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 1) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: -4.8145, Y: 3.2, Z: 0},
	}
	p, ok = tgr.FromLaunchAngle(math.Pi * 53 / 180)
	want1, want2 = true, maf.Vec{
		X: -math.Cos(math.Pi * 53 / 180),
		Y: math.Sin(math.Pi * 53 / 180),
		Z: 0}.Unit().Scale(10)
	got1, got2 = ok, p.U
	if want1 != got1 || !maf.CompareVec(want2, got2, 1) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
}

func TestProjectileTargeter_FromSpeed(t *testing.T) {
	// X ~= 0 and negative acceleration

	tgr := kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 0, Y: -1, Z: 0},
	}
	p1, p2, n := tgr.FromSpeed(10)
	want1, want2, want3 := 2, -maf.HalfPi, maf.HalfPi
	got1, got2, got3 := n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 0, Y: -1, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(0)
	want1, want2, want3 = 1, 0, 0
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 0, Y: 1, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(10)
	want1, want2, want3 = 1, maf.HalfPi, maf.HalfPi
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 0, Y: 1, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(0.1)
	want1, want2, want3 = 0, 0, 0
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	// X ~= 0 and positive acceleration

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: 0, Y: 1, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(10)
	want1, want2, want3 = 2, maf.HalfPi, -maf.HalfPi
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: 0, Y: 1, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(0)
	want1, want2, want3 = 1, 0, 0
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: 0, Y: -1, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(10)
	want1, want2, want3 = 1, -maf.HalfPi, -maf.HalfPi
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: 0, Y: -1, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(0.1)
	want1, want2, want3 = 0, 0, 0
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	// X !~= 0

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 2, Y: 0, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(5)
	want1, want2, want3 = 2, 0.4636, 1.1071
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 2, Y: 1, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(10)
	want1, want2, want3 = 2, 0.5701, 1.4644
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: -2, Y: 0, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(5)
	want1, want2, want3 = 2, math.Pi-0.4636, math.Pi-1.1071
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 3) || !tezd.CompareFloat64(want3, got3, 3) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: 2, Y: 0, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(5)
	want1, want2, want3 = 2, -0.4636, -1.1071
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      10,
		Target: maf.Vec{X: -2, Y: 0, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(5)
	want1, want2, want3 = 2, -math.Pi+0.4636, -math.Pi+1.1071
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 3) || !tezd.CompareFloat64(want3, got3, 3) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 2, Y: -2, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(10)
	want1, want2, want3 = 2, -0.6933, 1.4787
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 2, Y: 0, Z: 0},
	}
	p1, _, n = tgr.FromSpeed(math.Sqrt(2 * 10)) // When angle=Pi/4, v=sqrt(d*g)
	want1, want2 = 1, maf.QuartPi
	got1, got2 = n, p1.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 2, Y: 0, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(0.5)
	want1, want2, want3 = 0, 0, 0
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}

	tgr = kinem.ProjectileTargeter{
		A:      -10,
		Target: maf.Vec{X: 2, Y: 0, Z: 0},
	}
	p1, p2, n = tgr.FromSpeed(0)
	want1, want2, want3 = 0, 0, 0
	got1, got2, got3 = n, p1.LaunchAngle(), p2.LaunchAngle()
	if want1 != got1 || !tezd.CompareFloat64(want2, got2, 4) || !tezd.CompareFloat64(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n",
			want1, got1, want2, got2, want3, got3)
	}
}
