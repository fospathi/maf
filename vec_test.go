package maf

import (
	"math"
	"testing"

	"gitlab.com/fospathi/universal/tezd"
)

func BenchmarkVec_Angle(b *testing.B) {
	v1 := Vec{1, -1, 0}
	v2 := Vec{1, 1, 1}
	for n := 0; n < b.N; n++ {
		v1.Angle(v2)
	}
}

func BenchmarkVec_Perpendicular(b *testing.B) {
	v1 := Vec{1, -1, 0}
	for n := 0; n < b.N; n++ {
		v1.Perpendicular()
	}
}

func BenchmarkVec2_Angle(b *testing.B) {
	v1 := Vec2{1, -1}
	v2 := Vec2{1, 1}
	for n := 0; n < b.N; n++ {
		v1.Angle(v2)
	}
}

func TestScalarTripleProduct(t *testing.T) {
	v1 := Vec{1, 0, 0}
	v2 := Vec{0, 1, 0}
	v3 := Vec{-1, 0, 0}
	want := 0.0
	got := ScalarTripleProduct(v1, v2, v3)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{1, 0, 0}
	v2 = Vec{0, 1, 0}
	v3 = Vec{1, 0, 0}
	want = 0.0
	got = ScalarTripleProduct(v1, v2, v3)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{1, 0, 0}
	v2 = Vec{0, 1, 0}
	v3 = Vec{0, 0, 1}
	want = 1.0
	got = ScalarTripleProduct(v1, v2, v3)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{2, 0, 0}
	v2 = Vec{0, 2, 0}
	v3 = Vec{0, 0, 2}
	want = 8.0
	got = ScalarTripleProduct(v1, v2, v3)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{-2, 0, 0}
	v2 = Vec{0, 2, 0}
	v3 = Vec{0, 0, 2}
	want = -8.0
	got = ScalarTripleProduct(v1, v2, v3)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_Angle(t *testing.T) {
	v1 := Vec{1, 0, 0}
	v2 := Vec{1, 0, 0}
	want1 := 0.0
	got1 := v1.Angle(v2)
	if want1 != got1 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want1, got1)
	}

	v3 := Vec2{1, 0}
	v4 := Vec2{1, 0}
	want2 := 0.0
	got2 := v3.Angle(v4)
	if want2 != got2 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want2, got2)
	}

	v1 = Vec{-1, 0, 0}
	v2 = Vec{1, 0, 0}
	want1 = math.Pi
	got1 = v1.Angle(v2)
	if want1 != got1 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want1, got1)
	}

	v3 = Vec2{-1, 0}
	v4 = Vec2{1, 0}
	want2 = math.Pi
	got2 = v3.Angle(v4)
	if want2 != got2 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want2, got2)
	}

	v1 = Vec{1, 1, 0}
	v2 = Vec{-1, 1, 0}
	want1 = math.Pi / 2
	got1 = v1.Angle(v2)
	if want1 != got1 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want1, got1)
	}

	v3 = Vec2{1, 1}
	v4 = Vec2{-1, 1}
	want2 = math.Pi / 2
	got2 = v3.Angle(v4)
	if want2 != got2 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want2, got2)
	}

	// The ratio of the dot product to the magnitude product of these vectors
	// slightly exceeds one due to floating point arithmetic error.
	// Check the error doesn't yield a NaN for the angle.
	v1 = Vec{1.00000000000022, 1, 1}
	v2 = Vec{1, 1, 1}
	want1 = 0
	got1 = v1.Angle(v2)
	if want1 != got1 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want1, got1)
	}

	v3 = Vec2{1.00000000000022, 1}
	v4 = Vec2{1, 1}
	want2 = 0
	got2 = v3.Angle(v4)
	if want2 != got2 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want2, got2)
	}

	v1 = Vec{-1.00000000000022, -1, -1}
	v2 = Vec{1, 1, 1}
	want1 = math.Pi
	got1 = v1.Angle(v2)
	if want1 != got1 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want1, got1)
	}

	v3 = Vec2{-1.00000000000022, -1}
	v4 = Vec2{1, 1}
	want2 = math.Pi
	got2 = v3.Angle(v4)
	if want2 != got2 {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want2, got2)
	}

	v1 = Vec{math.NaN(), 1, 0}
	v2 = Vec{-1, 1, 0}
	got1 = v1.Angle(v2)
	if !math.IsNaN(got1) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nan, got1)
	}

	v3 = Vec2{math.NaN(), 1}
	v4 = Vec2{-1, 1}
	got2 = v3.Angle(v4)
	if !math.IsNaN(got2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nan, got2)
	}

	v1 = Vec{math.Inf(0), 1, 0}
	v2 = Vec{-1, 1, 0}
	got1 = v1.Angle(v2)
	if !math.IsNaN(got1) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nan, got1)
	}

	v3 = Vec2{math.Inf(0), 1}
	v4 = Vec2{-1, 1}
	got2 = v3.Angle(v4)
	if !math.IsNaN(got2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nan, got2)
	}

	v1 = Vec{0, 0, 0}
	v2 = Vec{-1, 1, 0}
	got1 = v1.Angle(v2)
	if !math.IsNaN(got1) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nan, got1)
	}

	v3 = Vec2{0, 0}
	v4 = Vec2{-1, 1}
	got2 = v3.Angle(v4)
	if !math.IsNaN(got2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nan, got2)
	}

	v1 = Vec{-1, 1, 0}
	v2 = Vec{0, 0, 0}
	got1 = v1.Angle(v2)
	if !math.IsNaN(got1) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nan, got1)
	}

	v3 = Vec2{-1, 1}
	v4 = Vec2{0, 0}
	got2 = v3.Angle(v4)
	if !math.IsNaN(got2) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nan, got2)
	}
}

func TestVec_Add(t *testing.T) {
	v1 := Vec{1, 2, 3}
	v2 := Vec{4, 5, 6}
	want := Vec{5, 7, 9}
	got := v1.Add(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_Cross(t *testing.T) {
	want := Vec{-3, 6, -3}
	got := Vec{1, 2, 3}.Cross(Vec{4, 5, 6})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_DistTo(t *testing.T) {
	v1 := Vec{1, 2, 3}
	v2 := Vec{3, 2, 1}
	want := math.Sqrt(8)
	got := v1.DistTo(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v3 := Vec2{1, 2}
	v4 := Vec2{3, 2}
	want = 2.0
	got = v3.DistTo(v4)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_DivTo(t *testing.T) {
	want := Vec{0, 0, 0}
	got := Vec{-1, 0, 0}.DivTo(Vec{2, 0, 0}, 1, 2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Vec{-2, 0, 0}
	got = Vec{0, 0, 0}.DivTo(Vec{2, 0, 0}, -1, 2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_Dot(t *testing.T) {
	want := 3.0
	func() {
		got := Vec{1, 1, 1}.Dot(Vec{1, 1, 1})
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		got = Vec{0, 2, 1}.Dot(Vec{1, 1, 1})
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}()
}

func TestVec_Higher(t *testing.T) {
	v4 := Vec4{1, 2, 3, 4}
	v3 := Vec{1, 2, 3}
	v2 := Vec2{1, 2}

	want := true
	got := v2.Higher(3) == v3 && v3.Higher(4) == v4
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = true
	got = v4.Lower() == v3 && v3.Lower() == v2
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_IsParallel(t *testing.T) {
	v1 := Vec{1, 1, 1}
	v2 := Vec{2, 2, 2}

	want := true
	got := v1.IsParallel(v2, SmallAngle)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v2 = Vec{-2, -2, -2}
	got = v1.IsParallel(v2, SmallAngle)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = false
	v2 = Vec{0, -2, -2}
	got = v1.IsParallel(v2, SmallAngle)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_IsPerpendicular(t *testing.T) {
	v1 := Vec{1, 1, 1}
	v2 := Vec{2, 2, 2}

	want := false
	got := v1.IsPerpendicular(v2, SmallAngle)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v2 = Vec{-2, -2, -2}
	got = v1.IsPerpendicular(v2, SmallAngle)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = true
	got = PosY.IsPerpendicular(PosX, SmallAngle)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_Mag(t *testing.T) {
	v1 := Vec{1, 2, 3}
	want := math.Sqrt(14)
	got := v1.Mag()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_MagSqd(t *testing.T) {
	v1 := Vec{1, 2, 3}
	want := 14.0
	got := v1.MagSqd()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_MidTo(t *testing.T) {
	v1 := Vec{1, 2, 3}
	v2 := Vec{3, 2, 1}
	want := Vec{2, 2, 2}
	got := v1.MidTo(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_Opposite(t *testing.T) {
	want := Vec{-3, 6, -3}
	got := Vec{3, -6, 3}.Opposite()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_Perpendicular(t *testing.T) {
	v1 := Vec{1, 2, 0}
	v := v1.Perpendicular()
	want := true
	got := v.IsPerpendicular(v1, SmallAngle) && tezd.CompareFloat64(v.Mag(), 1, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{-1, 0, 0}
	v = v1.Perpendicular()
	want = true
	got = v.IsPerpendicular(v1, SmallAngle) && tezd.CompareFloat64(v.Mag(), 1, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{0, 2, 0}
	v = v1.Perpendicular()
	want = true
	got = v.IsPerpendicular(v1, SmallAngle) && tezd.CompareFloat64(v.Mag(), 1, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{0, 0, 2}
	v = v1.Perpendicular()
	want = true
	got = v.IsPerpendicular(v1, SmallAngle) && tezd.CompareFloat64(v.Mag(), 1, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	for _, v1 := range []Vec{PosX, PosY, PosZ, NegX, NegY, NegZ} {
		v = v1.Perpendicular()
		want = true
		got = v.IsPerpendicular(v1, SmallAngle) && tezd.CompareFloat64(v.Mag(), 1, 5)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestVec_Resolution(t *testing.T) {
	v1 := Vec{1, 1, 0}
	want := Vec{1, 0, 0}
	got := v1.Resolution(PosX)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{2, 3, 0}
	want = Vec{2, 0, 0}
	got = v1.Resolution(PosX)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{2, 3, 0}
	want = Vec{2, 0, 0}
	got = v1.Resolution(NegX)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec{-2, -3, 0}
	want = Vec{-2, 0, 0}
	got = v1.Resolution(NegX)
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Vec{1, 1, 1}.Unit().Scale(math.Cos(HalfPi - math.Atan(1.0/math.Sqrt2)))
	got = PosY.Resolution(Vec{1, 1, 1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = Vec{-1, -1, -1}.Unit().Scale(-math.Cos(HalfPi - math.Atan(1.0/math.Sqrt2)))
	got = PosY.Resolution(Vec{-1, -1, -1})
	if !CompareVec(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_Scale(t *testing.T) {
	v1 := Vec{1, 2, 3}
	want := Vec{0.5, 1, 1.5}
	got := v1.Scale(0.5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_Subtract(t *testing.T) {
	v1 := Vec{1, 2, 3}
	v2 := Vec{3, 2, 1}
	want := Vec{-2, 0, 2}
	got := v1.Subtract(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_To(t *testing.T) {
	v1 := Vec{1, 2, 3}
	v2 := Vec{3, 2, 1}
	want := Vec{2, 0, -2}
	got := v1.To(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec_Unit(t *testing.T) {
	l := 1 / math.Sqrt(3)
	v1 := Vec{2, 2, 2}
	want := Vec{l, l, l}
	got := v1.Unit()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec2_Anticlockwise(t *testing.T) {
	v1 := Vec2{1, 0}
	v2 := Vec2{1, 0}
	want := 0.0
	got := v1.Anticlockwise(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1, v2 = Vec2{1, 0}, Vec2{0, -1}
	want, got = 1.5*math.Pi, v1.Anticlockwise(v2)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1, v2 = Vec2{1, 0}, Vec2{-1, 0}
	want, got = math.Pi, v1.Anticlockwise(v2)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1, v2 = Vec2{1, 0}, Vec2{0, 1}
	want, got = HalfPi, v1.Anticlockwise(v2)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec2_Clockwise(t *testing.T) {
	v1 := Vec2{1, 0}
	v2 := Vec2{1, 0}
	want := 0.0
	got := v1.Clockwise(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1, v2 = Vec2{1, 0}, Vec2{0, -1}
	want, got = HalfPi, v1.Clockwise(v2)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1, v2 = Vec2{1, 0}, Vec2{-1, 0}
	want, got = math.Pi, v1.Clockwise(v2)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1, v2 = Vec2{1, 0}, Vec2{0, 1}
	want, got = 1.5*math.Pi, v1.Clockwise(v2)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestVec2_CosAngle(t *testing.T) {
	v1 := Vec2{1, 0}
	v2 := Vec2{1, 0}
	want := 1.0
	got := v1.CosAngle(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec2{1, 0}
	v2 = Vec2{0, 1}
	want = 0.0
	got = v1.CosAngle(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec2{1, 0}
	v2 = Vec2{-1, 0}
	want = -1.0
	got = v1.CosAngle(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = Vec2{1, 0}
	v2 = Vec2{0, -1}
	want = 0.0
	got = v1.CosAngle(v2)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
