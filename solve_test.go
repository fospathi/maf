package maf

import (
	"math"
	"math/cmplx"
	"sort"
	"testing"

	"gitlab.com/fospathi/universal/tezd"
)

func BenchmarkMath_Cbrt(b *testing.B) {
	fl := 0.7894
	for n := 0; n < b.N; n++ {
		math.Cbrt(fl)
	}
}

func BenchmarkMath_Pow(b *testing.B) {
	fl := 0.7894
	power := 1.0 / 3.0
	for n := 0; n < b.N; n++ {
		math.Pow(fl, power)
	}
}

func BenchmarkCmplx_Pow(b *testing.B) {
	c1 := 0.3789 + 0.8872i
	power := 1.0/3.0 + 0i
	for n := 0; n < b.N; n++ {
		cmplx.Pow(c1, power)
	}
}

func BenchmarkCubic_Roots(b *testing.B) {
	cbc := Cubic{1, 2, 3, 4}
	for n := 0; n < b.N; n++ {
		cbc.Roots()
	}
}

func TestSolveSLE2(t *testing.T) {
	want1, want2, want3 := 0.5, 0.5, true
	got1, got2, got3 := SolveSLE2(2, 0, -1, 2, 2, -2)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 0.0, 0.0, true
	got1, got2, got3 = SolveSLE2(-1, 1, 0, 1, 1, 0)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 1.0, 1.0, true
	got1, got2, got3 = SolveSLE2(1, 0, -1, 0, 1, -1)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 1.0, 1.0, true
	got1, got2, got3 = SolveSLE2(0, 1, -1, -1, 1, 0)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 0.0, 0.0, true
	got1, got2, got3 = SolveSLE2(-1, 1, 0, 1, 0, 0)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 0.0, 0.0, false
	got1, got2, got3 = SolveSLE2(0, 1, -2, 0, 1, 1)
	if want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	want1, want2, want3 = 0.0, 0.0, false
	got1, got2, got3 = SolveSLE2(1, 0, -2, 2, 0, 1)
	if want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}

func TestQuadratic_Roots(t *testing.T) {
	q := Quadratic{1, 1, 1}
	want1, want2 := -0.5+0.86603i, -0.5-0.86603i
	got1, got2 := q.Roots()
	if !CompareComplex(want1, got1, 4) && !CompareComplex(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	q = Quadratic{1, 1, -1}
	want1, want2 = 0.6180+0i, -1.6180+0i
	got1, got2 = q.Roots()
	if !CompareComplex(want1, got1, 4) && !CompareComplex(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	q = Quadratic{1, 0, 0}
	want1, want2 = 0i, 0i
	got1, got2 = q.Roots()
	if !CompareComplex(want1, got1, 4) && !CompareComplex(want2, got2, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
}

func TestCubic_Roots(t *testing.T) {
	vals := make([]complex128, 3)
	less := func(i, j int) bool {
		mod1, arg1 := cmplx.Polar(vals[i])
		mod2, arg2 := cmplx.Polar(vals[j])
		// Get args in range 0..2Pi
		if arg1 < 0 {
			arg1 = math.Pi + (math.Pi + arg1)
		}
		if arg2 < 0 {
			arg2 = math.Pi + (math.Pi + arg2)
		}
		if arg1 == arg2 {
			return mod1 < mod2
		}
		return arg1 < arg2
	}

	c := Cubic{1, 0, -6, -6}
	want1, want2, want3 := 2.8473+0i, -1.4237+0.2836i, -1.4237-0.2836i
	got1, got2, got3 := c.Roots()
	if !CompareComplex(want1, got1, 4) && !CompareComplex(want2, got2, 4) && !CompareComplex(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	c = Cubic{1, 0, 0, 0}
	vals[0], vals[1], vals[2] = c.Roots()
	want1, want2, want3 = 0+0i, 0+0i, 0+0i
	got1, got2, got3 = vals[0], vals[1], vals[2]
	if !CompareComplex(want1, got1, 4) && !CompareComplex(want2, got2, 4) && !CompareComplex(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	c = Cubic{1, 0, 1, 0}
	vals[0], vals[1], vals[2] = c.Roots()
	want1, want2, want3 = 0+0i, 0+0i, 0+0i
	got1, got2, got3 = vals[0], vals[1], vals[2]
	if !CompareComplex(want1, got1, 4) && !CompareComplex(want2, got2, 4) && !CompareComplex(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	c = Cubic{5, 0, 3, 0}
	vals[0], vals[1], vals[2] = c.Roots()
	want1, want2, want3 = 0+0i, 0+0i, 0+0i
	got1, got2, got3 = vals[0], vals[1], vals[2]
	if !CompareComplex(want1, got1, 4) && !CompareComplex(want2, got2, 4) && !CompareComplex(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	c = Cubic{1, 0, -1, -0.001}
	vals[0], vals[1], vals[2] = c.Roots()
	sort.Slice(vals, less)
	want1, want2, want3 = 1.0005+0i, -0.001+0i, -0.9995+0i
	got1, got2, got3 = vals[0], vals[1], vals[2]
	if !CompareComplex(want1, got1, 4) && !CompareComplex(want2, got2, 4) && !CompareComplex(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	c = Cubic{1, -1, 0, 0}
	vals[0], vals[1], vals[2] = c.Roots()
	sort.Slice(vals, less)
	want1, want2, want3 = 0+0i, 0+0i, 1+0i
	got1, got2, got3 = vals[0], vals[1], vals[2]
	if !CompareComplex(want1, got1, 4) && !CompareComplex(want2, got2, 4) && !CompareComplex(want3, got3, 4) {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	c = Cubic{1, 0, -3, 1}
	vals[0], vals[1], vals[2] = c.Roots()
	m := []complex128{vals[0], vals[1], vals[2]}
	for _, want := range []complex128{0.34730 + 0i, 1.5321 + 0i, -1.8794 + 0i} {
		found := false
		for _, got := range m {
			if CompareComplex(want, got, 4) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, m)
		}
	}

	c = Cubic{1, -1, 1, -1}
	vals[0], vals[1], vals[2] = c.Roots()
	m = []complex128{vals[0], vals[1], vals[2]}
	for _, want := range []complex128{1 + 0i, 0 + 1i, 0 - 1i} {
		found := false
		for _, got := range m {
			if CompareComplex(want, got, 4) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, m)
		}
	}

	c = Cubic{1, 2, 3, 4}
	vals[0], vals[1], vals[2] = c.Roots()
	m = []complex128{vals[0], vals[1], vals[2]}
	for _, want := range []complex128{-1.6506 + 0i, -0.1747 + 1.5469i, -0.1747 - 1.5469i} {
		found := false
		for _, got := range m {
			if CompareComplex(want, got, 4) {
				found = true
				break
			}
		}
		if !found {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, m)
		}
	}
}

func Test_CubeRootsOfOne(t *testing.T) {
	want := cmplx.Rect(1, 0)
	got := CubeRootsOfOne[0]
	if !CompareComplex(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = cmplx.Rect(1, 2*math.Pi/3)
	got = CubeRootsOfOne[1]
	if !CompareComplex(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = cmplx.Rect(1, 4*math.Pi/3)
	got = CubeRootsOfOne[2]
	if !CompareComplex(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func Test_NewtonsMethod(t *testing.T) {
	f := func(x float64) float64 {
		return x*x - 1
	}
	d := func(x float64) float64 {
		return 2 * x
	}

	want := 1.0
	got := NewtonsMethod(f, d, 1.5, 0.000001, 50)
	if !tezd.CompareFloat64(want, got, 5) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
