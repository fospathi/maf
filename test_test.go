package maf

import "testing"

func Test_CompareComplex(t *testing.T) {
	c1 := complex(0.001, -2)
	c2 := complex(0.001, -2)
	c3 := complex(0.001000000001, -2.000000001)
	c4 := complex(0.01, -2)

	want := true
	got := CompareComplex(c1, c2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = true
	got = CompareComplex(c1, c3, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = false
	got = CompareComplex(c3, c4, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func Test_CompareLine(t *testing.T) {
	l1 := Line{Dir: PosX, P: Zero}
	l2 := Line{Dir: PosX, P: Zero}
	want := true
	got := CompareLine(l1, l2, 1e-6, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	l2 = Line{Dir: PosX, P: Vec{-1e10, 0, 0}}
	got = CompareLine(l1, l2, 1e-6, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	l2 = Line{Dir: NegX, P: Vec{-1e10, 0, 0}}
	got = CompareLine(l1, l2, 1e-6, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	l2 = Line{Dir: PosX, P: Vec{0, 1, 0}}
	want = false
	got = CompareLine(l1, l2, 1e-6, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	l2 = Line{Dir: PosY, P: Zero}
	got = CompareLine(l1, l2, 1e-6, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	l1 = Line{Dir: PosX.Add(PosY).Unit(), P: Zero}
	l2 = Line{Dir: NegX.Add(NegY.Scale(1.0000001)).Unit(), P: Vec{1, 1, 0}}
	want = true
	got = CompareLine(l1, l2, 1e-6, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	l1 = Line{Dir: PosX.Add(PosY).Unit(), P: Zero}
	l2 = Line{Dir: NegX.Add(NegY.Scale(1.01)).Unit(), P: Vec{1, 1, 0}}
	want = false
	got = CompareLine(l1, l2, 1e-6, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func Test_CompareVecs(t *testing.T) {
	v1 := []Vec{}
	v2 := []Vec{}
	want := true
	got := CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{}}
	v2 = []Vec{{}}
	want = true
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{}, {X: 0.001}}
	v2 = []Vec{{X: 0.001}, {}}
	want = true
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{Y: 0.002}, {X: 0.001}, {X: 0.001}}
	v2 = []Vec{{X: 0.001}, {Y: 0.002}, {X: 0.001}}
	want = true
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{X: 0.001}, {X: 0.001}, {X: 0.001}}
	v2 = []Vec{{X: 0.001000001}, {X: 0.001}, {X: 0.001}}
	want = true
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{X: 0.001}}
	v2 = []Vec{{X: 0.002}}
	want = false
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{X: 0.001}}
	v2 = []Vec{{X: 0.001}, {X: 0.001}}
	want = false
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{X: 0.001}, {X: 0.001}, {X: 0.001}}
	v2 = []Vec{{X: 0.002}, {X: 0.001}, {X: 0.001}}
	want = false
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{X: 0.001}, {X: 0.001}, {X: 0.001}}
	v2 = []Vec{{X: 0.001}, {X: 0.001}, {X: 0.001}, {X: 0.002}}
	want = false
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func Test_CompareOrderedVecs(t *testing.T) {
	v1 := []Vec{}
	v2 := []Vec{}
	want := true
	got := CompareOrderedVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{}}
	v2 = []Vec{{}}
	want = true
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{X: 0.001}, {X: 0.001}, {X: 0.001}}
	v2 = []Vec{{X: 0.002}, {X: 0.001}, {X: 0.001}}
	want = false
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{X: 0.001}, {X: 0.001}, {X: 0.001}}
	v2 = []Vec{{X: 0.002}, {X: 0.001}, {X: 0.001}, {X: 0.002}}
	want = false
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	v1 = []Vec{{X: 0.002}, {X: 0.001}, {X: 0.001}}
	v2 = []Vec{{X: 0.002}, {X: 0.001}, {X: 0.001}}
	want = true
	got = CompareVecs(v1, v2, 5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
