package maf

import (
	"math"
)

// ExpSin represents a product of two functions: an exponential function and a
// sin function.
//
// y = e^(Ax) * sin(Bx)
type ExpSin struct {
	// A is the coefficient of the variable in the exponential function.
	A float64
	// B is the coefficient of the variable in the sin function.
	B float64
}

// At returns the value of the receiver expression at the argument variable
// value.
func (es ExpSin) At(x float64) float64 {
	return math.Exp(es.A*x) * math.Sin(es.B*x)
}

// D returns the nth derivative of the receiver expression where n is given by
// the argument.
func (es ExpSin) D(n int) func(float64) float64 {
	var k1, k2 float64
	switch n {
	case 1:
		k1 = math.Sqrt(es.A*es.A + es.B*es.B)
	case 2:
		k1 = (es.A*es.A + es.B*es.B)
	case 3:
		k1 = (es.A*es.A + es.B*es.B) * math.Sqrt(es.A*es.A+es.B*es.B)
	case 4:
		k1 = (es.A*es.A + es.B*es.B) * (es.A*es.A + es.B*es.B)
	default:
		k1 = math.Pow(es.A*es.A+es.B*es.B, float64(n)/2.0)
	}
	k2 = float64(n) * math.Atan2(es.B, es.A)
	return func(x float64) float64 {
		return k1 * math.Exp(es.A*x) * math.Sin(es.B*x+k2)
	}
}
